(function() {
    'use strict';

    module.exports = function(config) {
        config.set({
            basePath: '',

            frameworks: ['jasmine'],

            files: [
                'bower_components/jquery/dist/jquery.min.js',
                'bower_components/angular/angular.js',
                'bower_components/angular-mocks/angular-mocks.js',
                'bower_components/angular-cookies/angular-cookies.js',
                'bower_components/angular-sanitize/angular-sanitize.js',
                'bower_components/angular-route/angular-route.js',
                'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                'bower_components/angular-ui-ace/ui-ace.js',
                'bower_components/angular-local-storage/angular-local-storage.js',
                'bower_components/d3/d3.js',
                'bower_components/c3/c3.js',
                'bower_components/angular-busy/angular-busy.js',
                'bower_components/amitava82-angular-multiselect/src/multiselect.js',
                'bower_components/angulartics/src/angulartics.js',
                'bower_components/angulartics/src/angulartics-ga.js',
                'bower_components/moment/min/moment.min.js',
                'bower_components/angular-moment/angular-moment.js',
                'bower_components/angular-pageslide-directive/src/angular-pageslide-directive.js',
                'bower_components/json-formatter/dist/json-formatter.js',
                'bower_components/ng-prettyjson/dist/ng-prettyjson.min.js',
                'bower_components/lodash/lodash.js',
                'bower_components/angular-ui-layout/ui-layout.js',
                'bower_components/angular-loading-bar/src/loading-bar.js',
                'bower_components/angular-poller/angular-poller.js',
                'bower_components/angular-smart-table/dist/smart-table.js',
                'bower_components/angular-options-disabled/src/angular-options-disabled.js',
                'bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
                'bower_components/uri.js/src/URI.js',
                'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                'bower_components/angular-bootstrap-datetimepicker-directive/angular-bootstrap-datetimepicker-directive.js',
                'bower_components/angularjs-dropdown-multiselect/src/angularjs-dropdown-multiselect.js',
                'bower_components/karma-read-json/karma-read-json.js',
                'app/**/*.module.js',
                'app/**/*.js',
                'app/**/*-spec.js',
                'test/mock/mixpanel.js',
                'app/**/*.html',
                { pattern: 'app/**/fixtures/*.json', watched: true, served: true, included: false }
            ],

            port: 8080,

            logLevel: config.LOG_INFO,

            autoWatch: false,

            plugins: [
                'karma-teamcity-reporter',
                'karma-jasmine',
                'karma-coverage',
                'karma-mocha-reporter',
                'karma-phantomjs-launcher',
                'karma-chrome-launcher',
                'karma-firefox-launcher',
                'karma-ng-html2js-preprocessor'
            ],

            preprocessors: {
                'app/**/!(*spec).js': ['coverage'],
                'app/**/*.html': ['ng-html2js']
            },

            ngHtml2JsPreprocessor: {
                moduleName: 'directiveTemplates'
            },

            mochaReporter: {
                output: 'autowatch'
            },

            reporters: ['mocha', 'coverage'],

            coverageReporter: {
                dir: 'coverage',
                type: 'html'
            },

            browsers: ['PhantomJS'],

            singleRun: false
        });
    };
}());
