(function() {

'use strict';

angular.module('fcApp', [
    'ui.bootstrap',
    'angular-loading-bar',
    'cgBusy',
    'ngRoute',
    'ngSanitize',
    'LocalStorageModule',
    'emguo.poller',
    'ngMaterial',
    'materialDatePicker',
    '720kb.datepicker',
    'fcApp.common',
    'fcApp.authentication',
    'fcApp.technical',
    'fcApp.commercial',
    'fcApp.layout',
    'fcApp.dashboard',
    'fcApp.feedback',
    'fcApp.apiHealth',
    'fcApp.permission',
    'fcApp.documentation',
    'fcApp.travelRankings',
    'fcApp.wherewego'
]);

angular.module('fcApp')
    .constant('moment', window.moment)
    .constant('_', window._);

angular.module('fcApp').config(function(
    $routeProvider,
    $httpProvider,
    $mdThemingProvider,
    localStorageServiceProvider,
    cfpLoadingBarProvider,
    pollerConfig) {

    localStorageServiceProvider.setPrefix('iq');
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    cfpLoadingBarProvider.includeSpinner = false;

    $httpProvider.defaults.timeout = 5000;
    $httpProvider.interceptors.push('authInterceptor');
    $httpProvider.interceptors.push('errorInterceptor');

    $routeProvider
        // .when('/', {
        //     redirectTo: '/dashboard'
        // })
        .when('/', {
            templateUrl: 'app/wherewego/templates/home/home.html',
            controller: 'HomeCtrl'
        })
        .when('/browser', {
            templateUrl: 'app/wherewego/templates/browser/browser.html',
            controller: 'BrowserCtrl'
        })
        .when('/flight-select', {
            templateUrl: 'app/wherewego/templates/browser/flight-select.html',
            controller: 'FlightSelectCtrl'
        })        
        .when('/hotel-browser', {
            templateUrl: 'app/wherewego/templates/browser/hotel-browser.html',
            controller: 'HotelBrowserCtrl'
        })                
        .when('/dashboard', {
            templateUrl: 'app/dashboard/dashboard.html',
            controller: 'DashboardCtrl',
            controllerAs: 'vm'
        })        
        .when('/login', {
            templateUrl: 'app/authentication/login.html',
            controller: 'LoginCtrl'
        });

    // This is to make sure that Django receives our POST data
    // Use x-www-form-urlencoded Content-Type
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data) {
        /**
         * The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */
        var param = function(obj) {
            var query = '';
            var name;
            var value;
            var fullSubName;
            var subName;
            var subValue;
            var innerObj;
            var i;

            for (name in obj) {
                if (obj.hasOwnProperty(name)) {
                    value = obj[name];

                    if (value instanceof Array) {
                        for (i = 0; i < value.length; ++i) {
                            subValue = value[i];
                            fullSubName = name + '[' + i + ']';
                            innerObj = {};
                            innerObj[fullSubName] = subValue;
                            query += param(innerObj) + '&';
                        }
                    } else if (value instanceof Object) {
                        for (subName in value) {
                            if (value.hasOwnProperty(subName)) {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                    } else if (value !== undefined && value !== null) {
                        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                    }
                }
            }

            return query.length ? query.substr(0, query.length - 1) : query;
        };

        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];

    // stop pollerService if view is changed
    pollerConfig.stopOnRouteChange = true;

    $mdThemingProvider.theme('default');
});

angular.module('fcApp').run(function(
    $rootScope,
    $location,
    $route,
    $window,
    $http,
    localStorageService,
    userSessionService,
    authService) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof fn === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    var original = $location.path;

    // make sure that the query string is properly set
    $location.path = function(path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function() {
                $route.current = lastRoute;
                un();
            });
        }

        return original.apply($location, [path]);
    };

    $rootScope.$on('$locationChangeSuccess', userSessionService.updateWebsiteId);

    $rootScope.session = {
        isLoggedin: false,
        username: ''
    };

    $rootScope.location = $location;
    $rootScope.integrationState = {};
    $rootScope.dateFormat = {
        heading: 'EEE dd MMM yyyy',
        quote: 'HH:mm EEE',
        detailed: 'd MMM y h:mm a'
    };

    $rootScope.datepicker_config = {
        depart_options: {
            'year-format': 'yy',
            'starting-day': 1,
            format: 'yyyy-MMM-dd'
        },
        return_options: {
            'year-format': 'yy',
            'starting-day': 1,
            format: 'yyyy-MMM-dd'
        },
        format: 'yyyy-MMM-dd'
    };


    $rootScope.test = {
        hello:'world'
    };

});

}());
