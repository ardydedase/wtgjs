# Contributing to Connect Frontend

We would love for you to contribute to Connect Frontend and help make it even better than it is
today! As a contributor, here are the guidelines we would like you to follow:

 - [Question or Problem?](#question-or-problem)
 - [Issues and Bugs](#issues-and-bugs)
 - [Feature Requests](#feature-requests)
 - [Submission Guidelines](#submission-guidelines)
 - [Module](#module)
 - [Directives](#directives)
 - [Definition of done](#done)
 - [Coding Rules](#coding-rules)
 - [Commit Message Guidelines](#commit-message-guidelines)

## <a name="question"></a> Got a Question or Problem?

TODO

## <a name="issue"></a> Found an Issue?

TODO

## <a name="feature"></a> Want a Feature?

TODO

## <a name="submit"></a> Submission Guidelines

### <a name="submit-issue"></a> Submitting an Issue

TODO

### <a name="submit-pr"></a> Submitting a Merge Request

TODO

That's it! Thank you for your contribution!

#### After your pull request is merged

After your pull request is merged, you can safely delete your branch and pull the changes
from the main (upstream) repository:

* Delete the remote branch on Gitlab either through the Gitlab web UI or your local shell as follows:

    ```shell
    git push origin --delete my-fix-branch
    ```

* Check out the master branch:

    ```shell
    git checkout master -f
    ```

* Delete the local branch:

    ```shell
    git branch -D my-fix-branch
    ```

* Update your master with the latest upstream version:

    ```shell
    git pull --ff upstream master
    ```

## <a name="module"></a> Adding a New Model

TODO








# Coding Conventions and Guidelines

 - [Project Structure](#structure)
 - [Coding Rules](#rules)


## <a name="structure"></a> Project Structure

```text
├── fcApp
│   ├── index.html
│   ├── app.js
│   ├── app.less
│   ├── app
│   │   ├── appconfig.js
│   │   ├── aModuleName
│   │   │   ├── aModuleName.module.js
│   │   │   ├── aModuleName.less
│   │   │   ├── templates
│   │   │   |   |── template-name.html
│   │   │   |   |── some-other-template.html
│   │   │   ├── styles
│   │   │   |   |── shared-ui-component.less
│   │   │   |   |── template-name.less
│   │   │   ├── controllers
│   │   │   |   |── template1.controller.js
│   │   │   |   |── template1.controller-spec.js
│   │   │   |   |── template2.controller.js
│   │   │   |   |── template2.controller-spec.js
│   │   │   ├── services
│   │   │   |   |── serviceName.service.js
│   │   │   |   |── serviceName.service-spec.js
│   │   │   ├── directives
│   │   │   |   |── someDirective
|   |   |   |   |   |--directive-name.html
|   |   |   |   |   |--directiveName.directive.js
|   |   |   |   |   |--directiveName.directive-spec.js
│   │   │   ├── filters
│   │   │   |   |── filterName.filter.js
│   │   │   |   |── filterName.filter-spec.js
│   │   │   ├── constants
│   │   │   |   |── someConstantName.constant.js
│   ├── E2E
│   ├── bower_components
│   ├── images
│   ├── fonts
│   ├── dist
│   ├── coverage
│   ├── karma.conf.js
│   ├── protractor.conf.js
```

The project is going to be compiled into `dist` deployment package, in the following structure:

```text
├── index.html
├── mixpanel.js
├── app
│   ├── module
│   │   ├── moduleContentNotCSSorJS.ext
├── js
│   ├── a1b2c3d4.vendor.js
│   ├── z1x2y3w4.scripts.js
├── css
│   ├── a1b2c3d4.vendor.css
│   ├── z1x2y3w4.main.css
├── fonts
├── images
```
> NOTE: the `dist` directory is **not** version controlled.

> mixpanel.js contains the API key that's dynamically replaced in CI and prod environment. See [mixpanel](#mixpanel) for more info.

## <a name="rules"></a> Coding Rules

#### Coding conventions:

Our javascript coding style is loosely based on [AirBnB Javascript Guide Style](https://github.com/airbnb/javascript/tree/master/es5). If you're more familiar with [Google's JavaScript Style Guide](https://google-styleguide.googlecode.com/svn/trunk/javascriptguide.xml), it works too.

In general, stick to DRY fashion.

Here are a few basic style rules:
- camelCase for javascript related names, kebab-case for HTML and CSS related names.
- Single quote for javascript string, double-quote for HTML string.
- 4 spaces only, not mixed with tabs.
- Strip off console logging from code.
- ```use strict``` whenever possible, wrap inside IIFE.

#### Dependency Injection
* Use Angular modules as DI whenever possible, avoid polluting global.
* Use `ngConstant` to expose global third-party libraries into your module, e.g.:

    ```
    angular.module('fcApp')
        .constant('moment', window.moment)
        .constant('_', window._);
    ```

* The list of globally exposed third-party libraries is available in `predef` section of `.jshintrc`. Currently, excepting the protractor-specific keywords, only `mixpanel` is exposed globally due to its asynchronous nature.
* We use [ng-annotate](https://github.com/olov/ng-annotate) to manage our dependency injection, so you can write your code like this:

    ```
    angular.module("MyMod").controller("MyCtrl", function($scope, $timeout) {
    });
    ```

    instead of like this:

    ```
    angular.module("MyMod").controller("MyCtrl", ["$scope", "$timeout", function($scope, $timeout) {
    }]);
    ```
* `lodash` and `moment` are exposed global libraries that are very useful.


#### Testing

* All components must have valid, **passing** unit tests.
* All features or bug fixes **must be tested** by one or more
  [specs](https://docs.angularjs.org/guide/unit-testing).

#### Coding

* Wrap all code at **100 characters**.
* Do not use tabs. Use two (2) spaces to represent a tab or indent.
* Constructors are PascalCase, closures and variables are lowerCamelCase.
* When enhancing or fixing existing code
  * Do not reformat the author's code
  * Conform to standards and practices used within that code; unless overridden by best practices or patterns.
  * Provide jsDocs for functions and single-line comments for code blocks
  * Be careful of regression errors introduce by your changes
  * **Always** test your changes with unit tests and manual user testing.

#### Patterns

* All components should be wrapped in an anonymous closure using the Module Pattern.
* Use the **Revealing Pattern** as a coding style.
* Do **not** use the global variable namespace, export our API explicitly via Angular DI.
* Instead of complex inheritance hierarchies, use **simple** objects.
* Avoid prototypal inheritance unless warranted by performance or other considerations.
* We **love** functions and closures and, whenever possible, prefer them over objects.<br/>

    > Do not use anonymous functions. All closures should be named.

#### Documentation

* All non-trivial functions should have a jsdoc description.
* To write concise code that can be better minified, we **use aliases internally** that map to the
  external API.
* Use of argument **type annotations** for private internal APIs is not encouraged, unless it's an
  internal API that is used throughout Angular Material.



## <a name="rules"></a> Coding Rules
To ensure consistency throughout the source code, keep these rules in mind as you are working:

* All features or bug fixes **must be tested** by one or more specs (unit-tests).
* All public API methods **must be documented**. (Details TBC).
* With the exceptions listed below, we follow the rules contained in
  [Google's JavaScript Style Guide][js-style-guide]:
    * Wrap all code at **100 characters**.

Observe ```.jshintrc```

A few basic ones:
* camelCase for javascript related names.
* dash-separated for HTML and CSS related names.
* 4 spaces, no tabs.
* Single quote.
* Strip off console logging from code.
* ```use strict``` whenever possible, wrap inside IIFE.
* Use Angular modules as DI whenever possible, avoid polluting global.
* Avoid using watch in Angular.

## <a name="commit"></a> Commit Message Guidelines

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
we use the git commit messages to **generate the Connect Frontend change log**.

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **Bug ID**, **type**, a **scope** and a **subject**. The **body** and **footer** are **optional**:

```
<Bug ID> <type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier
to read on Gitlab as well as in various git tools.

### Bug ID
The (JIRA) bug tracker ID e.g. **PE-122** or **DAT-123**. This is useful in tracing our code changes in Jira.

### Type
Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing
  semi-colons, etc)
* **refactor**: A code change that neither fixes a bug or adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation
  generation

### Scope
The scope could be anything specifying place of the commit change. For example
`apiHealth`, `comData`, etc.

### Subject
The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

### Body (optional)
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer (optional)
The footer should contain any information about **Breaking Changes** and is also the place to
reference GitHub issues that this commit **Closes**.

### Special commits
There can be commits where you don't need to include the Bug ID and scope. For example, simple doc updates or chores like deleting unused files.

### Example
`DAT-123 fix(scrapetester): change an error message to be more descriptive`