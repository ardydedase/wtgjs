(function() {

'use strict';

angular.module('fcApp.dashboard', [
    'ngRoute',
    'LocalStorageModule',
    'fcApp.common',
    'fcApp.apiHealth',
    'fcApp.commercial'
]);

}());
