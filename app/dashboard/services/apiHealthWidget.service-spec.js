(function() {

'use strict';

describe('Service: apiHealthWidgetService', function() {

    var rootScope;
    var apiHealthWidgetService;

    beforeEach(module('fcApp.dashboard'));

    beforeEach(module('fcApp.dashboard', function($provide) {
        $provide.value('_', window._);
        $provide.value('c3', window.c3);
    }));

    beforeEach(inject(function($rootScope, _apiHealthWidgetService_) {
        rootScope = $rootScope.$new();
        apiHealthWidgetService = _apiHealthWidgetService_;
    }));

    describe('#getLiveUpdatesChartData', function() {
        var fakeData;

        beforeEach(function() {
            fakeData = [{
                type: 'success',
                value: 80
            },            {
                type: 'noquotes',
                value: 5
            },            {
                type: 'failure',
                value: 15
            },            {
                type: 'total',
                value: 100
            }];
        });

        it('should be defined', function() {
            expect(apiHealthWidgetService.getLiveUpdatesChartData).toBeDefined();
        });

        it('should return the api status for failures, no quotes, and success rates', function() {
            var result = apiHealthWidgetService.getLiveUpdatesChartData(fakeData);

            expect(result.length).toEqual(3);
            expect(result[0]).toEqual(['Failures Rate', '0.150']);
            expect(result[1]).toEqual(['No Quotes Rate', '0.050']);
            expect(result[2]).toEqual(['Success Rate', '0.800']);
        });
    });

    describe('#getDeeplinkChartData', function() {
        var fakeData;

        beforeEach(function() {

        });

        it('should be defined', function() {
            expect(apiHealthWidgetService.getDeeplinkChartData).toBeDefined();
        });

        it('should return the api status for failures, partial failures, and success rates', function() {
            fakeData = {
                errors: {
                    partial_failures: 5
                },
                failures: {
                    total: 10,
                    failed: 2
                }
            };
            var result = apiHealthWidgetService.getDeeplinkChartData(fakeData.failures, fakeData.errors);

            expect(result.length).toEqual(3);
            expect(result[0]).toEqual(['Failures Rate', '20.00']);
            expect(result[1]).toEqual(['Partial Failures Rate', '50.00']);
            expect(result[2]).toEqual(['Success Rate', '30.00']);
        });

        it('should return 100% success rate if data is all 0', function() {
            fakeData = {
                errors: {
                    partial_failures: 0
                },
                failures: {
                    total: 0,
                    failed: 0
                }
            };
            var result = apiHealthWidgetService.getDeeplinkChartData(fakeData.failures, fakeData.errors);

            expect(result.length).toEqual(3);
            expect(result[0]).toEqual(['Failures Rate', '0.00']);
            expect(result[1]).toEqual(['Partial Failures Rate', '0.00']);
            expect(result[2]).toEqual(['Success Rate', '100.00']);
        });
    });

    describe('#getDonutChart', function() {
        it('should be defined', function() {
            expect(apiHealthWidgetService.getDonutChart).toBeDefined();
        });

        it('should return a c3 chart', function() {
            spyOn(window.c3, 'generate');

            apiHealthWidgetService.getDonutChart({
                el: '#fake',
                data: []
            });

            expect(window.c3.generate).toHaveBeenCalled();
        });
    });
});

}());
