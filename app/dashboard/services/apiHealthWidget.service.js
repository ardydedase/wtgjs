(function() {

'use strict';

angular.module('fcApp.dashboard').factory('apiHealthWidgetService', function(
    _,
    c3,
    $filter) {

    function getLiveUpdatesChartData(summaryTotals) {
        var chartData = [];
        var summaryData = {};

        var total = _.find(summaryTotals, { type: 'total' }).value || 1;

        _.forEach(summaryTotals, function(d) {
            summaryData[d.type] = $filter('number')(d.value / total, 3);
        });

        chartData.push(['Failures Rate', summaryData.failure]);
        chartData.push(['No Quotes Rate', summaryData.noquotes]);
        chartData.push(['Success Rate', summaryData.success]);

        return chartData;
    }

    function getDeeplinkChartData(failures, errors) {
        var data = [];
        var hasFailure = failures.total > 0;
        var failurePct = hasFailure ? (failures.failed / failures.total) * 100 : 0;

        var partialFailurePct = 0;
        var successRate = 0;

        if (errors.partial_failures >= failures.total) {
            errors.partial_failures = failures.total;
            failurePct = 0;
            partialFailurePct = 100;
            successRate = 0;
        } else {
            if (errors.partial_failures + failures.failed > failures.total) {
                failurePct = 0;
            }

            partialFailurePct = hasFailure ? (errors.partial_failures / failures.total) * 100 : 0;
            successRate = 100 - failurePct - partialFailurePct;
        }

        if (!hasFailure) {
            partialFailurePct = 0;
            successRate = 100;
        }

        failurePct = $filter('number')(failurePct, 2);
        partialFailurePct = $filter('number')(partialFailurePct, 2);
        successRate = $filter('number')(successRate, 2);

        data.push(['Failures Rate', failurePct]);
        data.push(['Partial Failures Rate', partialFailurePct]);
        data.push(['Success Rate', successRate]);

        return data;
    }

    function getDonutChart(options) {
        var callback = options.onClickCallback || function() {};

        return c3.generate({
            bindto: options.el,
            data: {
                columns: options.data,
                type: 'donut',
                onclick: callback
            },
            donut: {
                expand: false
            },
            color: {
                pattern: ['#d9534f', '#f0ad4e', '#5cb85c']
            },
            size: {
                height: 240
            },
            legend: {
                item: {
                    onclick: function() {
                        return false;
                    }
                }
            }
        });
    }

    return {
        getLiveUpdatesChartData: getLiveUpdatesChartData,
        getDeeplinkChartData: getDeeplinkChartData,
        getDonutChart: getDonutChart
    };
});

}());
