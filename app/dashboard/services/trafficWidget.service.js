(function() {

'use strict';

angular.module('fcApp.dashboard').factory('trafficWidgetService', function(
    METRICS,
    _,
    $filter) {

    function getMetricSummary(data) {
        return _.map(data, function(value, key) {
            var view = _.find(METRICS, { id: key });
            var formattedValue = $filter('number')(value);
            var metric = {
                code: view.code,
                type: view.type,
                title: view.title,
                desc: view.shortDesc
            };

            if (view.type === 'percentage') {
                formattedValue = $filter('number')(value, 2) + '%';
            }

            if (view.type === 'ratio') {
                formattedValue += ':1';
            }

            if (value === null) {
                formattedValue = 'N/A';
            }

            metric.value = formattedValue;

            return metric;
        });
    }

    return {
        getMetricSummary: getMetricSummary
    };
});

}());
