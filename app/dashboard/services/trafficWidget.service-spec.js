(function() {

'use strict';

describe('Service: trafficWidgetService', function() {

    var rootScope;
    var trafficWidgetService;

    beforeEach(module('fcApp.dashboard'));

    beforeEach(module('fcApp.dashboard', function($provide) {
        $provide.value('_', window._);
    }));

    beforeEach(inject(function($rootScope, _trafficWidgetService_) {
        rootScope = $rootScope.$new();
        trafficWidgetService = _trafficWidgetService_;
    }));

    describe('#getMetricSummary', function() {
        var fakeData;

        beforeEach(function() {
            fakeData = {
                scrape_count: 168957,
                redirect_count: 6462,
                conversion_percentage: 2.321,
                booking_count: 1,
                scrape2book_ratio: 183
            };
        });

        it('should be defined', function() {
            expect(trafficWidgetService.getMetricSummary).toBeDefined();
        });

        it('should return all metrics', function() {
            var result = trafficWidgetService.getMetricSummary(fakeData);

            expect(result.length).toEqual(5);
        });

        it('should return formatted metric', function() {
            var result = trafficWidgetService.getMetricSummary(fakeData);

            expect(result[2].code).toEqual('conversion');
            expect(result[2].value).toEqual('2.32%');
            expect(result[2].type).toEqual('percentage');
            expect(result[2].title).toEqual('Conversion');
        });

        it('should format count type', function() {
            var result = trafficWidgetService.getMetricSummary(fakeData);

            expect(result[0].value).toEqual('168,957');
        });

        it('should format ratio type', function() {
            var result = trafficWidgetService.getMetricSummary(fakeData);

            expect(result[4].value).toEqual('183:1');
        });

        it('should format value as N/A if value is null', function() {
            fakeData.booking_count = null;

            var result = trafficWidgetService.getMetricSummary(fakeData);

            expect(result[3].value).toEqual('N/A');
        });
    });
});

}());
