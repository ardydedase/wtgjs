(function() {

'use strict';

angular.module('fcApp.dashboard').controller('DashboardCtrl', function(
    $scope,
    $location,
    $route,
    $q,
    moment,
    _,
    localStorageService,
    authService,
    funnelMetricsService,
    trafficWidgetService,
    apiHealthWidgetService,
    monitorService,
    apiHealthService,
    mixpanelService) {

    var self = this;

    var DATEFORMAT = 'DD MMM YYYY';
    var URL_DATEFORMAT = 'YYYY-MM-DD';

    var selectedWebsiteId = localStorageService.get('selectedwebsite');

    self.traffic = {
        startDate: moment().subtract(9, 'day').format(DATEFORMAT),
        endDate: moment().subtract(2, 'day').format(DATEFORMAT),
        weeklyStartDate: moment().subtract(9, 'day').format(DATEFORMAT),
        monthlyStartDate: moment().subtract(1, 'month').format(DATEFORMAT),
        data: [],
        allowed: authService.hasCommercialAccess(),
        error: false,
    };

    self.liveUpdatesHealth = {
        dataCollectionPeriod: 0,
        error: false
    };

    self.deeplinkHealth = {
        error: false
    };

    self.redirectPage = redirectPage;

    self.traffic.changeStartDate = function(date) {
        self.traffic.startDate = date;
        getTrafficData();

        mixpanelTracking({
            action: 'Change Date',
            widget: 'traffic',
            date: getDateTerm(date)
        });
    };

    self.traffic.isStartDateActive = function(date) {
        return date === self.traffic.startDate;
    };

    init();
    registerDashboardMixpanelTracking();

    function init() {
        getTrafficData();
        getLiveUpdatesHealthData();
        getDeeplinkHealthData();
    }

    function getTrafficData() {
        if (authService.hasCommercialAccess()) {
            var options = {
                start_timestamp: moment.utc(self.traffic.startDate, DATEFORMAT).unix(),
                end_timestamp: moment.utc(self.traffic.endDate, DATEFORMAT).unix(),
                dimensions: ['date'],
                segments: [],
                website_id: selectedWebsiteId,
                summary_only: true
            };

            self.traffic.error = false;

            self.traffic.commercialDataPromise = funnelMetricsService.postQuery(options).then(function(data) {
                self.traffic.metrics = trafficWidgetService.getMetricSummary(data.summary);

            },

            function(err) {
                if (err.status === 403) {
                    self.traffic.error = 403;
                } else {
                    self.traffic.error = 'There was a problem loading the data.';
                }
            });
        } else {
            self.traffic.allowed = false;
        }
    }

    function getLiveUpdatesHealthData() {
        self.liveUpdatesHealth.liveUpdatesPromise = apiHealthService.getSummaryTimeSeries({
            website_id: selectedWebsiteId
        }).then(function(data) {
            var chartData = apiHealthWidgetService.getLiveUpdatesChartData(data.summary.totals);

            apiHealthWidgetService.getDonutChart({
                el: '#api-health-chart',
                data: chartData,
                onClickCallback: function() {
                    redirectPage('LU');
                    $route.reload();
                }
            });

            self.liveUpdatesHealth.dataCollectionPeriod = data.time_series.length - 1;

        },

        function() {
            self.liveUpdatesHealth.error = true;
        });
    }

    function getDeeplinkHealthData() {
        var requests = {
            deepLinkFailures: monitorService.getDeeplinkFailures(),
            deepLinkErrors: monitorService.getDeeplinkErrors()
        };

        self.deeplinkHealth.deepLinkPromise = $q.all(requests).then(function(response) {
            var failures = response.deepLinkFailures;
            var errors = response.deepLinkErrors;

            var chartData = apiHealthWidgetService.getDeeplinkChartData(failures, errors);

            apiHealthWidgetService.getDonutChart({
                el: '#deeplink-health-chart',
                data: chartData,
                onClickCallback: function() {
                    redirectPage('DL');
                    $route.reload();
                }
            });
        },

        function() {
            self.deeplinkHealth.error = true;
        });
    }

    function redirectPage(page, type) {
        var url = '';
        var parameters = {};

        if (page === 'traffic') {
            parameters = {
                dimension: 'date',
                view: type,
                sdate: moment(self.traffic.startDate, DATEFORMAT).format(URL_DATEFORMAT),
                edate: moment(self.traffic.endDate, DATEFORMAT).format(URL_DATEFORMAT),
                src: 'dashboard'
            };

            url = '/traffic';
        }

        if (page === 'LU') {
            parameters = {
                website_id: selectedWebsiteId
            };

            url = '/live-updates';
        }

        if (page === 'DL') {
            parameters = {
                query_website: selectedWebsiteId
            };

            url = '/monitor';
        }

        mixpanelTracking({
            action: 'Redirect from widget',
            widget: page,
            metric: type
        });

        $location.search(parameters);
        $location.path(url);
    }

    function getDateTerm(date) {
        if (date === self.traffic.weeklyStartDate) {
            return 'lastWeek';
        }

        if (date === self.traffic.monthlyStartDate) {
            return 'lastMonth';
        }

        return '';
    }

    function mixpanelTracking(trackingData) {
        mixpanelService.track('FC Dashboard', trackingData);
    }

    function registerDashboardMixpanelTracking() {
        var trackingData = {
            action: 'Loaded Page'
        };

        if (authService.hasCommercialAccess()) {
            trackingData = _.extend(trackingData, {
                widget: 'traffic',
                date: getDateTerm(self.traffic.startDate)
            });
        }

        mixpanelTracking(trackingData);
    }
});

}());
