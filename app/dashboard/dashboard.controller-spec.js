(function() {

'use strict';

describe('Controller: DashboardCtrl', function() {

    var controller;
    var defaultMocks;
    var location;
    var q;
    var scope;

    var createPromise = function(resolveOrReject, data) {
        resolveOrReject = resolveOrReject === undefined ? true : resolveOrReject;
        data = data ? data : {};

        return function() {
            var dfr = q.defer();

            if (resolveOrReject === true) {
                dfr.resolve(data);
            } else {
                dfr.reject(data);
            }

            return dfr.promise;
        };
    };

    var defer = function() {
        var dfr = q.defer();
        return dfr.promise;
    };

    var noop = function() {};

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('DashboardCtrl', {
            $scope: scope,
            $location: location,
            localStorageService: mocks.localStorageService,
            authService: mocks.authService,
            funnelMetricsService: mocks.funnelMetricsService,
            trafficWidgetService: mocks.trafficWidgetService,
            apiHealthWidgetService: mocks.apiHealthWidgetService,
            monitorService: mocks.monitorService,
            apiHealthService: mocks.apiHealthService,
            mixpanelService: mocks.mixpanelService,
            _: window._,
            moment: window.moment
        });
    };

    beforeEach(module('fcApp.dashboard'));

    beforeEach(inject(function($controller, $rootScope, $location, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        location = $location;
        q = $q;

        defaultMocks = {
            localStorageService: {
                get: function() {
                    return 'poxx';
                }
            },
            authService: {
                hasCommercialAccess: function() {
                    return true;
                }
            },
            funnelMetricsService: {
                postQuery: defer
            },
            monitorService: {
                getDeeplinkFailures: defer,
                getDeeplinkErrors: defer
            },
            apiHealthService: {
                getSummaryTimeSeries: defer
            },
            trafficWidgetService: {
                getMetricSummary: function() {}
            },
            apiHealthWidgetService: {
                getLiveUpdatesChartData: noop,
                getDeeplinkChartData: noop,
                getDonutChart: noop
            },
            mixpanelService: {
                track: noop
            }
        };
    }));

    it('should initialize defaults', function() {
        var ctrl = createController();

        expect(ctrl.traffic).toBeDefined();
        expect(ctrl.traffic.startDate).toBeDefined();
        expect(ctrl.traffic.endDate).toBeDefined();
        expect(ctrl.traffic.weeklyStartDate).toBeDefined();
        expect(ctrl.traffic.monthlyStartDate).toBeDefined();
        expect(ctrl.traffic.data).toBeDefined();
        expect(ctrl.traffic.allowed).toBeDefined();
        expect(ctrl.traffic.error).toBeDefined();
        expect(typeof ctrl.traffic.changeStartDate).toEqual('function');
        expect(typeof ctrl.traffic.isStartDateActive).toEqual('function');

        expect(ctrl.liveUpdatesHealth).toBeDefined();
        expect(ctrl.liveUpdatesHealth.dataCollectionPeriod).toBeDefined();
        expect(ctrl.liveUpdatesHealth.error).toBeDefined();

        expect(ctrl.deeplinkHealth).toBeDefined();
        expect(ctrl.deeplinkHealth.error).toBeDefined();

        expect(typeof ctrl.redirectPage).toEqual('function');
    });

    describe('getTrafficData', function() {
        it('should only run this function if user has commercial access', function() {
            var localMocks = window._.extend(defaultMocks, {
                authService: {
                    hasCommercialAccess: function() {
                        return false;
                    }
                }
            });

            var ctrl = createController(localMocks);

            expect(ctrl.traffic.allowed).toEqual(false);
        });

        it('should reset error message', function() {
            var ctrl = createController();

            expect(ctrl.traffic.error).toEqual(false);
        });

        it('should make an ajax call to retrieve traffic summary data', function() {
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.callThrough();

            createController();

            var lastCall = defaultMocks.funnelMetricsService.postQuery.calls.mostRecent().args[0];

            expect(defaultMocks.funnelMetricsService.postQuery).toHaveBeenCalled();
            expect(lastCall.start_timestamp).toBeDefined();
            expect(lastCall.end_timestamp).toBeDefined();
            expect(lastCall.dimensions).toEqual(['date']);
            expect(lastCall.segments).toEqual([]);
            expect(lastCall.website_id).toEqual('poxx');
            expect(lastCall.summary_only).toEqual(true);
        });

        it('should format metrics data', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(dfr.promise);
            spyOn(defaultMocks.trafficWidgetService, 'getMetricSummary').and.returnValue('summaryData');

            var ctrl = createController();
            dfr.resolve({
                summary: 'hello'
            });
            scope.$digest();

            expect(defaultMocks.trafficWidgetService.getMetricSummary).toHaveBeenCalledWith('hello');
            expect(ctrl.traffic.metrics).toEqual('summaryData');
        });

        it('should handle 403 error', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(dfr.promise);

            var ctrl = createController();
            dfr.reject({
                status: 403
            });
            scope.$digest();

            expect(ctrl.traffic.error).toEqual(403);
        });

        it('should handle other errors', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(dfr.promise);

            var ctrl = createController();
            dfr.reject({
                status: 500
            });
            scope.$digest();

            expect(ctrl.traffic.error).toEqual('There was a problem loading the data.');
        });
    });

    describe('getLiveUpdatesHealthData', function() {
        var fakeData;

        beforeEach(function() {
            fakeData = {
                time_series: [1, 2, 3],
                summary: {
                    totals: [{
                        type: 'success',
                        value: 8
                    }, {
                        type: 'failure',
                        value: 1
                    }, {
                        type: 'noquotes',
                        value: 1
                    }, {
                        type: 'total',
                        value: 10
                    }]
                }
            };
        });

        it('should make an ajax call to get time series data', function() {
            spyOn(defaultMocks.apiHealthService, 'getSummaryTimeSeries').and.callThrough();

            createController();

            expect(defaultMocks.apiHealthService.getSummaryTimeSeries).toHaveBeenCalledWith({
                website_id: 'poxx'
            });
        });

        it('should draw a donut chart', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.apiHealthService, 'getSummaryTimeSeries').and.returnValue(dfr.promise);
            spyOn(defaultMocks.apiHealthWidgetService, 'getLiveUpdatesChartData').and.returnValue('timeSeriesData');
            spyOn(defaultMocks.apiHealthWidgetService, 'getDonutChart');

            createController();
            dfr.resolve(fakeData);
            scope.$digest();

            expect(defaultMocks.apiHealthWidgetService.getLiveUpdatesChartData).toHaveBeenCalledWith(fakeData.summary.totals);

            var lastCall = defaultMocks.apiHealthWidgetService.getDonutChart.calls.mostRecent().args[0];
            expect(defaultMocks.apiHealthWidgetService.getDonutChart).toHaveBeenCalled();
            expect(lastCall.data).toEqual('timeSeriesData');
        });

        it('should display the widget\'s data collection period', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.apiHealthService, 'getSummaryTimeSeries').and.returnValue(dfr.promise);

            var ctrl = createController();
            dfr.resolve(fakeData);
            scope.$digest();

            expect(ctrl.liveUpdatesHealth.dataCollectionPeriod).toEqual(2);
        });

        it('should handle error', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.apiHealthService, 'getSummaryTimeSeries').and.returnValue(dfr.promise);

            var ctrl = createController();
            dfr.reject();
            scope.$digest();

            expect(ctrl.liveUpdatesHealth.error).toEqual(true);
        });
    });

    describe('getDeeplinkHealthData', function() {
        it('should make ajax calls to get deeplink failures and erroes', function() {
            spyOn(defaultMocks.monitorService, 'getDeeplinkFailures').and.callThrough();
            spyOn(defaultMocks.monitorService, 'getDeeplinkErrors').and.callThrough();

            createController();

            expect(defaultMocks.monitorService.getDeeplinkFailures).toHaveBeenCalled();
            expect(defaultMocks.monitorService.getDeeplinkErrors).toHaveBeenCalled();
        });

        it('should draw a donut chart', function() {
            var localMocks = window._.extend(defaultMocks, {
                monitorService: {
                    getLiveUpdateStats: createPromise(false),
                    getDeeplinkFailures: createPromise(true, {
                        failed: 1,
                        total: 10
                    }),
                    getDeeplinkErrors: createPromise(true, {
                        partial_failures: 7
                    })
                }
            });

            spyOn(defaultMocks.apiHealthWidgetService, 'getDeeplinkChartData').and.returnValue('deeplinkChartData');
            spyOn(defaultMocks.apiHealthWidgetService, 'getDonutChart');

            createController(localMocks);
            scope.$digest();

            expect(defaultMocks.apiHealthWidgetService.getDeeplinkChartData).toHaveBeenCalled();

            var lastCall = defaultMocks.apiHealthWidgetService.getDonutChart.calls.mostRecent().args[0];
            expect(defaultMocks.apiHealthWidgetService.getDonutChart).toHaveBeenCalled();
            expect(lastCall.data).toEqual('deeplinkChartData');
        });

        it('should handle error', function() {
            var localMocks = window._.extend(defaultMocks, {
                monitorService: {
                    getLiveUpdateStats: createPromise(false),
                    getDeeplinkFailures: createPromise(false),
                    getDeeplinkErrors: createPromise(false)
                }
            });

            var ctrl = createController(localMocks);
            scope.$digest();

            expect(ctrl.deeplinkHealth.error).toEqual(true);
        });
    });

    describe('#redirectPage', function() {
        it('should set location.path to /traffic with correct parameters', function() {
            var twoDaysAgo = window.moment().subtract(2, 'day').format('YYYY-MM-DD');
            var nineDaysAgo = window.moment().subtract(9, 'day').format('YYYY-MM-DD');

            var expectedPath = '/traffic';
            var expectedParameters = {
                dimension: 'date',
                view: 'whatever',
                sdate: nineDaysAgo,
                edate: twoDaysAgo,
                src: 'dashboard'
            };

            spyOn(location, 'path');
            spyOn(location, 'search');

            var ctrl = createController();
            ctrl.redirectPage('traffic', 'whatever');

            expect(location.path).toHaveBeenCalledWith(expectedPath);
            expect(location.search).toHaveBeenCalledWith(expectedParameters);
        });

        it('should set location.path to /monitor with correct parameters', function() {
            var expectedPath = '/monitor';
            var expectedParameters = {
                query_website: 'poxx'
            };

            spyOn(location, 'path');
            spyOn(location, 'search');

            var ctrl = createController();
            ctrl.redirectPage('DL');
            scope.$digest();

            expect(location.path).toHaveBeenCalledWith(expectedPath);
            expect(location.search).toHaveBeenCalledWith(expectedParameters);
        });

        it('should set location.path to /live-updates with correct parameters', function() {
            var expectedPath = '/live-updates';
            var expectedParameters = {
                website_id: 'poxx'
            };

            spyOn(location, 'path');
            spyOn(location, 'search');

            var ctrl = createController();
            ctrl.redirectPage('LU');
            scope.$digest();

            expect(location.path).toHaveBeenCalledWith(expectedPath);
            expect(location.search).toHaveBeenCalledWith(expectedParameters);
        });
    });

    describe('#changeStartDate', function() {
        it('should update scope.startDate', function() {
            var ctrl = createController();

            ctrl.traffic.changeStartDate('2015-01-01');
            scope.$digest();

            expect(ctrl.traffic.startDate).toEqual('2015-01-01');
        });
    });

    describe('#isStartDateActive', function() {
        it('should return true if startDate is equal to passed date', function() {
            var dateFormat = 'DD MMM YYYY';
            var nineDaysAgo = window.moment().subtract(9, 'day').format(dateFormat);
            var ctrl = createController();

            var test = ctrl.traffic.isStartDateActive(nineDaysAgo);
            scope.$digest();
            expect(test).toEqual(true);

            var threeDaysAgo = window.moment().subtract(3, 'day').format(dateFormat);
            ctrl.traffic.changeStartDate(threeDaysAgo);
            test = ctrl.traffic.isStartDateActive(threeDaysAgo);
            scope.$digest();
            expect(test).toEqual(true);
        });
    });
});

}());
