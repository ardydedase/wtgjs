(function() {

'use strict';

angular.module('fcApp.technical')
    .controller('TestsCtrl', function(
        $scope,
        $rootScope,
        $window,
        $location,
        $routeParams,
        $route,
        $analytics,
        $timeout,
        $filter,
        $interval,
        URI,
        moment,
        modalService,
        testService,
        integrationService,
        quoteService,
        webConfigService,
        alertService,
        localStorageService,
        accountService,
        authService,
        jiraService,
        mixpanelService,
        utilService) {

        var selectedWebsiteId = localStorageService.get('selectedwebsite') || '';
        var username = localStorageService.get('username');

        // var webConfig = localStorageService.get('webconfig') || {};

        function init() {
            $scope.departDate = testService.offsetDate(30);
            $scope.returnDate = testService.offsetDate(37);
            $scope.is_skyscanner_staff = accountService.isMemberOf('Skyscanner');
            $scope.formatDate = testService.formatDate;
            $scope.is_oneway = false;
            $scope.test_flight_heading = 'Save Test Case';
            $scope.loading_skippy_deeplink = [];
            $scope.showExpiredTests = false;
            $scope.show_filtered_quotes = true;
            $scope.testConfigs = [];
            $scope.departDatePickerOptions = {
                icons: {
                    next: 'glyphicon glyphicon-arrow-right',
                    previous: 'glyphicon glyphicon-arrow-left',
                    up: 'glyphicon glyphicon-arrow-up',
                    down: 'glyphicon glyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };
            $scope.returnDatePickerOptions = {
                icons: {
                    next: 'glyphiconglyphicon-arrow-right',
                    previous: 'glyphiconglyphicon-arrow-left',
                    up: 'glyphiconglyphicon-arrow-up',
                    down: 'glyphiconglyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };

            // is internal user
            $scope.isInternalUser = authService.isInternalUser;

            // util service
            $scope.toTitleCase = utilService.toTitleCase;
            $scope.encodeURL = utilService.encodeURL;

            // quotes service
            $scope.filterReasons = quoteService.filterReasons;
            $scope.getAirlineID = quoteService.getAirlineID;
            $scope.isFiltered = quoteService.isFiltered;
            $scope.countFiltered = quoteService.countFiltered;
            $scope.isValidJacquardResponse = quoteService.isValidJacquardResponse;

            // carriers/airlines filter
            $scope.selected_carriers = [];
            $scope.addToCarrierFilter = quoteService.addToCarrierFilter;
            $scope.showByCarrier = quoteService.showByCarrier;

            // alert service
            $scope.closeAlert = alertService.closeAlert;

            $scope.datacenter_map = {
                SG1: 'Singapore Data Center',
                UK1: 'United Kingdom Data Center 1',
                UK2: 'United Kingdom Data Center 2',
                HK1: 'Hong Kong Data Center',
                OPSTEST: 'Test Environment Data Center (Results may not be stable.)'
            };

            $scope.cabinClassMap = {
                economy: 'Economy',
                premium_economy: 'Premium Economy',
                business: 'Business',
                first: 'First'
            };

            $scope.test_result = {
                quotes: [],
                elapsed: '',
                show: true,
                pricing_type: 'Scheduled',
                show_inbound: false,
                carriers: []
            };
        }

        function analyticsTracking(action) {
            $analytics.eventTrack(action, {
                category: selectedWebsiteId,
                label: username
            });
        }

        $scope.handleFilters = function() {
            try {
                if (localStorageService.get('account').groups.indexOf('Skyscanner') !== -1) {
                    $scope.show_filter_reasons = true;
                }
            } catch (e) {
                $scope.show_filter_reasons = false;
            }
        };

        $scope.checkUsername = function() {
            // check the username
            try {
                $scope.username = username;
            } catch (e) {
                $scope.username = 'NA';
            }
        };

        $scope.raised_ticket = {};
        $scope.button_disabled = {};

        $scope.raiseJira = function(alert, index) {
            var url = 'http://business.skyscanner.net/connect/#' + $location.path() + '?website_id=' + selectedWebsiteId;
            var info = JSON.stringify($scope.testConfig, null, 4);
            var summary = alert.msg;
            var description = url + '\n' + info ;

            jiraService.jira(summary, description).then(function(response) {
                response = response.data;
                if (response.hasOwnProperty('id')) {
                    $scope.raised_ticket[index] = true;
                    $scope.button_disabled[index] = true;

                    mixpanelService.track('FC Technical JiraTicketRaised', {
                        errorType: alert.msg,
                        serviceType: 'testdashboard'
                    });
                } else {
                    console.log('jira service failed');
                }
            });
        };

        $scope.loadingNext = false;
        $scope.skip = 0;
        $scope.loadedLast = false;

        $scope.loadTests = function() {
            if ($scope.loadingNext) {
                return;
            }

            analyticsTracking('loadTests');
            $scope.loadingNext = true;
            testService.get(undefined, $scope.skip, 30).then(function(response) {
                for (var i = 0; i < response.length; i++) {
                    $scope.testConfigs.push(response[i]);
                }

                if (response.length < 30) {
                    $scope.loadedLast = true;
                }

                $scope.skip = $scope.skip + 30;
                $scope.loadingNext = false;
            });
        };

        // TODO: Add unit test
        $scope.convertReproLink =  function(linkToConvert) {
            var uri = new URI(linkToConvert);
            $scope.testConfig.from_place = $scope.formatGeoLabel(uri.segment(2));
            $scope.testConfig.to_place = $scope.formatGeoLabel(uri.segment(3));

            var departDate = uri.segment(4);
            $scope.testConfig.depart_date = new Date('20' + departDate.substring(0, 2) + '-' + departDate.substring(2, 4) + '-' + departDate.substring(4, 6));
            $scope.departDate = $scope.testConfig.depart_date;
            if (uri.segment(5).length === 6) {
                $scope.is_oneway = false;
                var returnDate = uri.segment(5);
                $scope.testConfig.returnDate = new Date('20' + returnDate.substring(0, 2) + '-' + returnDate.substring(2, 4) + '-' + returnDate.substring(4, 6));
                $scope.returnDate = $scope.testConfig.return_date;
            } else {
                $scope.is_oneway = true;
            }

            var params = (uri.search(true));
            if ('adults' in params) {
                $scope.testConfig.adults = params.adults;
            }

            if ('children' in params) {
                $scope.testConfig.children = params.children;
            }

            if ('infants' in params) {
                $scope.testConfig.infants = params.infants;
            }

            if ('cabinclass' in params) {
                $scope.testConfig.cabin_class = params.cabinclass;
            }

            if (uri.subdomain().includes('opstest')) {
                $scope.testConfig.datacenter = 'opstest';
            }
        };

        $scope.loadTestConfigs = function(testId) {
            analyticsTracking('loadTestConfigs');

            if (typeof testId === 'undefined') {
                testId = $routeParams.testcase_id;
            }

            $scope.testLoadingMessage = 'Loading test cases.';

            $scope.testConfigsPromise = testService.get().then(function(response) {
                $scope.testConfigs = response;

                if ($routeParams.testcase_id) {
                    $scope.showAPIresponse(testId);
                }

                analyticsTracking('loadedTestCases');
            });
        };

        /**
         * Update Website Config and Default Test Config
         * @return {[type]} [description]
         */
        $scope.updateConfig = function(runRouteParams) {
            webConfigService.get().then(function(webConfig) {
                $scope.web_config = webConfig;

                webConfig.market = (webConfig.market) ? webConfig.market : webConfig.home_country;

                // make sure that the previous webconfig is cleared
                localStorageService.remove('webconfig');
                localStorageService.add('webconfig', webConfig);

                // Get the default test config
                $scope.testConfig = testService.getDefaultTestConfig();
                console.log($scope.testConfig);

                try {
                    angular.extend($scope.testConfig, {
                        market: webConfig.market,
                        currency: webConfig.currency,
                        lang: webConfig.lang
                    });
                } catch (e) {
                    // default to Singapore
                    angular.extend($scope.testConfig, {
                        market: 'SG',
                        currency: 'SGD',
                        lang: 'en'
                    });
                }

                if (runRouteParams) {
                    $scope.runRouteParams();
                }

            },

            function() {
                alertService.add('warning', 'Can\'t retrieve website config. Setting default.');
                $scope.testConfig = {
                    website_id: selectedWebsiteId,
                    pricing_type: 'Scheduled',
                    partner_type: 'TravelAgent',
                    homepage: 'http://www.skyscanner.net',
                    home_country: 'SG'
                };
            });
        };

        $scope.showExpired = function(past, showExpiredTests) {
            return past && showExpiredTests;
        };

        $scope.getLocationAutoSuggest = function(entry) {
            return testService.getLocationAutoSuggest(entry).then(function(res) {
                return res;
            });
        };

        $scope.formatGeoLabel = function(location) {
            if (location === undefined) {
                return null;
            }

            try {
                if (location.hasOwnProperty('PlaceId')) {
                    if (location.PlaceId.length > 3) {
                        return location.PlaceName + ' (Any)';
                    } else if (location.PlaceId.length === 3) {
                        return location.PlaceName + ' (' + location.PlaceId + ')';
                    }
                }
            } catch (e) {
                console.log(e);
            }

            try {
                return location.toUpperCase();
            } catch (e) {
                return null;
            }

        };

        $scope.selectLocation = function($item, $model, $label, placeType) {
            if (placeType === 'from') {
                $scope.testConfig.from_place = $item.PlaceId;
            } else {
                $scope.testConfig.to_place = $item.PlaceId;
            }

        };

        var validImageMap = {};
        var isImage = function(url) {
            if (url in validImageMap) {
                return validImageMap[url];
            }

            // check first if image is accessible
            utilService.isImage(url).then(function(result) {
                validImageMap[url] = false;

                if (result === true) {
                    validImageMap[url] = true;
                }

                return validImageMap[url];
            });

        };

        /**
         * [getAirlineLogo description]
         * @return {[type]} [description]
         */
        $scope.getCarrierLogo = function(marketingCarrierId) {
            //http://www.skyscanner.net/images/airlines/small/{{segment.skyscanner_marketing_carrier_id || getAirlineID(segment.marketing_carrier_id)}}.png
            var imgFile = marketingCarrierId ? marketingCarrierId : quoteService.getAirlineID(marketingCarrierId);
            var url = 'http://www.skyscanner.net/images/airlines/small/' + imgFile + '.png';

            if (isImage(url)) {
                return url;
            }

            return null;
        };

        $scope.getLocationID = function(locationId) {
            if (locationId.length > 3) {
                locationId = 'Any';
            }

            return locationId;
        };

        /**
         * Running tests via redirection/URL
         * @param  {[type]} testConfig [description]
         * @param  {[type]} filter     [description]
         * @param  {[type]} oneway     [description]
         * @return {[type]}            [description]
         */
        $scope.getRedirectURL = function(testConfig, filter, oneway) {
            analyticsTracking('getRedirectURL');

            if (oneway || !testConfig.return_date) {
                testConfig.return_date = '';
            }

            testConfig = angular.extend(testConfig, {
                filter: filter
            });

            //ucy=SG&lang=en&ccy=SGD&adults=1&children=0&infants=0&cabinclass=economy
            var queryString = {
                adults: testConfig.adults,
                children: testConfig.children,
                infants: testConfig.infants,
                cabinclass: testConfig.cabin_class,
                datacenter: testConfig.datacenter,
                filter: testConfig.filter,
                ucy: testConfig.market,
                lang: testConfig.lang,
                ccy: testConfig.currency,
                website_id: selectedWebsiteId
            };

            var pathString = [testConfig.from_place, testConfig.to_place, $filter('date')(testConfig.depart_date, 'yyyy-MM-dd').replace(/-/g, '')];

            if (testConfig.return_date) {
                pathString.push($filter('date')(testConfig.return_date, 'yyyy-MM-dd').replace(/-/g, ''));
            }

            return queryString;
        };

        /**
         * Initialize the ticker scope variables
         */
        $scope.tickRunning = false;
        $scope.running_task_idx = 0;
        $scope.runningTestMap = {};
        $scope.running_test = [];
        $scope.running_lastrun = {};

        var testTicker;
        /**
         * [stopTestTicker description]
         * @return {[type]} [description]
         */
        var stopTestTicker = function() {
            if (angular.isDefined(testTicker)) {
                $interval.cancel(testTicker);
                testTicker = undefined;
            }
        };
        /**
         * run the ticker that will poll the backend for running tasks
         * @return {[type]} [description]
         */
        var runTestTicker = function(testId) {
            $scope.tickRunning = true;
            var delay = 5000;

            if (angular.isDefined(testTicker)) {
                return;
            }

            testTicker = $interval(function() {
                // don't start the test ticker if it has already started

                var tasks = localStorageService.get('tasks');

                if (tasks) {
                    // reset the counter if it gone through the active tasks
                    if ((tasks.length) === $scope.running_task_idx) {
                        $scope.running_task_idx = 0;
                    }

                    if (tasks.length === 0) {
                        $scope.tickRunning = false;
                    }

                    var uid = null;

                    try {
                        uid = tasks[$scope.running_task_idx].uid;
                    } catch (e) {
                        console.log(e);
                        $scope.running_task_idx = 0;

                        // $timeout(tick, tick_interval);
                        return;
                    }

                    if (uid) {
                        testService.getRunningTest(uid).then(function(response) {
                            try {
                                if (response.ready === true) {
                                    $scope.running_test[tasks[$scope.running_task_idx].test_id] = false;
                                    $scope.running_lastrun[tasks[$scope.running_task_idx].test_id] = response.lastrun;

                                    if (tasks[$scope.running_task_idx].test_id === $scope.selected_test_id && testId) {
                                        $scope.showAPIresponse(testId);
                                    }

                                    // remove the task if it has successfully returned results
                                    tasks.splice($scope.running_task_idx, 1);

                                    localStorageService.remove('tasks');
                                    localStorageService.add('tasks', tasks);

                                    if (tasks.length === 0) {
                                        // stop interval if there are no tasks left
                                        stopTestTicker();
                                        $scope.tickRunning = false;
                                        $scope.running_task_idx = 0;

                                        if (testId) {
                                            $scope.loadTestConfigs(testId);

                                            // $scope.showAPIresponse(testId);

                                        } else {
                                            $scope.test_result = quoteService.formatResults(response.result.jac_response, $scope.testConfig);
                                        }

                                    }
                                }
                            } catch (e) {

                                try {
                                    $scope.running_test[tasks[$scope.running_task_idx].test_id] = false;
                                    $scope.cancelRunningTest(tasks[$scope.running_task_idx].test_id, tasks[$scope.running_task_idx].index);
                                } catch (e) {
                                    // when we can't get the test_id
                                    console.log(e);
                                }

                            }

                            $scope.running_task_idx++;

                            // $timeout(tick, tick_interval);

                        },

                        function() {
                            $scope.cancelRunningTest(tasks[$scope.running_task_idx].test_id, tasks[$scope.running_task_idx].index);
                        });
                    }
                }
            }, delay);
        };

        /**
         * Run active tasks if they are present.
         * @return {[type]} [description]
         */
        $scope.runActiveTasks = function() {
            var tasks = {};

            if (localStorageService.get('tasks') === null) {
                tasks = {};
            } else {
                tasks = localStorageService.get('tasks');
            }

            var taskLen = 0;

            try {
                taskLen = tasks.length;
            } catch (e) {
                console.log(e);
            }

            if (taskLen > 0) {
                // found existing tasks in the session
                // iterate through the tasks

                for (var x in tasks) {
                    if (tasks.hasOwnProperty(x)) {
                        // tasksList.push(tasks[x]);
                        // // remove tasks first
                        // localStorageService.remove('tasks');
                        // localStorageService.add('tasks', tasksList);
                        $scope.running_test[tasks[x].test_id] = true;
                        runTestTicker(tasks[x].test_id);
                    }
                }
            }
        };

        /**
         * Cancel the running tests. This will remove the task from RabbitMQ.
         * @param  {[type]} test_id [description]
         * @param  {[type]} index   [description]
         * @return {[type]}         [description]
         */
        $scope.cancelRunningTest = function(testId) {
            var test = testService.getRunningTestByTestId(testId);
            $scope.running_test[testId] = false;

            testService.cancelRunningTest(test.uid).then(function() {
                testService.removeRunningTestByTestId(testId);
            });
        };

        /**
         * Run the Jacquard Test
         * @param  {[type]} testConfig [description]
         * @param  {[type]} id         [description]
         * @return {[type]}            [description]
         */
        $scope.runTest = function(testConfig, id, filter, index, option, isOneway) {
            // update so that it will work with jacquard posting
            // there are differences with the naming convention, e.g. adults => adult in the existing ATD model
            alertService.reset();
            $scope.test_result = null;
            $scope.test_results_loading_message = 'Running test.';
            $scope.running_test = [];
            $scope.running_test[index] = true;
            $scope.selected_carriers = [];
            $scope.selected_test_id = id;
            $scope.show_search = false;
            $scope.test_results_promise = null;
            $scope.is_oneway = (testConfig.return_date) ? false : true;

            analyticsTracking('runTest');

            if (id) {
                // if saved test case is being run
                // clear previous query string
                $location.search({});
                $location.search('website_id', selectedWebsiteId);
                $location.path('/tests/' + id, false);
            } else {
                // when trying to run a test directly from the flight search box

                if (testConfig.from_place.hasOwnProperty('PlaceId')) {
                    testConfig.from_place = testConfig.from_place.PlaceId;
                }

                if (testConfig.to_place.hasOwnProperty('PlaceId')) {
                    testConfig.to_place = testConfig.to_place.PlaceId;
                }

                var pathString = [testConfig.from_place, testConfig.to_place, $filter('date')(testConfig.depart_date, 'yyyy-MM-dd').replace(/-/g, '')];

                if (!isOneway) {
                    pathString.push($filter('date')(testConfig.return_date, 'yyyy-MM-dd').replace(/-/g, ''));
                }

                var queryString = $scope.getRedirectURL(testConfig, filter, isOneway);

                //'/tests/transport/flights/' + pathString.join('/') + '/?' + $.param(queryString)

                $location.search(queryString);
                $location.path('/tests/transport/flights/' + pathString.join('/'), false);
            }

            testConfig = angular.extend(testConfig, {
                pricing_type: $scope.web_config.pricing_type,
                adults: testConfig.adults,
                children: testConfig.children,
                infants: testConfig.infants,
                cabin_class: testConfig.cabin_class,
                datacenter: testConfig.datacenter,
                depart_date: $filter('date')(testConfig.depart_date, 'yyyy-MM-dd'),
                uid: id,
                filter: filter
            });

            if (!isOneway) {
                angular.extend(testConfig, {
                    return_date: $filter('date')(testConfig.return_date, 'yyyy-MM-dd')
                });
            }

            // do not include the YAML file for now. we should get the IQ page right first
            $scope.test_results_promise = testService.run(testConfig, '', option, false).then(function(response) {
                $scope.running_test[index] = false;

                try {
                    $scope.test_result = quoteService.formatResults(response.jac_response, testConfig);
                    analyticsTracking('runTestSuccess');
                } catch (e) {
                    console.log(e);

                    if (e instanceof TypeError) {
                        $scope.test_result = quoteService.formatResults({}, testConfig);
                        angular.extend($scope.test_result, response);
                    }
                }

                // update the details in the search results
                angular.extend($scope.testConfig, {
                    from_place: testConfig.from_place,
                    to_place: testConfig.to_place,
                    depart_date: $filter('date')(testConfig.depart_date, 'yyyy-MM-dd'),
                    filter: testConfig.filter
                });

                if (testConfig.return_date) {
                    angular.extend($scope.testConfig, {
                        return_date: $filter('date')(testConfig.return_date, 'yyyy-MM-dd')
                    });
                }

                analyticsTracking('loadedTestResults');

                mixpanelService.track('FC Loaded Test Results');
            },

            function(response) {
                $scope.test_result = quoteService.formatResults(response.jac_response, testConfig);
                $scope.running_test[index] = false;
            });
        };

        /**
         * Poll the Jacquard Test
         * @param  {[type]} testConfig [description]
         * @param  {[type]} id         [description]
         * @return {[type]}            [description]
         */
        $scope.pollTests = function(testConfig, id, filter, index, option, isOneway) {
            // update so that it will work with jacquard posting
            // there are differences with the naming convention, e.g. adults => adult in the existing ATD model
            alertService.reset();
            $scope.test_result = null;
            $scope.test_results_loading_message = 'Running test.';
            $scope.running_test[id] = true;
            $scope.selected_carriers = [];
            $scope.selected_test_id = id;
            $scope.show_search = false;
            $scope.test_results_promise = null;
            $scope.is_oneway = (testConfig.return_date) ? false : true;

            if (id) {
                // if saved test case is being run
                // clear previous query string
                $location.search({});
                $location.search('website_id', selectedWebsiteId);
                $location.path('/tests/' + id, false);
            } else {
                // when trying to run a test directly from the flight search box

                if (testConfig.from_place.hasOwnProperty('PlaceId')) {
                    testConfig.from_place = testConfig.from_place.PlaceId;
                }

                if (testConfig.to_place.hasOwnProperty('PlaceId')) {
                    testConfig.to_place = testConfig.to_place.PlaceId;
                }

                var pathString = [testConfig.from_place, testConfig.to_place, $filter('date')(testConfig.depart_date, 'yyyy-MM-dd').replace(/-/g, '')];

                if (!isOneway) {
                    pathString.push($filter('date')(testConfig.return_date, 'yyyy-MM-dd').replace(/-/g, ''));
                }

                $scope.getRedirectURL(testConfig, filter, isOneway);
                $location.path('/tests/transport/flights/' + pathString.join('/'), false);
            }

            testConfig = angular.extend(testConfig, {
                pricing_type: $scope.web_config.pricing_type,
                adults: testConfig.adults,
                children: testConfig.children,
                infants: testConfig.infants,
                cabin_class: testConfig.cabin_class,
                datacenter: testConfig.datacenter,
                depart_date: $filter('date')(testConfig.depart_date, 'yyyy-MM-dd'),
                uid: id,
                filter: filter
            });

            if (!isOneway) {
                angular.extend(testConfig, {
                    return_date: $filter('date')(testConfig.return_date, 'yyyy-MM-dd')
                });
            }

            // do not include the YAML file for now. we should get the IQ page right first
            testService.run(testConfig, '', option, true).then(function(jacResponse) {
                var tasks = [];

                if (jacResponse.state === 'PENDING') {

                    if (localStorageService.get('tasks')) {
                        tasks = localStorageService.get('tasks');
                    }

                    // make sure that the current test index not in the current tasks list
                    try {
                        var existingRunningTest = testService.getRunningTestByTestId(id);

                        if (existingRunningTest.hasOwnProperty('test_id')) {
                            // remove task if it already exists
                            testService.removeRunningTestByTestId(existingRunningTest.test_id);
                        }
                    } catch (e) {
                        console.log('There\'s no existing task found.');
                    }

                    tasks.push({
                        uid: jacResponse.task_id,
                        index: index,
                        test_id: id
                    });

                    // replace the tasks local storage
                    localStorageService.remove('tasks');
                    localStorageService.add('tasks', tasks);

                    $scope.runningTestMap = {};

                    if (!$scope.tickRunning) {
                        runTestTicker(id);
                    }
                }

            },

            function() {
                $scope.running_test[id] = false;
            });
        };

        /**
         * Run using the URL Params / Query String
         * Same structure as in the Flights Day View page
         * @return {[type]} [description]
         */
        $scope.runRouteParams = function() {
            var parseDate = function(dateString) {

                if (dateString.length !== 8 && dateString.length !== 6) {
                    alertService.add('danger', 'Invalid date format.');
                    return null;
                }

                var dateStrOffset = (dateString.length === 8) ? 4 : 2;

                var year = (dateString.length === 6) ? '20' + dateString.substring(0, dateStrOffset) : dateString.substring(0, dateStrOffset);
                var month = dateString.substring(dateStrOffset, dateStrOffset + 2);
                var day = dateString.substring(dateStrOffset + 2, dateStrOffset + 4);

                return [year, month, day].join('-');
            };

            if ($routeParams.from_place && $routeParams.to_place && $routeParams.depart_date) {
                $scope.show_search = false;
                $scope.is_oneway = true;

                mixpanelService.track('FlightsConnect Ran URL Test Case');

                var testConfig = {
                    from_place: $routeParams.from_place.toUpperCase(),
                    to_place: $routeParams.to_place.toUpperCase(),
                    depart_date: parseDate($routeParams.depart_date),
                    return_date: '', // return date should be empty by default
                    adults: $routeParams.adults ?  $routeParams.adults : '1',
                    children: $routeParams.children ?  $routeParams.children : '0',
                    infants: $routeParams.infants ?  $routeParams.infants : '0',
                    cabin_class: $routeParams.cabinclass ? $routeParams.cabinclass : $scope.testConfig.cabinclass,
                    datacenter: $routeParams.datacenter ? $routeParams.datacenter : $scope.testConfig.datacenter,
                    filter: $routeParams.filter ? $routeParams.filter : true, // filter = true by default
                    lang: $routeParams.lang ? $routeParams.lang : $scope.web_config.language,
                    market: $routeParams.ucy ? $routeParams.ucy : $scope.web_config.market,
                    currency: $routeParams.ccy ? $routeParams.ccy : $scope.web_config.currency,
                };

                if (!testConfig.lang) {
                    testConfig.lang = 'en';
                }

                if ($.isNumeric($routeParams.return_date)) {
                    // if it's a return flight
                    $scope.is_oneway = false;
                    angular.extend(testConfig, {
                        return_date: parseDate($routeParams.return_date)
                    });
                }

                angular.extend($scope.testConfig, testConfig);

                // run the test
                // $scope.runTest($scope.testConfig, null, true, -1, 'nosave');
                $scope.pollTests($scope.testConfig, null, true, -1, 'nosave');

            }
        };

        $scope.redirectSkippyDeeplink = function(deeplinkUrl, quote, debug) {
            analyticsTracking('redirectSkippyDeeplink');

            if ('deeplink_data' in quote) {
                // only if deeplink data is available.
                var itinerary = quoteService.construcDeeplinkItinerary(quote);
                deeplinkUrl = deeplinkUrl + '&deeplink_ids=' + quote.deeplink_data.deeplink_id + '&itinerary=' + itinerary + '&carriers=' + (quote.carriers).join(',');
            }

            if (debug === true) {
                deeplinkUrl = deeplinkUrl + '&debug=True';
            }

            $window.open(deeplinkUrl, '_blank');
        };

        $scope.loading_pa = {};
        $scope.loading_pa_results = false;
        $scope.price_diff = {};
        $scope.pa_results = [];

        /**
         * Call the price accuracy service. This takes 5~ minutes
         * @param  {[type]} testConfig [description]
         * @param  {[type]} quote       [description]
         * @param  {[type]} oneway      [description]
         * @param  {[type]} index       [description]
         * @return {[type]}             [description]
         */
        $scope.checkPriceAccuracy = function(testConfig, quote, oneway, index) {
            $scope.loading_pa[index] = true;
            $scope.loading_pa_results = true;
            $scope.pa_results.push({
                done: false
            });

            // var startDate = new Date();

            testService.getPriceAccuracyUID(testConfig, quote, oneway).then(function(response) {
                $scope.pa_results[index] = {
                    done: false
                };

                if (response.error) {
                    alertService.reset();
                    alertService.add('danger', 'Price Accuracy Service has responded with: ' + response.error + '. Please try again later.');
                }

                var timer = 360;
                var onTimeout = function() {
                    if ((timer % 30) === 0 && $scope.pa_results[index].done === false) {
                        testService.checkPARequest(response.task_uuid, testConfig.datacenter).then(function(res) {
                            $scope.pa_results[index] = res;
                            $scope.price_diff[index] = 0;

                            if (Boolean(res.done) === true) {
                                $timeout.cancel(timeout);
                                $scope.loading_pa[index] = false;
                                $scope.loading_pa_results = false;

                                if (res.status === 'ERROR') {
                                    $location.hash('top');
                                    alertService.add('danger', 'Price Accuracy Service has responded with: ' + res.message.error_type + '. Please try again later.');
                                } else {
                                    // $scope.price_diff[index] = quote.price.total.taxpx - res.result.price.value;
                                    $scope.price_diff[index] = parseFloat($filter('number')(quote.price.total.taxpx, 2)) - parseFloat($filter('number')(res.result.price.value, 2));

                                    // var dateDiff = Math.abs(new Date() - startDate);
                                }
                            }
                        });
                    }

                    timer--;

                    if (timer > 0) {
                        timeout = $timeout(onTimeout, 1000);
                    } else if (timer <= 0 || $scope.pa_results[index].done === true) {
                        $timeout.cancel(timeout);
                        $scope.loading_pa[index] = false;
                        $scope.loading_pa_results = false;
                    }

                };

                var timeout = $timeout(onTimeout, 1000);
            });
        };

        /**
         * Load the latest integration state.
         * @return {[type]} [description]
         */
        $scope.loadIntegrationState = function() {
            if (localStorageService.get('integrationstate')) {
                // retrieve the integration state:
                $scope.integrationStateTransportYAML = localStorageService.get('integrationstate').transport_config;
                $scope.integrationStateTransportDeeplinkYAML = localStorageService.get('integrationstate').transport_deeplink_config;

            } else {
                if (!selectedWebsiteId) {
                    alertService.add('warning', 'You can\'t retrieve the Test Cases. No selected website.');
                } else {
                    integrationService.get().then(function(response) {
                        try {
                            $scope.integrationStateTransportYAML = response[0].value.transport_config;
                            $scope.integrationStateTransportDeeplinkYAML = response[0].value.transport_deeplink_config;
                        } catch (e) {
                            console.log(e);
                        }

                        try {
                            localStorageService.add('integrationstate', response[0].value);
                        } catch (e) {
                            console.log('no integration state found');
                        }

                    });
                }
            }
        };

        // $scope.loadIntegrationState();

        /**
         * Check if test config is in the past
         * @param  {[type]}  testConfig [description]
         * @return {Boolean}             [description]
         */
        $scope.isInThePast = testService.isInThePast;

        // get the min date in the calendar
        var dateNow = new Date();
        $scope.min_date = [dateNow.getFullYear(), ('0' + (dateNow.getMonth() + 1)).slice(-2), ('0' + dateNow.getDate()).slice(-2)].join('-');

        $scope.showLocale = function(testConfig) {
            var showLocaleModalInstance = modalService.showLocale(testConfig);

            showLocaleModalInstance.result.then(function(testConfig) {
                angular.extend($scope.testConfig, testConfig);
            }, function() {
                // modal dismissed, do nothing..
                // console.log('modal dismissed.');
            });
        };

        /**
         * Show the partner's quotes and API response
         * @param  {[type]} index [description]
         * @return {[type]}       [description]
         */
        $scope.showAPIresponse = function(id) {
            analyticsTracking('showAPIresponse');

            alertService.reset();
            $scope.selected_test_id = id;
            $scope.test_results_loading_message = 'Loading test case.';
            $scope.testConfigsPromise = null;
            $scope.selected_carriers = [];
            $scope.show_search = false;
            $scope.last_run_status = {};

            $location.search({
                website_id: selectedWebsiteId
            });

            // Change the URL path to the direct link to the test case.
            if (id) {
                $location.path('/tests/' + id, false);
            }

            $scope.test_results_promise = testService.get(id).then(function(response) {

                var getTestCasePoller = testService.getTestCasePoller(response.task_id);

                $scope.test_results_promise = getTestCasePoller.promise.then(null, null, function(pollerResponse) {

                    if (pollerResponse.data.ready) {
                        response = pollerResponse.data.result;
                        getTestCasePoller.stop();
                        $scope.test_results_promise = null;

                        analyticsTracking('showedAPIresponseSuccess');

                        try {
                            var jacResponse = response.jac_response;
                            var testConfig = response;

                            // delete testConfig.jac_response;
                            // delete testConfig.counter;
                            // delete testConfig.id;
                            // delete testConfig.uid;
                            // delete testConfig.doc_type;

                            $scope.testConfig = {};

                            console.log('testconfig frow show api response');
                            console.log(testConfig);

                            // update the details in the search results
                            angular.extend($scope.testConfig, testConfig);
                            console.log($scope.testConfig);

                            // update whether it's oneway or not
                            $scope.is_oneway = ($scope.testConfig.return_date) ? false : true;

                        } catch (e) {
                            console.log(e);
                            $scope.test_result.answers = null;
                        }

                        //replace the existing website_id if needed
                        if (response.website_id !== selectedWebsiteId) {
                            localStorageService.remove('selectedwebsite');
                            localStorageService.add('selectedwebsite', response.website_id);
                        }

                        $scope.test_result = quoteService.formatResults(jacResponse, testConfig);

                        $scope.last_run_status[id] = $scope.test_result.status;

                    }
                });

            }, function() {

            });
        };

        $scope.saveTest = function(testConfig, oneway) {
            $scope.saving_testcase = true;

            // make sure to remove the items that we don't need to save.
            delete testConfig.jac_response;
            delete testConfig.counter;
            delete testConfig.id;
            delete testConfig.uid;
            delete testConfig.doc_type;
            delete testConfig.lastrun;

            if (oneway) {
                testConfig.return_date = '';
            }

            if (testConfig.from_place.hasOwnProperty('PlaceId')) {
                testConfig.from_place = testConfig.from_place.PlaceId;
            }

            if (testConfig.to_place.hasOwnProperty('PlaceId')) {
                testConfig.to_place = testConfig.to_place.PlaceId;
            }

            $scope.testLoadingMessage = 'Updating test cases.';

            testService.save(testConfig).then(function() {
                $scope.saving_testcase = false;
                $scope.testConfigsPromise = null;
                $scope.testConfigsPromise = testService.get().then(function(response) {
                    $scope.testConfigs = response;
                });
            });
        };

        $scope.deleting_testcase = [];
        $scope.deleteTest = function(uid, index) {
            $scope.testLoadingMessage = 'Deleting test case.';
            $scope.testConfigsPromise = testService.delete(uid).then(function() {
                // remove test results
                $scope.test_result = {
                    quotes: [],
                    elapsed: '',
                };
                $scope.testConfigs.splice(index, 1);
            });
        };

        $scope.rawResponse = false;
        $scope.showRawResponse = function() {
            $scope.rawResponse = ($scope.rawResponse) ? false : true;
        };

        $scope.loading_deeplink = [];
        $scope.getDeeplink = function(testConfig, index) {
            testConfig.depart_date = testConfig.depart_date;
            testConfig.return_date = testConfig.return_date;

            $scope.loading_deeplink[index] = true;
            testService.getDeeplink(testConfig, $scope.integrationStateTransportDeeplinkYAML).then(function(response) {
                var method = $.isEmptyObject(response.post_data) ? 'get' : 'post';
                var formData = !$.isEmptyObject(response.post_data) ? response.post_data : response.get_data;

                // open new window only if url exists
                if (response.url) {
                    testService.redirectToDeeplink(response.url, formData, method);
                }

                $scope.loading_deeplink[index] = false;
            });
        };

        $scope.formIsValid = function(testConfig) {
            try {
                return testConfig.from_place && testConfig.to_place && testConfig.depart_date;
            } catch (e) {
                return false;
            }
        };

        $scope.getFilterDetails = function(id) {
            var filterMap = {
                max_results_with_tolerance: 'Max results returned. Found more than 150 quotes, API should return less.',
                max_results: 'Max results returned. Found more than 150 quotes, API should return less.',
                remove_duplicates: 'Found duplicates. They should be removed from the API response.'
            };

            if (filterMap[id]) {
                return filterMap[id];
            }

            return null;
        };

        $scope.timeDiff = function(start, end) {
            start = moment(start);
            end = moment(end);
            return start.diff(end);
        };

        $scope.showFilterReasons = function() {
            modalService.showStatic('filter-reasons.html');
            return false;
        };

        // TODO: Clean-up
        // This is a temporary hack for depart date update.
        function processDate(date) {
            if (typeof date === 'undefined') {
                return false;
            }

            var parts = date.split('-');
            return new Date(parts[0], parts[1] - 1, parts[2]);
        }

        $scope.updateReturn = function(departDate, returnDate) {

            try {
                if (processDate(returnDate) < processDate(departDate)) {
                    departDate = returnDate;
                    $scope.departDate = departDate;
                    console.log(departDate);
                    console.log($scope.departDate);
                }
            } catch (e) {
                // for debugging
                console.log(e);
            }

            try {
                $scope.testConfig.depart_date = departDate;
                $scope.testConfig.return_date = returnDate;
            } catch (e) {
                console.log(e);
            }
        };

        $scope.updateDepart = function(departDate, returnDate) {

            try {
                if (processDate(departDate) > processDate(returnDate)) {
                    returnDate = departDate;
                    $scope.returnDate = returnDate;
                    console.log(returnDate);
                    console.log($scope.returnDate);
                }
            } catch (e) {
                // for debugging
                console.log(e);
            }

            try {
                $scope.testConfig.depart_date = departDate;
                $scope.testConfig.return_date = returnDate;
            } catch (e) {
                console.log(e);
            }
        };

        $scope.$on('$destroy', function() {
            // make sure that the testTicker interval is destroyed too.
            stopTestTicker();
        });

        $scope.updateConfig(true);
        $scope.checkUsername();
        $scope.runActiveTasks();
        $scope.handleFilters();
        $scope.loadTests();

        if (typeof ($routeParams.testcase_id) !== 'undefined') {
            $scope.showAPIresponse($routeParams.testcase_id);
        }

        alertService.reset();
        init();

    })
    .filter('quoteFilter', function() {
        return function(input, filter, testResponse) {
            if (filter) {
                return input;
            }

            try {
                var unfiltered = [];
                angular.forEach(input, function(value) {
                    if (typeof (testResponse.filtering_info.route_enabled) !== 'undefined' && (!testResponse.filtering_info.route_enabled || testResponse.filtering_info.all_quotes_filtered)) {
                        return;
                    }

                    if (typeof (value.passed_commercial_filters) === 'undefined' && value.passed_quote_filters === 'true') {
                        this.push(value);
                    } else if (value.passed_quote_filters === 'true' && value.passed_commercial_filters === 'true') {
                        this.push(value);
                    }
                }, unfiltered);

                return unfiltered;
            } catch (e) {
                return input;
            }
        };
    });

}());
