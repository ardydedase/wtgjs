(function() {

'use strict';

describe('Controller: ScrapeTestResultCtrl', function() {

    // load the controller's module
    beforeEach(module('fcApp.technical'));

    var ScrapeTestResultCtrl;
    var scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function($controller, $rootScope) {
        scope = $rootScope.$new();
        ScrapeTestResultCtrl = $controller('ScrapeTestResultCtrl', {
            $scope: scope
        });
    }));
});

}());
