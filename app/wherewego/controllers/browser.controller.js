(function() {

'use strict';

angular.module('fcApp.wherewego')
    .controller('BrowserCtrl', function(
        $scope,
        $rootScope,
        $route,
        $routeParams,
        amadeusService) {

        function init() {
            $scope.filter = {
                flights: true,
                hotels: true,
            };

            $scope.budget = 3000;

            $scope.departDatePickerOptions = {
                icons: {
                    next: 'glyphicon glyphicon-arrow-right',
                    previous: 'glyphicon glyphicon-arrow-left',
                    up: 'glyphicon glyphicon-arrow-up',
                    down: 'glyphicon glyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };
            $scope.returnDatePickerOptions = {
                icons: {
                    next: 'glyphiconglyphicon-arrow-right',
                    previous: 'glyphiconglyphicon-arrow-left',
                    up: 'glyphiconglyphicon-arrow-up',
                    down: 'glyphiconglyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };

            getFlights();
        }

        function getFlights(params) {
            $scope.flightsPromise = null;

            $scope.flightsPromise = amadeusService.whenToGo(params).then(function(data) {
                $scope.flights = data;
            },
            function() {
                $scope.flightsError = true;
            });
        }

        $scope.convertToUSD = amadeusService.convertToUSD;
        $scope.getCityName = function(code) {
            var params = {
                code:code
            };
            $scope.locPromise[code] = amadeusService.getLocation(params).then(function(data) {
                $scope.loc[code] = data;
            }, function() {
                $scope.locError = true;
            });
        }

        $scope.updateBudget = function(budgetAmount) {
            console.log('updateBudget');
            console.log(budgetAmount);
            console.log($scope.departDate);
            console.log($scope.returnDate);

            if (budgetAmount && $scope.departDate && $scope.returnDate) {
                var params = {
                    max_price: amadeusService.convertToUSD(budgetAmount, 'THB'),
                    departure_date: $scope.departDate,
                    return_date: $scope.returnDate,
                };

                getFlights(params);

            }
        };

        $scope.constructParams = function(latitude, longitude, origin, destination) {
            var params = {
                departure_date: $scope.departDate,
                return_date: $scope.returnDate,
                origin: origin,
                destination: destination
            };

            if ($scope.filter.hotels == true) {
                console.log('filter it');
                $.extend(params, {latitude: latitude, longitude: longitude});
            }
            return "#flight-select?" + $.param(params);
        };

        init();
    });

}());
