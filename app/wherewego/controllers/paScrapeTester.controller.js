(function() {

'use strict';

angular.module('fcApp.technical')
    .controller('PAScrapeTesterCtrl', function(
        $scope,
        $rootScope,
        $interval,
        $route,
        $location,
        $window,
        $filter,
        alertService,
        testService,
        utilService,
        authService,
        modalService,
        webConfigService,
        localStorageService,
        paScrapeTesterService,
        jiraService,
        pollerService,
        mixpanelService
        ) {

        authService.restrictToGroup('Skyscanner');
        authService.redirectToWebsiteId();

        var scrapeTicker;

        // authService
        $scope.isInternalUser = authService.isInternalUser;

        /**
         * Initialize scope variables.
         * @return {[type]} [description]
         */
        function init() {

            $scope.scrapeTester = true;
            $scope.websiteId = localStorageService.get('selectedwebsite')  || $route.current.params.websiteId;
            $scope.toTitleCase = utilService.toTitleCase;
            $scope.encodeURL = utilService.encodeURL;
            $scope.runningScrapeIdx = 0;
            $scope.scrapeTasksLen = 0;
            $scope.isInThePast = testService.isInThePast;
            $scope.runningTest = {};
            $scope.runningLastRun = {};
            $scope.runningPAResponse = {};
            $scope.runningDeeplink = {};
            $scope.runningResponse = {};
            $scope.webConfig = {};
            $scope.scrapeTests = [];
            $scope.testLoadingMessage = 'Loading Tests..';

            $scope.raisedTicket = {};
            $scope.jiraResponse = {};
            $scope.raisingJira = {};
            $scope.skip = 0;
            $scope.startScrapeError = '';
        }

        /**
         * Initialize scraper settings. Minimize inputs from user.
         * @param  {[type]} updatedWebConfig [description]
         * @return {[type]}                  [description]
         */
        function initScraperSettings(updatedWebConfig, updatedScraperSettings) {
            // default scraper settings
            var webConfig = updatedWebConfig || localStorageService.get('webconfig');
            $scope.webConfig = webConfig;
            $scope.scraperSettings = {
                testsuite: '',
                isRerun: 0,
                request_count: 2,
                datacenter: 'europe',
                max_adults: 4,
                max_children: 2,
                max_infants: 2,
                market: $scope.webConfig.market,
                is_ota: $scope.webConfig.partner_type === 'TravelAgent',
                website_id: $scope.websiteid
            };

            $.extend($scope.scraperSettings, updatedScraperSettings);

            return $scope.scraperSettings;
        }

        /**
         * Stop the scrape ticker when it's done.
         * @return {[type]} [description]
         */
        function stopScrapeTicker() {
            if (angular.isDefined(scrapeTicker)) {
                $interval.cancel(scrapeTicker);
                scrapeTicker = undefined;
            }
        }

        /**
         * [runScrapeTicker description]
         * @param  {[type]} scrapeTasks [description]
         * @return {[type]}             [desrciption]
         */
        function runScrapeTicker(tasks, scrapeId, scraperSettings) {
            var SCRAPETICKER_DELAY = 2000;
            if (typeof scraperSettings === 'undefined') {
                scraperSettings = $scope.scraperSettings;
            }

            var scrapeTasks;

            if (typeof scrapeTasks !== 'undefined') {
                scrapeTasks = tasks;

                // store tasks in local storage
                localStorageService.remove('scrape_tasks');
                localStorageService.add('scrape_tasks', scrapeTasks);
            } else if (localStorageService.get('scrape_tasks') !== null) {
                scrapeTasks = localStorageService.get('scrape_tasks');
            } else {
                scrapeTasks = [];
            }

            if (scrapeTasks.length > 0) {

                scrapeTicker = $interval(function() {
                    scrapeTasks = localStorageService.get('scrape_tasks');
                    if (scrapeTasks.length === 0) {
                        stopScrapeTicker();
                    }

                    var taskId = null;

                    try {
                        taskId = scrapeTasks[$scope.runningScrapeIdx].task_id;
                    } catch (e) {
                        $scope.runningScrapeIdx = 0;
                        return;
                    }

                    if (taskId === undefined) {
                        $scope.runningScrapeIdx = 0;
                        return;
                    }

                    $scope.runningTest[scrapeTasks[$scope.runningScrapeIdx].id] = true;

                    paScrapeTesterService.checkScrape(taskId, scraperSettings).then(function(response) {
                        if (response.ready === true) {
                            try {
                                $scope.runningResponse[scrapeTasks[$scope.runningScrapeIdx].id] = response.result;
                                $scope.runningTest[scrapeTasks[$scope.runningScrapeIdx].id] = false;
                                $scope.runningLastRun[scrapeTasks[$scope.runningScrapeIdx].id] = response.lastrun;
                                $scope.runningPAResponse[scrapeTasks[$scope.runningScrapeIdx].id] = response.result.response;
                            } catch (e) {
                                console.log('There was a problem getting the scrapee ID:');
                                console.log(e);
                            }

                            scrapeTasks.splice($scope.runningScrapeIdx, 1);

                            localStorageService.remove('scrape_tasks');
                            localStorageService.add('scrape_tasks', scrapeTasks);
                        }

                        $scope.scrapeTasksLen = scrapeTasks.length;

                        // When there are no more tasks left
                        if ($scope.scrapeTasksLen === 0) {
                            stopScrapeTicker();
                            $scope.runningScrapeIdx = 0;
                        } else {
                            $scope.runningScrapeIdx++;
                        }
                    });

                }, SCRAPETICKER_DELAY);
            }
        }

        function mixpanelTracking(options) {
            var trackingData = {
                serviceType: 'pascrapetester'
            };

            if (options) {
                $.extend(trackingData, options);
            }

            mixpanelService.track(options.actionName, trackingData);
        }

        $scope.loadedLast = false;

        $scope.loadScrapes = function() {
            if ($scope.loadingScrapeTests) {
                return;
            }

            $scope.loadingScrapeTests = true;
            var params = {skip: $scope.skip};

            pollerService.start({
                url: 'v0.4/flights/pa_scrapes/past',
                params: params
            }).then(function(taskId) {
                var loadScrapeTestsPoller = pollerService.poll({
                    url: 'v0.4/flights/pa_scrapes/past/async',
                    taskId: taskId
                });

                loadScrapeTestsPoller.promise.then(null, null, function(pollerResponse) {
                    if (pollerResponse.data.ready) {
                        loadScrapeTestsPoller.stop();
                        var scrapeTasks = [];

                        for (var i = 0; i < pollerResponse.data.result.length; i++) {
                            $scope.scrapeTests.push(pollerResponse.data.result[i]);
                            if (pollerResponse.data.result[i].active) {
                                scrapeTasks.push(pollerResponse.data.result[i]);
                            }
                        }

                        if (scrapeTasks.length > 0) {
                            runScrapeTicker(scrapeTasks);
                        }

                        if (pollerResponse.data.result.length < 25) {
                            $scope.loadedLast = true;
                        }

                        $scope.loadingScrapeTests = false;
                    }
                });

            });

            $scope.skip = $scope.skip + 25;
        };

        /**
         * Load all the Scrape Tests
         * @return {[type]} [description]
         */

        /* commenting this out because not used.  Please remove.
        function loadScrapeTests() {
            $scope.loadingScrapeTests = true;
            $scope.skip = 0;
            // remove existing task
            localStorageService.set('scrape_tasks', []);
            var params = {skip: $scope.skip};

            pollerService.start({
                url: 'v0.4/flights/pa_scrapes/past',
                params: params
            }).then(function(taskId) {
                var loadScrapeTestsPoller = pollerService.poll({
                    url: 'v0.4/flights/pa_scrapes/past/async',
                    taskId: taskId
                });

                loadScrapeTestsPoller.promise.then(null, null, function(pollerResponse) {
                    if (pollerResponse.data.ready) {
                        loadScrapeTestsPoller.stop();
                        $scope.scraperSettings.testsuite = $route.current.params.scrapeTestId;
                        $scope.scrapeTests  = pollerResponse.data.result;

                    }
                    $scope.loadingScrapeTests = false;
                });
            });
            $scope.skip = $scope.skip+25;
        }
        */

        /**
         * [processCabinClasses description]
         * @return {[type]} [description]
         */

        /* commenting this out because not used.  Please remove.
        function processCabinClasses(availableCabinClasses) {
            var cabinClasses = $scope.scraperSettings.cabin_classes;

            for (var i = 0; i < availableCabinClasses.length; i++) {
                availableCabinClasses[i] = (availableCabinClasses[i]).toLowerCase();
            }

            for (var key in cabinClasses) {

                if (availableCabinClasses.indexOf(key) == -1) {
                    $scope.scraperSettings.cabin_classes[key] = false;
                }
            }
        }
        */

        /**
         * Check if form is incomplete
         * @return {[type]} [description]
         */
        $scope.formIncomplete = function() {
            return false;
        };

        /**
         * Update current selected website.
         * @param  {[type]} selectedWebsite [description]
         * @return {[type]}                 [description]
         */

        //$scope.$on('websiteIdChanged', updateConfig);
        // $on doesn't work properly as inheritence is extremely strange here
        $scope.$watch('websiteId', updateConfig);
        function updateConfig(selectedWebsite) {
            $scope.loadSettingsPromise = webConfigService.get(selectedWebsite).then(function(response) {
                // localStorageService.remove('webconfig');
                // localStorageService.add('webconfig', response);

                initScraperSettings(response);

                var redirectPath = '/pa-scrapetester/' + selectedWebsite;
                if ($route.current.params.scrapeTestId) {
                    redirectPath += '/' + $route.current.params.scrapeTestId;
                }

                $location.path(redirectPath);
            });
        }

        $scope.rerunScrapes = function(scraperSettings) {
            $scope.scrapeTests = [];
            $scope.scraperSettings.isRerun = 1;
            $scope.scraperSettings.scrape_id = $route.current.params.scrapeTestId;
            initScraperSettings(null, scraperSettings);
            $scope.runningScrapes = $scope.reRunningScrapes = true;
            $scope.runningLastRun = {};
            $scope.runningPAResponse = {};
            $scope.runningResponse = {};
            $scope.testConfigs = [];

            // $scope.runningLastRun = [];

            mixpanelTracking({
                actionName: 'FC Technical Start PA Scrapes'
            });

            $scope.testConfigsPromise = paScrapeTesterService.start(scraperSettings).then(function(response) {
                var startScrapesPoller;

                if (!startScrapesPoller) {
                    startScrapesPoller = paScrapeTesterService.startScrapesPoller(response.task_id);

                    mixpanelTracking({
                        actionName: 'FC Technical Successfully Started PA Scrapes'
                    });

                    $scope.testConfigsPromise = startScrapesPoller.promise.then(null, null, function(pollerResponse) {
                        if (pollerResponse.data.ready) {
                            mixpanelTracking({
                                actionName: 'FC Technical Received PA Scrape Results'
                            });

                            pollerResponse = pollerResponse.data.result;
                            startScrapesPoller.stop();

                            // confirm that promise is fulfilled
                            $scope.testConfigsPromise = null;

                            // store tasks in local storage
                            localStorageService.remove('scrape_tasks');
                            localStorageService.add('scrape_tasks', pollerResponse.tasks);

                            $scope.testConfigs = pollerResponse.tasks;
                            runScrapeTicker(pollerResponse.tasks, pollerResponse.scrape_id, scraperSettings);
                            $location.path('/pa-scrapetester/' + $scope.websiteId + '/' + scraperSettings.scrape_id, false);
                            $scope.runningScrapes = $scope.reRunningScrapes = false;
                        }
                    });
                }

            },

            function() {
                $scope.runningScrapes = $scope.reRunningScrapes = false;
            });
        };

        /**
         * Start Scrape tests.
         * @param  {[type]} scraperSettings [description]
         * @return {[type]}                 [description]
         */
        $scope.startScrapes = function(scraperSettings) {
            $scope.scraperSettings.isRerun = 0;
            initScraperSettings(null, scraperSettings);
            $scope.runningScrapes = true;

            mixpanelTracking({
                actionName: 'FC Technical Start PA Scrapes'
            });

            $scope.testConfigsPromise = paScrapeTesterService.start(scraperSettings).then(function(response) {
                var startScrapesPoller;

                if (typeof startScrapesPollers === 'undefined') {

                    startScrapesPoller = paScrapeTesterService.startScrapesPoller(response.task_id);

                    mixpanelTracking({
                        actionName: 'FC Technical Successfully Started PA Scrapes'
                    });

                    $scope.testConfigsPromise = startScrapesPoller.promise.then(null, null, function(pollerResponse) {

                        if (pollerResponse.data.ready) {
                            mixpanelTracking({
                                actionName: 'FC Technical Received PA Scrape Results'
                            });

                            pollerResponse = pollerResponse.data.result;
                            startScrapesPoller.stop();

                            // confirm that promise is fulfilled
                            $scope.testConfigsPromise = null;
                            if (pollerResponse.tasks.length === 0) {
                                $scope.startScrapeError = pollerResponse.message;
                            }

                            // store tasks in local storage
                            localStorageService.remove('scrape_tasks');
                            localStorageService.add('scrape_tasks', pollerResponse.tasks);

                            $scope.testConfigs = pollerResponse.tasks;
                            runScrapeTicker(pollerResponse.tasks, pollerResponse.scrape_id, scraperSettings);

                            $location.path('/pa-scrapetester/' + $scope.websiteId + '/' + pollerResponse.scrape_id, false);
                            $scope.scraperSettings.testsuite = pollerResponse.scrape_id;
                            $scope.scraperSettings.scrape_id = pollerResponse.scrape_id;
                            $scope.runningScrapes = false;
                        }
                    });
                }
            },

            function() {
                $scope.runningScrapes = false;
            });
        };

        /**
         * Run Scrape tests.
         * @param  {[type]} scraperSettings [description]
         * @return {[type]}                 [description]
         */
        $scope.runScrapes = function(scraperSettings) {
            $scope.raisedTicket = {};
            initScraperSettings(null, scraperSettings);
            $scope.runningScrapes = true;
            $scope.testConfigsPromise = paScrapeTesterService.run(scraperSettings).then(function(response) {

                mixpanelTracking({
                    actionName: 'FC Technical RunPAScrapes'
                });

                localStorageService.remove('scrape_tasks');
                localStorageService.add('scrape_tasks', response.tasks);
                $scope.testConfigs = response.tasks;
                runScrapeTicker(response.tasks, response.scrape_id, scraperSettings);

                $location.path('/pa-scrapetester/' + $scope.websiteId + '/' + response.scrape_id, false);
                $scope.runningScrapes = false;
            },

            function() {
                $scope.runningScrapes = false;
            });
        };

        /**
         * Show scrape test result
         * @param  {[type]} response [description]
         * @return {[type]}             [description]
         */
        $scope.showScrapeTestResult = function(response) {
            if (typeof response === 'undefined') {
                return false;
            }

            modalService.showScrapeTestResult(response);
        };

        /**
         * Check the boolean flag whether we have the load the scrape tests or not.
         * @return {[type]} [description]
         */
        $scope.$watch('showScrapeTests', function() {
        });

        /**
         * Check the selected scrape test and load all the tests under it.
         * @return {[type]} [description]
         */
        $scope.$watch('routeParams.scrapeTestId', function() {

            if ($route.current.params.scrapeTestId) {
                var params = {scrape_id: $route.current.params.scrapeTestId};
                $scope.testConfigsPromise = $scope.scraperSettingsPromise = pollerService.start({
                    url: 'v0.4/flights/pa_scrapes/past',
                    params: params
                }).then(function(taskId) {
                    // start polling here...
                    var getScrapeTestsPoller = pollerService.poll({
                        url: 'v0.4/flights/pa_scrapes/past/async',
                        taskId: taskId
                    });

                    $scope.testConfigsPromise = $scope.scraperSettingsPromise = getScrapeTestsPoller.promise.then(null, null, function(pollerResponse) {

                        if (pollerResponse.data.ready) {
                            mixpanelTracking({
                                actionName: 'FC Technical Successfully Loaded Scrape Tests'
                            });

                            $scope.scraperSettingsPromise = null;
                            $scope.testConfigsPromise = null;
                            getScrapeTestsPoller.stop();

                            var scraperSettings = pollerResponse.data.result.scrape_test;

                            scraperSettings.datacenter = scraperSettings.datacenter ? scraperSettings.datacenter : 'all';

                            $scope.scraperSettings = scraperSettings;
                            $scope.testConfigs =  pollerResponse.data.result.test_cases;
                            var tasks = [];
                            for (var idx = 0; idx < $scope.testConfigs.length; idx++) {
                                if ($scope.testConfigs[idx].active && $scope.testConfigs[idx].task_id) {
                                    tasks.push($scope.testConfigs[idx]);
                                }
                            }

                            if (tasks.length > 0) {
                                runScrapeTicker(tasks);
                            }

                            $scope.scraperSettings.testsuite =  pollerResponse.data.result.scrape_test.testsuite;
                        }
                    });

                });
            }
        });

        init();
        initScraperSettings();
        runScrapeTicker();

        alertService.reset();
    });

}());
