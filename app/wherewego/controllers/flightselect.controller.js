(function() {

'use strict';

angular.module('fcApp.wherewego')
    .controller('FlightSelectCtrl', function(
        $scope,
        $rootScope,
        $route,
        $routeParams,
        amadeusService) {

        function init() {
            $scope.filter = {
                flights: true,
                hotels: true,
            };

            $scope.flights = {};

            $scope.budget = 3000;

            $scope.departDatePickerOptions = {
                icons: {
                    next: 'glyphicon glyphicon-arrow-right',
                    previous: 'glyphicon glyphicon-arrow-left',
                    up: 'glyphicon glyphicon-arrow-up',
                    down: 'glyphicon glyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };
            $scope.returnDatePickerOptions = {
                icons: {
                    next: 'glyphiconglyphicon-arrow-right',
                    previous: 'glyphiconglyphicon-arrow-left',
                    up: 'glyphiconglyphicon-arrow-up',
                    down: 'glyphiconglyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };
            var params = {
                originplace: $routeParams.origin,
                destinationplace: $routeParams.origin,
            };
            getFlights(params);
        }

        function getFlights(params) {
            $scope.flightsPromise = null;

            $scope.flightsPromise = amadeusService.getFlights(params).then(function(data) {
                $scope.flights.Agents = data.Agents;
                $scope.flights.Carriers = data.Carriers;
                $scope.flights.Currencies = data.Currencies;
                $scope.flights.Itineraries = data.Itineraries;
                $scope.flights.Legs = data.Legs;                
                $scope.flights.Places = data.Places;                
                $scope.flights.Query = data.Query;
                $scope.flights.Segments = data.Segments;                
            },
            function() {
                $scope.flightsError = true;
            });
        }

        $scope.convertToUSD = amadeusService.convertToUSD;
        $scope.getCityName = function(code) {
            var params = {
                code:code
            };
            $scope.locPromise[code] = amadeusService.getLocation(params).then(function(data) {
                $scope.loc[code] = data;
            }, function() {
                $scope.locError = true;
            });
        }

        $scope.updateBudget = function(budgetAmount) {
            console.log('updateBudget');
            console.log(budgetAmount);
            console.log($scope.departDate);
            console.log($scope.returnDate);

            if (budgetAmount && $scope.departDate && $scope.returnDate) {
                var params = {
                    max_price: amadeusService.convertToUSD(budgetAmount, 'THB'),
                    departure_date: $scope.departDate,
                    return_date: $scope.returnDate,
                };

                getFlights(params);
            }
        };

        $scope.getLeg = function(legId, legs) {

            for (var i=0;i < legs.length; i++) {
                if (legs[i].Id == legId) {
                    console.log(legs[i].Id + ' ' + legId);
                    return legs[i]
                }
            }

            return '';
        }

        $scope.getCarrier = function(carrierId, carriers) {

            for (var i=0;i < carriers.length; i++) {
                if (carriers[i].Id == carrierId) {
                    console.log(carriers[i].Id + ' ' + carrierId);
                    return carriers[i]
                }
            }

            return '';
        }        

        $scope.constructParams = function(latitude, longitude) {
            var params = {
                hotels: $scope.filter.hotels,
                checkin: $scope.departDate,
                checkout: $scope.returnDate
            };

            if ($scope.filter.hotels == true) {
                console.log('filter it');
                $.extend(params, {latitude: latitude, longitude: longitude});
            }
            return "#hotel-browser?" + $.param(params);
        };

        init();
    });

}());
