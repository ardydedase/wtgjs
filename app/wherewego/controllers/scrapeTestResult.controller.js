(function() {

'use strict';

angular.module('fcApp.technical')
    .controller('ScrapeTestResultCtrl', function($scope, jacResponse) {
        $scope.jacResponse = jacResponse;
        $scope.jsonFormatterOpen = 3;
        $scope.isOpen = true;
        $scope.open = true;
    });

}());
