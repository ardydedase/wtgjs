(function() {

'use strict';

describe('Controller: ScrapeTesterCtrl', function() {

    var controller;
    var defaultMocks;
    var q;
    var scope;

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('ScrapeTesterCtrl', {
            $scope: scope,
            localStorageService: mocks.localStorageService,
            conversionPixelHealthService: mocks.conversionPixelHealthService,
            authService: mocks.authService,
            testService: mocks.testService,
            pollerService: mocks.pollerService,
            mixpanelService: mocks.mixpanelService,
            scrapeTesterService: mocks.scrapeTesterService
        });
    };

    var noop = function() {};

    var createPromise = function() {
        return function() {
            var dfr = q.defer();
            dfr.resolve({
                status: 200,
                data: []
            });
            return dfr.promise;
        };
    };

    beforeEach(module('fcApp.technical'));

    beforeEach(inject(function($controller, $rootScope, $location, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        scope = $rootScope.$new();
        scope.websites = ['xpsg'];
        scope.test_config = {
            lang: 'en',
            market: 'SG',
            currency: 'SGD'
        };

        scope.sampleTestConfig = {
            active: true,
            adults: '2',
            cabin_class: 'economy',
            children: '1',
            counter: 1,
            created_at: '2015-03-20T18:31:44.598Z',
            currency: 'IDR',
            datacenter: 'sg1',
            depart_date: '2015-04-20',
            doc_type: 'jacquard_test_case',
            from_place: 'CGK',
            infants: '1',
            lang: 'id',
            lastrun: {status: 'successful', date: '2015-03-23T14:13:38.660249'},
            market: 'ID',
            return_date: '2015-04-27',
            task_id: '',
            testsuite: 'xxxxxxxxxxxx',
            to_place: 'KUL',
            type: 'flights',
            uid: '67c2757e18fe',
            website_id: 'nusa'
        };

        defaultMocks = {
            localStorageService: {
                get: function() {
                    var returnData = {
                        user_countries: ['SG'],
                        user_languages: ['en'],
                        user_currency: ['SGD'],
                    };
                    return returnData;
                },

                remove: function() {
                    return 'removed';
                },

                add: function() {
                    return 'added';
                }
            },
            authService: {
                getUserType: function() {
                    return 'King';
                },

                restrictToGroup: function() {
                    return 'restrictToGroup';
                },

                redirectToWebsiteId: function() {
                    return 'redirectToWebsiteId';
                }
            },
            pollerService: {
                start: createPromise(),
                poll: createPromise(),
            },
            scrapeTesterService: {
                run: createPromise(),
                startScrapesPoller: createPromise(),
                start: createPromise()
            },
            testService: {
                cabinClassMap: {},
                isInThePast: {},
                offsetDate: noop,
                getSkippyDeeplink: createPromise(),
                redirectToDeeplink: noop
            },
            jiraService: {
                jira: createPromise()
            },
            mixpanelService: {
                track: noop
            }
        };
    }));

    it('should have raiseJira function', function() {
        createController();

        expect(scope.raiseJira).toBeDefined();
    });

    it('should be able to raise Jira tickets', function() {
        createController();
        var index = 1;

        scope.raiseJira(scope.sampleTestConfig, index);
        expect(scope.raisingJira[index]).toBe(true);
    });

    it('should have a default scraper settings', function() {
        createController();
        expect(scope.scraperSettings).toBeDefined();
    });

    it('should update scraperSettings when running scrapes', function() {

        beforeEach(function() {
            var dfr = q.defer();
            spyOn(defaultMocks.scrapeTesterService, 'run').and.returnValue(dfr.promise);

            dfr.resolve([]);
        });

        createController();

        var scraperSettings = {
            request_count: 5,
            user_currency: ['SGD'],
            user_languages: ['en'],
            user_countries: ['SG'],
        };

        scope.runScrapes(scraperSettings);

        expect(scope.scraperSettings.request_count).toEqual(5);
        expect(scope.scraperSettings.user_currency).toEqual(['SGD']);
        expect(scope.scraperSettings.user_languages).toEqual(['en']);
        expect(scope.scraperSettings.user_countries).toEqual(['SG']);

    });

    it('should show port input when using localhost datacenter', function() {
        createController();

        scope.scraperSettings.datacenter = 'localhost';
        scope.change();

        expect(scope.showPortInput).toEqual(true);
    });

    it('should check if the form has incomplete information', function() {
        createController();

        var scraperSettings = {
            request_count: 5,
            user_currency: ['SGD'],
            user_languages: ['en'],
            user_countries: ['SG'],
        };

        expect(scope.formIncomplete(scraperSettings)).toEqual(true);
    });

    it('should be able to start scrapes', function() {
        createController();

        var scraperSettings = {};
        scope.startScrapes(scraperSettings);
        expect(scope.runningScrapes).toEqual(true);
    });

    it('should be able to rerun scrapes', function() {
        createController();
        var sampleTestConfig = {};
        scope.rerunScrapes(sampleTestConfig);

        expect(scope.scraperSettings.request_count).toEqual(2);
    });

    it('should now show scrape result if jacquard response is undefined', function() {
        createController();

        expect(scope.showScrapeTestResult()).toEqual(false);
    });

    it('should now show scrape result if jacquard response is undefined', function() {
        createController();

        expect(scope.showScrapeTestResult()).toEqual(false);
    });

    // TODO trigger website changed? - $scope.$on('websiteIdChanged', updateConfig);

});

}());
