(function() {

'use strict';

angular.module('fcApp.wherewego')
    .controller('HotelBrowserCtrl', function(
        $scope,
        $rootScope,
        $route,
        $routeParams,
        amadeusService) {

        function init() {
            $scope.filter = {
                flights: true,
                hotels: true,
            };

            $scope.budget = 200;

            $scope.departDatePickerOptions = {
                icons: {
                    next: 'glyphicon glyphicon-arrow-right',
                    previous: 'glyphicon glyphicon-arrow-left',
                    up: 'glyphicon glyphicon-arrow-up',
                    down: 'glyphicon glyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };
            $scope.returnDatePickerOptions = {
                icons: {
                    next: 'glyphiconglyphicon-arrow-right',
                    previous: 'glyphiconglyphicon-arrow-left',
                    up: 'glyphiconglyphicon-arrow-up',
                    down: 'glyphiconglyphicon-arrow-down'
                },
                format: 'YYYY-MM-DD',
                minDate: Date()
            };

            var params = {
                latitude: $routeParams.latitude,
                longitude: $routeParams.longitude,
                check_in: $scope.checkIn,
                check_out: $scope.checkOut
            };

            getHotels(params);            
        }

        function getHotels(params) {
            console.log('get hotels');
            $scope.hotelsPromise = null;

            $scope.hotelsPromise = amadeusService.cheapestRoomsRadius(params).then(function(data) {
                $scope.hotels = data;
            },
            function() {
                $scope.hotelsError = true;
            });
        }        


        $scope.getCityName = function(code) {
            var params = {
                code:code
            };
            $scope.locPromise[code] = amadeusService.getLocation(params).then(function(data) {
                $scope.loc[code] = data;
            }, function() {
                $scope.locError = true;
            });
        }

        $scope.updateBudget = function(budgetAmount) {
            console.log('updateBudget');
            console.log(budgetAmount);
            console.log($scope.checkIn);
            console.log($scope.checkIn);

            if (budgetAmount && $scope.checkIn && $scope.checkOut) {
                var params = {
                    max_rate: budgetAmount,
                    check_in: $scope.checkIn,
                    check_out: $scope.checkOut,
                };

                getHotels(params);
            }
        };

        $scope.constructParams = function(latitude, longitude) {
            var params = {
                hotels: $scope.filter.hotels
            };

            // if ($scope.filter.hotels == true) {
            //     $.extend(params, {latitude: latitude, longitude: longitude});
            // }
            return "#browser?" + $.param(params);
        };

        $scope.getImage = function(src) {
          if (src !== "") {
            return src;  
          } else {
           return "//:0"; 
          }
        };        

        init();
    });

}());
