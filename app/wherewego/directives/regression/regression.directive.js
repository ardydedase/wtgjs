(function() {

'use strict';

angular.module('fcApp.technical')
.directive('fcRegression', function() {
    return {
        restrict: 'E',
        templateUrl: 'app/technical/directives/regression/regression-tests.html',
        scope: {
            regressionTest: '=testConfigs',
            scrapeSummary: '=scrapeSummary',
            testConfigsPromise: '=testConfigsPromise',
            resultFilter: '=resultFilter'
        },
        controller: function($scope) {
            $scope.scrapeTester = true;
            $scope.testLoadingMessage = 'Loading Tests..';
        }
    };
});

}());
