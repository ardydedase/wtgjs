(function() {

'use strict';

angular.module('fcApp.wherewego', [
    'appConfig',
    'LocalStorageModule',
    'angulartics',
    'angulartics.google.analytics',
    'angularMoment',
    'ui.bootstrap',
    'ui.layout',
    'ui.multiselect',
    'ngPrettyJson',
    'jsonFormatter',
    'ui.ace',
    'pageslide-directive',
    'fcApp.common',
    'infinite-scroll'
]);

angular.module('fcApp.wherewego')
    .constant('vkbeautify', window.vkbeautify)
    .constant('URI', window.URI);
}());

