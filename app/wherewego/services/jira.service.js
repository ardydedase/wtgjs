(function() {

'use strict';

/**
 * @ngdoc service
 * @name atdJsApp.jiraService
 * @description
 * # jiraService
 * Factory in the atdJsApp.
 */
angular.module('fcApp.technical')
    .factory('jiraService', function(
        $http,
        config,
        localStorageService) {

        return {
            jira: function(testConfig) {
                return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/jira/',
                    data: JSON.stringify({
                        test_config: testConfig,
                        website_id: localStorageService.get('selectedwebsite')
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response;
                });
            }
        };
    });

}());
