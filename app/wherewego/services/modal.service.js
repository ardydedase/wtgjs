(function() {

'use strict';

angular.module('fcApp.technical').factory('modalService', function($modal) {
    // Public API here
    return {

        /**
         * Website configuration dialog
         * @param  {[type]} web_config [description]
         * @return {[type]}            [description]
         */
        showStatic: function(template) {

            var modalInstance = $modal.open({
                templateUrl: 'app/technical/templates/modal/' + template,
                controller: 'StaticModalCtrl',
                size: 'xl',
                resolve: {}
            });

            return modalInstance;
        },

        /**
         * Website locale dialog
         * @param  {[type]} web_config [description]
         * @return {[type]}            [description]
         */
        showLocale: function(testConfig) {
            var modalInstance = $modal.open({
                templateUrl: 'app/technical/templates/modal/locale-box.html',
                controller: 'LocaleCtrl',
                resolve: {
                    testConfig: function() {
                        return testConfig;
                    }
                }
            });

            return modalInstance;
        },

        showScrapeTestResult: function(jacResponse) {
            var modalInstance = $modal.open({
                templateUrl: 'app/technical/templates/modal/scrape-test-result.html',
                controller: 'ScrapeTestResultCtrl',
                size: 'lg',
                windowClass: 'scrape_test_results',
                resolve: {
                    jacResponse: function() {
                        return jacResponse;
                    }
                }
            });

            return modalInstance;
        }
    };
});

}());
