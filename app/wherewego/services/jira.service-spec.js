(function() {

'use strict';

describe('Service: jiraService', function() {

    beforeEach(module('fcApp.technical'));

    var jiraService;

    beforeEach(inject(function(_jiraService_) {
        jiraService = _jiraService_;
    }));

    it('should do something', function() {
        expect(!!jiraService).toBe(true);
    });

    it('should succesfully create a test jira ticket and return 201', function() {
        var summary = 'jira service view test';
        var description = 'jira service frontend view test';

        jiraService.jira(summary, description).then(function(response) {
            expect(response.status_code).toBe(201);
        });

    });
});

}());
