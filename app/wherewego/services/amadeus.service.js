(function() {

'use strict';

angular.module('fcApp.wherewego').factory('amadeusService', function(
    $http,
    config
    ) {

    var baseUrl = config.API_URL + config.API_BASE;
    var currencyConversionUSD = {"AUD":1.3765,"BGN":1.7879,"BRL":3.3336,"CAD":1.3091,"CHF":0.961,"CNY":6.2088,"CZK":24.721,"DKK":6.821,"GBP":0.6462,"HKD":7.7514,"HRK":6.9321,"HUF":284.48,"IDR":13476.84,"ILS":3.8327,"INR":64.12,"JPY":124.05,"KRW":1174.12,"MXN":16.279,"MYR":3.8252,"NOK":8.2233,"NZD":1.5239,"PHP":45.561,"PLN":3.7742,"RON":4.0416,"RUB":58.432,"SEK":8.6171,"SGD":1.3748,"THB":34.951,"TRY":2.7503,"ZAR":12.633,"EUR":0.9142}

    function whenToGo(options) {

        return $http({
            method: 'get',
            url: baseUrl + 'where-to-go',
            params: options
        }).then(function(res) {
            return res.data;
        });
    }

    function convertToUSD(amount, currency) {
        var convertedAmount = Math.floor(amount / currencyConversionUSD[currency]);
        console.log('convertedAmount: ' + convertedAmount);
        return convertedAmount;
    }

    function getLocation(options) {
        return $http({
            method: 'get',
            url: baseUrl + 'get-location',
            params: options,
            ignoreLoadingBar: true
        }).then(function(res) {
            return res.data;
        });
    }

    function cheapestRoomsRadius(options) {

        return $http({
            method: 'get',
            url: baseUrl + 'cheapest-rooms-radius',
            params: options
        }).then(function(res) {
            return res.data;
        });
    }


    function getFlights(options) {

        return $http({
            method: 'get',
            url: baseUrl + 'get-flights',
            params: options
        }).then(function(res) {
            return res.data;
        });
    }    

    return {
        whenToGo: whenToGo,
        convertToUSD: convertToUSD,
        getLocation: getLocation,
        getFlights: getFlights,
        cheapestRoomsRadius: cheapestRoomsRadius
    };
});

}());
