(function() {

'use strict';

angular.module('fcApp.technical').factory('quoteService', function(
    $analytics,
    $location,
    alertService,
    vkbeautify,
    localStorageService) {

    var serviceObj = {};

    var inSelectedCarriers = function(selectedCarriers, carrierName) {
        var selected = false;

        selectedCarriers.forEach(function(selectedCarrier) {
            if (selectedCarrier.name === carrierName) {
                selected = true;
            }
        });

        return selected;
    };

    serviceObj = {
        /**
         * [getAirlineID description]
         * @param  {[type]} marketingCarrierId [description]
         * @return {[type]}                    [description]
         */
        getAirlineID: function(marketingCarrierId) {
            if (marketingCarrierId) {
                return marketingCarrierId.split(':')[1].trim();
            }

            return null;
        },

        /**
         * Construct Itinerary value
         * Format: {segment_mode}|{marketing_carrier_numericID}|{flight_number}|{dep_time}|{arr_time}
         *
         * flight|-32677|259|16292|2015-03-27T01:05|10413|2015-03-27T08:15
         * Legs are comma separated, segments on a leg are semi-colon separated.
         *
         * @param  {[object]} quote [description]
         * @return {[string]}       [description]
         */
        construcDeeplinkItinerary: function(quote) {
            var legs = quote.legs;
            var segmentMode = 'flight';
            var segmentArray = [];
            var legSetArray = [];

            for (var i = 0; i < legs.length; i++) {
                var segmentSetArray = [];
                for (var j = 0; j < legs[i].segments.length; j++) {
                    segmentArray = [
                        segmentMode,
                        legs[i].segments[j].marketing_carrier,
                        legs[i].segments[j].marketing_segment_code,
                        legs[i].segments[j].from_place,
                        legs[i].segments[j].dep_time,
                        legs[i].segments[j].to_place,
                        legs[i].segments[j].arr_time
                    ];

                    segmentSetArray.push(segmentArray.join('|'));
                }

                legSetArray.push(segmentSetArray.join(';'));
            }

            return legSetArray.join(',');
        },

        /**
         * Check if it's a valid jacquard response
         * @return {Boolean} [description]
         */
        isValidJacquardResponse: function(answers) {
            try {
                for (var i = 0; i < answers.length; i++) {
                    for (var j = 0; j < answers[i].journey.quotes.length; j++) {
                        for (var k = 0; k < answers[i].journey.quotes[j].legs.length; k++) {
                            for (var l = 0; l < answers[i].journey.quotes[j].legs[k].length; l++) {

                                // we're checking to make sure the formatting is good.
                                return true;
                            }
                        }
                    }
                }

                return true;
            } catch (e) {
                console.log(e);
                return false;
            }
        },

        /**
         * Show the carrier based on the selected carriers list
         * @param  {[type]} selectedCarriers [description]
         * @param  {[type]} quote             [description]
         * @return {[type]}                   [description]
         */
        showByCarrier: function(selectedCarriers, quote) {
            var show = true;

            for (var i = 0; i < quote.legs[0].segments.length; i++) {
                if (quote.legs[0].segments[i].market_carrier_name && !(inSelectedCarriers(selectedCarriers, quote.legs[0].segments[i].market_carrier_name))) {
                    show = false;
                }
            }

            try {
                for (var j = 0; j < quote.legs[0].segments.length; j++) {
                    if (quote.legs[0].segments[j].market_carrier_name && !(inSelectedCarriers(selectedCarriers, quote.legs[0].segments[j].market_carrier_name))) {
                        show = false;
                    }
                }
            } catch (e) {
                // do nothing

                console.log('This is a one-way flight', e);
            }

            return show;
        },

        /**
         * Add carrier to the filter dropdown box list
         * @param {[type]} testResult       [description]
         * @param {[type]} carrier_name      [description]
         * @param {[type]} marketing_id      [description]
         * @param {[type]} selectedCarriers [description]
         */
        addToCarrierFilter: function(testResult, carrierName, marketingId, selectedCarriers) {
            var carrierExists = false;

            (testResult.carriers).forEach(function(carrier) {
                // console.log('carrier.name: ' + carrier.name);
                if (carrier.name === carrierName) {
                    carrierExists = true;
                }
            });

            if (!carrierExists && carrierName) {
                testResult.carriers.push({
                    marketing_id: marketingId,
                    name: carrierName
                });
                selectedCarriers.push({
                    marketing_id: marketingId,
                    name: carrierName
                });
            }
        },
        /**
         * Messaging for the filtered quotes.
         * @param  {[type]} testResponse [description]
         * @param  {[type]} quote         [description]
         * @return {[type]}               [description]
         */
        filterReasons: function(testResponse, quote) {
            if (!testResponse.filtering_info.route_enabled) {
                return 'Route is disabled';
            }

            var reasons = [];

            if (testResponse.filtering_info.all_quotes_filtered) {
                reasons.push('all quotes filtered by ');
            }

            if (quote.passed_commercial_filters !== 'true') {
                reasons.push('Jacquard commercial filters');
            } else if (quote.passed_quote_filters !== 'true') {
                reasons.push('quote filters');
            }

            return reasons.join(', ');
        },

        /**
         * Determine whether the quote is filtered
         * @param  {[type]}  quote [description]
         * @return {Boolean}       [description]
         */
        isFiltered: function(quote) {
            if (typeof (quote.passed_commercial_filters) !== 'undefined' && quote.passed_commercial_filters === 'false') {
                return true;
            } else if (typeof (quote.passed_quote_filters) !== 'undefined' && quote.passed_quote_filters === 'false') {
                return true;
            } else {
                return '';
            }
        },

        countFiltered: function(quotes) {
            var numFiltered = 0;
            for (var i in quotes) {
                if (serviceObj.isFiltered(quotes[i])) {
                    numFiltered++;
                }
            }

            return numFiltered;
        },

        /**
         * Modify the Budget quote structure such that we can
         * @param  {[type]} testResult [description]
         * @param  {[type]} testConfig  [description]
         * @return {[type]}             [description]
         */
        modifyBudgetQuotes: function(testResult, testConfig) {
            var modifiedTestResult = {
                answers: [{
                    journey: {
                        quotes: []
                    }
                }]
            };

            var getBudgetTotal = function(quote1, quote2) {
                var total = {
                    total: 0
                };

                // get the total for each leg if available
                total.total += (quote1.price.total.total) ? quote1.price.total.total : 0;
                try {
                    total.total += (quote2.price.total.total) ? quote2.price.total.total : 0;
                } catch (e) {
                    console.log('Can\'t retrieve the inbound total price', e);
                }

                return total;
            };

            var isRoundtrip = false;
            for (var i = 0; i < testResult.answers.length; i++) {
                // console.log("testConfig.depart_date: " + testConfig.depart_date);
                // console.log("journey_leg_date: " + testResult.answers[i].journey.journey_legs[0].date);

                if (testConfig.depart_date === testResult.answers[i].journey.journey_legs[0].date) {
                    var outboundQuotes = testResult.answers[i].journey.quotes;
                }

                if (testConfig.return_date === testResult.answers[i].journey.journey_legs[0].date) {
                    var inboundQuotes = testResult.answers[i].journey.quotes;
                    isRoundtrip = true;
                }
            }

            if (isRoundtrip) {
                // roundtrip
                outboundQuotes.forEach(function(quote1) {
                    inboundQuotes.forEach(function(quote2) {
                        var modifiedQuote = {
                            legs: {},
                            passed_commercial_filters: '',
                            passed_quote_filters: '',
                            price: {},
                            routedate_uid: ''
                        };

                        var price = {
                            currency: quote1.price.currency,
                            total: getBudgetTotal(quote1, quote2)
                        };

                        // var legs = {
                        //     leg_1: quote1.legs[0],
                        //     leg_2: quote2.legs[0]
                        // };

                        var legs = [];
                        legs.push(quote1.legs[0]);
                        legs.push(quote2.legs[0]);

                        modifiedQuote.legs = legs;
                        modifiedQuote.price = price;
                        modifiedQuote.passed_commercial_filters = quote1.passed_commercial_filters;
                        modifiedQuote.filter_message = quote1.filter_message;
                        modifiedQuote.passed_quote_filters = quote1.passed_quote_filters;
                        modifiedQuote.routedate_uid = quote1.routedate_uid;

                        modifiedTestResult.answers[0].journey.quotes.push(modifiedQuote);

                    });
                });
            } else {
                // for one-way

                for (var j = 0; j < outboundQuotes.length; j++) {
                    var modifiedQuote = {
                        legs: {},
                        passed_commercial_filters: '',
                        passed_quote_filters: '',
                        price: {},
                        routedate_uid: ''
                    };

                    var quote1 = outboundQuotes[i];

                    var legs = [];
                    legs.push(quote1.legs[0]);

                    modifiedQuote.legs = {
                        leg_1: legs
                    };

                    modifiedQuote.price = {
                        currency: quote1.price.currency,
                        total: getBudgetTotal(quote1, {})
                    };

                    modifiedQuote.passed_commercial_filters = quote1.passed_commercial_filters;
                    modifiedQuote.filter_message = quote1.filter_message;
                    modifiedQuote.passed_quote_filters = quote1.passed_quote_filters;
                    modifiedQuote.routedate_uid = quote1.routedate_uid;

                    modifiedTestResult.answers[0].journey.quotes.push(modifiedQuote);
                }

            }

            return modifiedTestResult;
        },

        isJSON: function(str) {
            try {
                var json = $.parseJSON(str);

                if (json) {
                    return true;
                }
            } catch (e) {
                return false;
            }
        },

        isXML: function(str) {
            try {
                var xml = $.parseXML(str);

                if (xml) {
                    return true;
                }
            } catch (e) {
                return false;
            }
        },

        /**
         * [formatRawResponse description]
         * @param  {[type]} response [description]
         * @return {[type]}          [description]
         */
        formatRawResponse: function(response) {
            if (serviceObj.isJSON(response)) {
                return vkbeautify.json(response);
            } else {
                return vkbeautify.xml(response);
            }
        },

        /**
         * [handleResponseErrors description]
         * @param  {[type]} jacResponse [description]
         * @return {[type]}              [description]
         */
        handleResponseErrors: function(jacResponse) {
            if (typeof jacResponse === 'undefined' || $.isEmptyObject(jacResponse)) {
                return false;
            }

            var errorMessage = '';
            console.log(jacResponse);
            try {
                if ('error_details' in jacResponse && 'exception_text' in jacResponse.error_details) {
                    errorMessage = 'There\'s a problem retrieving the quotes. Server responded with: ""' + jacResponse.error_details.exception_name + ' - ' + jacResponse.error_details.exception_text;

                    $analytics.eventTrack('jacNonFatal-' + $location.path(), {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username
                    });
                } else if (jacResponse.jac_out.hasOwnProperty('filtering_info') &&
                    !jacResponse.jac_out.filtering_info.route_enabled) {
                    errorMessage = 'This route is disabled.';

                    $analytics.eventTrack('routeIsDisabled-' + $location.path(), {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username
                    });
                } else if (jacResponse.jac_out.answer[0].journey.quotes.length === 0) {
                    errorMessage = 'No results found.';

                    $analytics.eventTrack('jacNoQuotes-' + $location.path(), {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username
                    });
                }

            } catch (e) {
                errorMessage = 'Empty results found.';
                $analytics.eventTrack('unknownError-' + $location.path(), {
                    category: localStorageService.get('selectedwebsite'),
                    label: localStorageService.get('account').username
                });
            }

            if (errorMessage && jacResponse !== {}) {
                alertService.reset();
                alertService.add('danger', errorMessage, 'testpanel');
            }
        },

        /**
         * [formatResults description]
         * @param  {[type]} jacResponse [description]
         * @return {[type]}              [description]
         */
        formatResults: function(jacResponse, testConfig) {
            // alertService.reset();
            var testResult = {};

            try {
                if (jacResponse.error_message) {

                    $analytics.eventTrack('jacquardError-' + $location.path(), {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username
                    });

                    alertService.add('danger', 'Something went wrong. Server responded with: ' + jacResponse.error_message, 'testpanel');
                    testResult.answers = null;
                    return testResult;
                }
            } catch (e) {
                console.log('Can\'t find error message');
            }

            // handle the Raw Request and response
            var rawRequestReponse = {
                request: '',
                response: '',
                response_type: ''
            };

            var rawRequestReponses = [];

            try {
                for (var i = 0; i < jacResponse.provider_out.external.length; i++) {

                    try {
                        rawRequestReponse = {
                            request: jacResponse.provider_out.external[i].request,
                            response: serviceObj.formatRawResponse(jacResponse.provider_out.external[i].response.body),
                            response_type: serviceObj.isJSON(jacResponse.provider_out.external[i].response.body) ? 'javascript' : 'xml'
                        };

                    } catch (e) {
                        rawRequestReponse = {
                            request: '',
                            response: '',
                            response_type: ''
                        };
                    }

                    // insert the raw request and response object into the array
                    rawRequestReponses.push(rawRequestReponse);
                }
            } catch (e) {
                console.log('caught exception');
                console.log(e);
            }

            // assign to testResult object
            testResult.raw_request_responses = rawRequestReponses;

            // always try to get the repro links
            try {
                testResult.repro_links = jacResponse.repro_links;
                testResult.request_url_debug = jacResponse.request_url_debug;
                testResult.deeplink = jacResponse.deeplink;
            } catch (e) {
                testResult.repro_links = {};
                testResult.request_url_debug = {};
                testResult.deeplink = {};
            }

            try {
                if (jacResponse.jac_out.answer.length > 0) {
                    testResult.success = true;
                    testResult.carriers = [];
                    testResult.filtering_info = jacResponse.jac_out.filtering_info;
                    testResult.answers = jacResponse.jac_out.answer;

                    try {
                        testResult.result_summary = jacResponse.result_summary;
                    } catch (e) {
                        testResult.result_summary = {};
                    }

                    // testResult.result_datetime = jacResponse.jac_out.result.result_datetime;
                    testResult.created_at = testConfig.created_at;
                    testResult.lastrun = testConfig.lastrun;

                    // modify the quotes structure if it's budget
                    if (localStorageService.get('webconfig').pricing_type === 'Budget') {
                        var modifiedBudgetQuotes = serviceObj.modifyBudgetQuotes(testResult, testConfig);
                        $.extend(testResult, modifiedBudgetQuotes);
                    }

                    $analytics.eventTrack('jacRequestSuccess-' + testResult.filtering_info + '-' + $location.path(), {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username
                    });

                } else {
                    // no quotes or no results
                    testResult.success = false;
                    testResult.answers = null;
                    testResult.filtering_info = jacResponse.jac_out.filtering_info;
                    testResult.created_at = testConfig.created_at;
                    serviceObj.handleResponseErrors(jacResponse);
                }
            } catch (e) {
                // one of the json elements is missing
                testResult.success = false;

                try {
                    testResult.answers = jacResponse.jac_out.answer;
                } catch (e) {
                    testResult.answers = null;
                }

                try {
                    testResult.filtering_info = jacResponse.jac_out.filtering_info;
                } catch (e) {
                    console.log(e);
                }

                serviceObj.handleResponseErrors(jacResponse);
            }

            return testResult;
        }

    };

    return serviceObj;
});

}());
