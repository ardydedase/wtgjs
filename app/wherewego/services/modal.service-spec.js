(function() {

'use strict';

describe('Service: modalService', function() {

    beforeEach(module('fcApp.technical'));

    var modalService;

    beforeEach(inject(function(_modalService_) {
        modalService = _modalService_;
    }));

    it('should do something', function() {
        expect(!!modalService).toBe(true);
    });
});

}());
