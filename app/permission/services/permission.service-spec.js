(function() {

'use strict';

describe('Service: permissionService', function() {

    var config;
    var permissionService;
    var httpBackend;
    var baseUrl;

    beforeEach(module('fcApp.permission'));

    beforeEach(inject(function($httpBackend, _config_, _permissionService_) {
        config = _config_;
        httpBackend = $httpBackend;
        permissionService = _permissionService_;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#get', function() {
        it('should be defined', function() {
            expect(permissionService.get).toBeDefined();
        });

        it('should GET /v0.4/flights/admins/permissions/users/', function() {
            var expectedResponse = [{
                username: 'hello'
            }];

            var url = baseUrl + '/v0.4/flights/admins/permissions/users/';

            httpBackend.whenGET(url).respond(expectedResponse);

            permissionService.get().then(function(response) {
                expect(response.data).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });

        it('should GET /v0.4/flights/admins/permissions/users/ with username parameter', function() {
            var expectedResponse = [{
                username: 'hello'
            }];

            var url = baseUrl + '/v0.4/flights/admins/permissions/users/?username=hello';

            httpBackend.whenGET(url).respond(expectedResponse);

            permissionService.get('hello').then(function(response) {
                expect(response.data).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });
    });

    describe('#post', function() {
        it('should be defined', function() {
            expect(permissionService.post).toBeDefined();
        });

        it('should POST /v0.4/flights/admins/permissions/users/', function() {
            var expectedResponse = 'ok';

            var url = baseUrl + '/v0.4/flights/admins/permissions/users/';
            var data = {
                username: 'hello'
            };

            httpBackend.expectPOST(url, JSON.stringify(data)).respond(204, expectedResponse);

            permissionService.post(data).then(function(response) {
                expect(response.status).toEqual(204);
                expect(response.data).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });
    });
});

}());
