(function() {

'use strict';

describe('Service: userService', function() {

    var config;
    var userService;
    var httpBackend;
    var baseUrl;

    beforeEach(module('fcApp.permission'));

    beforeEach(inject(function($httpBackend, _config_, _userService_) {
        config = _config_;
        httpBackend = $httpBackend;
        userService = _userService_;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#get', function() {
        it('should be defined', function() {
            expect(userService.get).toBeDefined();
        });

        it('should GET /v0.4/flights/admins/users', function() {
            var expectedResponse = [{
                username: 'hello'
            }];

            var url = baseUrl + '/v0.4/flights/admins/users';

            httpBackend.whenGET(url).respond(expectedResponse);

            userService.get().then(function(response) {
                expect(response.data).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });

        it('should GET /v0.4/flights/admins/users with username parameter', function() {
            var expectedResponse = [{
                username: 'hello'
            }];

            var url = baseUrl + '/v0.4/flights/admins/users?username=hello';

            httpBackend.whenGET(url).respond(expectedResponse);

            userService.get('hello').then(function(response) {
                expect(response.data).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });
    });

    describe('#post', function() {
        it('should be defined', function() {
            expect(userService.post).toBeDefined();
        });

        it('should POST /v0.4/flights/admins/users', function() {
            var expectedResponse = 'ok';

            var url = baseUrl + '/v0.4/flights/admins/users';
            var data = {
                username: 'hello'
            };

            httpBackend.expectPOST(url, JSON.stringify(data)).respond(204, expectedResponse);

            userService.post(data).then(function(response) {
                expect(response.status).toEqual(204);
                expect(response.data).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });
    });
});

}());
