(function() {

'use strict';

angular.module('fcApp.permission').factory('permissionService', function(
    $http,
    config
    ) {

    var baseUrl = config.API_URL + config.API_BASE + '/v0.4/flights/admins/permissions/users/';
    var headers = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    function get(username) {
        var url = baseUrl;

        if (username) {
            url += '?username=' + username;
        }

        return $http.get(url);
    }

    function post(data) {
        return $http.post(baseUrl, JSON.stringify(data), headers);
    }

    return {
        get: get,
        post: post
    };
});

}());
