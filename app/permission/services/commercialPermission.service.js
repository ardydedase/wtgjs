(function() {

'use strict';

angular.module('fcApp.permission').factory('commercialPermissionService', function(
    $http,
    alertService,
    localStorageService,
    authService,
    config) {

    var serviceObj = {};

    serviceObj = {
        getWebsitePartners: function(websiteId) {
            return $http.get(config.API_URL + config.API_BASE + '/com_permission/allowed/partners', {
                params: { website_id: websiteId }
            }).then(function(response) {
                // PE-791
                if (websiteId === 'a001') {
                    response.data = ['Fake OTA.com'];
                }

                // --PE-791
                return response.data;
            });
        },

        getFullPermissionList: function() {
            return $http.get(config.API_URL + config.API_BASE + '/com_permission/summary')
                .then(function(response) {
                    return response.data;
                });
        }
    };

    return serviceObj;
});

}());
