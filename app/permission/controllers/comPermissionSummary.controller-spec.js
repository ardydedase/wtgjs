(function() {

'use strict';

describe('Controller: ComPermissionSummaryCtrl', function() {

    beforeEach(module('fcApp.permission'));

    var mockPermissionListData;
    var defaultMocks;
    var controller;
    var timeout;
    var httpBackend;
    var scope;
    var getCacheStatusPromise;
    var getComDataPromise;
    var commercialPermissionServicePromise;

    beforeEach(inject(function($controller, $rootScope, $q, $timeout, $httpBackend) {
        scope = $rootScope.$new();
        controller = $controller;
        timeout = $timeout;
        httpBackend = $httpBackend;

        defaultMocks = {
            dataService: {
                getCacheStatus: function() {
                    getCacheStatusPromise = $q.defer();
                    return getCacheStatusPromise.promise;
                },

                getComData: function() {
                    getComDataPromise = $q.defer();
                    return getComDataPromise.promise;
                }
            },
            commercialPermissionService: {
                getFullPermissionList: function() {
                    commercialPermissionServicePromise = $q.defer();
                    commercialPermissionServicePromise.resolve(mockPermissionListData);
                    return commercialPermissionServicePromise.promise;
                }
            },
            authService: {
                restrictToGroup: function() {}
            },
            mixpanelService: {
                track: function() {}
            }
        };

        mockPermissionListData = [{
            partner_name: 'Asia',
            status: 'none_deployed',
            website_ids: [{
                id: 'jpan'
            }, {
                id: 'npal'
            }]
        }, {
            partner_name: 'Middle East',
            status: 'none_deployed',
            website_ids: [{
                id: 'oman'
            }]
        }, {
            partner_name: 'Europe',
            status: 'some_deployed',
            website_ids: [{
                id: 'germ'
            }, {
                id: 'esto'
            }]
        }, {
            partner_name: 'America',
            status: 'all_deployed',
            website_ids: [{
                id: 'mexi'
            }, {
                id: 'nica'
            }, {
                id: 'braz'
            }]
        }];
    }));

    function createController(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('ComPermissionSummaryCtrl', {
            $scope: scope,
            commercialPermissionService: mocks.commercialPermissionService,
            dataService: mocks.dataService,
            authService: mocks.authService,
            mixpanelService: mocks.mixpanelService,
            _: window._
        });
    }

    it('should initialize defaults', function() {
        createController();

        expect(scope.websiteIdList).toBeDefined();
        expect(scope.partnerList).toBeDefined();
        expect(scope.filter).toBeDefined();
    });

    it('should call authService.restrictToGroup', function() {
        spyOn(defaultMocks.authService, 'restrictToGroup');

        createController();

        expect(defaultMocks.authService.restrictToGroup).toHaveBeenCalledWith('Skyscanner');
    });

    describe('init', function() {
        it('should call commercialPermissionService to retrieve full permission list', function() {
            spyOn(defaultMocks.commercialPermissionService, 'getFullPermissionList').and.callThrough();

            createController();

            expect(defaultMocks.commercialPermissionService.getFullPermissionList).toHaveBeenCalled();
        });

        it('should set websiteIds as a dictionary containing all website ids', function() {
            var expectedDictionary = {
                jpan: {
                    partner: 'Asia'
                },
                npal: {
                    partner: 'Asia'
                },
                oman: {
                    partner: 'Middle East'
                },
                germ: {
                    partner: 'Europe'
                },
                esto: {
                    partner: 'Europe'
                },
                mexi: {
                    partner: 'America'
                },
                nica: {
                    partner: 'America'
                },
                braz: {
                    partner: 'America'
                }
            };

            createController();
            scope.$digest();

            expect(scope.websiteIdList).toEqual(expectedDictionary);
        });

        it('should calculate the totals for each permission status', function() {
            createController();

            commercialPermissionServicePromise.resolve(mockPermissionListData);
            scope.$digest();

            expect(scope.statusSummary.all_deployed.length).toBe(1);
            expect(scope.statusSummary.some_deployed.length).toBe(1);
            expect(scope.statusSummary.none_deployed.length).toBe(2);
        });

        it('should have an "ALL" status to tally all total statuses', function() {
            createController();

            commercialPermissionServicePromise.resolve(mockPermissionListData);
            scope.$digest();

            expect(scope.statusSummary.all).toBeDefined();
            expect(scope.statusSummary.all.length).toBe(4);
        });
    });

    describe('#filterList', function() {
        it('should filter number of partners based on selected status', function() {
            createController();
            scope.$digest();

            scope.filterList('none_deployed');
            expect(scope.partnerList.length).toBe(2);

            scope.filterList('all_deployed');
            expect(scope.partnerList.length).toBe(1);

            scope.filterList('some_deployed');
            expect(scope.partnerList.length).toBe(1);

            scope.filterList('all');
            expect(scope.partnerList.length).toBe(4);
        });

        it('should clear filter.partner when filterList is called', function() {
            createController();

            scope.filterList('none_deployed');
            expect(scope.filter.partner).toEqual('');
        });
    });

    describe('#searchPartner', function() {
        it('should filter partnerList based on websiteId', function() {
            createController();

            scope.filter.partner = 'npal';
            scope.searchPartner();

            timeout.flush();
            expect(scope.partnerList.length).toBe(1);
            expect(scope.partnerList[0].partner_name).toEqual('Asia');
        });

        it('should filter partnerList based on partner\'s name', function() {
            createController();

            scope.filter.partner = 'as';
            scope.searchPartner();

            timeout.flush();
            expect(scope.partnerList.length).toBe(2);
            expect(scope.partnerList[0].partner_name).toEqual('Asia');
            expect(scope.partnerList[1].partner_name).toEqual('Middle East');
        });

        it('should filter partner names on lowercase term', function() {
            createController();

            scope.filter.partner = 'AMERICA';
            scope.searchPartner();

            timeout.flush();
            expect(scope.partnerList.length).toBe(1);
        });

        it('should set partnerList to empty array if filter.partner is empty string', function() {
            createController();
            scope.$digest();

            scope.filterList('none_deployed');
            expect(scope.partnerList.length).toBe(2);

            scope.filter.partner = '';
            scope.searchPartner();
            expect(scope.partnerList.length).toBe(0);
        });
    });

    describe('#getProgressClassName', function() {
        it('should return "danger" if progress is less than 50', function() {
            createController();

            expect(scope.getProgressClassName(45)).toEqual('danger');
        });

        it('should return "warning" if progress is less than 80', function() {
            createController();

            expect(scope.getProgressClassName(79)).toEqual('warning');
        });

        it('should return "success" if progress is equal to 100', function() {
            createController();

            expect(scope.getProgressClassName(100)).toEqual('success');
        });

        it('should return "success" if progress is less than 100', function() {
            createController();

            expect(scope.getProgressClassName(80)).toEqual('success');
        });
    });
});

}());
