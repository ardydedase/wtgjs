(function() {

'use strict';

angular.module('fcApp.permission').controller('UserPermissionsCtrl', function(
    $q,
    $scope,
    _,
    authService,
    localStorageService,
    emailValidationService,
    userService,
    permissionService,
    mixpanelService) {

    authService.restrictToGroup('Skyscanner');

    function init() {
        $scope.queryUsername = '';
        $scope.username = '';
        $scope.showPermissions = false;
        $scope.invalidEmail = false;
        $scope.userExists = false;
        $scope.submitAction = {};
        $scope.permissionList = [];
    }

    function mixpanelTracking(data) {
        var mixpanelEventName = 'FC Admin UserPermissions';

        mixpanelService.track(mixpanelEventName, _.extend({
            user: $scope.username
        }, data));
    }

    function clearPermissionList() {
        $scope.permissionList = null;
        $scope.submitAction.message = '';
        $scope.submitAction.btnDisabled = true;
    }

    function formatInPermissionList(data) {
        if (!data.length) {
            clearPermissionList();
            return;
        }

        var techPermissions = data[0].technical_permissions;
        var commPermissions = data[0].commercial_permissions;
        var combinedPermissions = [];

        if (techPermissions.length || commPermissions.length) {
            combinedPermissions = _.map(techPermissions, function(id) {
                return {
                    websiteId: id,
                    technical: true,
                    commercial: false
                };
            });

            _.forEach(commPermissions, function(id) {
                var match = _.findWhere(combinedPermissions, { websiteId: id });

                if (match) {
                    match.commercial = true;
                } else {
                    combinedPermissions.push({
                        websiteId: id,
                        technical: false,
                        commercial: true
                    });
                }
            });

            $scope.permissionList = _.sortBy(combinedPermissions, 'websiteId');
            onPermissionListChanged();
        } else {
            clearPermissionList();
        }
    }

    function formatOutPermissionList() {
        var data = {
            username: $scope.username,
            technical_permissions: [],
            commercial_permissions: []
        };

        _.forEachRight($scope.permissionList, function(permission, index) {
            var hasTechnical = true;
            var hasCommercial = true;

            if (permission.technical === true) {
                data.technical_permissions.push(permission.websiteId);
            } else {
                hasTechnical = false;
            }

            if (permission.commercial === true) {
                data.commercial_permissions.push(permission.websiteId);
            } else {
                hasCommercial = false;
            }

            if (!hasTechnical && !hasCommercial) {
                $scope.permissionList.splice(index, 1);
            }
        });

        return data;
    }

    function onUserNotFound() {
        if (emailValidationService.isValid($scope.username)) {
            $scope.showPermissions = true;
            $scope.userExists = false;
            clearPermissionList();
        } else {
            $scope.invalidEmail = true;
            $scope.showPermissions = false;
        }
    }

    function onPermissionListChanged() {
        var activePermissions = $scope.permissionList;

        if (activePermissions !== null) {
            if (activePermissions.length > 0) {
                if (_.any(activePermissions, { technical: true })) {
                    $scope.submitAction.message = '';
                    $scope.submitAction.btnDisabled = false;
                } else {
                    $scope.submitAction.btnDisabled = true;
                    $scope.submitAction.message = 'There must be at least one Website ID with technical permission.';
                    $scope.submitAction.isSuccess = false;
                }
            } else {
                $scope.submitAction.message = '';
                $scope.submitAction.btnDisabled = false;
            }
        }
    }

    function onRemoteRequestFailure(err) {
        $scope.submitAction.message = err.error_message ? err.error_message : 'Something went wrong when performing your request.';
        $scope.submitAction.isSuccess = false;
        $scope.submitAction.btnDisabled = false;
    }

    function lookupUserPermissions(username) {
        var requests = {
            userQuery: userService.get(username),
            permissionListQuery: permissionService.get(username)
        };

        $scope.promise = $q.all(requests).then(function(response) {
            $scope.userExists = true;
            $scope.showPermissions = true;
            $scope.submitAction.message = '';

            formatInPermissionList(response.permissionListQuery.data);

            mixpanelTracking({
                userExists: true,
                action: 'Lookup User'
            });
        },

        function(err) {
            onUserNotFound(err);

            mixpanelTracking({
                userExists: false,
                action: 'Lookup User'
            });
        });
    }

    $scope.lookupUser = function(queryUsername) {
        var username = queryUsername.toLowerCase();

        lookupUserPermissions(username);

        $scope.username = username;
    };

    $scope.addWebsite = function(websiteId) {
        var match;

        $scope.submitAction.btnDisabled = false;

        $scope.permissionList = $scope.permissionList === null ? [] : $scope.permissionList;

        _.forEach($scope.permissionList, function(p, index) {
            if (p.websiteId === websiteId) {
                match = $scope.permissionList.splice(index, 1)[0];
                $scope.permissionList.unshift(match);
                return false;
            }
        });

        if (!match) {
            $scope.permissionList.unshift({
                websiteId: websiteId,
                technical: true,
                commercial: false
            });
        }
    };

    $scope.removePermission = function(index) {
        $scope.permissionList.splice(index, 1);

        onPermissionListChanged();
    };

    $scope.checkTechnicalPermission = function() {
        onPermissionListChanged();
    };

    $scope.updatePermissions = function() {
        var data = formatOutPermissionList();

        $scope.submitAction.message = '';

        $scope.promise = permissionService.post(data).then(function() {
            $scope.submitAction.message = 'User permissions updated.';
            $scope.submitAction.isSuccess = true;
            $scope.submitAction.btnDisabled = false;

            mixpanelTracking({
                action: 'Update Permissions',
                technicalPermissions: data.technical_permissions.join(','),
                commercialPermissions: data.commercial_permissions.join(','),
                success: true
            });
        },

        function(err) {
            onRemoteRequestFailure(err);

            mixpanelTracking({
                action: 'Update Permissions',
                technicalPermissions: data.technical_permissions.join(','),
                commercialPermissions: data.commercial_permissions.join(','),
                success: false
            });
        });
    };

    $scope.addUser = function() {
        var data = {
            username: $scope.username
        };

        $scope.submitAction.message = '';

        $scope.promise = userService.post(data).then(function() {
            $scope.userExists = true;
            $scope.updatePermissions();

            mixpanelTracking({
                action: 'Add User',
                success: true
            });
        },

        function(err) {
            if (err.status === 409) {
                $scope.userExists = true;
                $scope.updatePermissions();
            } else {
                $scope.userExists = false;
                onRemoteRequestFailure(err);
            }

            mixpanelTracking({
                action: 'Add User',
                success: false
            });
        });
    };

    init();
});

}());
