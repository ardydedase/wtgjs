(function() {

'use strict';

angular.module('fcApp.permission').controller('ComPermissionSummaryCtrl', function(
    $scope,
    $timeout,
    _,
    mixpanelService,
    authService,
    commercialPermissionService) {

    authService.restrictToGroup('Skyscanner');

    var allPartners;
    var searchTimer;

    function calculateTotals(permissionList) {
        $scope.statusSummary = _.groupBy(permissionList, function(partner) {
            return partner.status;
        });

        $scope.statusSummary.all = permissionList;
    }

    function indexWebsiteIds(list) {
        var websiteIdList = {};

        _.each(list, function(item) {
            _.each(item.website_ids, function(wid) {
                websiteIdList[wid.id] = {
                    partner: item.partner_name
                };
            });
        });

        $scope.websiteIdList = websiteIdList;
    }

    function init() {
        $scope.websiteIdList = {};
        $scope.partnerList = [];
        $scope.filter = {};

        commercialPermissionService.getFullPermissionList().then(function(response) {
            allPartners = response;

            indexWebsiteIds(allPartners);
            calculateTotals(allPartners);
        });

        mixpanelTracking({
            action: 'Loaded Page'
        });
    }

    function mixpanelTracking(data) {
        mixpanelService.track('FC Admin ComPermission', data);
    }

    $scope.filterList = function(status) {
        $scope.filter.partner = '';

        if (status === 'all') {
            $scope.partnerList = allPartners;
            return;
        }

        $scope.partnerList = _.filter(allPartners, function(partner) {
            return partner.status === status;
        });

        mixpanelTracking({
            action: 'Filter by Status',
            keyword: status
        });
    };

    $scope.searchPartner = function() {
        $timeout.cancel(searchTimer);

        if ($scope.filter.partner.length === 0) {
            $scope.partnerList = [];
            return;
        }

        searchTimer = $timeout(function() {
            var keyword =  $scope.filter.partner.toLowerCase();

            $scope.partnerList = _.filter(allPartners, function(d) {
                if (_.has($scope.websiteIdList, keyword)) {
                    return $scope.websiteIdList[keyword].partner === d.partner_name;
                }

                return _.contains(d.partner_name.toLowerCase(), keyword);
            });

            mixpanelTracking({
                action: 'Search by Partner',
                keyword: $scope.filter.partner,
                result: $scope.partnerList.length
            });
        }, 750);
    };

    $scope.getProgressClassName = function(progress) {
        if (progress < 50) {
            return 'danger';
        }

        if (progress < 80) {
            return 'warning';
        }

        if (progress <= 100) {
            return 'success';
        }
    };

    init();
});

}());
