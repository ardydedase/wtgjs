(function() {

'use strict';

describe('Controller: UserPermissionsCtrl', function() {
    var controller;
    var defaultMocks;
    var q;
    var scope;

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('UserPermissionsCtrl', {
            $scope: scope,
            authService: mocks.authService,
            localStorageService: mocks.localStorageService,
            emailValidationService: mocks.emailValidationService,
            userService: mocks.userService,
            permissionService: mocks.permissionService,
            mixpanelService: mocks.mixpanelService,
            _: window._
        });
    };

    var createPromise = function() {
        return function() {
            var dfr = q.defer();
            dfr.resolve({
                status: 200,
                data: []
            });
            return dfr.promise;
        };
    };

    beforeEach(module('fcApp.permission'));

    beforeEach(inject(function($controller, $rootScope, $location, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        defaultMocks = {
            authService: {
                restrictToGroup: function() {}
            },
            localStorageService: {
                get: function() {
                    return ['aaaa'];
                }
            },
            emailValidationService: {
                isValid: function() {}
            },
            userService: {
                get: createPromise(),
                post: createPromise()
            },
            permissionService: {
                get: createPromise(),
                post: createPromise()
            },
            mixpanelService: {
                track: function() {}
            }
        };
    }));

    it('should initialize defaults', function() {
        createController();

        expect(scope.queryUsername).toEqual('');
        expect(scope.username).toEqual('');
        expect(scope.showPermissions).toEqual(false);
        expect(scope.invalidEmail).toEqual(false);
        expect(scope.userExists).toEqual(false);
        expect(scope.submitAction).toBeDefined();
        expect(scope.permissionList).toEqual([]);

        expect(typeof scope.lookupUser).toBe('function');
        expect(typeof scope.addWebsite).toBe('function');
        expect(typeof scope.removePermission).toBe('function');
        expect(typeof scope.checkTechnicalPermission).toBe('function');
        expect(typeof scope.updatePermissions).toBe('function');
        expect(typeof scope.addUser).toBe('function');
    });

    it('should restrict access to internal users only', function() {
        spyOn(defaultMocks.authService, 'restrictToGroup');

        createController();

        expect(defaultMocks.authService.restrictToGroup).toHaveBeenCalledWith('Skyscanner');
    });

    describe('#lookupUser', function() {
        it('should keep a copy of active username in lowercase', function() {
            createController();

            scope.lookupUser('Guitar@Player.Com.SG');

            expect(scope.username).toEqual('guitar@player.com.sg');
        });

        it('should make ajax calls to GET user and GET permission', function() {
            spyOn(defaultMocks.userService, 'get').and.callThrough();
            spyOn(defaultMocks.permissionService, 'get').and.callThrough();

            createController();

            scope.lookupUser('soliloquist');

            expect(defaultMocks.userService.get).toHaveBeenCalledWith('soliloquist');
            expect(defaultMocks.permissionService.get).toHaveBeenCalledWith('soliloquist');
        });

        describe('user exists', function() {
            beforeEach(function() {
                spyOn(defaultMocks.permissionService, 'get').and.callThrough();
                spyOn(defaultMocks.userService, 'get').and.callThrough();

                createController();

                scope.lookupUser('soliloquist');
                scope.$digest();
            });

            it('should set userExists to true', function() {
                expect(scope.userExists).toEqual(true);
            });

            it('should show the permissions panel', function() {
                expect(scope.showPermissions).toEqual(true);
            });

            it('should clear any leftover alert messages', function() {
                expect(scope.submitAction.message.length).toEqual(0);
            });
        });

        describe('user does not exist', function() {
            it('should check if the username format is valid email', function() {
                var dfr = q.defer();
                spyOn(defaultMocks.emailValidationService, 'isValid').and.callThrough();
                spyOn(defaultMocks.permissionService, 'get').and.callThrough();
                spyOn(defaultMocks.userService, 'get').and.returnValue(dfr.promise);

                createController();

                scope.lookupUser('soliloquist');

                dfr.reject({
                    status: 404,
                    data: ''
                });

                scope.$digest();

                expect(defaultMocks.emailValidationService.isValid).toHaveBeenCalledWith('soliloquist');
            });

            describe('valid email - new user', function() {
                beforeEach(function() {
                    var dfr = q.defer();
                    spyOn(defaultMocks.emailValidationService, 'isValid').and.returnValue(true);
                    spyOn(defaultMocks.permissionService, 'get').and.callThrough();
                    spyOn(defaultMocks.userService, 'get').and.returnValue(dfr.promise);

                    createController();

                    scope.lookupUser('not@validEmail');

                    dfr.reject({
                        status: 404,
                        data: ''
                    });

                    scope.$digest();
                });

                it('should set userExists to false', function() {
                    expect(scope.userExists).toEqual(false);
                });

                it('should show the permissions panel', function() {
                    expect(scope.showPermissions).toEqual(true);
                });

                it('should set permissionList to null', function() {
                    expect(scope.permissionList).toEqual(null);
                });

                it('should clear any leftover alert messages', function() {
                    expect(scope.submitAction.message.length).toEqual(0);
                });
            });

            describe('invalid email', function() {
                beforeEach(function() {
                    var dfr = q.defer();
                    spyOn(defaultMocks.emailValidationService, 'isValid').and.returnValue(false);
                    spyOn(defaultMocks.permissionService, 'get').and.callThrough();
                    spyOn(defaultMocks.userService, 'get').and.returnValue(dfr.promise);

                    createController();

                    scope.lookupUser('not@validEmail');

                    dfr.reject({
                        status: 404,
                        data: ''
                    });

                    scope.$digest();
                });

                it('should set invalidEmail to true', function() {
                    expect(scope.invalidEmail).toEqual(true);
                });

                it('should not show the permissions panel', function() {
                    expect(scope.showPermissions).toEqual(false);
                });
            });
        });

        describe('permission exists', function() {
            it('should format permissionList', function() {
                var expectedPermissionList = [{
                    websiteId: 'a',
                    technical: true,
                    commercial: false
                }, {
                    websiteId: 'c',
                    technical: true,
                    commercial: true
                }, {
                    websiteId: 'x',
                    technical: false,
                    commercial: true
                }];

                var dfr = q.defer();
                spyOn(defaultMocks.permissionService, 'get').and.returnValue(dfr.promise);
                spyOn(defaultMocks.userService, 'get').and.callThrough();

                createController();

                scope.lookupUser('valid@email.com');

                dfr.resolve({
                    data: [{
                        username: 'soli@loqui.st',
                        technical_permissions: ['a', 'c'],
                        commercial_permissions: ['x', 'c']
                    }]
                });

                scope.$digest();

                expect(scope.permissionList.length).toEqual(3);
                expect(scope.permissionList).toEqual(expectedPermissionList);
            });

            it('should format set permissionList to null if there is no permission', function() {
                var dfr = q.defer();
                spyOn(defaultMocks.permissionService, 'get').and.returnValue(dfr.promise);
                spyOn(defaultMocks.userService, 'get').and.callThrough();

                createController();

                scope.lookupUser('valid@email.com');

                dfr.resolve({
                    data: [{
                        username: 'soli@loqui.st',
                        technical_permissions: [],
                        commercial_permissions: []
                    }]
                });

                scope.$digest();

                expect(scope.permissionList).toEqual(null);
            });
        });
    });

    describe('#addWebsite', function() {
        it('should enable submission', function() {
            createController();

            scope.permissionList = [];
            scope.addWebsite('a');

            expect(scope.submitAction.btnDisabled).toEqual(false);
        });

        it('should add websiteId to permissionList if it does not exist', function() {
            createController();

            scope.permissionList = [{
                websiteId: 'a'
            }];

            scope.addWebsite('b');

            expect(scope.permissionList.length).toEqual(2);
            expect(scope.permissionList[0].websiteId).toEqual('b');
            expect(scope.permissionList[0].technical).toEqual(true);
            expect(scope.permissionList[0].commercial).toEqual(false);
        });

        it('should not add websiteId to permissionList if it already exists', function() {
            createController();

            scope.permissionList = [{
                websiteId: 'a'
            }, {
                websiteId: 'b'
            }];

            scope.addWebsite('b');

            expect(scope.permissionList.length).toEqual(2);
        });

        it('should shift websiteId to the first array position if it already exists', function() {
            createController();

            scope.permissionList = [{
                websiteId: 'a'
            }, {
                websiteId: 'b'
            }];

            scope.addWebsite('b');

            expect(scope.permissionList[0].websiteId).toEqual('b');
        });
    });

    describe('#removePermission', function() {
        it('should remove websiteId from permissionList', function() {
            createController();

            scope.permissionList = [{
                websiteId: 'a'
            }];

            scope.removePermission('a');

            expect(scope.permissionList.length).toEqual(0);
        });
    });

    describe('onPermissionListChanged (#checkTechnicalPermission, #removePermission)', function() {
        describe('permissionList is not empty', function() {
            it('should display alert if there is no websiteId with technical permission', function() {
                createController();

                scope.permissionList = [{
                    websiteId: 'a',
                    technical: false
                }, {
                    websiteId: 'b',
                    technical: false
                }];

                scope.checkTechnicalPermission();

                expect(scope.submitAction.message.length).toBeGreaterThan(0);
                expect(scope.submitAction.btnDisabled).toEqual(true);
                expect(scope.submitAction.isSuccess).toEqual(false);
            });

            it('should not display alert if there is at least one websiteId with technical permission', function() {
                createController();

                scope.permissionList = [{
                    websiteId: 'a',
                    technical: false
                }, {
                    websiteId: 'b',
                    technical: true
                }, {
                    websiteId: 'c',
                    technical: true
                }];

                scope.checkTechnicalPermission();

                expect(scope.submitAction.message.length).toEqual(0);
                expect(scope.submitAction.btnDisabled).toEqual(false);
            });
        });

        describe('permissionList is empty', function() {
            it('should clear alert message and enable submission', function() {
                createController();

                scope.permissionList = [];

                scope.checkTechnicalPermission();

                expect(scope.submitAction.message.length).toEqual(0);
                expect(scope.submitAction.btnDisabled).toEqual(false);
            });
        });
    });

    describe('#updatePermissions', function() {
        it('should clear any alert message before submission', function() {
            createController();

            scope.updatePermissions();

            expect(scope.submitAction.message.length).toEqual(0);
        });

        it('should make ajax call to POST permissions', function() {
            spyOn(defaultMocks.permissionService, 'post').and.callThrough();

            var expectedPostData = {
                username: 'soliloquist',
                technical_permissions: ['a'],
                commercial_permissions: ['a'],
            };

            createController();

            scope.username = 'soliloquist';
            scope.permissionList = [{
                websiteId: 'a',
                technical: true,
                commercial: true
            }];

            scope.updatePermissions();

            expect(defaultMocks.permissionService.post).toHaveBeenCalledWith(expectedPostData);
        });

        it('should weed out websiteId with no technical or commercial permissions', function() {
            spyOn(defaultMocks.permissionService, 'post').and.callThrough();

            var expectedPostData = {
                username: 'soliloquist',
                technical_permissions: ['a'],
                commercial_permissions: ['a', 'c'],
            };

            createController();

            scope.username = 'soliloquist';
            scope.permissionList = [{
                websiteId: 'c',
                technical: false,
                commercial: true
            }, {
                websiteId: 'b',
                technical: false,
                commercial: false
            }, {
                websiteId: 'a',
                technical: true,
                commercial: true
            }];

            scope.updatePermissions();

            expect(defaultMocks.permissionService.post).toHaveBeenCalledWith(expectedPostData);
        });

        it('should display success message when update is successful', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.permissionService, 'post').and.returnValue(dfr.promise);

            createController();

            scope.username = 'soliloquist';
            scope.permissionList = [{
                websiteId: 'a',
                technical: true,
                commercial: true
            }];

            scope.updatePermissions();
            dfr.resolve();

            scope.$digest();

            expect(scope.submitAction.message.length).toBeGreaterThan(0);
            expect(scope.submitAction.isSuccess).toEqual(true);
            expect(scope.submitAction.btnDisabled).toEqual(false);
        });

        it('should display failure message when update is successful and enable re-submission', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.permissionService, 'post').and.returnValue(dfr.promise);

            createController();

            scope.username = 'soliloquist';
            scope.permissionList = [{
                websiteId: 'a',
                technical: true,
                commercial: true
            }];

            scope.updatePermissions();
            dfr.reject({
                error_message: 'Boo!'
            });

            scope.$digest();

            expect(scope.submitAction.message).toEqual('Boo!');
            expect(scope.submitAction.isSuccess).toEqual(false);
            expect(scope.submitAction.btnDisabled).toEqual(false);
        });
    });

    describe('#addUser', function() {
        it('should clear any alert message before submission', function() {
            createController();

            scope.addUser();

            expect(scope.submitAction.message.length).toEqual(0);
        });

        it('should make ajax call to POST user', function() {
            spyOn(defaultMocks.userService, 'post').and.callThrough();

            var expectedPostData = {
                username: 'soliloquist'
            };

            createController();

            scope.username = 'soliloquist';

            scope.addUser();

            expect(defaultMocks.userService.post).toHaveBeenCalledWith(expectedPostData);
        });

        describe('user successfully added', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.userService, 'post').and.returnValue(dfr.promise);
                spyOn(defaultMocks.permissionService, 'post').and.callThrough();

                createController();

                scope.username = 'soliloquist';

                scope.addUser();
                dfr.resolve();
                scope.$digest();
            });

            it('should set userExists to true', function() {
                expect(scope.userExists).toEqual(true);
            });

            it('should #updatePermissions', function() {
                expect(defaultMocks.permissionService.post).toHaveBeenCalled();
            });
        });

        describe('user not added', function() {
            it('should set userExists to true if user already exists', function() {
                var dfr = q.defer();
                spyOn(defaultMocks.userService, 'post').and.returnValue(dfr.promise);

                createController();

                scope.username = 'soliloquist';

                scope.addUser();
                dfr.reject({
                    status: 409
                });
                scope.$digest();

                expect(scope.userExists).toEqual(true);
            });

            it('should #updatePermissions if user already exists', function() {
                var dfr = q.defer();
                spyOn(defaultMocks.userService, 'post').and.returnValue(dfr.promise);
                spyOn(defaultMocks.permissionService, 'post').and.callThrough();

                createController();

                scope.username = 'soliloquist';

                scope.addUser();
                dfr.reject({
                    status: 409
                });
                scope.$digest();

                expect(defaultMocks.permissionService.post).toHaveBeenCalled();
            });

            it('should display failure message for all other failures', function() {
                var dfr = q.defer();
                spyOn(defaultMocks.userService, 'post').and.returnValue(dfr.promise);

                createController();

                scope.username = 'soliloquist';

                scope.addUser();
                dfr.reject({
                    status: 500,
                    error_message: 'Nooooo'
                });
                scope.$digest();

                expect(scope.userExists).toEqual(false);
                expect(scope.submitAction.message).toEqual('Nooooo');
                expect(scope.submitAction.btnDisabled).toEqual(false);
                expect(scope.submitAction.isSuccess).toEqual(false);
            });
        });
    });
});

}());
