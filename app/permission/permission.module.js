(function() {

'use strict';

angular.module('fcApp.permission', [
    'fcApp.authentication',
    'fcApp.commercial',
    'fcApp.common'
]);

}());
