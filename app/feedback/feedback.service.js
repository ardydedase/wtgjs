(function() {

'use strict';

angular.module('fcApp.feedback').factory('feedbackService', function(
    $http,
    mixpanelService,
    config) {

    return {
        submitFeedback: function(comments, email, websiteId) {
            mixpanelService.track('FC Submit Feedback Form');

            var url = config.API_URL + config.API_BASE + '/feedback/?' + $.param({
                website_id: websiteId
            });

            return $http({
                    method: 'POST',
                    url: url,
                    data: JSON.stringify({
                        comments: comments,
                        email: email,
                        website_id: websiteId
                    }),
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function() {
                    return 'Success';
                });
        }
    };
});

}());
