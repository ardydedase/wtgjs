(function() {

'use strict';

angular.module('fcApp.feedback').controller('FeedbackCtrl', function(
    $scope,
    $http,
    localStorageService,
    mixpanelService,
    alertService,
    feedbackService) {

    $scope.closeAlert = alertService.closeAlert;
    $scope.website_id = localStorageService.get('selectedwebsite');
    $scope.sending_feedback = false;
    $scope.messages = 'Ready to Submit Feedback for ' + $scope.website_id;

    $scope.sendFeedback = function(comments, email, websiteId) {
        $scope.messages = 'Submitting Feedback';
        var resp = feedbackService.submitFeedback(comments, email, websiteId);
        $scope.sending_feedback = true;

        resp.then(function(response) {
            if (response === 'Success') {
                mixpanelService.track('FC Successful Feedback');

                $scope.messages = 'Submitted';
            } else {
                $scope.messages = 'Error';
            }

            $scope.sending_feedback = false;
        });
    };
});

}());
