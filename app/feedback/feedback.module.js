(function() {

'use strict';

angular.module('fcApp.feedback', [
    'LocalStorageModule',
    'fcApp.common'
]);

}());
