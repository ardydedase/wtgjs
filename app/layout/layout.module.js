(function() {

'use strict';

angular.module('fcApp.layout', [
    'angulartics',
    'angulartics.google.analytics',
    'LocalStorageModule',
    'fcApp.common'
]);

}());
