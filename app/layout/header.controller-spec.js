(function() {

'use strict';

describe('Controller: HeaderCtrl', function() {

    var controller;
    var defaultMocks;
    var analytics;
    var store;
    var q;
    var scope;

    var noop = function() {};

    function setUpMocks() {
        store = {
            account: {
                username: 'orange',
                groups: []
            },
            selectedwebsite: 'zzzz',
            websiteDetails: {
                websiteId: 'zzzz',
                partnerName: 'Orange',
                partners: ['Orange', 'Apple']
            },
            websites: ['aaa', 'bbb'],
            commercial_websites: ['ccc'],
            webconfig: {}
        };

        defaultMocks = {
            localStorageService: {
                get: function(item) {
                    return store[item];
                },

                add: function(key, value) {
                    store[key] = value;
                },

                remove: function(item) {
                    delete store[item];
                }
            },
            userSessionService: {
                updateWebConfig: function() {
                    var dfr = q.defer();
                    return dfr.promise;
                },

                updateWebsiteDetails: function() {
                    var dfr = q.defer();
                    return dfr.promise;
                }
            },
            mixpanelService: {
                track: function() {}
            },
            location: {
                path: function(location) {
                    if (location) {
                        return location;
                    }

                    return '/scrapetester/zzzz';
                },

                search: noop
            },
            authService: {
                hasCommercialAccess: noop,
                isInternalUser: noop,
                logout: noop,
                getUserType: noop
            }
        };
    }

    function createController(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('HeaderCtrl', {
            $scope: scope,
            $analytics: analytics,
            $location: mocks.location,
            localStorageService: mocks.localStorageService,
            userSessionService: mocks.userSessionService,
            mixpanelService: mocks.mixpanelService,
            authService: mocks.authService,
            _: window._
        });
    }

    beforeEach(module('fcApp.layout'));

    beforeEach(inject(function($controller, $rootScope, $analytics, $routeParams, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        analytics = $analytics;
        q = $q;

        setUpMocks();
    }));

    afterEach(function() {
        store = {};
    });

    it('should initialize defaults', function() {
        createController();

        expect(scope.username).toEqual('orange');
        expect(scope.websites).toEqual(['aaa', 'bbb', 'ccc']);
        expect(scope.selectedWebsiteId).toEqual('zzzz');

        expect(typeof scope.hasCommercialAccess).toEqual('function');
        expect(typeof scope.isInternalUser).toEqual('function');
        expect(typeof scope.logout).toEqual('function');
        expect(typeof scope.updateLocation).toEqual('function');
        expect(typeof scope.isNavActive).toEqual('function');
        expect(typeof scope.appendWebsiteId).toEqual('function');
        expect(typeof scope.constructJiraURL).toEqual('function');
    });

    it('should call analytics eventTrack on $viewContentLoaded', function() {
        spyOn(analytics, 'eventTrack').and.callThrough();

        createController();
        scope.$emit('$viewContentLoaded');

        expect(analytics.eventTrack).toHaveBeenCalled();
    });

    describe('isTRUser', function() {
        it('should return true if username is miland', function() {
            store.account.username = 'miland';

            createController();

            expect(scope.isTRUser()).toEqual(true);
        });

        it('should return false if there is no trPermission key in localStorage', function() {
            createController();

            expect(scope.isTRUser()).toEqual(false);
        });

        it('should return Boolean true if trPermission is true', function() {
            store.trPermission = 'true';

            createController();

            expect(scope.isTRUser()).toEqual(true);
        });

        it('should return Boolean false if trPermission is false', function() {
            store.trPermission = 'false';

            createController();

            expect(scope.isTRUser()).toEqual(false);
        });
    });

    describe('updateLocalStorage', function() {
        beforeEach(function() {
            spyOn(defaultMocks.localStorageService, 'remove');
            spyOn(defaultMocks.localStorageService, 'add');
        });

        it('should set partnerName from localstorage.websiteDetails if available', function() {
            createController();

            expect(scope.partnerName).toEqual('Orange');
        });

        it('should call userSessionService.updateWebsiteDetails when websiteId is changed', function() {
            spyOn(defaultMocks.userSessionService, 'updateWebsiteDetails').and.callThrough();

            store.selectedwebsite = 'cccc';
            createController();

            expect(defaultMocks.userSessionService.updateWebsiteDetails).toHaveBeenCalledWith('cccc');
        });

        it('should set partnerName after userSessionService.updateWebsiteDetails resolves', function() {
            delete store.websiteDetails;
            var dfr = q.defer();
            spyOn(defaultMocks.userSessionService, 'updateWebsiteDetails').and.returnValue(dfr.promise);

            createController();
            dfr.resolve({
                partnerName: 'hello'
            });
            scope.$digest();

            expect(scope.partnerName).toEqual('hello');
        });

        it('should call userSessionService.updateWebConfig when websiteId is changed', function() {
            spyOn(defaultMocks.userSessionService, 'updateWebConfig').and.callThrough();

            store.selectedwebsite = 'cccc';
            createController();

            expect(defaultMocks.userSessionService.updateWebConfig).toHaveBeenCalledWith('cccc');
        });

        it('should call broadcast event when websiteId is changed', function() {
            spyOn(scope, '$broadcast');

            store.selectedwebsite = 'cccc';
            createController();

            expect(scope.$broadcast).toHaveBeenCalledWith('websiteIdChanged', 'cccc');
        });
    });

    describe('appendWebsiteId', function() {
        it('should append websiteId as querystring to a path', function() {
            var fakePath = '/fakepath';
            var expectedPath = fakePath + '?website_id=zzzz';

            createController();
            var result = scope.appendWebsiteId(fakePath);
            scope.$digest();

            expect(result).toEqual(expectedPath);
        });

        it('should append websiteId as trailing url to a #/scrapetester path', function() {
            var scrapeTesterPath = '#/scrapetester';
            var expectedPath = scrapeTesterPath + '/zzzz';

            createController();
            var result = scope.appendWebsiteId(scrapeTesterPath);
            scope.$digest();

            expect(result).toEqual(expectedPath);
        });
    });

    describe('updateLocation', function() {
        // to be resoleved - unable to resolve $location.path().indexOf()
    });
});

}());
