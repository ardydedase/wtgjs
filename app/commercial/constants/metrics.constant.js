(function() {

'use strict';

angular.module('fcApp.commercial').constant('METRICS', [{
        id: 'search_count',
        code: 'searches',
        title: 'Searches',
        type: 'count',
        relationship: ['clickthrough']
    }, {
        id: 'scrape_count',
        code: 'apiqueries',
        title: 'API Queries',
        type: 'count',
        relationship: ['look2book']
    }, {
        id: 'redirect_count',
        code: 'redirects',
        title: 'Redirects',
        type: 'count',
        relationship: ['clickthrough', 'conversion']
    }, {
        id: 'booking_count',
        code: 'bookings',
        title: 'Bookings',
        type: 'count',
        relationship: ['look2book', 'conversion']
    }, {
        id: 'clickthrough_percentage',
        code: 'clickthrough',
        title: 'Clickthrough',
        type: 'percentage',
        shortDesc: 'Searches to Redirects',
        description: 'Overall ratio between searches and redirects',
        relationship: ['searches', 'redirects']
    }, {
        id: 'conversion_percentage',
        code: 'conversion',
        title: 'Conversion',
        type: 'percentage',
        shortDesc: 'Redirects to Bookings',
        description: 'Overall ratio between redirects and bookings',
        relationship: ['redirects', 'bookings']
    }, {
        id: 'scrape2book_ratio',
        code: 'look2book',
        title: 'Look2Book',
        type: 'ratio',
        shortDesc: 'API Queries to Bookings',
        description: 'Overall ratio between API queries and bookings',
        relationship: ['apiqueries', 'bookings']
    }]);
}());
