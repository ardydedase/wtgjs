(function() {

'use strict';

angular.module('fcApp.commercial').directive('fcMaskFilter', function() {

    return {
        restrict: 'E',
        templateUrl: 'app/commercial/directives/maskFilter/mask-filter.html',
        scope: {
            filter: '=',
            disabled: '=',
            selectedValue: '='
        },
        replace: true,
        link: function(scope, elem) {
            var button = elem.find('button.dropdown-toggle');
            var ul = elem.find('ul.dropdown-menu');

            var maxItems = scope.filter.values.length;
            var type = scope.filter.name;

            var changeButtonTextStyle = function(numSelected) {
                if (numSelected === maxItems) {
                    button.text('All ' + type + 's');
                } else if (numSelected === 0) {
                    button.text('None selected').addClass('error');
                } else {
                    button.text(numSelected + '/' + maxItems + ' selected').addClass('active');
                }
            };

            changeButtonTextStyle(scope.selectedValue.length);

            ul.on('click', 'a', function() {
                button.removeClass('active error');

                changeButtonTextStyle(scope.selectedValue.length);
            });
        }
    };
});

}());
