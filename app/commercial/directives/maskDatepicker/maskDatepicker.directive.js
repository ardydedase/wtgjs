(function() {

'use strict';

angular.module('fcApp.commercial').directive('fcMaskDatepicker', function() {

    return {
        restrict: 'E',
        templateUrl: 'app/commercial/directives/maskDatepicker/mask-datepicker.html',
        scope: {
            id: '@',
            label: '@',
            dateFormat: '=',
            selectedValue: '=',
            minDate: '=',
            maxDate: '=',
            disabled: '=',
            onFilterChanged: '&'
        },
        replace: true,
        controller: function($scope) {
            $scope.configs = {
                format: $scope.dateFormat,
                viewMode: 'days',
                minDate: $scope.minDate,
                maxDate: $scope.maxDate,
                focusOnShow: false
            };
        },

        link: function(scope, elem) {
            var input = elem.find('input');

            input.on('dp.hide', function() {
                this.blur();
            });

            scope.$watch('minDate', function(newValue) {
                if (newValue) {
                    input.data('DateTimePicker').minDate(newValue);
                }
            });

            scope.$watch('maxDate', function(newValue) {
                if (newValue) {
                    input.data('DateTimePicker').maxDate(newValue);
                }
            });
        }
    };
});

}());
