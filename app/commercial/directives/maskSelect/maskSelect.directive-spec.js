(function() {

'use strict';

describe('Directive: maskSelect', function() {
    var compile;
    var scope;

    beforeEach(function() {
        module('fcApp.commercial');
        module('directiveTemplates');

        inject(function($compile, $rootScope) {
            compile = $compile;
            scope = $rootScope.$new();
        });
    });

    function getCompiledElement(elm) {
        var element = angular.element(elm);
        var compiledElement = compile(element)(scope);

        scope.$digest();

        return compiledElement;
    }

    describe('when created', function() {
        it('should render a label, a select, and a span', function() {
            var el = getCompiledElement('<fc-mask-select></fc-select-mask>');

            expect(el.find('label').length).toEqual(1);
            expect(el.find('span.mask-label').length).toEqual(1);
            expect(el.find('select').length).toEqual(1);
        });

        it('should assign attribute name to the select and atttribute for the label', function() {
            var el = getCompiledElement('<fc-mask-select id="myDirectiveId" label="hello!"></fc-select-mask>');

            expect(el.find('label').attr('for')).toEqual('myDirectiveId');
            expect(el.find('select').attr('name')).toEqual('myDirectiveId');
        });

        it('should render the label text', function() {
            var el = getCompiledElement('<fc-mask-select id="myDirectiveId" label="hello!"></fc-select-mask>');

            expect(el.find('label').text()).toEqual('hello!');
        });

        it('should render the span text', function() {
            scope.myValue = {
                name: 'ftw'
            };

            var el = getCompiledElement('<fc-mask-select selected-value="myValue"></fc-select-mask>');

            expect(el.find('span').text()).toEqual('ftw');
        });

        it('should append a "disabled" class', function() {
            scope.myDirectiveIsDisabled = true;

            var el = getCompiledElement('<fc-mask-select selected-value="myValue" disabled="myDirectiveIsDisabled"></fc-select-mask>');

            expect(el.find('span').attr('class')).toMatch('disabled');
        });

        it('should populate the select options', function() {
            scope.myDirectiveOptions = [{
                name: 'one',
                code: 1
            }, {
                name: 'two',
                code: 2
            }];

            var el = getCompiledElement('<fc-mask-select options="myDirectiveOptions" selected-value="myDirectiveOptions[0]"></fc-select-mask>');

            expect(el.find('select option').length).toEqual(2);
        });

        it('should assign the selected value to the select and span', function() {
            scope.myDirectiveOptions = [{
                name: 'one',
                code: 1
            }, {
                name: 'two',
                code: 2
            }];

            var el = getCompiledElement('<fc-mask-select options="myDirectiveOptions" selected-value="myDirectiveOptions[0]"></fc-select-mask>');

            expect(el.find('select').val()).toEqual('1');
            expect(el.find('span.mask-label').text()).toEqual('one');
        });
    });

    describe('when scope changes', function() {
        it('should do something when the select value changes', function() {
            scope.fakeCall = function() {};

            scope.myDirectiveOptions = [{
                name: 'one',
                code: 1
            }, {
                name: 'two',
                code: 2
            }];

            spyOn(scope, 'fakeCall');

            var el = getCompiledElement('<fc-mask-select options="myDirectiveOptions" selected-value="myDirectiveOptions[0]" on-filter-changed="fakeCall(value, name)"></fc-select-mask>');

            el.find('select').val(scope.myDirectiveOptions[1]).trigger('change');
            scope.$digest();

            expect(scope.fakeCall).toHaveBeenCalled();
        });
    });
});

}());
