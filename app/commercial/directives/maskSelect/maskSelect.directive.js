(function() {

'use strict';

angular.module('fcApp.commercial').directive('fcMaskSelect', function() {

    return {
        restrict: 'E',
        templateUrl: 'app/commercial/directives/maskSelect/mask-select.html',
        scope: {
            id: '@',
            label: '@',
            selectedValue: '=',
            options: '=',
            onFilterChanged: '&',
            disabled: '='
        },
        replace: true
    };
});

}());
