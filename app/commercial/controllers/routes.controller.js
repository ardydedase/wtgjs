(function() {

'use strict';

angular.module('fcApp.commercial').controller('RoutesCtrl', function(
    $scope,
    $timeout,
    authService,
    _,
    moment,
    localStorageService,
    websiteIdService,
    routeService,
    mixpanelService) {

    authService.redirectFromCommercialPage();

    var websiteId = localStorageService.get('selectedwebsite');
    var defaultErrorMessage = 'There was a problem retrieving requested data. Please try again.';
    var mixpanelEventName = 'FC Routes TopDestinations';

    function getRandomTime() {
        return parseInt(Math.random() * 2000, 10);
    }

    function getDestination(id) {
        return _.find($scope.destinationList, { routeId: id });
    }

    function getChangePercentage(changeValue, currentValue) {
        var lastWeekValue = currentValue - changeValue;

        if (changeValue === 0) {
            return 0;
        } else {
            if (lastWeekValue === 0) {
                return (changeValue / 1) * 100;
            } else {
                return (changeValue / lastWeekValue) * 100;
            }
        }
    }

    function formatRouteList(list, key) {
        return _.map(list, function(item) {
            var place = {};

            place.iata = item.route[key].iata_id;
            place.routeId = item.route[key].routenode_id;
            place.name = item.route[key].name;
            place.ui = {
                isHeaderClicked: false,
                tooltipTimeout: getRandomTime()
            };

            _.forEach(item.metrics, function(metric) {
                place[metric.type] = metric.value;
                place['wow_' + metric.type] = getChangePercentage(metric.wow_change, metric.value, metric.type);
            });

            return place;
        });
    }

    function updateMarketDropdown(isDisabled, selectedTextValue, feedback) {
        $scope.isMarketDisabled = isDisabled;
        $scope.markets[0].name = selectedTextValue;
        $scope.feedback = feedback;
    }

    function getMarkets() {
        $scope.feedback = null;

        websiteIdService.getMarkets({
            websiteId: websiteId,
            cache: true
        }).then(function(markets) {
            if (!markets.length) {
                updateMarketDropdown(true, 'Markets unavailable', {
                    message: 'There is no redirects data available.',
                    type: 'info'
                });
            } else {
                $scope.markets = $scope.markets.concat(markets);
                updateMarketDropdown(false, '-- Please select a market --', {
                    message: 'Please select a market to continue.',
                    type: 'info'
                });

                if (markets.length === 1) {
                    $scope.selectedMarket = $scope.markets[1];
                    $scope.getDestinationList($scope.selectedMarket);
                }
            }
        },

        function() {
            updateMarketDropdown(true, 'Markets unavailable', {
                message: defaultErrorMessage,
                type: 'danger'
            });
        });
    }

    function init() {
        $scope.originList = [];

        $scope.markets = [{
            id: 'none',
            name: 'Loading markets...'
        }];

        $scope.selectedMarket = $scope.markets[0];

        getMarkets();
    }

    $scope.getDestinationList = function(market) {
        var options;

        if (market && market.id && market.id !== 'none' && market.id !== $scope.listMarketId) {
            $scope.isMarketDisabled = true;
            $scope.feedback = null;
            $scope.dataCollectionPeriod = null;

            options = {
                limit: 10,
                websiteId: websiteId,
                market: market.id
            };

            $scope.destinationListPromise = routeService.getDestinations(options).then(function(res) {
                if (res.data.length) {
                    $scope.destinationList = formatRouteList(res.data, 'destination');
                    $scope.listMarketId = market.id;
                    $scope.isViewMoreClicked = false;
                    $scope.showTooltip = false;
                    $scope.tooltipTimeout = getRandomTime();

                    if (res.data[0].dates) {
                        $scope.dataCollectionPeriod = {
                            startDate: moment(res.data[0].dates.start_date).format('DD MMMM YYYY'),
                            endDate: moment(res.data[0].dates.end_date).format('DD MMMM YYYY')
                        };
                    }
                } else {
                    $scope.destinationList = [];
                    $scope.feedback = {
                        message: 'No redirects data available for ' + market.name + ' market.',
                        type: 'info'
                    };
                }

                mixpanelService.track(mixpanelEventName, {
                    action: 'Select Market',
                    marketName: market.name,
                    marketId: market.id,
                    destinationListLength: res.data.length
                });
            },

            function(err) {
                var message = defaultErrorMessage;

                if (err.status === 403) {
                    message = 'This data is currently unavailable. Please contact your Account Manager for more information.';
                }

                $scope.feedback = {
                    message: message,
                    type: 'danger'
                };
            }).finally(function() {
                $scope.isMarketDisabled = false;
            });
        }
    };

    $scope.getOriginList = function(destinationId) {
        var destination = getDestination(destinationId);
        var options;

        if (destination.hasOriginList) {
            return false;
        } else {

            options = {
                limit: 5,
                websiteId: websiteId,
                market: $scope.selectedMarket.id,
                destination: destinationId
            };

            $scope.feedback = null;

            destination.ui.isHeaderClicked = true;

            routeService.getOrigins(options).then(function(res) {
                destination.ui.isHeaderClicked = false;

                if (res.data.length) {
                    destination.hasOriginList = true;

                    $scope.originList.push({
                        destination: destinationId,
                        origins: formatRouteList(res.data, 'origin')
                    });
                }

                mixpanelService.track(mixpanelEventName, {
                    action: 'Select Destination',
                    marketName: $scope.selectedMarket.name,
                    marketId: $scope.selectedMarket.id,
                    destinationId: destinationId,
                    destinationName: destination.name,
                    destinationIATA: destination.iata,
                    originListLength: res.data.length
                });
            },

            function() {
                destination.hasOriginList = false;
                destination.ui.isHeaderClicked = false;

                $scope.feedback = {
                    message: defaultErrorMessage,
                    type: 'danger'
                };
            });
        }
    };

    $scope.viewMore = function(destinationId) {
        var context;
        var destination;
        var trackingData;

        if (destinationId) {
            destination = getDestination(destinationId);
            context = destination.ui;
        } else {
            context = $scope;
        }

        context.isViewMoreClicked = true;

        $timeout(function() {
            context.isViewMoreClicked = false;
            context.showTooltip = true;
            context.tooltipTimeout = getRandomTime();
        }, context.tooltipTimeout);

        trackingData = {
            action: 'Click View More ' + (destinationId ? 'Origins' : 'Destinations'),
            marketName: $scope.selectedMarket.name,
            marketId: $scope.selectedMarket.id,
            destinationListLength: $scope.destinationList.length
        };

        if (destinationId) {
            _.extend(trackingData, {
                destinationId: destinationId,
                destinationName: destination.name,
                destinationIATA: destination.iata
            });
        }

        mixpanelService.track(mixpanelEventName, trackingData);
    };

    $scope.clearTooltip = function(destinationId) {
        var context;

        if (destinationId) {
            context = getDestination(destinationId).ui;
        } else {
            context = $scope;
        }

        context.showTooltip = false;
    };

    $scope.registerTRInterest = function(destinationId, originId) {
        var destination = getDestination(destinationId);
        var originList = _.find($scope.originList, { destination: destinationId });
        var origin = _.find(originList.origins, { routeId: originId
         });

        mixpanelService.track(mixpanelEventName, {
            action: 'Click Origin SOV',
            marketName: $scope.selectedMarket.name,
            marketId: $scope.selectedMarket.id,
            destinationId: destinationId,
            destinationName: destination.name,
            destinationIATA: destination.iata,
            originId: originId,
            originName: origin.name,
            originIATA: origin.iata
        });
    };

    init();
});

}());
