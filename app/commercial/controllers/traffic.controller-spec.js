(function() {

'use strict';

describe('Controller: TrafficCtrl', function() {
    var controller;
    var defaultMocks;
    var q;
    var timeout;
    var scope;
    var METRICS;
    var trafficService;

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('TrafficCtrl', {
            $scope: scope,
            $location: mocks.location,
            $route: mocks.route,
            METRICS: METRICS,
            csvDownloader: mocks.csvDownloader,
            trafficService: mocks.trafficService,
            funnelMetricsService: mocks.funnelMetricsService,
            localStorageService: mocks.localStorageService,
            mixpanelService: mocks.mixpanelService,
            _: window._,
            moment: window.moment
        });
    };

    var createPromise = function(reject) {
        return function() {
            var dfr = q.defer();

            if (reject) {
                dfr.reject();
            } else {
                dfr.resolve();
            }

            return dfr.promise;
        };
    };

    var noop = function() { return {}; };

    var fakeVariables = {
        searchOptions: {
            dates: {
                earliest_start_date: '2015-04-29T00:00:00',
                latest_end_date: '2015-06-07T23:59:59'
            },
            field_names: {
                carrier: 'Airlines',
                device_type: 'Device',
                date: 'Date',
                flight_type: 'Flight Types',
                traffic_type: 'Traffic Sources',
                market: 'Markets'
            },
            segments: ['market', 'device_type', 'flight_type', 'traffic_type'],
            dimensions: ['date', 'market', 'carrier'],
            filters: {
                flight_type: [
                    { code: 'I', name: 'International' },
                    { code: 'D', name: 'Domestic '}
                ],
                carrier: [
                    { code: 'AA', name: 'American Airlines' },
                    { code: 'QF', name: 'Qantas' },
                    { code: 'JL', name: 'Japan Airlines '}
                ],
                market: [
                    { code: 'qa', name: 'Qatar' },
                    { code: 'Nz', name: 'New Zealand '}
                ],
                device_type: [
                    { code: 'd', name: 'Desktop' },
                    { code: 'M', name: 'Mobile Phone' },
                    { code: 'O', name: 'Other'}
                ],
                traffic_type: [
                    { code: 'flights', name: 'FLIGHTS' },
                    { code: 'alsoflies', name: 'ALSOFLIES '}
                ]
            }
        },
        activeSearchFilters: {
            dimension: { code: 'date', name: 'Date' },
            segment: { code: 'none', name: 'No breakdown' },
            startDate: '21 Jun 2015',
            endDate: '28 Jun 2015',
            includeFilters: {
                flight_type: [
                    { id: 'd'},
                    { id: 'i'}
                ],
                carrier: [
                    { id: 'ak'},
                    { id: '04'},
                    { id: 'zm'}
                ],
                device_type: [
                    { id: 'd'},
                    { id: 'm'},
                    { id: 't'},
                    { id: 'o'}
                ],
                traffic_type: [
                    { id: 'flights'},
                    { id: 'alsoflies'}
                ]
            },
            excludeFilters: {}
        },
        tableData: {
            header: [
                { date: 'Date'}, { search_count: 'Searches'}, { scrape_count: 'API Queries'}, { redirect_count: 'Redirects'}, { booking_count: 'Bookings'}, { clickthrough_rate: 'Clickthrough'}, { conversion_rate: 'Conversion'}, { scrape2book_rate: 'Look2Book'}
            ],
            body: [
                [{ date: 'Totals'}, { search_count: '0'}, { scrape_count: '211,303'}, { redirect_count: '86,640'}, { booking_count: '0'}, { clickthrough_rate: '0'}, { conversion_rate: '0'}, { scrape2book_rate: '0.41%'}],
                [{ date: '27 Jun 15'}, { search_count: '0'}, { scrape_count: '211,303'}, { redirect_count: '0'}, { booking_count: '0'}, { clickthrough_rate: '0'}, { conversion_rate: '0'}, { scrape2book_rate: '0'}],
                [{ date: '22 Jun 15'}, { search_count: '0'}, { scrape_count: '0'}, { redirect_count: '17,268'}, { booking_count: '0'}, { clickthrough_rate: '0'}, { conversion_rate: '0'}, { scrape2book_rate: '0'}]
            ]
        }
    };

    beforeEach(module('fcApp.commercial'));

    beforeEach(inject(function($controller, $rootScope, $q, $timeout, _METRICS_, _trafficService_) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;
        timeout = $timeout;
        METRICS = _METRICS_;
        trafficService = _trafficService_;

        defaultMocks = {
            location: {
                search: noop
            },
            route: {
                reload: noop
            },
            csvDownloader: {
                download: noop
            },
            funnelMetricsService: {
                getSearchOptions: createPromise(true),
                postQuery: createPromise(true)
            },
            trafficService: {
                getConstants: noop,
                getSearchFilterOptions: noop,
                getFilterDropdownSettings: noop,
                getActiveFilters: noop,
                parseQueryStringParameters: noop,
                makeFilterQueryStringParameters: noop,
                validateDimension: noop,
                validateSegment: noop,
                validateDate: noop,
                validateFilters: noop,
                validateView: noop,
                validateUrlParams: noop,
                getExcludedFilterList: noop,
                getMetricsSummary: noop,
                getMetricsData: noop,
                getChart: noop,
                getChartData: noop,
                updateChart: noop,
                getTableData: noop,
            },
            localStorageService: {
                get: function() {
                    return 'aaaa';
                }
            },
            mixpanelService: {
                track: noop
            }
        };
    }));

    it('should initialize defaults', function() {
        var ctrl = createController();

        expect(ctrl.calendarDateFormat).toEqual('DD MMM YYYY');
        expect(ctrl.metricsData.staticTableData).toEqual([]);
        expect(ctrl.feedback).toEqual({});

        expect(typeof ctrl.applyFilters).toBe('function');
        expect(typeof ctrl.onFilterChanged).toBe('function');
        expect(typeof ctrl.onDimensionChanged).toBe('function');
        expect(typeof ctrl.onStartDateChanged).toBe('function');
        expect(typeof ctrl.switchMetricView).toBe('function');
        expect(typeof ctrl.download).toBe('function');
        expect(typeof ctrl.scrollTable).toBe('function');
    });

    describe('populate filters', function() {
        it('should make ajax call to GET searchOptions', function() {
            spyOn(defaultMocks.funnelMetricsService, 'getSearchOptions').and.callThrough();

            createController();

            expect(defaultMocks.funnelMetricsService.getSearchOptions).toHaveBeenCalledWith({
                website_id: 'aaaa',
                cache: true
            });
        });

        describe('filters returned', function() {
            var fakeSearchOptions;
            var fakeActiveFilters;

            beforeEach(function() {
                fakeSearchOptions = window._.clone(fakeVariables.searchOptions, true);
                fakeActiveFilters = window._.clone(fakeVariables.activeSearchFilters, true);

                var dfr = q.defer();
                dfr.resolve(fakeSearchOptions);

                spyOn(defaultMocks.funnelMetricsService, 'getSearchOptions').and.returnValue(dfr.promise);
                spyOn(defaultMocks.trafficService, 'getActiveFilters').and.returnValue(fakeActiveFilters);
            });

            it('should store the values to local constants variable for reference', function() {
                spyOn(defaultMocks.trafficService, 'getConstants').and.callThrough();

                createController();
                scope.$digest();

                expect(defaultMocks.trafficService.getConstants).toHaveBeenCalledWith(fakeSearchOptions);
            });

            it('should populate the options into a local searchOptions variable', function() {
                spyOn(defaultMocks.trafficService, 'getSearchFilterOptions').and.returnValue('searchOptions');

                var ctrl = createController();
                scope.$digest();

                expect(ctrl.searchOptions).toEqual('searchOptions');
                expect(defaultMocks.trafficService.getSearchFilterOptions).toHaveBeenCalled();
            });

            it('should populate the filters with extra settings required by 3rd party directive', function() {
                spyOn(defaultMocks.trafficService, 'getFilterDropdownSettings').and.returnValue('filterSettings');

                var ctrl = createController();
                scope.$digest();

                expect(ctrl.searchOptions.filters).toEqual('filterSettings');
                expect(defaultMocks.trafficService.getFilterDropdownSettings).toHaveBeenCalled();
            });

            it('should parse querystring parameters', function() {
                spyOn(defaultMocks.trafficService, 'parseQueryStringParameters');

                var fakeParams = {
                    dimension: 'test'
                };

                createController(angular.extend(defaultMocks, {
                    location: {
                        search: function() {
                            return fakeParams;
                        }
                    }
                }));
                scope.$digest();

                expect(defaultMocks.trafficService.parseQueryStringParameters).toHaveBeenCalled();
                expect(defaultMocks.trafficService.parseQueryStringParameters.calls.mostRecent().args[0]).toEqual(fakeParams);
            });

            it('should validate the querystring parameters', function() {
                spyOn(defaultMocks.trafficService, 'validateUrlParams').and.returnValue('validUrlParams');

                createController();
                scope.$digest();

                expect(defaultMocks.trafficService.validateUrlParams).toHaveBeenCalled();
            });

            it('should populate active search filters', function() {
                spyOn(defaultMocks.trafficService, 'getSearchFilterOptions').and.returnValue('searchOptions');

                createController();
                scope.$digest();

                expect(defaultMocks.trafficService.getActiveFilters).toHaveBeenCalled();
            });

            it('should assign active view', function() {
                spyOn(defaultMocks.trafficService, 'validateUrlParams').and.returnValue({
                    view: 'testView'
                });

                var ctrl = createController();
                scope.$digest();

                expect(ctrl.activeSearchFilters.view).toEqual('testView');
            });
        });

        describe('filters not returned', function() {
            it('should display feedback to user', function() {
                var defer = q.defer();
                spyOn(defaultMocks.funnelMetricsService, 'getSearchOptions').and.returnValue(defer.promise);
                defer.reject();

                var ctrl = createController();
                scope.$digest();

                expect(ctrl.feedback.area).toEqual('filters');
                expect(ctrl.feedback.message.length).toBeGreaterThan(0);
            });
        });
    });

    describe('query data', function() {
        var fakeSearchOptions;
        var fakeActiveFilters;

        beforeEach(function() {
            fakeSearchOptions = window._.clone(fakeVariables.searchOptions, true);
            fakeActiveFilters = window._.clone(fakeVariables.activeSearchFilters, true);

            var dfr = q.defer();
            dfr.resolve(fakeSearchOptions);

            spyOn(defaultMocks.funnelMetricsService, 'getSearchOptions').and.returnValue(dfr.promise);
            spyOn(defaultMocks.trafficService, 'getActiveFilters').and.returnValue(fakeActiveFilters);
        });

        it('should collect a list of unselected filters', function() {
            spyOn(defaultMocks.trafficService, 'getExcludedFilterList').and.returnValue('excludedFilterList');

            var ctrl = createController();
            scope.$digest();

            expect(ctrl.activeSearchFilters.excludeFilters).toEqual('excludedFilterList');
            expect(defaultMocks.trafficService.getExcludedFilterList).toHaveBeenCalled();
        });

        it('should make an ajax call to POST query', function() {
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.callThrough();

            createController();
            scope.$digest();

            var lastCall = defaultMocks.funnelMetricsService.postQuery.calls.mostRecent().args[0];

            expect(defaultMocks.funnelMetricsService.postQuery).toHaveBeenCalled();
            expect(lastCall.start_timestamp).toEqual(1434844800);
            expect(lastCall.end_timestamp).toEqual(1435449600);
            expect(lastCall.dimensions).toBeDefined();
            expect(lastCall.segments).toBeDefined();
            expect(lastCall.exclude_filters).toBeDefined();
            expect(lastCall.format).toBeDefined();
        });

        describe('data returned', function() {
            var fakeMetricsData;
            var fakeTableData;
            var fakeSummary;

            beforeEach(function() {
                fakeTableData = window._.clone(fakeVariables.tableData);
                fakeMetricsData = {
                    summary: {},
                    dimensions: [{}]
                };
                fakeSummary = [{
                    id: 'redirect_count',
                    code: 'redirects',
                    value: '87,277',
                    active: true
                }, {
                    value: '187,277',
                    code: 'apiqueries',
                    type: 'count',
                    relationship: ['bookings']
                }];

                var defer = q.defer();
                defer.resolve(fakeMetricsData);

                spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(defer.promise);
                spyOn(defaultMocks.trafficService, 'getTableData').and.returnValue(fakeTableData);
                spyOn(defaultMocks.trafficService, 'getMetricsSummary').and.returnValue(fakeSummary);
            });

            it('should populate the metrics summary', function() {
                var ctrl = createController();
                scope.$digest();

                expect(ctrl.metricsData.summary).toEqual(fakeSummary);
                expect(defaultMocks.trafficService.getMetricsSummary).toHaveBeenCalled();
            });

            describe('chart', function() {
                it('should draw an empty chart', function() {
                    spyOn(defaultMocks.trafficService, 'getChartData').and.returnValue({});

                    createController();
                    scope.$digest();

                    expect(defaultMocks.trafficService.getChartData).toHaveBeenCalled();
                });

                it('should load data into the chart', function() {
                    spyOn(defaultMocks.trafficService, 'updateChart').and.returnValue({});

                    var ctrl = createController();
                    scope.$digest();

                    expect(ctrl.metricsData.chartView).toBeDefined();
                    expect(defaultMocks.trafficService.updateChart).toHaveBeenCalled();
                });

                it('should cache the chart data', function() {
                    var ctrl = createController();
                    scope.$digest();

                    var defaultViewCode = ctrl.activeSearchFilters.view.code;
                    var anotherView = {
                        value: '187,277',
                        code: 'apiqueries',
                        relationship: ['bookings']
                    };

                    expect(ctrl.metricsData.chartView[defaultViewCode]).toBeDefined();
                    expect(ctrl.metricsData.chartView[anotherView.code]).not.toBeDefined();

                    ctrl.switchMetricView(anotherView);

                    scope.$digest();
                    expect(ctrl.metricsData.chartView[defaultViewCode]).toBeDefined();
                    expect(ctrl.metricsData.chartView[anotherView.code]).toBeDefined();
                });
            });

            describe('table', function() {
                it('should populate table', function() {
                    var ctrl = createController();
                    scope.$digest();

                    expect(ctrl.metricsData.tableView).toBeDefined();
                    expect(defaultMocks.trafficService.getTableData).toHaveBeenCalled();
                });

                it('should populate staticTableData', function() {
                    var ctrl = createController();
                    scope.$digest();

                    expect(ctrl.metricsData.staticTableData.length).toBeGreaterThan(1);
                });

                it('should cache the table by the code "all" if there is no segment', function() {
                    spyOn(defaultMocks.trafficService, 'getMetricsData').and.returnValue({
                        segment: undefined
                    });

                    var ctrl = createController();
                    scope.$digest();

                    expect(ctrl.metricsData.tableView.all).toBeDefined();
                });

                it('should populate table header and body separately', function() {
                    var ctrl = createController();
                    scope.$digest();

                    expect(ctrl.metricsData.tableView.all.header).toBeDefined();
                    expect(ctrl.metricsData.tableView.all.body).toBeDefined();
                });

                it('should cache the table data by view code, only if there is segment', function() {
                    spyOn(defaultMocks.trafficService, 'getMetricsData').and.returnValue({
                        segment: 'market'
                    });

                    var ctrl = createController();
                    scope.$digest();

                    var anotherView = {
                        value: '187,277',
                        code: 'apiqueries',
                        type: 'count',
                        relationship: ['bookings']
                    };

                    expect(ctrl.metricsData.tableView[anotherView.code]).not.toBeDefined();

                    ctrl.switchMetricView(anotherView);

                    scope.$digest();
                    expect(ctrl.metricsData.tableView[anotherView.code]).toBeDefined();
                });
            });
        });

        describe('data not returned', function() {
            it('should display feedback to user', function() {
                var defer = q.defer();
                spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(defer.promise);
                defer.reject();

                var ctrl = createController();
                scope.$digest();

                expect(ctrl.feedback.area).toEqual('chart');
                expect(ctrl.feedback.message.length).toBeGreaterThan(0);
            });
        });

        describe('empty data returned', function() {
            it('should display feedback to user', function() {
                var defer = q.defer();
                spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(defer.promise);
                defer.resolve({
                    dimensions: [],
                    summary: {}
                });

                var ctrl = createController();
                scope.$digest();

                expect(ctrl.feedback.area).toEqual('chart');
                expect(ctrl.feedback.message.length).toBeGreaterThan(0);
            });
        });
    });

    describe('#applyFilters', function() {
        var fakeActiveFilters;
        var fakeSearchOptions;

        beforeEach(function() {
            fakeSearchOptions = window._.clone(fakeVariables.searchOptions, true);

            fakeActiveFilters = {
                dimension: { code: 'date' },
                segment: { code: 'market' },
                startDate: new Date(),
                endDate: new Date(),
            };
        });

        it('should update the page querystring if filters are changed', function() {
            spyOn(defaultMocks.location, 'search');

            var ctrl = createController();

            ctrl.fakeSearchOptions = fakeSearchOptions;
            ctrl.activeSearchFilters = fakeActiveFilters;
            ctrl.applyFilters();

            scope.$digest();

            var lastCall = defaultMocks.location.search.calls.mostRecent().args[0];

            expect(defaultMocks.location.search).toHaveBeenCalled();
            expect(lastCall.dimension).toEqual('date');
            expect(lastCall.segment).toEqual('market');
            expect(lastCall.sdate).toBeDefined();
            expect(lastCall.edate).toBeDefined();
        });

        it('should reload the page if the querystring remains the same', function() {
            var fakeParams = {
                dimension: 'date',
                segment: 'market',
                sdate: '2015-04-05',
                edate: '2015-04-06'
            };

            spyOn(defaultMocks.location, 'search').and.returnValue(fakeParams);
            spyOn(defaultMocks.route, 'reload');

            var ctrl = createController();

            ctrl.fakeSearchOptions = fakeSearchOptions;
            ctrl.activeSearchFilters = {
                dimension: { code: 'date' },
                segment: { code: 'market' },
                startDate: '5 Apr 2015',
                endDate: '6 Apr 2015'
            };

            ctrl.applyFilters();

            scope.$digest();

            expect(defaultMocks.route.reload).toHaveBeenCalled();
        });

        it('should parse filters to querystring-friendly format', function() {
            spyOn(defaultMocks.location, 'search');
            spyOn(defaultMocks.trafficService, 'makeFilterQueryStringParameters').and.returnValue({
                someFilter: 'test'
            });

            var ctrl = createController();

            ctrl.fakeSearchOptions = fakeSearchOptions;
            ctrl.activeSearchFilters = fakeActiveFilters;
            ctrl.applyFilters();

            scope.$digest();

            var lastCall = defaultMocks.location.search.calls.mostRecent().args[0];

            expect(defaultMocks.trafficService.makeFilterQueryStringParameters).toHaveBeenCalled();
            expect(defaultMocks.location.search).toHaveBeenCalled();
            expect(lastCall.someFilter).toEqual('test');
        });
    });

    describe('#onFilterChanged', function() {
        it('should update the current activeSearchFilters', function() {
            var ctrl = createController();

            ctrl.activeSearchFilters = {
                city: 'buda',
                town: 'pest'
            };

            ctrl.onFilterChanged('budapest', 'cityTown');

            expect(ctrl.activeSearchFilters.cityTown).toEqual('budapest');
        });

        it('should unlock the filters', function() {
            var ctrl = createController();

            ctrl.activeSearchFilters = {
                city: 'buda',
                town: 'pest'
            };

            ctrl.onFilterChanged('Buda', 'city');

            expect(ctrl.activeSearchFilters.city).toEqual('Buda');
            expect(ctrl.activeSearchFilters.isDisabled).toEqual(false);
        });

        it('should not update the current activeSearchFilters if the value is the same', function() {
            var ctrl = createController();

            ctrl.activeSearchFilters = {
                city: 'buda',
                town: 'pest',
                isDisabled: true
            };

            ctrl.onFilterChanged('buda', 'city');

            expect(ctrl.activeSearchFilters.isDisabled).toEqual(true);
        });
    });

    describe('#onDimensionChanged', function() {
        var fakeActiveFilters;
        var fakeSearchOptions;
        var ctrl;

        beforeEach(function() {
            fakeActiveFilters = window._.clone(fakeVariables.activeSearchFilters, true);
            fakeSearchOptions = {
                segment: [{
                    code: 'none',
                }, {
                    code: 'buda',
                }, {
                    code: 'pest',
                }]
            };

            ctrl = createController();

            ctrl.searchOptions = fakeSearchOptions;
            ctrl.activeSearchFilters = fakeActiveFilters;
        });

        describe('value is the same as segment', function() {
            beforeEach(function() {
                ctrl.activeSearchFilters.segment = {
                    code: 'buda'
                };

                ctrl.onDimensionChanged({
                    code: 'buda'
                });
            });

            it('should set the segment to first value', function() {
                expect(ctrl.activeSearchFilters.segment).toEqual(fakeSearchOptions.segment[0]);
            });

            it('should disable the segment option', function() {
                expect(ctrl.searchOptions.segment[1].disabled).toEqual(true);
            });
        });

        describe('value is not the same as segment', function() {
            beforeEach(function() {
                ctrl.activeSearchFilters.segment = {
                    code: 'pest'
                };

                ctrl.onDimensionChanged({
                    code: 'buda'
                });
            });

            it('should re-enable all other segment values but the one that matches the dimension', function() {
                ctrl.searchOptions.segment[0].disable = true;
                ctrl.searchOptions.segment[1].disable = false;
                ctrl.searchOptions.segment[2].disable = true;

                expect(ctrl.searchOptions.segment[0].disabled).toEqual(false);
                expect(ctrl.searchOptions.segment[2].disabled).toEqual(false);
                expect(ctrl.searchOptions.segment[1].disabled).toEqual(true);
            });
        });
    });

    describe('#onStartDateChanged', function() {
        it('should update the end date range to follow the sliding nature of start date', function() {
            var ctrl = createController();

            ctrl.searchOptions = {
                startDate: {
                    start: '01 Jan 2015',
                    end: '30 Jan 2015'
                },
                endDate: {
                    start: '02 Jan 2015',
                    end: '31 Jan 2015'
                }
            };

            ctrl.activeSearchFilters = {
                startDate: '15 Jan 2015'
            };

            ctrl.onStartDateChanged();

            expect(ctrl.searchOptions.endDate.start).toEqual('15 Jan 2015');
        });
    });

    describe('#switchMetricView', function() {
        var fakeViews;

        beforeEach(function() {
            fakeViews = [{
                    code: 'searches',
                    active: false,
                    relationship: ['clickthrough']
                }, {
                    code: 'apiqueries',
                    active: false,
                    relationship: ['look2book']
                }, {
                    code: 'redirects',
                    active: false,
                    relationship: ['clickthrough', 'conversion']
                }, {
                    code: 'bookings',
                    active: false,
                    relationship: ['look2book', 'conversion']
                }, {
                    code: 'clickthrough',
                    active: false,
                    relationship: ['searches', 'redirects']
                }, {
                    code: 'conversion',
                    active: false,
                    relationship: ['redirects', 'bookings']
                }, {
                    code: 'look2book',
                    active: false,
                    relationship: ['apiqueries', 'bookings']
                }
            ];
        });

        it('should return false if the passed view is already active', function() {
            var testView = {
                code: 'apiqueries',
                relationship: ['look2book'],
                active: true
            };

            var ctrl = createController();

            ctrl.metricsData = {
                summary: window._.clone(fakeViews)
            };

            var result = ctrl.switchMetricView(testView);

            expect(result).toEqual(false);
        });

        it('should return false if the passed view is disabled', function() {
            var testView = {
                code: 'apiqueries',
                relationship: ['look2book'],
                disabled: true
            };

            var ctrl = createController();

            ctrl.metricsData = {
                summary: window._.clone(fakeViews)
            };

            var result = ctrl.switchMetricView(testView);

            expect(result).toEqual(false);
        });

        it('should set the new view as active and assign it as the current view', function() {
            var testView = {
                code: 'bookings',
                relationship: ['look2book', 'conversion']
            };

            var ctrl = createController();

            ctrl.activeSearchFilters = {};
            ctrl.metricsData = {
                summary: window._.clone(fakeViews),
                chartView: {},
                _normalizedData: {}
            };

            ctrl.switchMetricView(testView);

            expect(ctrl.activeSearchFilters.view).toEqual(testView);
            expect(ctrl.activeSearchFilters.view.active).toEqual(true);
        });

        it('should reset the current active metric to inactive', function() {
            var testView = {
                code: 'apiqueries',
                relationship: ['look2book']
            };

            var ctrl = createController();

            ctrl.activeSearchFilters = {};
            ctrl.metricsData = {
                summary: window._.clone(fakeViews),
                chartView: {},
                _normalizedData: {}
            };
            ctrl.metricsData.summary[3].active = true;

            ctrl.switchMetricView(testView);

            expect(ctrl.metricsData.summary[3].active).toEqual(false);
        });

        it('should highlight the other metrics related to the new active view', function() {
            var testView = {
                code: 'apiqueries',
                relationship: ['look2book']
            };

            var ctrl = createController();

            ctrl.activeSearchFilters = {};
            ctrl.metricsData = {
                summary: window._.clone(fakeViews),
                chartView: {},
                _normalizedData: {}
            };

            ctrl.switchMetricView(testView);

            expect(ctrl.metricsData.summary[6].highlighted).toEqual(true);
        });

        it('should redraw chart', function() {
            var testView = {
                code: 'apiqueries',
                relationship: ['look2book']
            };

            var ctrl = createController();

            ctrl.activeSearchFilters = {};
            ctrl.metricsData = {
                summary: window._.clone(fakeViews),
                chartView: {},
                _normalizedData: {}
            };

            ctrl.switchMetricView(testView);

            expect(ctrl.metricsData.chartView.apiqueries).toBeDefined();
        });

        it('should re-populate table if segment is present', function() {
            spyOn(defaultMocks.trafficService, 'getTableData').and.returnValue({
                body: []
            });

            var testView = {
                code: 'apiqueries',
                relationship: ['look2book']
            };

            var ctrl = createController();

            ctrl.activeSearchFilters = {};
            ctrl.metricsData = {
                summary: window._.clone(fakeViews),
                chartView: {},
                tableView: {},
                staticTableData: [],
                _normalizedData: {},
                _segmentCode: 'segment'
            };

            ctrl.switchMetricView(testView);

            expect(ctrl.metricsData.tableView.apiqueries).toBeDefined();
        });
    });

    describe('#download', function() {
        var fakeActiveFilters;
        var fakeSearchOptions;

        beforeEach(function() {
            fakeSearchOptions = window._.clone(fakeVariables.searchOptions, true);
            fakeActiveFilters = window._.clone(fakeVariables.activeSearchFilters, true);
        });

        it('should set loading indicator', function() {
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.callThrough();

            var ctrl = createController();
            ctrl.activeSearchFilters = fakeActiveFilters;
            ctrl.searchOptions = fakeSearchOptions;
            ctrl.download();

            expect(ctrl.activeSearchFilters.isLoadingDownload).toEqual(true);
        });

        it('should make an ajax call to POST query with a CSV format', function() {
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.callThrough();

            var ctrl = createController();
            ctrl.activeSearchFilters = fakeActiveFilters;
            ctrl.searchOptions = fakeSearchOptions;
            ctrl.download();

            var lastCall = defaultMocks.funnelMetricsService.postQuery.calls.mostRecent().args[0];

            expect(defaultMocks.funnelMetricsService.postQuery).toHaveBeenCalled();
            expect(lastCall.format).toEqual('csv');
        });

        it('should construct a csv', function() {
            var defer = q.defer();
            defer.resolve('testData');
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(defer.promise);
            spyOn(defaultMocks.csvDownloader, 'download');

            var ctrl = createController();
            ctrl.activeSearchFilters = fakeActiveFilters;
            ctrl.activeSearchFilters.view = {
                code: 'test'
            };
            ctrl.searchOptions = fakeSearchOptions;
            ctrl.download();
            timeout.flush();

            var lastCall = defaultMocks.csvDownloader.download.calls.mostRecent().args;

            expect(defaultMocks.csvDownloader.download).toHaveBeenCalled();
            expect(lastCall[0]).toEqual('testData');
            expect(lastCall[1].indexOf('.csv')).toBeGreaterThan(-1);
        });

        it('should display feedback to user', function() {
            var defer = q.defer();
            defer.reject();
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(defer.promise);

            var ctrl = createController();
            ctrl.activeSearchFilters = fakeActiveFilters;
            ctrl.searchOptions = fakeSearchOptions;
            ctrl.download();
            scope.$digest();

            expect(ctrl.feedback.area).toEqual('table');
            expect(ctrl.feedback.message.length).toBeGreaterThan(0);
        });

        it('should remove loading indicator', function() {
            var defer = q.defer();
            defer.resolve();
            spyOn(defaultMocks.funnelMetricsService, 'postQuery').and.returnValue(defer.promise);

            var ctrl = createController();
            ctrl.activeSearchFilters = fakeActiveFilters;
            ctrl.activeSearchFilters.view = {
                code: 'test'
            };
            ctrl.searchOptions = fakeSearchOptions;
            ctrl.download();
            timeout.flush();

            expect(ctrl.activeSearchFilters.isLoadingDownload).toEqual(false);
        });
    });

    describe('#scrollTable', function() {
        it('should append rows to the current table', function() {
            var ctrl = createController();

            ctrl.metricsData = {
                tableView: {},
                staticTableData:[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                _tableData: {
                    body: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
                }
            };

            ctrl.activeSearchFilters = {
                view: {
                    code: 'test'
                }
            };

            ctrl.scrollTable();

            expect(ctrl.metricsData.staticTableData.length).toEqual(20);
        });
    });
});

}());
