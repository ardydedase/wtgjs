(function() {

'use strict';

describe('Controller: RoutesCtrl', function() {
    var controller;
    var defaultMocks;
    var q;
    var timeout;
    var scope;

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('RoutesCtrl', {
            $scope: scope,
            authService: mocks.authService,
            localStorageService: mocks.localStorageService,
            websiteIdService: mocks.websiteIdService,
            routeService: mocks.routeService,
            mixpanelService: mocks.mixpanelService,
            _: window._,
            moment: window.moment
        });
    };

    var createPromise = function() {
        return function() {
            var dfr = q.defer();
            dfr.resolve({
                status: 200,
                data: []
            });
            return dfr.promise;
        };
    };

    beforeEach(module('fcApp.commercial'));

    beforeEach(inject(function($controller, $rootScope, $q, $timeout) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;
        timeout = $timeout;

        defaultMocks = {
            authService: {
                redirectFromCommercialPage: function() {}
            },
            localStorageService: {
                get: function() {
                    return 'aaaa';
                }
            },
            websiteIdService: {
                getMarkets: createPromise()
            },
            routeService: {
                getDestinations: createPromise(),
                getOrigins: createPromise()
            },
            mixpanelService: {
                track: function() {}
            }
        };
    }));

    it('should initialize defaults', function() {
        createController();

        expect(scope.originList).toEqual([]);
        expect(scope.markets.length).toEqual(1);
        expect(scope.markets[0].id).toEqual('none');
        expect(scope.selectedMarket).toEqual(scope.markets[0]);

        expect(typeof scope.getDestinationList).toBe('function');
        expect(typeof scope.getOriginList).toBe('function');
        expect(typeof scope.viewMore).toBe('function');
        expect(typeof scope.clearTooltip).toBe('function');
        expect(typeof scope.registerTRInterest).toBe('function');
    });

    it('should redirect user if user has no commercial permission', function() {
        spyOn(defaultMocks.authService, 'redirectFromCommercialPage');
        createController();

        expect(defaultMocks.authService.redirectFromCommercialPage).toHaveBeenCalled();
    });

    describe('getMarkets', function() {
        it('should clear feedback', function() {
            createController();

            expect(scope.feedback).toEqual(null);
        });

        it('should make ajax call to GET markets', function() {
            spyOn(defaultMocks.websiteIdService, 'getMarkets').and.callThrough();

            createController();

            expect(defaultMocks.websiteIdService.getMarkets).toHaveBeenCalledWith({
                websiteId: 'aaaa',
                cache: true
            });
        });

        describe('market list exists', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.websiteIdService, 'getMarkets').and.returnValue(dfr.promise);

                dfr.resolve([{
                    id: 'PO',
                    name: 'Polynesian Islands'
                }]);
            });

            it('should append markets to market dropdown if market list exists', function() {
                createController();
                scope.$digest();

                expect(scope.markets.length).toEqual(2);
                expect(scope.markets[1].id).toEqual('PO');
            });

            it('should enable the dropdown for user to change the market', function() {
                createController();
                scope.$digest();

                expect(scope.isMarketDisabled).toEqual(false);
            });

            it('should update the dropdown placeholder text', function() {
                createController();
                scope.$digest();

                expect(scope.markets[0].name).toEqual('-- Please select a market --');
            });

            it('should select the only market if market list returns only 1', function() {
                createController();
                scope.$digest();

                expect(scope.selectedMarket.id).toEqual('PO');
            });
        });

        describe('market list is empty', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.websiteIdService, 'getMarkets').and.returnValue(dfr.promise);

                dfr.resolve([]);
            });

            it('should disable the dropdown because there is no market to select', function() {
                createController();
                scope.$digest();

                expect(scope.isMarketDisabled).toEqual(true);
            });

            it('should update the dropdown placeholder text', function() {
                createController();
                scope.$digest();

                expect(scope.markets[0].name).toEqual('Markets unavailable');
            });

            it('should provide feedback to user', function() {
                createController();
                scope.$digest();

                expect(scope.feedback.message.length).toBeGreaterThan(0);
            });
        });

        describe('GET request fails', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.websiteIdService, 'getMarkets').and.returnValue(dfr.promise);

                dfr.reject();
            });

            it('should disable the dropdown because there is no market to select', function() {
                createController();
                scope.$digest();

                expect(scope.isMarketDisabled).toEqual(true);
            });

            it('should update the dropdown placeholder text', function() {
                createController();
                scope.$digest();

                expect(scope.markets[0].name).toEqual('Markets unavailable');
            });

            it('should provide feedback to user', function() {
                createController();
                scope.$digest();

                expect(scope.feedback.message.length).toBeGreaterThan(0);
                expect(scope.feedback.type).toEqual('danger');
            });
        });
    });

    describe('#getDestinationList', function() {
        var fakeMarket;

        beforeEach(function() {
            fakeMarket = {
                id: 'NY',
                name: 'New York (NY)'
            };
        });

        it('should not do anything if market is not passed', function() {
            createController();

            scope.getDestinationList();

            expect(scope.destinationListPromise).not.toBeDefined();
        });

        it('should not do anything if market passed is a placeholder text', function() {
            createController();

            fakeMarket = {
                id: 'none',
                name: 'Please select a market or...'
            };

            scope.getDestinationList(fakeMarket);

            expect(scope.destinationListPromise).not.toBeDefined();
        });

        it('should not do anything if market passed is the current active list\'s market', function() {
            createController();

            scope.listMarketId = 'NY';
            scope.getDestinationList(fakeMarket);

            expect(scope.destinationListPromise).not.toBeDefined();
        });

        it('should disable the market dropdown while retrieving data', function() {
            createController();

            scope.getDestinationList(fakeMarket);

            expect(scope.isMarketDisabled).toEqual(true);
        });

        it('should clear feedback', function() {
            createController();

            scope.getDestinationList(fakeMarket);

            expect(scope.feedback).toEqual(null);
        });

        it('should clear data collection period dates', function() {
            createController();

            scope.getDestinationList(fakeMarket);

            expect(scope.dataCollectionPeriod).toEqual(null);
        });

        it('should make ajax call to GET destinations', function() {
            spyOn(defaultMocks.routeService, 'getDestinations').and.callThrough();

            createController();

            scope.getDestinationList(fakeMarket);

            expect(defaultMocks.routeService.getDestinations).toHaveBeenCalledWith({
                limit: 10,
                websiteId: 'aaaa',
                market: fakeMarket.id
            });
        });

        describe('destination list exists', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.routeService, 'getDestinations').and.returnValue(dfr.promise);

                dfr.resolve({
                    data: [{
                        metrics: [{
                            wow_change: 15,
                            type: 'redirects',
                            name: 'Redirects',
                            value: 115
                        }, {
                            wow_change: -0.3,
                            type: 'sov',
                            name: 'Share of Voice',
                            value: 0.2
                        }],
                        dates: {
                            start_date: '2015-01-01',
                            end_date: '2015-01-07'
                        },
                        route: {
                            origin: {},
                            destination: {
                                name: 'Sarajevo International',
                                iata_id: 'SJJ',
                                routenode_id: '16314'
                            }
                        }
                    }]
                });
            });

            it('should format destinationList', function() {
                var expectedFormattedDestination = {
                    iata: 'SJJ',
                    routeId: '16314',
                    name: 'Sarajevo International',
                    redirects: 115,
                    wow_redirects: 15,
                    sov: 0.2,
                    wow_sov: -60
                };

                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.destinationList[0]).toEqual(jasmine.objectContaining(expectedFormattedDestination));
            });

            it('should set listMarketId with the selected market id to indicate active market data is populated', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.listMarketId).toEqual('NY');
            });

            it('should set default values for UI interaction', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.destinationList[0].ui).toBeDefined();
                expect(scope.destinationList[0].ui.isHeaderClicked).toEqual(false);
                expect(scope.destinationList[0].ui.tooltipTimeout).toBeDefined();
            });

            it('should show and set default settings for the Load More Destinations button', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.isViewMoreClicked).toEqual(false);
                expect(scope.showTooltip).toEqual(false);
                expect(scope.tooltipTimeout).toBeGreaterThan(0);
            });

            it('should assign start date and end date to dataCollectionPeriod', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.dataCollectionPeriod).toBeDefined();
                expect(scope.dataCollectionPeriod.startDate).toEqual('01 January 2015');
                expect(scope.dataCollectionPeriod.endDate).toEqual('07 January 2015');
            });

            it('should re-enable the market dropdown', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.isMarketDisabled).toEqual(false);
            });

            it('should not do another ajax call if the current list is populated with selected market', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();
                expect(scope.destinationList.length).toEqual(1);

                scope.getDestinationList(fakeMarket);
                expect(scope.isMarketDisabled).toEqual(false);  //can only assert something that is guaranteed unchanged
            });
        });

        describe('destination list is empty', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.routeService, 'getDestinations').and.returnValue(dfr.promise);

                dfr.resolve({
                    data: []
                });
            });

            it('should provide feedback to user', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.feedback.message.length).toBeGreaterThan(0);
            });

            it('should clear previous destination list', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                scope.$digest();

                expect(scope.destinationList.length).toEqual(0);
            });
        });

        describe('GET request fails', function() {
            var dfr;

            beforeEach(function() {
                dfr = q.defer();
                spyOn(defaultMocks.routeService, 'getDestinations').and.returnValue(dfr.promise);
            });

            it('should provide specific feedback if user has no commercial permission', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                dfr.reject({
                    status: 403
                });
                scope.$digest();

                expect(scope.feedback.message.length).toBeGreaterThan(0);
                expect(scope.feedback.type).toEqual('danger');
            });

            it('should provide generic feedback for non-403 error', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                dfr.reject({
                    status: 500
                });
                scope.$digest();

                expect(scope.feedback.message.length).toBeGreaterThan(0);
                expect(scope.feedback.type).toEqual('danger');
            });

            it('should re-enable the market dropdown', function() {
                createController();

                scope.getDestinationList(fakeMarket);
                dfr.reject({
                    status: 500
                });
                scope.$digest();

                expect(scope.isMarketDisabled).toEqual(false);
            });
        });
    });

    describe('#getOriginList', function() {
        var fakeDestinationId;
        var fakeDestination;

        beforeEach(function() {
            fakeDestinationId = '19384';

            fakeDestination = {
                routeId: fakeDestinationId,
                hasOriginList: false,
                ui: {}
            };

            scope.destinationList = [fakeDestination];
        });

        it('should return false if origins exist for given destination', function() {
            createController();

            scope.destinationList[0].hasOriginList = true;

            expect(scope.getOriginList(fakeDestinationId)).toEqual(false);
        });

        it('should set ui.isHeaderClicked to true for ajax blocking', function() {
            createController();

            scope.getOriginList(fakeDestinationId);

            expect(fakeDestination.ui.isHeaderClicked).toEqual(true);
        });

        it('should clear feedback', function() {
            createController();

            scope.getOriginList(fakeDestinationId);

            expect(scope.feedback).toEqual(null);
        });

        it('should make ajax call to GET origins', function() {
            spyOn(defaultMocks.routeService, 'getOrigins').and.callThrough();

            createController();
            scope.selectedMarket = {
                id: 'BF'
            };

            scope.getOriginList(fakeDestinationId);

            expect(defaultMocks.routeService.getOrigins).toHaveBeenCalledWith({
                limit: 5,
                websiteId: 'aaaa',
                market: 'BF',
                destination: fakeDestinationId
            });
        });

        describe('origin list exists', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.routeService, 'getOrigins').and.returnValue(dfr.promise);

                dfr.resolve({
                    data: [{
                        metrics: [{
                            wow_change: -20,
                            type: 'redirects',
                            name: 'Redirects',
                            value: 60
                        }, {
                            wow_change: 0.06,
                            type: 'sov',
                            name: 'Share of Voice',
                            value: 0.56
                        }],
                        route: {
                            destination: {},
                            origin: {
                                name: 'Sarajevo International',
                                iata_id: 'SJJ',
                                routenode_id: '16314'
                            }
                        }
                    }]
                });
            });

            it('should append a formatted origin object to originList', function() {
                var expectedFormattedOrigin = {
                    destination: fakeDestinationId,
                    origins: [{
                        iata: 'SJJ',
                        routeId: '16314',
                        name: 'Sarajevo International',
                        redirects: 60,
                        wow_redirects: -25,
                        sov: 0.56,
                        wow_sov: 12
                    }]
                };

                createController();

                scope.getOriginList(fakeDestinationId);
                scope.$digest();

                expect(scope.originList[0].destination).toEqual(expectedFormattedOrigin.destination);
                expect(scope.originList[0].origins[0]).toEqual(jasmine.objectContaining(expectedFormattedOrigin.origins[0]));
            });

            it('should set destination.hasOriginList to true', function() {
                createController();

                scope.getOriginList(fakeDestinationId);
                scope.$digest();

                expect(fakeDestination.hasOriginList).toEqual(true);
            });

            it('should set ui.isHeaderClicked to false for ajax blocking', function() {
                createController();

                scope.getOriginList(fakeDestinationId);
                scope.$digest();

                expect(fakeDestination.ui.isHeaderClicked).toEqual(false);
            });
        });

        describe('GET request fails', function() {
            beforeEach(function() {
                var dfr = q.defer();
                spyOn(defaultMocks.routeService, 'getOrigins').and.returnValue(dfr.promise);

                dfr.reject();
            });

            it('should provide feedback to user', function() {
                createController();

                scope.getOriginList(fakeDestinationId);
                scope.$digest();

                expect(scope.feedback.message.length).toBeGreaterThan(0);
                expect(scope.feedback.type).toEqual('danger');
            });

            it('should set destination.hasOriginList to false', function() {
                createController();

                scope.getOriginList(fakeDestinationId);
                scope.$digest();

                expect(fakeDestination.hasOriginList).toEqual(false);
            });

            it('should set ui.isHeaderClicked to false for ajax blocking', function() {
                createController();

                scope.getOriginList(fakeDestinationId);
                scope.$digest();

                expect(fakeDestination.ui.isHeaderClicked).toEqual(false);
            });
        });
    });

    describe('#viewMore', function() {
        var fakeDestinationId;
        var fakeDestination;

        beforeEach(function() {
            fakeDestinationId = '19384';

            fakeDestination = {
                routeId: fakeDestinationId,
                ui: {
                    tooltipTimeout: 10
                }
            };

            scope.destinationList = [fakeDestination];
        });

        it('should define the context as scope and set scope.isViewMoreClicked to true if no destinationId is passed', function() {
            createController();

            scope.viewMore();

            expect(scope.isViewMoreClicked).toEqual(true);
        });

        it('should define the context as destination and set destination.ui.isViewMoreClicked to true to indicate button has been clicked', function() {
            createController();

            scope.viewMore(fakeDestinationId);

            expect(fakeDestination.ui.isViewMoreClicked).toEqual(true);
        });

        it('should run a fake timer before showing cross-sell tooltip', function() {
            createController();

            scope.viewMore(fakeDestinationId);
            timeout.flush();

            expect(fakeDestination.ui.isViewMoreClicked).toEqual(false);
            expect(fakeDestination.ui.showTooltip).toEqual(true);
            expect(fakeDestination.ui.tooltipTimeout).not.toEqual(10);
        });
    });

    describe('#clearTooltip', function() {
        var fakeDestinationId;
        var fakeDestination;

        beforeEach(function() {
            fakeDestinationId = '19384';

            fakeDestination = {
                routeId: fakeDestinationId,
                ui: {}
            };

            scope.destinationList = [fakeDestination];
        });

        it('should define the context as scope and clear tooltip if no destinationId is passed', function() {
            createController();

            scope.clearTooltip();

            expect(scope.showTooltip).toEqual(false);
        });

        it('should define the context as destination and clear tooltip for given destination', function() {
            createController();

            scope.clearTooltip(fakeDestinationId);

            expect(fakeDestination.ui.showTooltip).toEqual(false);
        });
    });
});

}());
