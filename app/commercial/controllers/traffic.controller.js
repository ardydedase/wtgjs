(function() {

'use strict';

angular.module('fcApp.commercial').controller('TrafficCtrl', function(
    $scope,
    $location,
    $route,
    $timeout,
    moment,
    _,
    METRICS,
    csvDownloader,
    trafficService,
    funnelMetricsService,
    localStorageService,
    mixpanelService) {

    var self = this;
    var chart;
    var chartEl = '#traffic-chart';
    var websiteId = localStorageService.get('selectedwebsite');

    var calendarDateFormat = 'DD MMM YYYY';
    var urlDateFormat = 'YYYY-MM-DD';
    var infiniteScrollingRows = 10;

    /**
     * List of map definition for the Views (Metrics)
     *  ordered by display - left to right
     *  @id:     JSON data identifier
     *  @code:   queryString identifier
     *  @title:  Friendly text for display
     *  @type:   "count" or "rate"
     */

    var _CONSTANTS = {
        view: _.clone(METRICS)
    };

    var defaultView = _CONSTANTS.view[2];

    mixpanelTracking('Page Loaded', (function() {
        var pageParams = $location.search() || {};

        var trackingData = {
            isRedirected: false
        };

        if (pageParams.src) {
            trackingData.isRedirected = true;
            trackingData.redirectSource = pageParams.src || '';
        }

        return trackingData;
    }()));

    function init() {
        self.calendarDateFormat = calendarDateFormat;
        self.metricsData = {};
        self.metricsData.staticTableData = [];
        self.feedback = {};

        var searchOptionsParams = {
            website_id: websiteId,
            cache: true
        };

        self.getFiltersPromise = funnelMetricsService.getSearchOptions(searchOptionsParams).then(function(data) {
            _CONSTANTS = _.extend(_CONSTANTS, trafficService.getConstants(data));

            self.searchOptions = trafficService.getSearchFilterOptions({
                data: data,
                dateFormat: calendarDateFormat
            });
            self.searchOptions.filters = trafficService.getFilterDropdownSettings(self.searchOptions.filters, _CONSTANTS.dictionary);

            var urlParams = trafficService.parseQueryStringParameters($location.search(), _CONSTANTS.filters);
            var validators = {
                dimension: trafficService.validateDimension.bind(self.searchOptions.dimension),
                segment: trafficService.validateSegment.bind(self.searchOptions.segment),
                startDate: trafficService.validateDate.bind(self.searchOptions.startDate),
                endDate: trafficService.validateDate.bind(self.searchOptions.endDate),
                filters: trafficService.validateFilters.bind(self.searchOptions.filters),
                view: trafficService.validateView.bind(_CONSTANTS.view)
            };

            var validUrlParams = trafficService.validateUrlParams(urlParams, validators, _CONSTANTS.filters);

            self.activeSearchFilters = trafficService.getActiveFilters({
                filters: data.filters,
                presetValues: validUrlParams,
                searchOptions: self.searchOptions,
                dateFormat: calendarDateFormat
            });

            self.activeSearchFilters.view = validUrlParams.view || defaultView;

            self.activeSearchFilters.isDisabled = true;
            self.activeSearchFilters.isLoading = true;

            requestTrafficData({
                isCsv: false,
                successCallback: showChartTable,
                failureCallback: function(err) {
                    showFeedback('chart', 'Something went wrong when retriving data.');

                    mixpanelTracking('Errors', {
                        errorCode: err ? err.status : 'unknown',
                        errorType: 'data'
                    });
                },

                doneCallback: function() {
                    self.activeSearchFilters.isLoading = false;
                    self.activeSearchFilters.isDisabled = true;
                }
            });
        },

        function(err) {
            showFeedback('filters', 'Something went wrong when retrieving filters.');

            mixpanelTracking('Errors', {
                errorCode: err ? err.status : 'unknown',
                errorType: 'filters'
            });
        });
    }

    function showFeedback(area, message) {
        self.feedback = {
            area: area,
            message: message
        };
    }

    function showChartTable(data) {
        var emptyData = false;

        if (data.dimensions && data.dimensions.length) {
            chart = trafficService.getChart(chartEl);
            self.metricsData.summary = trafficService.getMetricsSummary({
                data: data.summary,
                fieldNames: _CONSTANTS.view,
                activeViewCode: self.activeSearchFilters.view.code,
                defaultView: defaultView,
                dimension: self.activeSearchFilters.dimension.code,
                segment: self.activeSearchFilters.segment.code
            });

            self.activeSearchFilters.view = _.find(self.metricsData.summary, { active: true });
            self.metricsData._dimensionTotals = data.dimension_totals || data.summary;
            self.metricsData._segmentTotals = data.segment_totals || [];

            self.metricsData._normalizedData = trafficService.getMetricsData(data.dimensions);

            self.metricsData._segmentCode = self.metricsData._normalizedData.segment;
            self.metricsData._dimensionCode = self.metricsData._normalizedData.dimension;

            self.metricsData.chartView = {};
            drawChart(self.activeSearchFilters.view.code);

            self.metricsData.tableView = {};
            populateTable(self.activeSearchFilters.view.code);
        } else {
            showFeedback('chart', 'Data not available for the selected options.');
            emptyData = true;
        }

        mixpanelTracking('Chart Rendered', {
            segment: self.activeSearchFilters.segment.code,
            dimension: self.activeSearchFilters.dimension.code,
            metric: self.activeSearchFilters.view.code,
            emptyData: emptyData
        });
    }

    function transformRequestData(filtersData, isCsv) {
        return {
            website_id: websiteId,
            start_timestamp: moment.utc(filtersData.startDate, calendarDateFormat).unix(),
            end_timestamp: moment.utc(filtersData.endDate, calendarDateFormat).unix(),
            dimensions: [filtersData.dimension.code],
            segments: filtersData.segment.code === 'none' ? [] : [filtersData.segment.code],
            exclude_filters: filtersData.excludeFilters,
            format: isCsv ? 'csv' : 'json'
        };
    }

    function populateTable(viewCode) {
        var initialDisplayedRows;
        var initialDisplayedRowsCopy;

        if (!self.metricsData._segmentCode) {
            self.metricsData.tableView.code = 'all';
            self.metricsData.tableView.title = 'All Metrics';
        } else {
            self.metricsData.tableView.code = viewCode;
            self.metricsData.tableView.title = _.find(_CONSTANTS.view, { code: viewCode }).title;
        }

        if (!self.metricsData.tableView[self.metricsData.tableView.code]) {
            self.metricsData.tableView[self.metricsData.tableView.code] = {};

            self.metricsData._tableData = trafficService.getTableData({
                data: self.metricsData._normalizedData.chartData,
                view: self.activeSearchFilters.view.id,
                viewTotal: _.find(self.metricsData.summary, { code: viewCode }).value,
                dimension: self.metricsData._dimensionCode,
                dimensionTotals: self.metricsData._dimensionTotals,
                segmentTotals: self.metricsData._segmentTotals,
                segment: self.metricsData._segmentCode,
                fieldNames: _CONSTANTS
            });

            self.metricsData.tableView[self.metricsData.tableView.code].header = self.metricsData._tableData.header;

            initialDisplayedRows = self.metricsData._tableData.body.slice(0, infiniteScrollingRows);
            self.metricsData.tableView[self.metricsData.tableView.code].body = initialDisplayedRows;
        }

        initialDisplayedRowsCopy = self.metricsData.tableView[self.metricsData.tableView.code].body.slice(0, infiniteScrollingRows);
        self.metricsData.staticTableData = self.metricsData.staticTableData.concat(initialDisplayedRowsCopy);
    }

    function drawChart(viewCode) {
        if (!self.metricsData.chartView[viewCode]) {
            self.metricsData.chartView[viewCode] = {};

            self.metricsData.chartView._chartData = trafficService.getChartData({
                data: self.metricsData._normalizedData.chartData,
                view: self.activeSearchFilters.view.id,
                dimension: self.metricsData._dimensionCode,
                segment: self.metricsData._segmentCode,
                fieldNames: _CONSTANTS
            });

            self.metricsData.chartView[viewCode] = {};
            self.metricsData.chartView[viewCode] = self.metricsData.chartView._chartData;
        }

        self.metricsData.chartLoad = trafficService.updateChart({
            el: chartEl,
            chart: chart,
            axesLabels: self.metricsData.chartView[viewCode].axesLabels,
            groups: self.metricsData.chartView[viewCode].groups,
            names: self.metricsData.chartView[viewCode].names,
            segments: self.metricsData.chartView[viewCode].segments,
            columns: self.metricsData.chartView[viewCode].columns,
            chartType: self.metricsData.chartView[viewCode].chartType,
            xTickLabels: self.metricsData.chartView[viewCode].xTickLabels,
            currentLoad: self.metricsData.chartLoad
        });
    }

    function requestTrafficData(options) {
        self.activeSearchFilters.excludeFilters = trafficService.getExcludedFilterList(self.activeSearchFilters.includeFilters, _CONSTANTS.filters);

        var requestData = transformRequestData(self.activeSearchFilters, options.isCsv);

        mixpanelTracking('Data Requested', (function() {
            var isCsv = options.isCsv;

            function getModifiedFilters() {
                var defaults = self.searchOptions;
                var current = self.activeSearchFilters;
                var changes = {
                    hasModifiedDefaults: false,
                    filters: []
                };

                var addKey = function(key) {
                    changes.hasModifiedDefaults = true;
                    changes.filters.push(key);
                };

                _.forEach(['dimension', 'segment', 'startDate', 'endDate'], function(key, index) {
                    var comparator;

                    if (index < 2) {
                        comparator = defaults[key] ? defaults[key][0].code : '';

                        if (current[key].code !== comparator) {
                            addKey(key);
                        }
                    } else {
                        comparator = defaults[key] ? moment(defaults[key].defaut).format(calendarDateFormat) : '';

                        if (current[key] !== comparator) {
                            addKey(key);
                        }
                    }
                });

                _.forOwn(self.activeSearchFilters.excludeFilters, function(values, key) {
                    if (values.length > 0) {
                        addKey(key);
                    }
                });

                if (changes.filters && changes.filters.length) {
                    changes.filters = changes.filters.join(', ');
                } else {
                    delete changes.filters;
                }

                return changes;
            }

            var trackingData = {
                action: isCsv ? 'Download CSV' : 'Apply Filters',
                dimension: self.activeSearchFilters.dimension.code,
                segment: self.activeSearchFilters.segment.code,
                dateRangeInDays: moment(self.activeSearchFilters.endDate).diff(moment(self.activeSearchFilters.startDate), 'day')
            };

            return _.extend(trackingData, getModifiedFilters());
        }()));

        self.metricDataPromise = funnelMetricsService.postQuery(requestData)
            .then(options.successCallback, options.failureCallback)
            .finally(options.doneCallback);
    }

    function mixpanelTracking(event, trackingData) {
        var eventName = 'FC ComData ' + event;
        trackingData = trackingData || {};

        mixpanelService.track(eventName, trackingData);
    }

    this.applyFilters = function() {
        var currentParams = $location.search();

        if (chart) {
            chart = chart.destroy();
        }

        var parameters = {
            dimension: self.activeSearchFilters.dimension.code,
            segment: self.activeSearchFilters.segment.code,
            sdate: moment(self.activeSearchFilters.startDate, calendarDateFormat).format(urlDateFormat),
            edate: moment(self.activeSearchFilters.endDate, calendarDateFormat).format(urlDateFormat)
        };

        var queryStringFilters = trafficService.makeFilterQueryStringParameters(self.activeSearchFilters.includeFilters, _CONSTANTS.filters);

        parameters = _.extend(parameters, queryStringFilters);

        if (_.isEqual(currentParams, parameters)) {
            $route.reload();
        } else {
            $location.search(parameters);
        }
    };

    this.onFilterChanged = function(value, name) {
        if (self.activeSearchFilters[name] !== value) {
            self.activeSearchFilters[name] = value;
            self.activeSearchFilters.isDisabled = false;
        }
    };

    this.onDimensionChanged = function(newValue) {
        if (newValue.code === self.activeSearchFilters.segment.code) {
            self.activeSearchFilters.segment = self.searchOptions.segment[0];
            _.find(self.searchOptions.segment, { code: newValue.code }).disabled = true;
        } else {
            _.forEach(self.searchOptions.segment, function(segment) {
                segment.disabled = false;

                if (newValue.code === segment.code) {
                    segment.disabled = true;
                }
            });
        }
    };

    this.onStartDateChanged = function() {
        self.searchOptions.endDate.start = moment(self.activeSearchFilters.startDate, calendarDateFormat).format(calendarDateFormat);
    };

    this.switchMetricView = function(view) {
        if (view.active === true || view.disabled === true) {
            return false;
        }

        _.forEach(self.metricsData.summary, function(metric) {
            if (metric.active === true) {
                metric.active = false;
            }

            metric.highlighted = view.relationship.indexOf(metric.code) > -1 ? true : false;
        });

        view.active = true;
        self.activeSearchFilters.view = view;

        drawChart(view.code);

        if (self.metricsData._segmentCode) {
            populateTable(view.code);
        }

        mixpanelTracking('Data Exploration', {
            action: 'Switch View',
            metric: view.code
        });
    };

    this.download = function() {
        self.activeSearchFilters.isLoadingDownload = true;
        self.feedback = {};

        requestTrafficData({
            isCsv: true,
            successCallback: function(data) {
                var fileName = [
                    self.activeSearchFilters.dimension.name,
                    moment(self.activeSearchFilters.startDate).format(urlDateFormat),
                    '-',
                    moment(self.activeSearchFilters.endDate).format(urlDateFormat)
                ].join('_');

                $timeout(function() {
                    csvDownloader.download(data, fileName + '.csv', true);
                });

                mixpanelTracking('Data Downloaded', {
                    segment: self.activeSearchFilters.segment.code,
                    dimension: self.activeSearchFilters.dimension.code,
                    metric: self.activeSearchFilters.view.code
                });
            },

            failureCallback: function(err) {
                showFeedback('table', 'Something went wrong when preparing the requested file.');

                mixpanelTracking('Errors', {
                    errorCode: err ? err.status : 'unknown',
                    errorType: 'csv'
                });
            },

            doneCallback: function() {
                self.activeSearchFilters.isLoadingDownload = false;
            }
        });
    };

    this.scrollTable = function() {
        var lastRow;
        var rowsToAppend;

        if (self.metricsData.tableView) {
            lastRow = self.metricsData.staticTableData.length;
            rowsToAppend = self.metricsData._tableData.body.slice(lastRow, lastRow + 10);
            self.metricsData.staticTableData = self.metricsData.staticTableData.concat(rowsToAppend);

            mixpanelTracking('Data Exploration', {
                action: 'View Table',
                metric: self.activeSearchFilters.view.code,
                numberOfRecords: self.metricsData.staticTableData.length
            });
        }
    };

    // filters
    $scope.$watch(function() {
        return self.activeSearchFilters && self.activeSearchFilters.includeFilters;
    },

    function(newValue) {
        var filtered;

        if (newValue) {
            filtered = _.some(newValue, function(selectedFilterValues) {
                return selectedFilterValues.length === 0;
            });

            if (filtered) {
                self.activeSearchFilters.isDisabled = true;
            } else {
                self.activeSearchFilters.isDisabled = false;
            }
        }
    }, true);

    init();
});

}());
