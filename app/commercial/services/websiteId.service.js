(function() {

'use strict';

angular.module('fcApp.commercial').factory('websiteIdService', function(
    $http,
    $q,
    _,
    timedCacheService,
    config
    ) {

    var baseUrl = config.API_URL + config.API_BASE + '/v0.4/flights/website_ids/';

    /**
     * Get a list of markets for a particular websiteId
     * @param  {String} options.websiteId - REQUIRED
     * @param  {String} options.cache - return data from and store it to localStorage if true
     * @return {Array} [{ id: 'JP', market: 'Japan' }]
     */
    function getMarkets(options) {
        var defer;
        var url;
        var cachedMarket;
        var parameters;

        var tempCache = timedCacheService.getInstance({
            name: 'markets',
            websiteId: options.websiteId,
            cacheTime: 24,
            cacheKey: 'markets'
        });

        //PE-791
        options.websiteId  = options.websiteId === 'a001' ? 'aljp' : options.websiteId;

        //--PE-791

        if (options.cache) {
            cachedMarket = tempCache.get();

            if (cachedMarket) {
                defer = $q.defer();
                defer.resolve(cachedMarket);
                return defer.promise;
            }
        } else {
            tempCache.remove();
        }

        url = baseUrl + 'markets/';
        parameters = {
            website_id: options.websiteId
        };

        return $http({
            method: 'get',
            url: url,
            params: parameters
        }).then(function(res) {
            var markets = res.data && res.data.length ? res.data : [];

            markets = _.sortBy(markets, 'name');

            if (options.cache) {
                tempCache.save(markets);
            }

            return markets;
        });
    }

    return {
        getMarkets: getMarkets
    };
});

}());
