(function() {

'use strict';

describe('Service: routeService', function() {

    var config;
    var routeService;
    var httpBackend;
    var baseUrl;

    beforeEach(module('fcApp.commercial'));

    beforeEach(inject(function($httpBackend, _config_, _routeService_) {
        config = _config_;
        httpBackend = $httpBackend;
        routeService = _routeService_;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#getDestinations', function() {
        it('should be defined', function() {
            expect(routeService.getDestinations).toBeDefined();
        });

        it('should GET /v0.4/flights/routes/destinations/', function() {
            var options = {
                limit: 7,
                market: 'SS',
                websiteId: 'aaaa'
            };

            var url = baseUrl + '/v0.4/flights/routes/destinations/?limit=7&market=SS&website_id=aaaa';

            httpBackend.whenGET(url).respond({});

            routeService.getDestinations(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should by default set "limit" as 10 when not specified', function() {
            var options = {
                market: 'SS',
                websiteId: 'aaaa'
            };

            var url = baseUrl + '/v0.4/flights/routes/destinations/?limit=10&market=SS&website_id=aaaa';

            httpBackend.whenGET(url).respond({});

            routeService.getDestinations(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should be able to send extra "origin" parameter', function() {
            var options = {
                limit: 10,
                market: 'SS',
                websiteId: 'aaaa',
                origin: 'CC'
            };

            var url = baseUrl + '/v0.4/flights/routes/destinations/?limit=10&market=SS&origin=CC&website_id=aaaa';

            httpBackend.whenGET(url).respond({});

            routeService.getDestinations(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });
    });

    describe('#getOrigins', function() {
        it('should be defined', function() {
            expect(routeService.getOrigins).toBeDefined();
        });

        it('should GET /v0.4/flights/routes/origins/', function() {
            var options = {
                limit: 4,
                market: 'FC',
                websiteId: 'bbbb'
            };

            var url = baseUrl + '/v0.4/flights/routes/origins/?limit=4&market=FC&website_id=bbbb';

            httpBackend.whenGET(url).respond({});

            routeService.getOrigins(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should by default set "limit" as 10 when not specified', function() {
            var options = {
                market: 'FC',
                websiteId: 'bbbb'
            };

            var url = baseUrl + '/v0.4/flights/routes/origins/?limit=10&market=FC&website_id=bbbb';

            httpBackend.whenGET(url).respond({});

            routeService.getOrigins(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should be able to send extra "destination" parameter', function() {
            var options = {
                limit: 10,
                market: 'FC',
                websiteId: 'bbbb',
                destination: 'CC'
            };

            var url = baseUrl + '/v0.4/flights/routes/origins/?destination=CC&limit=10&market=FC&website_id=bbbb';

            httpBackend.whenGET(url).respond({});

            routeService.getOrigins(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });
    });
});

}());
