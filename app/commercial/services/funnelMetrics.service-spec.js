(function() {

'use strict';

describe('Service: funnelMetricsService', function() {

    var config;
    var rootScope;
    var funnelMetricsService;
    var mockTimedCacheService;
    var httpBackend;
    var baseUrl;

    beforeEach(module('fcApp.commercial'));

    beforeEach(module('fcApp.commercial', function($provide) {
        mockTimedCacheService = {
            getInstance: function() {
                return {};
            }
        };

        $provide.value('timedCacheService', mockTimedCacheService);
    }));

    beforeEach(inject(function($httpBackend, $rootScope, _config_, _funnelMetricsService_) {
        config = _config_;
        httpBackend = $httpBackend;
        rootScope = $rootScope.$new();
        funnelMetricsService = _funnelMetricsService_;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#getSearchOptions', function() {
        var fakeInstance;
        var noop = function() {};

        beforeEach(function() {
            fakeInstance = {
                remove: noop,
                get: noop,
                save: noop
            };

            spyOn(mockTimedCacheService, 'getInstance').and.returnValue(fakeInstance);
        });

        afterEach(function() {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        it('should be defined', function() {
            expect(funnelMetricsService.getSearchOptions).toBeDefined();
        });

        it('should GET /v0.4/flights/funnel_metrics/search_options/', function() {
            var url = baseUrl + '/v0.4/flights/funnel_metrics/search_options/?website_id=aaaa';

            httpBackend.whenGET(url).respond({});

            funnelMetricsService.getSearchOptions({ website_id: 'aaaa' }).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should return search options', function() {
            var expectedResponse = {
                dates: {
                    earliest_start_date: '2015-04-29T00:00:00',
                    latest_end_date: '2015-06-26T23:59:59'
                },
                field_names:{
                    carrier: 'Carrier',
                    date: 'Date',
                    market: 'Market'
                },
                segments: ['market'],
                dimensions: ['date', 'carrier', 'market'],
                filters: {
                    carrier: [{
                        code: 'nh',
                        name: 'NH'
                    }, {
                        code: 'jl',
                        name: 'JL'
                    }],
                    market: [{
                        code: 'jp',
                        name: 'JP'
                    }]
                }
            };

            var url = baseUrl + '/v0.4/flights/funnel_metrics/search_options/?website_id=aaaa';

            httpBackend.whenGET(url).respond(expectedResponse);

            funnelMetricsService.getSearchOptions({ website_id: 'aaaa' }).then(function(res) {
                expect(res).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });

        describe('cache', function() {
            var defaultOptions;

            beforeEach(function() {
                defaultOptions = {
                    website_id: 'aaaa',
                    cache: true
                };
            });

            it('should initialize an instance of TimedCache', function() {
                fakeInstance.get = function() {
                    return 'existingCachedData';
                };

                funnelMetricsService.getSearchOptions(defaultOptions);

                expect(mockTimedCacheService.getInstance).toHaveBeenCalledWith({
                    name: 'trafficConfig',
                    websiteId: 'aaaa',
                    cacheTime: 8,
                    cacheKey: 'filters'
                });
            });

            it('should return cached data as promise', function() {
                fakeInstance.get = function() {
                    return 'existingCachedData';
                };

                funnelMetricsService.getSearchOptions(defaultOptions).then(function(data) {
                    expect(data).toEqual('existingCachedData');
                });
            });

            it('should cache data if there is no existing cache', function() {
                var savedExecuted = false;

                fakeInstance.save = function() {
                    savedExecuted = true;
                };

                var url = baseUrl + '/v0.4/flights/funnel_metrics/search_options/?website_id=aaaa';
                var fakeData = ['test', 'response'];

                httpBackend.whenGET(url).respond(fakeData);

                funnelMetricsService.getSearchOptions(defaultOptions).then(function() {
                    expect(savedExecuted).toEqual(true);
                });

                httpBackend.flush();
            });
        });
    });

    describe('#postQuery', function() {
        it('should POST /v0.4/flights/funnel_metrics/queries/', function() {
            var expectedResponse = 'OK';

            var url = baseUrl + '/v0.4/flights/funnel_metrics/queries/';
            var data = {
                website_id: 'aaaa',
                start_timestamp: 100000000,
                end_timestamp: 100000001,
                dimensions: ['date'],
                segments: ['market'],
                exclude_filters: { market: ['JP'] },
                format: 'json',
                summary_only: false
            };

            httpBackend.expectPOST(url, JSON.stringify(data)).respond(200, expectedResponse);

            funnelMetricsService.postQuery(data).then(function(response) {
                expect(response).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });
    });
});

}());
