(function() {

'use strict';

describe('Service: trafficService', function() {

    var rootScope;
    var trafficService;

    var globalSearchFilters = {
        dates: {
            earliest_start_date: '2015-04-29T00:00:00',
            latest_end_date: '2015-06-07T23:59:59'
        },
        field_names: {
            carrier: 'Airlines',
            device_type: 'Device',
            date: 'Date',
            flight_type: 'Flight Types',
            traffic_type: 'Traffic Sources',
            market: 'Markets'
        },
        segments: ['market', 'device_type', 'flight_type', 'traffic_type'],
        dimensions: ['date', 'market'],
        filters: {
            flight_type: [{
                code: 'I',
                name: 'International'
            }, {
                code: 'D',
                name: 'Domestic'
            }],
            carrier: [{
                code: 'AA',
                name: 'American Airlines'
            }, {
                code: 'QF',
                name: 'Qantas'
            }, {
                code: 'JL',
                name: 'Japan Airlines'
            }],
            market: [{
                code: 'qa',
                name: 'Qatar'
            }, {
                code: 'Nz',
                name: 'New Zealand'
            }],
            device_type: [{
                code: 'd',
                name: 'Desktop'
            }, {
                code: 'M',
                name: 'Mobile Phone'
            }, {
                code: 'O',
                name: 'Other'
            }],
            traffic_type: [{
                code: 'flights',
                name: 'FLIGHTS'
            }, {
                code: 'alsoflies',
                name: 'ALSOFLIES'
            }]
        },
        view: [{
            id: 'search_count',
            code: 'searches',
            title: 'Searches',
            type: 'count',
            relationship: ['clickthrough']
        }, {
            id: 'scrape_count',
            code: 'apiqueries',
            title: 'API Queries',
            type: 'count',
            relationship: ['look2book']
        }, {
            id: 'redirect_count',
            code: 'redirects',
            title: 'Redirects',
            type: 'count',
            relationship: ['clickthrough', 'conversion']
        }, {
            id: 'booking_count',
            code: 'bookings',
            title: 'Bookings',
            type: 'count',
            relationship: ['look2book', 'conversion']
        }, {
            id: 'clickthrough_percentage',
            code: 'clickthrough',
            title: 'Clickthrough',
            type: 'percentage',
            description: 'Overall ratio between searches and redirects',
            relationship: ['searches', 'redirects']
        }, {
            id: 'conversion_percentage',
            code: 'conversion',
            title: 'Conversion',
            type: 'percentage',
            description: 'Overall ratio between redirects and bookings',
            relationship: ['redirects', 'bookings']
        }, {
            id: 'scrape2book_percentage',
            code: 'look2book',
            title: 'Look2Book',
            type: 'percentage',
            description: 'Overall ratio between apiqueries and bookings',
            relationship: ['apiqueries', 'bookings']
        }]
    };

    beforeEach(module('fcApp.commercial'));

    beforeEach(module('fcApp.commercial', function($provide) {
        $provide.value('_', window._);
        $provide.value('moment', window.moment);
        $provide.value('c3', window.c3);
        $provide.value('d3', window.d3);
    }));

    beforeEach(inject(function($rootScope, _trafficService_) {
        rootScope = $rootScope.$new();
        trafficService = _trafficService_;
    }));

    describe('#getConstants', function() {
        it('should be defined', function() {
            expect(trafficService.getConstants).toBeDefined();
        });

        it('should return an object of filters, dictionary, and dates', function() {
            var fakeData = {
                filters: 1,
                field_names: 2,
                dates: 3
            };

            var result = {
                filters: 1,
                dictionary: 2,
                dates: 3
            };

            expect(trafficService.getConstants(fakeData)).toEqual(result);
        });
    });

    describe('#getSearchFilterOptions', function() {
        var options;

        beforeEach(function() {
            options = {
                data: window._.clone(globalSearchFilters, true),
                dateFormat: 'DD-MM-YYYY'
            };
        });

        it('should be defined', function() {
            expect(trafficService.getSearchFilterOptions).toBeDefined();
        });

        it('should return an object of startDate, endDate, dimension, segment, and filters', function() {
            var result = trafficService.getSearchFilterOptions(options);

            expect(result.startDate.start).toBeDefined();
            expect(result.startDate.end).toBeDefined();
            expect(result.startDate.defaut).toBeDefined();
            expect(result.startDate.default).not.toBeDefined();
            expect(result.endDate.start).toBeDefined();
            expect(result.endDate.end).toBeDefined();
            expect(result.endDate.defaut).toBeDefined();
            expect(result.endDate.default).not.toBeDefined();
            expect(result.dimension).toBeDefined();
            expect(result.segment).toBeDefined();
            expect(result.filters).toBeDefined();
        });

        it('should format startDate', function() {
            var format = 'DD MM YYYY';
            var result = trafficService.getSearchFilterOptions(options);

            expect(window.moment.isMoment(result.startDate.start)).toEqual(true);
            expect(window.moment(result.startDate.start).format(format)).toEqual('29 04 2015');

            expect(window.moment.isMoment(result.startDate.end)).toEqual(true);
            expect(window.moment(result.startDate.end).format(format)).toEqual('07 06 2015');

            expect(window.moment.isMoment(result.startDate.defaut)).toEqual(true);
            expect(window.moment(result.startDate.defaut).diff(window.moment(), 'days')).toEqual(-9);
        });

        it('should format endDate', function() {
            var format = 'DD MM YYYY';
            var result = trafficService.getSearchFilterOptions(options);

            expect(window.moment.isMoment(result.endDate.start)).toEqual(true);
            expect(window.moment(result.endDate.start).format(format)).toEqual('29 04 2015');

            expect(window.moment.isMoment(result.endDate.end)).toEqual(true);
            expect(window.moment(result.endDate.end).format(format)).toEqual('07 06 2015');

            expect(window.moment.isMoment(result.endDate.defaut)).toEqual(true);
            expect(window.moment(result.endDate.defaut).diff(window.moment(), 'days')).toEqual(-2);
        });

        it('should return an array of dimensions in code/name object', function() {
            var result = trafficService.getSearchFilterOptions(options);

            expect(result.dimension.length).toEqual(2);
            expect(result.dimension[1].code).toEqual('market');
            expect(result.dimension[1].name).toEqual('Markets');
        });

        it('should return an array of segments in code/name object', function() {
            var result = trafficService.getSearchFilterOptions(options);

            expect(result.segment.length).toEqual(5);
            expect(result.segment[2].code).toEqual('device_type');
            expect(result.segment[2].name).toEqual('Device');
        });

        it('should append a dummy value as first value in segment because it\'s not mandatory', function() {
            var result = trafficService.getSearchFilterOptions(options);

            expect(result.segment[0].code).toBe('none');
        });

        it('should return a filter list with the filter names as the keys', function() {
            var result = trafficService.getSearchFilterOptions(options);

            expect(result.filters.flight_type).toBeDefined();
            expect(result.filters.carrier).toBeDefined();
            expect(result.filters.market).toBeDefined();
            expect(result.filters.device_type).toBeDefined();
            expect(result.filters.traffic_type).toBeDefined();
        });

        it('should return filter values in array format with lowercased values', function() {
            var result = trafficService.getSearchFilterOptions(options);

            var expectedValue = [{
                code: 'd',
                name: 'Domestic'
            }, {
                code: 'i',
                name: 'International'
            }];

            expect(result.filters.flight_type).toEqual(expectedValue);
        });

        it('should return filter values sorted by the names', function() {
            var result = trafficService.getSearchFilterOptions(options);

            var expectedValue = [{
                code: 'nz',
                name: 'New Zealand'
            }, {
                code: 'qa',
                name: 'Qatar'
            }];

            expect(result.filters.market).toEqual(expectedValue);
        });
    });

    describe('#getFilterDropdownSettings', function() {
        var fakeFilters;
        var fakeDictionary;

        beforeEach(function() {
            fakeFilters = {
                flight_type: [{
                    code: 'd',
                    name: 'Domestic'
                }, {
                    code: 'i',
                    name: 'International'
                }]
            };

            fakeDictionary = window._.clone(globalSearchFilters.field_names);
        });

        it('should be defined', function() {
            expect(trafficService.getFilterDropdownSettings).toBeDefined();
        });

        it('should return an object of configuration for dropdown filters', function() {
            var result = trafficService.getFilterDropdownSettings(fakeFilters, fakeDictionary);

            var expectedValue = {
                code: 'flight_type',
                name: 'Flight Types',
                values: [{
                    code: 'd',
                    name: 'Domestic'
                }, {
                    code: 'i',
                    name: 'International'
                }]
            };

            expect(result[0].code).toEqual(expectedValue.code);
            expect(result[0].name).toEqual(expectedValue.name);
            expect(result[0].values).toEqual(expectedValue.values);
            expect(result[0].settings).toBeDefined();
        });
    });

    describe('#parseQueryStringParameters', function() {
        var fakeParams;
        var fakeDictionary;

        beforeEach(function() {
            fakeParams = {
                dates: '2015-06-01',
                startDate: '03-01-2015',
                sdate: '2015-01-31',
                edate: '31-01-2015',
                dimension: 'myDimension',
                segment: 'myMarket',
                view: 'someView',
                traffictype: 'invalidTraffictype',
                traffic_type: 'internal flights',
                carriers: 'aa,ba',
                carrier: 'ba'
            };

            fakeDictionary = window._.clone(globalSearchFilters.filters, true);
        });

        it('should be defined', function() {
            expect(trafficService.parseQueryStringParameters).toBeDefined();
        });

        it('should return a dictionary of accepted url parameters', function() {
            var result = trafficService.parseQueryStringParameters(fakeParams, fakeDictionary);
            var expectedValue = {
                startDate: '2015-01-31',
                endDate: '31-01-2015',
                dimension: 'myDimension',
                segment: 'myMarket',
                view: 'someView',
                traffic_type: 'internal flights',
                carrier: 'ba'
            };

            expect(result).toEqual(jasmine.objectContaining(expectedValue));
        });

        it('should not return invalid parameter keys and values', function() {
            var result = trafficService.parseQueryStringParameters(fakeParams, fakeDictionary);

            expect(result.traffictype).not.toBeDefined();
            expect(result.carriers).not.toBeDefined();
            expect(result.dates).not.toBeDefined();
        });

        it('should not return valid keys that are not passed as parameters', function() {
            delete fakeParams.segment;

            var result = trafficService.parseQueryStringParameters(fakeParams, fakeDictionary);

            expect(result.segment).not.toBeDefined();
            expect(result.flight_type).not.toBeDefined();
            expect(result.device_type).not.toBeDefined();
            expect(result.market).not.toBeDefined();
        });
    });

    describe('#makeFilterQueryStringParameters', function() {
        var fakeFilters;
        var fakeDictionary;

        beforeEach(function() {
            fakeFilters = {
                flight_type: [{
                    id: 'i'
                }, {
                    id: 'd'
                }],
                carrier: [{
                    id: 'qf'
                }, {
                    id: 'aa'
                }],
                market: [{
                    id: 'qa'
                }],
                device_type: [{
                    id: 't'
                }, {
                    id: 'd'
                }]
            };

            fakeDictionary = window._.clone(globalSearchFilters.filters, true);
        });

        it('should be defined', function() {
            expect(trafficService.makeFilterQueryStringParameters).toBeDefined();
        });

        it('should return filter values in comma-separated format', function() {
            var result = trafficService.makeFilterQueryStringParameters(fakeFilters, fakeDictionary);

            expect(result.device_type).toEqual('t,d');
            expect(result.carrier).toEqual('qf,aa');
            expect(result.market).toEqual('qa');
        });

        it('should only return filters which values have been custom picked', function() {
            var result = trafficService.makeFilterQueryStringParameters(fakeFilters, fakeDictionary);

            expect(result.flight_type).not.toBeDefined();
            expect(result.carrier).toBeDefined();
            expect(result.market).toBeDefined();
            expect(result.device_type).toBeDefined();
        });
    });

    describe('#validateDimension', function() {
        var fakeContext;

        beforeEach(function() {
            fakeContext = [{
                code: 'market',
                name: 'Markets'
            }, {
                code: 'date',
                name: 'Date'
            }];
        });

        it('should be defined', function() {
            expect(trafficService.validateDimension).toBeDefined();
        });

        it('should return mapped object of passed dimension value', function() {
            var boundFn = trafficService.validateDimension.bind(fakeContext);

            var result = boundFn('date');
            var expectedValue = {
                code: 'date',
                name: 'Date'
            };

            expect(result).toEqual(expectedValue);
        });

        it('should lowercase passed value when checking against the context', function() {
            var boundFn = trafficService.validateDimension.bind(fakeContext);

            var result = boundFn('DATE');
            var expectedValue = {
                code: 'date',
                name: 'Date'
            };

            expect(result).toEqual(expectedValue);
        });

        it('should return nothing if no match is found', function() {
            var boundFn = trafficService.validateDimension.bind(fakeContext);

            var result = boundFn('startDate');

            expect(result).toBeUndefined();
        });
    });

    describe('#validateSegment', function() {
        var fakeContext;
        var fakeParameters;

        beforeEach(function() {
            fakeContext = [{
                code: 'none',
                name: 'No Segment'
            }, {
                code: 'flight_type',
                name: 'Flight Type'
            }, {
                code: 'device_type',
                name: 'Devices'
            }, {
                code: 'market',
                name: 'Markets'
            }];

            fakeParameters = {
                dimension: 'market',
                segment: 'device_type'
            };
        });

        it('should be defined', function() {
            expect(trafficService.validateSegment).toBeDefined();
        });

        it('should return mapped object of passed segment value', function() {
            var boundFn = trafficService.validateSegment.bind(fakeContext);

            var result = boundFn('device_type', 'segment', fakeParameters);
            var expectedValue = {
                code: 'device_type',
                name: 'Devices'
            };

            expect(result).toEqual(expectedValue);
        });

        it('should lowercase passed value when checking against the context', function() {
            var boundFn = trafficService.validateSegment.bind(fakeContext);

            var result = boundFn('DEVICE_TYPE', 'segment', fakeParameters);
            var expectedValue = {
                code: 'device_type',
                name: 'Devices'
            };

            expect(result).toEqual(expectedValue);
        });

        it('should return undefined if no match is found', function() {
            var boundFn = trafficService.validateSegment.bind(fakeContext);

            var result = boundFn('user_device', 'segment', fakeParameters);

            expect(result).toEqual(undefined);
        });

        describe('segment matches dimension', function() {
            beforeEach(function() {
                fakeParameters.dimension = 'device_type';
            });

            it('should disable the context\'s segment', function() {
                var boundFn = trafficService.validateSegment.bind(fakeContext);

                boundFn('device_type', 'segment', fakeParameters);

                expect(fakeContext[2].disabled).toEqual(true);
            });

            it('should return the first value from the context', function() {
                fakeParameters.dimension = 'device_type';

                var boundFn = trafficService.validateSegment.bind(fakeContext);

                var result = boundFn('device_type', 'segment', fakeParameters);

                expect(result).toEqual(fakeContext[0]);
            });
        });

        describe('segment does not match dimension', function() {
            it('should disable the context\'s segment where value is the same as dimension', function() {
                var boundFn = trafficService.validateSegment.bind(fakeContext);

                boundFn('device_type', 'segment', fakeParameters);

                expect(fakeContext[0].disabled).not.toBeDefined();
                expect(fakeContext[1].disabled).not.toBeDefined();
                expect(fakeContext[2].disabled).not.toBeDefined();
                expect(fakeContext[3].disabled).toEqual(true);
            });

            it('should return the context\'s segment match', function() {
                var boundFn = trafficService.validateSegment.bind(fakeContext);

                var result = boundFn('flight_type', 'segment', fakeParameters);

                expect(result).toEqual(fakeContext[1]);
            });
        });
    });

    describe('#validateDate', function() {
        var fakeContext;

        beforeEach(function() {
            fakeContext = {
                start: window.moment([2015, 6, 5]),
                end: window.moment([2015, 7, 1]),
                defaut: window.moment([2015, 6, 28]),
            };
        });

        it('should be defined', function() {
            expect(trafficService.validateDate).toBeDefined();
        });

        it('should return a window.moment object', function() {
            var boundFn = trafficService.validateDate.bind(fakeContext);

            var result = boundFn('2015-07-15');
            expect(window.moment.isMoment(result)).toEqual(true);
        });

        it('should return the input date if it falls within the start and end dates range', function() {
            var boundFn = trafficService.validateDate.bind(fakeContext);
            var expectedString = '2015-07-15';

            var result = boundFn(expectedString);
            expect(window.moment(result).format('YYYY-MM-DD')).toEqual(expectedString);
        });

        it('should return default date if date is before start range', function() {
            var boundFn = trafficService.validateDate.bind(fakeContext);

            var result = boundFn('2015-04-30');
            expect(result).toEqual(fakeContext.defaut);
        });

        it('should return default date if date is after end range', function() {
            var boundFn = trafficService.validateDate.bind(fakeContext);

            var result = boundFn('2015-08-30');
            expect(result).toEqual(fakeContext.defaut);
        });

        it('should return default date if date format is not YYYY-MM-DD', function() {
            var boundFn = trafficService.validateDate.bind(fakeContext);

            var result = boundFn('07-05-2015');
            expect(result).toEqual(fakeContext.defaut);
        });
    });

    describe('#validateFilters', function() {
        var fakeContext;

        beforeEach(function() {
            fakeContext = [{
                code: 'flight_type',
                name: 'Flight Type',
                values: [{
                    code: 'd',
                    name: 'Domestic'
                }, {
                    code: 'i',
                    name: 'International'
                }]
            }, {
                code: 'carrier',
                name: 'Carrier',
                values: [{
                    code: 'ba',
                    name: 'British Airways'
                }, {
                    code: 'qa',
                    name: 'Qatar Airways'
                }]
            }];
        });

        it('should be defined', function() {
            expect(trafficService.validateFilters).toBeDefined();
        });

        it('should return an array of valid filter codes that match the dictionary', function() {
            var boundFn = trafficService.validateFilters.bind(fakeContext);

            var result = boundFn('qa,ba', 'carrier');

            expect(result).toEqual(['qa', 'ba']);
        });

        it('should match uppercase key and values', function() {
            var boundFn = trafficService.validateFilters.bind(fakeContext);

            var result = boundFn('D,I', 'FLIGHT_type');

            expect(result).toEqual(['d', 'i']);
        });
    });

    describe('#validateView', function() {
        var fakeContext;

        beforeEach(function() {
            fakeContext = [{
                code: 'searches',
                name: 'Searches'
            }, {
                code: 'conversion',
                name: 'Conversion Rate'
            }];
        });

        it('should be defined', function() {
            expect(trafficService.validateView).toBeDefined();
        });

        it('should return mapped object of passed dimension value', function() {
            var boundFn = trafficService.validateView.bind(fakeContext);

            var result = boundFn('conversion');

            expect(result).toEqual(fakeContext[1]);
        });

        it('should lowercase passed value when checking against the context', function() {
            var boundFn = trafficService.validateView.bind(fakeContext);

            var result = boundFn('CONVERSION');

            expect(result).toEqual(fakeContext[1]);
        });

        it('should return nothing if no match is found', function() {
            var boundFn = trafficService.validateView.bind(fakeContext);

            var result = boundFn('booking');

            expect(result).toBeUndefined();
        });
    });

    describe('#validateUrlParams', function() {
        var fakeValidators;
        var fakeFilters;

        var returnTrue = function(value) {
            return value;
        };

        var returnFalse = function() {
            return false;
        };

        beforeEach(function() {
            fakeValidators = {
                dimension: returnTrue,
                startDate: returnTrue,
                endDate: returnFalse,
                filters: returnTrue
            };

            fakeFilters = window._.clone(globalSearchFilters.filters, true);
        });

        it('should be defined', function() {
            expect(trafficService.validateUrlParams).toBeDefined();
        });

        it('should return a map of parameters and the validated values', function() {
            var params = {
                dimension: 'date',
                segment: 'invalidParam',
                startDate: '2015-05-21',
                endDate: '2015-06-27',
                flight_type: ['i', 'd'],
                device_type: ['t', 'd']
            };

            var result = trafficService.validateUrlParams(params, fakeValidators, fakeFilters);
            var expectedValue = {
                dimension: 'date',
                startDate: '2015-05-21',
                endDate: false,
                flight_type: ['i', 'd'],
                device_type: ['t', 'd']
            };

            expect(result).toEqual(expectedValue);
        });
    });

    describe('#getActiveFilters', function() {
        var fakeOptions;

        beforeEach(function() {
            fakeOptions = {
                filters: window._.clone(globalSearchFilters.filters, true),
                searchOptions: {
                    dimension: [{
                        code: 'date',
                        name: 'Date'
                    }, {
                        code: 'market',
                        name: 'Markets'
                    }],
                    segment: [{
                        code: 'none',
                        name: 'Select a breakdown'
                    }, {
                        code: 'market',
                        name: 'Markets'
                    }],
                    startDate: {
                        start: window.moment([2015, 6, 1]),
                        end: window.moment([2015, 6, 30]),
                        defaut: window.moment([2015, 6, 18])
                    },
                    endDate: {
                        start: window.moment([2015, 6, 1]),
                        end: window.moment([2015, 6, 30]),
                        defaut: window.moment([2015, 6, 25])
                    }
                },
                dateFormat: 'DD MMM YYYY'
            };
        });

        it('should be defined', function() {
            expect(trafficService.getActiveFilters).toBeDefined();
        });

        it('should return an object of dimension, segment, startDate, endDate, includeFilters, and excludeFilters', function() {
            var result = trafficService.getActiveFilters(fakeOptions);

            expect(result.dimension).toBeDefined();
            expect(result.segment).toBeDefined();
            expect(result.startDate).toBeDefined();
            expect(result.endDate).toBeDefined();
            expect(result.includeFilters).toBeDefined();
            expect(result.excludeFilters).toBeDefined();
        });

        it('should return a default set of values if no url querystring values are passed', function() {
            var result = trafficService.getActiveFilters(fakeOptions);
            var expectedFilterValue = {
                flight_type: [{
                    id: 'I'
                }, {
                    id: 'D'
                }],
                carrier: [{
                    id: 'AA'
                }, {
                    id: 'QF'
                }, {
                    id: 'JL'
                }],
                market: [{
                    id: 'qa'
                }, {
                    id: 'Nz'
                }],
                device_type: [{
                    id: 'd'
                }, {
                    id: 'M'
                }, {
                    id: 'O'
                }],
                traffic_type: [{
                    id: 'flights'
                }, {
                    id: 'alsoflies'
                }]
            };

            expect(result.dimension).toEqual({ code: 'date', name: 'Date' });
            expect(result.segment).toEqual({ code: 'none', name: 'Select a breakdown' });
            expect(result.startDate).toEqual('18 Jul 2015');
            expect(result.endDate).toEqual('25 Jul 2015');
            expect(result.includeFilters).toEqual(expectedFilterValue);
            expect(result.excludeFilters).toEqual({});
        });

        it('should allow preset values (query parameters) to set values', function() {
            fakeOptions.presetValues = {
                dimension: {
                    code: 'carrier',
                    name: 'Airlines'
                },
                segment: {
                    code: 'flight_type',
                    name: 'Flight Type'
                },
                startDate: window.moment([2015, 6, 25]),
                endDate: window.moment([2015, 6, 28]),
                flight_type: ['D'],
                carrier: ['JL'],
                device_type: ['O'],
                traffic_type: ['flights'],
                market: ['Nz']
            };

            var result = trafficService.getActiveFilters(fakeOptions);
            var expectedFilterValue = {
                flight_type: [{
                    id: 'D'
                }],
                carrier: [{
                    id: 'JL'
                }],
                device_type: [{
                    id: 'O'
                }],
                traffic_type: [{
                    id: 'flights'
                }],
                market: [{
                    id: 'Nz'
                }]
            };

            expect(result.dimension).toEqual(fakeOptions.presetValues.dimension);
            expect(result.segment).toEqual(fakeOptions.presetValues.segment);
            expect(result.startDate).toEqual('25 Jul 2015');
            expect(result.endDate).toEqual('28 Jul 2015');
            expect(result.includeFilters).toEqual(expectedFilterValue);
        });

        it('should validate endDate - set to endDate.default if the preset value is after startDate', function() {
            fakeOptions.presetValues = {
                startDate: window.moment([2015, 6, 10]),
                endDate: window.moment([2015, 6, 9])
            };
            var result = trafficService.getActiveFilters(fakeOptions);

            expect(result.startDate).toEqual('10 Jul 2015');
            expect(result.endDate).toEqual('25 Jul 2015');
        });

        it('should validate endDate - set to endDate.end if the endDate.default is after startDate', function() {
            fakeOptions.presetValues = {
                startDate: window.moment([2015, 6, 26]),
                endDate: window.moment([2015, 6, 24])
            };
            var result = trafficService.getActiveFilters(fakeOptions);

            expect(result.startDate).toEqual('26 Jul 2015');
            expect(result.endDate).toEqual('30 Jul 2015');
        });
    });

    describe('#getExcludedFilterList', function() {
        var fakeIncludedList;
        var fakeFilters;

        beforeEach(function() {
            fakeIncludedList = {
                flight_type: [{
                    id: 'I'
                }],
                carrier: [{
                    id: 'AA'
                }, {
                    id: 'JL'
                }],
                market: [],
                device_type: [{
                    id: 'd'
                }, {
                    id: 'M'
                }],
                traffic_type: [{
                    id: 'flights'
                }, {
                    id: 'alsoflies'
                }]
            };

            fakeFilters = window._.clone(globalSearchFilters.filters, true);
        });

        it('should be defined', function() {
            expect(trafficService.getExcludedFilterList).toBeDefined();
        });

        it('should return a list of excluded filter values', function() {
            var result = trafficService.getExcludedFilterList(fakeIncludedList, fakeFilters);
            var expectedValue = {
                flight_type: ['D'],
                carrier: ['QF'],
                market: ['qa', 'Nz'],
                device_type: ['O'],
                traffic_type: []
            };

            expect(result).toEqual(expectedValue);
        });
    });

    describe('#getMetricsSummary', function() {
        var fakeData;
        var fakeDictionary;
        var fakeOptions;

        beforeEach(function() {
            fakeData = {
                summary: {
                    scrape2book_percentage: null,
                    clickthrough_percentage: 5.80,
                    booking_count: 0.0,
                    conversion_percentage: 0.0,
                    scrape_count: 7394,
                    redirect_count: 868.0,
                    search_count: 14951
                }
            };

            fakeDictionary = window._.clone(globalSearchFilters.view, true);

            fakeOptions = {
                data: fakeData.summary,
                fieldNames: fakeDictionary,
                defaultView: 'apiqueries',
                activeViewCode: 'redirects',
                dimension: 'date',
                segment: 'market'
            };
        });

        it('should be defined', function() {
            expect(trafficService.getMetricsSummary).toBeDefined();
        });

        it('should return an array of metrics', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result.length).toEqual(7);
        });

        it('should order the metrics', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[0].code).toEqual(fakeDictionary[0].code);
            expect(result[1].code).toEqual(fakeDictionary[1].code);
            expect(result[2].code).toEqual(fakeDictionary[2].code);
            expect(result[3].code).toEqual(fakeDictionary[3].code);
            expect(result[4].code).toEqual(fakeDictionary[4].code);
            expect(result[5].code).toEqual(fakeDictionary[5].code);
            expect(result[6].code).toEqual(fakeDictionary[6].code);
        });

        it('should set active to false if metric is disabled', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[3].active).toEqual(false);
            expect(result[5].active).toEqual(false);
            expect(result[6].active).toEqual(false);
        });

        it('should set highlight to the related metrics for the active metric view', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[4].highlighted).toEqual(true);
        });

        it('should set not set highlight to the related metrics for the active metric view if it is disabled', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[5].highlighted).toBeUndefined();
        });

        it('should set value to N/A if metric value is null', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[6].value).toEqual('N/A');
            expect(result[6].description).toBeDefined();
        });

        it('should format percentage value to % string', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[4].value).toEqual('5.80%');
            expect(result[5].value).toEqual('0.00%');
        });

        it('should format numbers to 2 decimal point', function() {
            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[0].value).toEqual('14,951');
            expect(result[1].value).toEqual('7,394');
            expect(result[2].value).toEqual('868');
        });

        it('should switch active view to default view if the active view is disabled', function() {
            fakeOptions.activeViewCode = 'bookings';
            fakeOptions.defaultView = fakeDictionary[2];

            var result = trafficService.getMetricsSummary(fakeOptions);

            expect(result[3].active).toEqual(false);
            expect(result[2].active).toEqual(true);
            expect(result[4].highlighted).toEqual(true);
            expect(result[5].highlighted).toEqual(true);
        });

        describe('disabling metrics', function() {
            var fakeSegments = ['device_type', 'traffic_type'];

            it('should disable metrics with value 0 or null', function() {
                var result = trafficService.getMetricsSummary(fakeOptions);

                expect(result[3].disabled).toEqual(true);
                expect(result[5].disabled).toEqual(true);
                expect(result[6].disabled).toEqual(true);
            });

            it('should disable metrics if dimension is carrier', function() {
                fakeOptions.dimension = 'carrier';

                var result = trafficService.getMetricsSummary(fakeOptions);

                expect(result[0].disabled).toEqual(true);
                expect(result[1].disabled).toEqual(true);
                expect(result[6].disabled).toEqual(true);
            });

            angular.forEach(fakeSegments, function(segment) {
                it('should disable metric if segment is ' + segment, function() {
                    fakeOptions.segment = segment;
                    var result = trafficService.getMetricsSummary(fakeOptions);

                    expect(result[0].disabled).toEqual(true);
                    expect(result[0].alerted).toEqual(true);
                    expect(result[1].disabled).toEqual(true);
                    expect(result[1].alerted).toEqual(true);
                    expect(result[6].disabled).toEqual(true);
                    expect(result[6].alerted).toEqual(true);
                });
            });
        });
    });

    describe('#getMetricsData', function() {
        var fakeData;

        it('should be defined', function() {
            expect(trafficService.getMetricsData).toBeDefined();
        });

        describe('non-segment', function() {
            beforeEach(function() {
                fakeData = [{
                    metrics: {
                        scrape2book_percentage: 0.0,
                        clickthrough_percentage: 6.24,
                        booking_count: 0.0,
                        conversion_percentage: 0.0,
                        scrape_count: 4091,
                        redirect_count: 457.0,
                        search_count: 7323
                    },
                    id: {
                        type: 'date',
                        value: '2015-06-20T00:00:00'
                    }
                }, {
                    metrics: {
                        scrape2book_percentage: 0.0,
                        clickthrough_percentage: 5.38,
                        booking_count: 0.0,
                        conversion_percentage: 0.0,
                        scrape_count: 3303,
                        redirect_count: 411.0,
                        search_count: 7628
                    },
                    id: {
                        type: 'date',
                        value: '2015-06-21T00:00:00'
                    }
                }];
            });

            it('should return an object of chartData and dimension', function() {
                var result = trafficService.getMetricsData(fakeData);

                expect(result.chartData).toBeDefined();
                expect(result.dimension).toBeDefined();
            });

            it('should return dimension as used in the data', function() {
                var result = trafficService.getMetricsData(fakeData);

                expect(result.dimension).toEqual('date');
            });

            it('should return chartData', function() {
                var result = trafficService.getMetricsData(fakeData);

                expect(result.chartData.length).toEqual(2);
                expect(result.chartData[0].date).toEqual('2015-06-20T00:00:00');
                expect(result.chartData[0].metrics).toEqual(fakeData[0].metrics);
            });
        });

        describe('segment', function() {
            beforeEach(function() {
                fakeData = [{
                    id: {
                        type: 'date',
                        value: '2015-06-20T00:00:00'
                    },
                    segments: [{
                        id: {
                            type: 'market',
                            value: 'jp'
                        },
                        metrics: {
                            scrape2book_percentage: 0.0,
                            clickthrough_percentage: 6.24,
                            booking_count: 0.0,
                            conversion_percentage: 0.0,
                            scrape_count: 4091,
                            redirect_count: 457.0,
                            search_count: 7323
                        }
                    }]
                }, {
                    id: {
                        type: 'date',
                        value: '2015-06-21T00:00:00'
                    },
                    segments: [{
                        id: {
                            type: 'market',
                            value: 'jp'
                        },
                        metrics: {
                            scrape2book_percentage: 0.0,
                            clickthrough_percentage: 5.38,
                            booking_count: 0.0,
                            conversion_percentage: 0.0,
                            scrape_count: 3303,
                            redirect_count: 411.0,
                            search_count: 7628
                        }
                    }]
                }];
            });

            it('should return an object of chartData, dimension, and segment', function() {
                var result = trafficService.getMetricsData(fakeData);

                expect(result.chartData).toBeDefined();
                expect(result.dimension).toBeDefined();
                expect(result.segment).toBeDefined();
            });

            it('should return dimension as used in the data', function() {
                var result = trafficService.getMetricsData(fakeData);

                expect(result.dimension).toEqual('date');
            });

            it('should return segment as used in the data', function() {
                var result = trafficService.getMetricsData(fakeData);

                expect(result.segment).toEqual('market');
            });

            it('should return chartData', function() {
                var result = trafficService.getMetricsData(fakeData);

                expect(result.chartData.length).toEqual(2);
                expect(result.chartData[0].date).toEqual('2015-06-20T00:00:00');
                expect(result.chartData[0].metrics.jp).toEqual(fakeData[0].segments[0].metrics);
            });
        });
    });

    describe('#getChart', function() {
        it('should be defined', function() {
            expect(trafficService.getChart).toBeDefined();
        });

        it('should call c3.generate to return a chart', function() {
            spyOn(window.c3, 'generate');

            trafficService.getChart();

            expect(window.c3.generate).toHaveBeenCalled();
        });
    });

    describe('#getChartData', function() {
        var fakeOptions;
        var json = readJSON('app/commercial/fixtures/traffic-chartData.json');
        var noSegmentData = json.date;
        var segmentData = json.marketByFlightType;

        it('should be defined', function() {
            expect(trafficService.getChartData).toBeDefined();
        });

        it('should return an object of chart data requirements', function() {
            fakeOptions = window._.clone(noSegmentData, 'true');

            var result = trafficService.getChartData(fakeOptions);

            expect(result.axesLabels).toBeDefined();
            expect(result.groups).toBeDefined();
            expect(result.names).toBeDefined();
            expect(result.columns).toBeDefined();
            expect(result.chartType).toBeDefined();
            expect(result.xTickLabels).toBeDefined();
        });

        describe('no segment (date)', function() {
            beforeEach(function() {
                fakeOptions = window._.clone(noSegmentData, 'true');
            });

            it('should not return segment', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.segments).not.toBeDefined();
            });

            it('should return axisLabel with friendly date format if dimension is date', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.xTickLabels[0]).toEqual('21 Jun');
            });

            it('should provide a view friendly name for the chart tooltip', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.names).toEqual({
                    redirect_count: 'Redirects'
                });

                fakeOptions.view = 'conversion_percentage';

                result = trafficService.getChartData(fakeOptions);

                expect(result.names).toEqual({
                    conversion_percentage: 'Conversion'
                });
            });

            it('should reset chart stacking context', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.groups).toEqual([]);
            });

            it('should draw a bar chart for the view type with raw numbers', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.chartType).toEqual('bar');
            });

            it('should draw a line chart for the view type with percentage for dimension = date', function() {
                fakeOptions.view = 'conversion_percentage';

                var result = trafficService.getChartData(fakeOptions);

                expect(result.chartType).toEqual('line');
            });

            it('should draw a scatter chart for the view type with percentage for dimension != date', function() {
                fakeOptions.view = 'conversion_percentage';
                fakeOptions.dimension = 'market';

                var result = trafficService.getChartData(fakeOptions);

                expect(result.chartType).toEqual('scatter');
            });

            it('should set friendly names for the axes labels', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.axesLabels.x).toEqual('Date');
                expect(result.axesLabels.y).toEqual('Number of Redirects');

                fakeOptions.view = 'conversion_percentage';
                result = trafficService.getChartData(fakeOptions);

                expect(result.axesLabels.y).toEqual('Conversion Rate (%)');
            });

            it('should return the data in two-dimensional array', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.columns.length).toEqual(1);
                expect(result.columns[0].length).toEqual(9);
                expect(result.columns[0][0]).toEqual('redirect_count');
            });
        });

        describe('with segment (market by flight type)', function() {
            beforeEach(function() {
                fakeOptions = window._.clone(segmentData, 'true');
            });

            it('should return segment', function() {
                var result = trafficService.getChartData(fakeOptions);
                expect(result.segments).toBeDefined();
            });

            it('should return the data in two-dimensional array', function() {
                var result = trafficService.getChartData(fakeOptions);

                expect(result.columns.length).toEqual(2);
                expect(result.columns[0].length).toEqual(62);
                expect(result.columns[0][0]).toEqual('domestic');
                expect(result.columns[1][0]).toEqual('international');
            });

            it('should reset chart stacking context for line chart', function() {
                fakeOptions.view = 'conversion_percentage';

                var result = trafficService.getChartData(fakeOptions);

                expect(result.groups).toEqual([]);
            });
        });
    });

    describe('#getTableData', function() {
        var fakeOptions;
        var json = readJSON('app/commercial/fixtures/traffic-tableData.json');
        var noSegmentData = json.date;
        var segmentData = json.dateByMarket;

        it('should be defined', function() {
            expect(trafficService.getTableData).toBeDefined();
        });

        it('should return table header and body data', function() {
            fakeOptions = window._.clone(noSegmentData, 'true');

            var result = trafficService.getTableData(fakeOptions);

            expect(result.header).toBeDefined();
            expect(result.body).toBeDefined();
        });

        describe('non-segment data (date)', function() {
            beforeEach(function() {
                fakeOptions = window._.clone(noSegmentData, 'true');
            });

            it('should return the column definition in the header', function() {
                var result = trafficService.getTableData(fakeOptions);

                expect(result.header[0].date).toEqual('Date');
                expect(result.header[1].search_count).toEqual('Searches');
                expect(result.header[2].scrape_count).toEqual('API Queries');
                expect(result.header[3].redirect_count).toEqual('Redirects');
                expect(result.header[4].booking_count).toEqual('Bookings');
                expect(result.header[5].clickthrough_percentage).toEqual('Clickthrough');
                expect(result.header[6].conversion_percentage).toEqual('Conversion');
                expect(result.header[7].scrape2book_ratio).toEqual('Look2Book');
            });

            it('should return the data in body as ordered by the header column', function() {
                var result = trafficService.getTableData(fakeOptions);

                expect(result.body[0][0].date).toEqual('Totals');
                expect(result.body[0][1].search_count).toBeDefined();
                expect(result.body[0][2].scrape_count).toBeDefined();
                expect(result.body[0][3].redirect_count).toBeDefined();
                expect(result.body[0][4].booking_count).toBeDefined();
                expect(result.body[0][5].clickthrough_percentage).toBeDefined();
                expect(result.body[0][6].conversion_percentage).toBeDefined();
                expect(result.body[0][7].scrape2book_ratio).toBeDefined();
            });

            it('should return the data in body as ordered by the header column', function() {
                var result = trafficService.getTableData(fakeOptions);

                expect(result.body[1][0].date).toBeDefined();
                expect(result.body[1][1].search_count).toBeDefined();
                expect(result.body[1][2].scrape_count).toBeDefined();
                expect(result.body[1][3].redirect_count).toBeDefined();
                expect(result.body[1][4].booking_count).toBeDefined();
                expect(result.body[1][5].clickthrough_percentage).toBeDefined();
                expect(result.body[1][6].conversion_percentage).toBeDefined();
                expect(result.body[1][7].scrape2book_ratio).toBeDefined();
            });

            it('should return the data in body as ordered by the header column', function() {
                var result = trafficService.getTableData(fakeOptions);

                expect(result.body[0][0].date).toEqual('Totals');
            });
        });

        describe('segment data (date)', function() {
            beforeEach(function() {
                fakeOptions = window._.clone(segmentData, 'true');
            });

            it('should append a total column in the second column of header', function() {
                var result = trafficService.getTableData(fakeOptions);

                expect(result.header[1]._total).toEqual('Totals');
            });

            it('should append a total column in the first row of body', function() {
                var result = trafficService.getTableData(fakeOptions);

                expect(result.body[0][0].date).toEqual('Totals');
            });
        });
    });
});

}());
