(function() {

'use strict';

describe('Service: websiteIdService', function() {

    var config;
    var rootScope;
    var websiteIdService;
    var mockTimedCacheService;
    var httpBackend;
    var baseUrl;

    beforeEach(module('fcApp.commercial'));

    beforeEach(module('fcApp.commercial', function($provide) {
        mockTimedCacheService = {
            getInstance: function() {
                return {};
            }
        };

        $provide.value('timedCacheService', mockTimedCacheService);
        $provide.value('_', window._);
    }));

    beforeEach(inject(function($httpBackend, $rootScope, _config_, _websiteIdService_) {
        config = _config_;
        httpBackend = $httpBackend;
        rootScope = $rootScope.$new();
        websiteIdService = _websiteIdService_;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#getMarkets', function() {
        var fakeInstance;
        var noop = function() {};

        beforeEach(function() {
            fakeInstance = {
                remove: noop,
                get: noop,
                save: noop
            };

            spyOn(mockTimedCacheService, 'getInstance').and.returnValue(fakeInstance);
        });

        afterEach(function() {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        it('should be defined', function() {
            expect(websiteIdService.getMarkets).toBeDefined();
        });

        it('should GET /v0.4/flights/website_ids/markets/', function() {
            var url = baseUrl + '/v0.4/flights/website_ids/markets/?website_id=aaaa';

            httpBackend.whenGET(url).respond({});

            websiteIdService.getMarkets({ websiteId: 'aaaa' }).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should return an array of markets', function() {
            var expectedResponse = [{
                name: 'Japan',
                code: 'JP'
            }];

            var url = baseUrl + '/v0.4/flights/website_ids/markets/?website_id=aaaa';

            httpBackend.whenGET(url).respond(expectedResponse);

            websiteIdService.getMarkets({ websiteId: 'aaaa' }).then(function(res) {
                expect(res).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });

        it('should return an array of markets sorted by name', function() {
            var expectedResponse = [{
                name: 'British Virgin Islands (VG)',
                code: 'VG'
            }, {
                name: 'Algeria (DZ)',
                code: 'DZ'
            }, {
                name: 'Japan (JP)',
                code: 'JP'
            }];

            var url = baseUrl + '/v0.4/flights/website_ids/markets/?website_id=aaaa';

            httpBackend.whenGET(url).respond(expectedResponse);

            websiteIdService.getMarkets({ websiteId: 'aaaa' }).then(function(res) {
                expect(res[0].name).toEqual('Algeria (DZ)');
                expect(res[1].name).toEqual('British Virgin Islands (VG)');
                expect(res[2].name).toEqual('Japan (JP)');
            });

            httpBackend.flush();
        });

        describe('cache', function() {
            var defaultOptions;

            beforeEach(function() {
                defaultOptions = {
                    websiteId: 'aaaa',
                    cache: true
                };
            });

            it('should initialize an instance of TimedCache', function() {
                fakeInstance.get = function() {
                    return 'existingCachedData';
                };

                websiteIdService.getMarkets(defaultOptions);

                expect(mockTimedCacheService.getInstance).toHaveBeenCalledWith({
                    name: 'markets',
                    websiteId: 'aaaa',
                    cacheTime: 24,
                    cacheKey: 'markets'
                });
            });

            it('should return cached data as promise', function() {
                fakeInstance.get = function() {
                    return 'existingCachedData';
                };

                websiteIdService.getMarkets(defaultOptions).then(function(data) {
                    expect(data).toEqual('existingCachedData');
                });
            });

            it('should cache data if there is no existing cache', function() {
                var savedExecuted = false;

                fakeInstance.save = function() {
                    savedExecuted = true;
                };

                var url = baseUrl + '/v0.4/flights/website_ids/markets/?website_id=aaaa';
                var fakeData = ['test', 'response'];

                httpBackend.whenGET(url).respond(fakeData);

                websiteIdService.getMarkets(defaultOptions).then(function() {
                    expect(savedExecuted).toEqual(true);
                });

                httpBackend.flush();
            });
        });
    });
});

}());
