(function() {

'use strict';

angular.module('fcApp.commercial').factory('funnelMetricsService', function(
    $http,
    $q,
    localStorageService,
    timedCacheService,
    config
    ) {

    var baseUrl = config.API_URL + config.API_BASE + '/v0.4/flights/funnel_metrics/';

    //PE-791
    function _normalizeWebsiteId(options) {
        options.website_id = (options.website_id === 'a001') ? 'aljp' : options.website_id;
        return options;
    }

    /**
     * Get search options (dimension, segment, date) and all possible filter values
     *     based on a set range of time for a particular websiteId
     *
     * @param  {String} options.websiteId - REQUIRED
     * @param  {String} options.cache - return data from and store it to localStorage if true
     *
     * @return {Object} {"dimensions": ["carrier", "date", "market"], "segments": ["device_type", "flight_type", "market", "traffic_type"], "date": {"earliest_start_date":"2015-04-25T00:00:00","latest_end_date":"2015-06-22T00:00:00"}, "filters": {"carrier": [{"code": "SI", "name": "Silk Air"}, ], "device_type": [{"code": "D", "name": "Desktop"} ], "flight_type": [{"code": "I", "name": "International"} ], "market": [{"code": "SG", "name": "Singapore"} ], "traffic_type": [{"code": "flights", "name": "Flights"} ] } }
     */
    function getSearchOptions(options) {
        var websiteId;
        var defer;
        var url;
        var parameters;
        var cachedFilters;

        options = _normalizeWebsiteId(options);
        websiteId = options.website_id;

        var filtersCache = timedCacheService.getInstance({
            name: 'trafficConfig',
            websiteId: websiteId,
            cacheTime: 8,
            cacheKey: 'filters'
        });

        if (options.cache) {
            cachedFilters = filtersCache.get();

            if (cachedFilters) {
                defer = $q.defer();
                defer.resolve(cachedFilters);
                return defer.promise;
            }
        } else {
            filtersCache.remove();
        }

        url = baseUrl + 'search_options/';

        parameters = {
            website_id: websiteId
        };

        return $http({
            method: 'get',
            url: url,
            params: parameters
        }).then(function(res) {
            var filters = res.data ? res.data : {};

            if (options.cache) {
                filtersCache.save(filters);
            }

            return filters;
        });
    }

    /**
     * Get summary and detail level data via a POST
     * @param  {String}     options.website_id
     * @param  {Number}     options.start_timestamp - unix format
     * @param  {Number}     options.end_timestamp - unix format
     * @param  {Array}      options.dimensions - currently 1 value
     * @param  {Array}      options.segments - currently 1 value
     * @param  {Collection} options.exclude_filters - JSON array format, key is filter name
     * @param  {String}     options.format - 'csv' or 'json'
     * @param  {Boolean}    options.summary_only
     * @return {[type]}         [description]
     */
    function postQuery(options) {
        var url = baseUrl + 'queries/';

        options = _normalizeWebsiteId(options);

        return $http({
            url: url,
            method: 'post',
            data: JSON.stringify(options),
            headers: { 'Content-Type': 'application/json' }
        }).then(function(res) {
            return res.data;
        });
    }

    return {
        getSearchOptions: getSearchOptions,
        postQuery: postQuery
    };
});

}());
