(function() {

'use strict';

angular.module('fcApp.commercial').factory('trafficService', function(
    _,
    moment,
    d3,
    c3,
    $filter) {

    /**
     * Return raw search options values to _CONSTANTS
     * @param  {Collection} data - raw data - REQUIRED
     * @return {Object} as below
     */
    function getConstants(data) {
        return {
            filters: data.filters,
            dictionary: data.field_names,
            dates: data.dates
        };
    }

    /**
     * Get values of search options (dimension, segment, dates) to populate UI
     *   Initiate an empty filters object
     *   Append a "none" to top of segment - because segment is not required
     *   The input dates are in UTC format, e.g. 2015-04-21T16:00:00.000Z
     *   The returned dates are in DD MMM YYYY to avoid FOUC
     *
     * @param  {Object} options.data - raw data - REQUIRED
     * @param  {Object} options.dateFormat - date format for the datepicker - REQUIRED
     *
     * @return {Object} {"startDate":{"start":momentObject,"end":momentObject},"endDate":{"start":momentObject,"end":momentObject},"dimension":[{"code":"date","name":"Date"}],"segment":[{"code":"none","name":"No breakdown"},{"code":"market","name":"Market"}],"filters":{"flight_type":[{"code":"d","name":"Domestic"},{"code":"i","name":"International"}],"device_type":[{"code":"d","name":"Desktop"},{"code":"m","name":"Mobile Phone"},{"code":"o","name":"Other"},{"code":"t","name":"Tablet"}]}}
     */
    function getSearchFilterOptions(options) {
        var data = options.data;
        var dictionary = data.field_names;

        var getValueMap = function(key) {
            return {
                code: key,
                name: dictionary[key]
            };
        };

        var searchOptions = {
            startDate: {
                start: moment(data.dates.earliest_start_date),
                end: moment(data.dates.latest_end_date),
                defaut: moment().subtract(9, 'day')
            },
            endDate: {
                start: moment(data.dates.earliest_start_date),
                end: moment(data.dates.latest_end_date),
                defaut: moment().subtract(2, 'day')
            },
            dimension: _.map(data.dimensions, getValueMap),
            segment: _.map(data.segments, getValueMap),
            filters: {}
        };

        searchOptions.segment.unshift({
            code: 'none',
            name: 'No breakdown'
        });

        _.forEach(data.filters, function(list, key) {
            list = _.sortBy(list, 'name');

            searchOptions.filters[key] = _.map(list, function(item) {
                item.code = item.code.toLowerCase();
                return item;
            });
        });

        return searchOptions;
    }

    /**
     * Get settings specific to angularjs-dropdown-multiselect directive to populate dropdown.
     *   Also populates actual filters values.
     * @param  {Collection} filters - raw filters data
     * @param  {Object}     dictionary - lookup map for filter text - REQUIRED
     * @return {Collection} [{"code":"device_type","name":"Device","values":[{"code":"d","name":"Desktop"},{"code":"g","name":"Game Console"}],"settings":{"displayProp":"name","idProp":"code","textLabels":{"checkAll":"Select all","uncheckAll":"Select none","buttonDefaultText":"Filter by ","dynamicButtonTextSuffix":"selected"}}}]
     */
    function getFilterDropdownSettings(filters, dictionary) {
        var defaultDropdownSettings = {
            displayProp: 'name',
            idProp: 'code',
            textLabels: {
                checkAll: 'Select all',
                uncheckAll: 'Select none',
                buttonDefaultText: 'Filter by ',
                dynamicButtonTextSuffix: 'selected'
            }
        };

        function extendSettings(data) {
            var settings = {};

            if (data.length > 10) {
                settings.enableSearch = true;
                settings.scrollable = true;
                settings.scrollableHeight = 'auto';
                settings.showCheckAll = true;
                settings.showUncheckAll = true;
            }

            settings = _.extend(_.clone(defaultDropdownSettings, true), settings);

            return settings;
        }

        return _.map(filters, function(list, key) {
            return {
                code: key,
                name: dictionary[key],
                values: list,
                settings: extendSettings(list)
            };
        });
    }

    /**
     * Return an object of querystring parameters + values on current page
     *    Validates the keys against a constant map. All values REQUIRED.
     *
     * @param  {Object}     params - key/value pair of current url parameters
     * @param  {Object}     filtersMap
     *
     * @return {Object}     {view: "something", dimension: "market", segment: "device_type"}
     */
    function parseQueryStringParameters(params, filtersMap) {
        var querystringMap = {
            sdate: 'startDate',
            edate: 'endDate',
            dimension: 'dimension',
            segment: 'segment',
            view: 'view'
        };

        _.forOwn(filtersMap, function(value, key) {
            querystringMap[key] = key;
        });

        var validKeys = {};

        _.forOwn(params, function(value, key) {
            if (querystringMap[key]) {
                validKeys[querystringMap[key]] = value;
            }
        });

        return validKeys;
    }

    /**
     * Translate included filters into query string parameters,
     *     values are comma-separated. All values required.
     *
     * @param  {Collection} includeFilters - list of included filters by key
     * @param  {Object}     filtersMap
     *
     * @return {Object} {flight_type: "i", market: "ag,co,ai"}
     */
    function makeFilterQueryStringParameters(includeFilters, filtersMap) {
        var queryStringMap = {};

        _.forOwn(includeFilters, function(values, key) {
            if (values.length !== filtersMap[key].length) {
                var rawValues = _.pluck(values, 'id');
                queryStringMap[key] = rawValues.join(',');
            }
        });

        return queryStringMap;
    }

    /**
     * Validates value of passed dimension. Bound via controller.
     *   this = dimension map
     * @param  {String} value - dimension value - REQUIRED
     * @return {Object} {code: "market", name: "Market"}
     */
    function validateDimension(value) {
        var self = this;

        return _.find(self, { code: value.toLowerCase() });
    }

    /**
     * Validates value of passed segment. Bound via controller.
     *   this = segment map
     *   Will set "this" attribute disabled to false conditionally.
     *
     * @param  {String} value - segment value - REQUIRED
     * @param  {String} key - not used
     * @param  {Object} params - used to validate dimension value, if present
     * @return {Object} {code: "market", name: "Market"}
     */
    function validateSegment(value, key, params) {
        var self = this;
        var activeSegment;

        value = value.toLowerCase();

        if (value === params.dimension) {
            activeSegment = _.find(self, { code: value });

            if (activeSegment) {
                activeSegment.disabled = true;
            }

            return self[0];
        } else {
            _.forEach(self, function(segment) {
                if (segment.code === params.dimension) {
                    segment.disabled = true;
                }

                if (segment.code === value) {
                    activeSegment = segment;
                }
            });

            return activeSegment;
        }
    }

    /**
     * Validates value of passed date. Bound via controller.
     *   this = dates map
     * @param  {String} value - date value - REQUIRED
     * @return {Object} Moment object
     */
    function validateDate(value) {
        var self = this;
        var inputDateFormat = 'YYYY-MM-DD';
        var tempValue;

        if (moment(value, inputDateFormat, true).isValid()) {
            tempValue = moment(value, inputDateFormat);

            if (tempValue.diff(self.start, 'day') >= 0 && tempValue.diff(self.end, 'day') <= 0) {
                return tempValue;
            }
        }

        return self.defaut;
    }

    /**
     * Validates comma-separated values of filters. Bound via controller.
     *   this = filters map
     * @param  {String} values - comma-separated values of filters - REQUIRED
     * @return {Array} ['jl', 'sq']
     */
    function validateFilters(values, key) {
        var self = this;
        var filterValues = _.find(self, { code: key.toLowerCase() }).values;
        var validValues = [];

        _.forEach(values.split(','), function(d) {
            var value = d.toLowerCase();

            if (_.some(filterValues, { code: value })) {
                validValues.push(value);
            }
        });

        return validValues;
    }

    /**
     * Validates value of passed view. Bound via controller.
     *   this = view map
     * @param  {String} value - view code value - REQUIRED
     * @return {Object} { id: 'scrape_count', code: 'apiqueries', title: 'API Queries', type: 'count', relationship: ['look2book'], disabled: false, highlighted: false }
     */
    function validateView(value) {
        var self = this;

        return _.find(self, { code: value.toLowerCase() });
    }

    /**
     * Validates key-pair value object against key-value validator.
     * @param  {Object} params - object to be validated
     * @param  {Object} validators - object containing validating functions
     * @return {Object} validated object and values
     */
    function validateUrlParams(params, validators, filtersConstants) {
        var validParams = {};

        _.forOwn(params, function(value, key) {
            if (validators[key]) {
                validParams[key] = validators[key](value, key, params);
                return;
            } else if (filtersConstants[key]) {
                validParams[key] = validators.filters(value, key);
            }
        });

        return validParams;
    }

    /**
     * Get a list of active filters used to control UI
     * @param  {Collection} options.filters - raw list of filter values
     * @param  {Object}     options.presetValues - values typically passed from querystrings
     * @param  {Collection} options.searchOptions
     * @param  {Collection} options.dateFormat - date format to shown on the datepicker
     * @return {Object} {"dimension":{"code":"market","name":"Market"},"segment":{"code":"device_type","name":"Device"},"startDate":"25 Jun 2015","endDate":"27 Jun 2015","includeFilters":{"carrier":[{"id":"SI"},{"id":"AF"}],"device_type":[{"id":"M"}],"flight_type":[{"id":"I"}],"market":[{"id":"AU"}],"traffic_type":[{"id":"alsoflies"},{"id":"flights"}]},"excludeFilters":{}}
     */
    function getActiveFilters(options) {
        var filters = options.filters;
        var presetValues = options.presetValues || {};
        var searchOptions = options.searchOptions || {};
        var calendarDateFormat = options.dateFormat;

        var activeFilters = {
            dimension: presetValues.dimension || _.find(searchOptions.dimension, { code: 'date' }),
            segment: presetValues.segment || searchOptions.segment[0],
            startDate: presetValues.startDate ? presetValues.startDate : searchOptions.startDate.defaut,
            endDate: presetValues.endDate ? presetValues.endDate : searchOptions.endDate.defaut,
            includeFilters: {},
            excludeFilters: {}
        };

        if (activeFilters.endDate.diff(activeFilters.startDate, 'day') < 0) {
            if (searchOptions.endDate.defaut.diff(activeFilters.startDate, 'day') < 0) {
                activeFilters.endDate = searchOptions.endDate.end;
            } else {
                activeFilters.endDate = searchOptions.endDate.defaut;
            }
        }

        activeFilters.startDate = moment(activeFilters.startDate).format(calendarDateFormat);
        activeFilters.endDate = moment(activeFilters.endDate).format(calendarDateFormat);

        _.forEach(filters, function(list, key) {
            if (presetValues[key] && presetValues[key].length) {
                activeFilters.includeFilters[key] = _.map(presetValues[key], function(key) {
                    return {
                        id: key
                    };
                });
            } else {
                activeFilters.includeFilters[key] = [];

                _.forEach(list, function(item) {
                    activeFilters.includeFilters[key].push({ id: item.code });
                });
            }
        });

        return activeFilters;
    }

    /**
     * Get a list of excluded filter values - maps to empty arrays if all values selected.
     * @param  {Collection} included list - maps to all values selected in the UI - REQUIRED
     * @param  {Object} originalFilterMap - the raw filters data - REQUIRED
     * @return {Collection} {"carrier":["AF"],"device_type":["D","G"],"flight_type":["I"],"market":[],"traffic_type":["alsoflies"]}
     */
    function getExcludedFilterList(selectedFilterList, originalFilterMap) {
        var excludeList = {};
        var originalFilterList = {};

        _.forOwn(originalFilterMap, function(values, key) {
            originalFilterList[key] = _.pluck(values, 'code');
        });

        _.forOwn(selectedFilterList, function(value, key) {
            var includes = _.pluck(value, 'id');
            var originals = originalFilterList[key];

            excludeList[key] = _.difference(originals, includes);
        });

        return excludeList;
    }

    /**
     * Mutate current collection to set active view and reset highlighted attribute
     *   based on the defaultView object passed.
     *   Assumes the defaultView is NOT disabled - all fields REQUIRED
     * @param  {Collection} views
     * @param  {Object}     defaultView
     * @return same as @getMetricsSummary
     */
    function _switchActiveView(views, defaultView) {
        _.forEach(views, function(v) {
            if (v.code === defaultView.code) {
                v.active = true;
            }

            v.highlighted = false;

            if (defaultView.relationship.indexOf(v.code) > -1) {
                v.highlighted = true;
            }
        });

        return views;
    }

    /**
     * Mutate current metric to disable metric based on the passed identifiers
     * @param  {Object} metric
     * @param  {Array}  [dimensionCode, segmentCode]
     * @return the mutated metric object
     */
    function _validateMetricDisabled(metric, identifiers) {
        var disabledIdentifiers = ['carrier', 'device_type', 'traffic_type'];
        var disabledMetrics = ['searches', 'apiqueries', 'look2book'];
        var matchIdentifier;

        if (disabledMetrics.indexOf(metric.code) > -1) {
            matchIdentifier = _.find(disabledIdentifiers, function(id) {
                return identifiers.indexOf(id) > -1;
            });

            if (matchIdentifier) {
                metric.alerted = true;
                metric.disabled = true;
                metric.description = _.startCase(matchIdentifier) + ' breakdown not available for ' + metric.title;
            }
        }

        return metric;
    }

    /**
     * Compact large collection at specified index into an "Other" item
     * @param  {Collection}     options.data - original data
     * @param  {Number}         options.startIndex - the array index to start compacting
     * @param  {String}         options.dimension
     * @param  {String}         options.segment
     * @param  {Collection}     options.viewMap - friendly view map
     * @return {[type]}         [description]
     */
    function _compactData(options) {
        var dimension = options.dimension;
        var segment = options.segment;
        var dataLen = options.data.length;
        var friendlyViewMaps = options.viewMap;

        var compactedData = options.data.slice(0, options.startIndex);
        var dataToCompact = options.data.slice(options.startIndex, dataLen);
        var dataToCompactLen = dataToCompact.length;
        var viewTypeMap = {};

        var compactedItem = {};
        var firstSegment;

        compactedItem[dimension] = [dataToCompact.length, ' other ', dimension, 's'].join('');
        compactedItem.metrics = _.clone(compactedData[0].metrics, true);

        if (segment) {
            firstSegment = _.findKey(compactedItem.metrics);

            _.forOwn(compactedItem.metrics[firstSegment], function(value, key) {
                viewTypeMap[key] = _.find(friendlyViewMaps, { id: key }).type;
            });

            _.forOwn(compactedItem.metrics, function(metrics, segment) {
                _.forOwn(metrics, function(metric, key) {
                    var total = _.sum(_.pluck(dataToCompact, 'metrics.' + segment + '.' + key));

                    if (viewTypeMap[key] !== 'count') {
                        total = (total / dataToCompactLen);
                    }

                    compactedItem.metrics[segment][key] = total;
                });
            });
        } else {
            _.forOwn(compactedItem.metrics, function(value, key) {
                viewTypeMap[key] = _.find(friendlyViewMaps, { id: key }).type;
            });

            _.forOwn(compactedItem.metrics, function(metric, key) {
                var total = _.sum(_.pluck(dataToCompact, 'metrics.' + key));

                if (viewTypeMap[key] !== 'count') {
                    total = (total / dataToCompactLen);
                }

                compactedItem.metrics[key] = total;
            });
        }

        compactedData = compactedData.concat(compactedItem);

        return compactedData;
    }

    /**
     * Return ng-repeat ready list of summary totals - all fields REQUIRED
     * @param  {Object} options.data - raw summary object
     * @param  {Object} options.fieldNames - view dictionary
     * @param  {String} options.activeViewCode - current active view code
     * @param  {String} options.dimension - code
     * @param  {String} options.segment - code
     * @param  {Object} options.defaultView - default view if active view is disabled -
     *   this view should logically be never disabled.
     *
     * @return {Collection} [{"id":"search_count","code":"searches","title":"Searches","type":"count","relationship":["clickthrough"],"disabled":false,"value":"332,810","active":true,"highlighted":false},{"id":"scrape_count","code":"apiqueries","title":"API Queries","type":"count","relationship":["look2book"],"disabled":false,"value":"178,211","active":false,"highlighted":false},{"id":"redirect_count","code":"redirects","title":"Redirects","type":"count","relationship":["clickthrough","conversion"],"disabled":false,"value":"39,313","active":false,"highlighted":false},{"id":"booking_count","code":"bookings","title":"Bookings","type":"count","relationship":["look2book","conversion"],"disabled":false,"value":"2,194","active":false,"highlighted":false},{"id":"clickthrough_rate","code":"clickthrough","title":"Clickthrough","type":"rate","description":"Overall ratio between searches and redirects","relationship":["searches","redirects"],"disabled":false,"value":"8.26%","active":false,"highlighted":true},{"id":"conversion_rate","code":"conversion","title":"Conversion","type":"rate","description":"Overall ratio between redirects and bookings","relationship":["redirects","bookings"],"disabled":false,"value":"12.00%","active":false,"highlighted":false},{"id":"scrape2book_rate","code":"look2book","title":"Look2Book","type":"rate","description":"Overall ratio between apiqueries and bookings","relationship":["apiqueries","bookings"],"disabled":false,"value":"9.19%","active":false,"highlighted":false}]
     */
    function getMetricsSummary(options) {
        var summaryData = options.data;
        var viewList = options.fieldNames;
        var activeViewCode = options.activeViewCode;
        var defaultView = options.defaultView;
        var dimension = options.dimension;
        var segment = options.segment;
        var requireSwitchingToDefaultView = false;

        var summary = [];

        _.forOwn(summaryData, function(value, key) {
            var arrayIndex = _.findIndex(viewList, { id: key });
            var extendedViewInfo = _.find(viewList, { id: key });

            var metric = {};

            if (!_.isEmpty(extendedViewInfo)) {
                metric = _.extend(metric, extendedViewInfo);
                metric.disabled = (value === null || value === 0) ? true : false;

                metric = _validateMetricDisabled(metric, [dimension, segment]);

                if (value === null) {
                    metric.value = 'N/A';
                    metric.description = 'Booking data not available';
                } else {
                    if (metric.type === 'percentage') {
                        metric.value = $filter('number')(value, 2) + '%';
                    } else if (metric.type === 'ratio') {
                        metric.value = $filter('number')(value) + ':1';
                    } else {
                        metric.value = $filter('number')(value);
                    }
                }

                if (metric.disabled) {
                    metric.active = false;

                    if (metric.code === activeViewCode) {
                        requireSwitchingToDefaultView = true;
                    }
                } else {
                    metric.active = (metric.code === activeViewCode) ? true : false;
                    metric.highlighted = metric.relationship.indexOf(activeViewCode) > -1 ? true : false;
                }
            }

            summary[arrayIndex] = metric;
        });

        summary = _.compact(summary);

        if (requireSwitchingToDefaultView) {
            summary = _switchActiveView(summary, defaultView);
        }

        return summary;
    }

    /**
     * Parse through raw data and return chart-friendly
     * @param  {Collection} data - raw dimensions data from backend
     * @return {Object} {"chartData": [{"date": "2015-06-22T00:00:00", "metrics": {"th": {"booking_count": 0, "redirect_count": 3648, "search_count": 0, "conversion_rate": 0, "clickthrough_rate": 0 } } } ], "dimension": "date", "segment": "market"}
     */
    function getMetricsData(data) {
        var chartData = [];
        var dimension;
        var segment;

        if (data.length) {
            dimension = data[0].id.type;
            segment = _.get(data[0], 'segments[0].id.type');
        }

        _.forEach(data, function(d1) {
            var row = {};
            row[d1.id.type] = d1.id.value;

            if (d1.metrics) {
                row.metrics = d1.metrics;
            }

            if (d1.segments) {
                row.metrics = {};
                _.forEach(d1.segments, function(d2) {
                    row.metrics[d2.id.value] = d2.metrics;
                });
            }

            chartData.push(row);
        });

        return {
            chartData: chartData,
            dimension: dimension,
            segment: segment
        };
    }

    /**
     * Return a pre-configured C3 bar chart with these settings:
     *   - fixed width: issues with bleeding chart if not set
     *   - maximum x ticks of 60
     *
     * @param  {String} el - jquery selector - REQUIRED
     * @return {Object} C3 chart
     */
    function getChart(el) {
        return c3.generate({
            bindto: el,
            data: {
                columns: []
            },
            transition: {
                duration: 0
            },
            axis: {
                x: {
                    height: 90,
                    type: 'category',
                    tick: {
                        fit: true,
                        rotate: 45,
                        multiline: false,
                        centered: false
                    },
                    label: {
                        position: 'outer-right'
                    },
                    padding: {
                        left: 0,
                        right: 0
                    }
                },
                y: {
                    min: 0,
                    padding: {
                        top: 5,
                        bottom: 0
                    },
                    label: {
                        position: 'outer-top'
                    },
                    tick: {
                        format: function(y) {
                            var format = d3.format(',');
                            return format(y);
                        }
                    }
                }
            },
            grid: {
                focus: {
                    show: true
                },
                y: {
                    show: true
                }
            },
            point: {
                r: 3,
                focus: {
                    expand: {
                        r: 5
                    }
                }
            },
            padding: {
                right: 15
            },
            size: {
                width: angular.element(el).width(),
                height: 350
            },
            legend: {
                position: 'bottom'
            },
            color: {
                pattern: [
                    '#44cdde', '#f0d700', '#91c043', '#f67979',
                    '#7ee8f1', '#f7e975', '#bfd992', '#fbbbbb',
                    '#21889e', '#e49404', '#5b821a', '#f23939',
                    '#5be2ed', '#f3e03b', '#a8cd6b', '#f9a0a0'
                ]
            },
            tooltip: {
                format: {
                    value: function(value, ratio, id) {
                        var format = d3.format(',');
                        value = d3.round(value, 2);

                        if (!_isInteger(value)) {
                            value = format(value) + '%';
                        } else if (_.endsWith(id, 'ratio')) {
                            value = format(value) + ':1';
                        } else {
                            value = format(value);
                        }

                        return value;
                    }
                }
            }
        });
    }

    /**
     * Get chart data and settings per view - all fields REQUIRED
     * @param  {Collection} options.data
     * @param  {String}     options.dimension
     * @param  {String}     options.segment
     * @param  {String}     options.view - viewId to match metric key
     * @param  {Object}     options.fieldNames - dictionary
     * @return {Object}     {"axesLabels":{"x":"Date","y":"Number of Redirects"},"groups":[],"names":{"redirect_count":"Redirects"},"columns":[["redirect_count",889,1095,1144,879,1491,1201,839,895,1934,819,1411]],"chartType":"bar","xTickLabels":["01 Jun","02 Jun","03 Jun","04 Jun","05 Jun","06 Jun","07 Jun","08 Jun","09 Jun","10 Jun","11 Jun"]}
     */
    function getChartData(options) {
        var view = options.view;
        var dimension = options.dimension;
        var segment = options.segment;
        var chartData = options.data;

        var friendlyDimensionMaps = options.fieldNames.dictionary;
        var friendlyViewMaps = options.fieldNames.view;
        var friendlyFilterMaps = options.fieldNames.filters;

        var axesLabels = {};
        var xTickLabels = [];
        var viewProps = _.find(friendlyViewMaps, { id: view });
        var chartType;

        var names = {};
        var viewName = {};
        var columns = [];
        var groups = [];
        var segments;

        var compactingIndex = 60;

        // lump long tail into Other
        if (options.data.length > compactingIndex && dimension !== 'date') {
            chartData = _compactData({
                data: chartData,
                startIndex: compactingIndex,
                dimension: dimension,
                segment: segment,
                viewMap: friendlyViewMaps
            });
        }

        // xTickLabels
        if (dimension === 'date') {
            xTickLabels = _.map(chartData, function(d) {
                return moment(d[dimension]).format('DD MMM');
            });
        } else {
            xTickLabels = _.map(chartData, function(d) {
                var friendlyNameMap = _.find(friendlyFilterMaps[dimension], { code: d[dimension] });
                var friendlyName = friendlyNameMap && friendlyNameMap.name || d[dimension];

                return friendlyName;
            });
        }

        // chartType & axesLabels
        viewName[view] = viewProps.title;
        axesLabels.x = friendlyDimensionMaps[dimension];

        if (viewProps.type === 'count') {
            axesLabels.y = 'Number of ' + viewName[view];
            chartType = 'bar';
        } else {
            axesLabels.y = viewName[view] + ' Rate (%)';

            if (dimension === 'date') {
                chartType = 'line';
            } else {
                chartType = 'scatter';
            }
        }

        // segments, names, columns, groups
        if (segment) {
            segments = _.keys(chartData[0].metrics);

            _.forEach(segments, function(d) {
                var column = [d].concat(_.pluck(chartData, 'metrics.' + d + '.' + [view]));
                column = _.map(column, function(d) {
                    return d === null ? 0 : d;
                });

                columns.push(column);
            });

            /** TODO: refactor this */
            if (segments.length > 10) {
                var compactedSegmentsLength = segments.length - 10;
                segments = segments.slice(0, 10);
                segments.push('others');

                var allSegmentsTotals = [];

                _.forEach(segments, function(d) {
                    var sumPerSegment = _.sum(_.pluck(chartData, 'metrics.' + d + '.' + [view]));
                    allSegmentsTotals.push({
                        segment: d,
                        total: sumPerSegment
                    });
                });

                allSegmentsTotals = _.sortBy(allSegmentsTotals, 'total');
                var top10Segments = allSegmentsTotals.reverse().slice(0, 10);

                var sortedColumns = [];

                _.forEach(top10Segments, function(d, index) {
                    columns = _.compact(columns);
                    var foundIndex = _.findIndex(columns, function(col) {
                        return _.first(col) === d.segment;
                    });

                    sortedColumns[index] = columns.splice(foundIndex, 1)[0];
                });

                sortedColumns[10] = [];
                for (var i = 0; i < sortedColumns[0].length; i++) {
                    if (i > 0) {
                        sortedColumns[10][i] = 0;
                    } else {
                        sortedColumns[10][0] = 'others';
                    }
                }

                _.forEach(columns, function(col) {
                    _.forEach(col, function(d, index) {
                        if (index > 0) {
                            sortedColumns[10][index] += +d;
                        }
                    });
                });

                if (viewProps.type !== 'count') {
                    _.forEach(sortedColumns[10], function(value, index, arr) {
                        if (index > 0) {
                            arr[index] = value / compactedSegmentsLength;
                        }
                    });
                }

                columns = sortedColumns;
                names.others = [compactedSegmentsLength, ' other ', segment, 's'].join('');
            }

            _.forEach(friendlyFilterMaps[segment], function(d) {
                names[d.code] = d.name;
            });

            // remove stacking in line chart
            if (viewProps.type === 'count') {
                groups = segments;
            } else {
                groups = [];
            }
        } else {
            columns.push(view);

            _.forEach(chartData, function(d) {
                var number = d.metrics[view] === null ? 0 : d.metrics[view];
                columns.push(number);
            });

            columns = [columns];
            names = viewName;
        }

        return {
            axesLabels: axesLabels,
            groups: groups,
            names: names,
            segments: segments,
            columns: columns,
            chartType: chartType,
            xTickLabels: xTickLabels
        };
    }

    /**
     * Render chart - all fields required
     * @param  {Object}     options.el - chart container string
     * @param  {Object}     options.axesLabels - x and y labels
     * @param  {Array}      options.groups - valid for when segment present - stacking
     * @param  {Object}     options.names - friendly names in tooltips
     * @param  {Object}     options.segments
     * @param  {Array}      options.columns - data
     * @param  {String}     options.chartType - "bar" or "line"
     * @param  {Array}      options.xTickLabels
     * @param  {Array}      options.currentLoad
     * @return  undefined
     */
    function updateChart(options) {
        options.chart.axis.labels({
            x: options.axesLabels.x,
            y: options.axesLabels.y
        });

        if (options.segments) {
            options.chart.groups([options.groups]);
        } else {
            options.chart.legend.hide();
        }

        options.chart.data.names(options.names);

        options.chart.load({
            unload: options.currentLoad,
            columns: options.columns,
            type: options.chartType,
            categories: options.xTickLabels
        });

        return _.chain(options.columns)
                .map(function(a) {
                    return a[0];
                })
                .value();
    }

    /**
     * Check if number is integer or floating point
     * @param  {Integer}  number
     * @return {Boolean}
     */
    function _isInteger(number) {
        return (typeof number === 'number') && (number % 1 === 0);
    }

    /**
     * Get the header row vales mapped in order to dictionary - all params REQUIRED
     * @param {Array}      sourceData - first row data of the table
     * @param {Collection} sourceMapData - lookup dictionary object
     * @param {String}     mapKeyId - the key used in dictionary to match the sourceData key
     * @param {String}     mapKeyText - the key used in dictionary to return the
     *   friendly text for the actual cell text
     * @returns {Object}   An object of two keys: index and data, e.g:
     *   {"index":["M","T","D"],"data":[{"M":"Mobile Phone"},{"T":"Tablet"},{"D":"Desktop"}]}
     */
    function _getTableHeaderRowIndexData(sourceData, sourceMapData, mapKeyId, mapKeyText) {
        var indexData = [];
        var cellTextData = [];

        _.forOwn(sourceData, function(value, key) {
            var arrayIndex = _.findIndex(sourceMapData, function(m) {
                return m[mapKeyId] === key;
            }) + 1;

            var metricMap = {};

            var friendlyName = _.find(sourceMapData, function(m) {
                return m[mapKeyId] === key;
            });

            metricMap[key] = friendlyName ? friendlyName[mapKeyText] : key;

            indexData[arrayIndex] = key;
            cellTextData[arrayIndex] = metricMap;
        });

        return {
            index: _.compact(indexData),
            data: _.compact(cellTextData)
        };
    }

    /**
     * Get the total row to be appended to top of tbody
     * @param {Array/Object}   summaryData - the totals summary, Object for non-segment, Array for segment - REQUIRED
     * @param {String}         dimension - REQUIRED
     * @param {Boolean}        hasSegment
     * @param {String}         viewType - "rate": "Overall", others: "Totals"
     * @returns {Object}       an object following format of tbody content, e.g:
     *   {"date":"Totals","metrics":{"search_count":132810,"scrape_count":58261,"redirect_count":9313,"booking_count":68,"conversion_rate":5.4,"clickthrough_rate":1.6,"scrape2book_rate":0.31}}
     */
    function _getTableRowTotals(summaryData, dimension, hasSegment, viewType) {
        var totalRow = {};
        var totalCellText = 'Totals';

        if (!_.isEmpty(summaryData) || summaryData.length) {

            // not applicable for non-segmented table due to one-time render, defaults to Totals
            if (hasSegment && viewType !== 'count') {
                totalCellText = 'Overall';
            }

            totalRow[dimension] = totalCellText;

            if (!hasSegment) {
                totalRow.metrics = summaryData;
            } else {
                totalRow.metrics = {};

                _.forEach(summaryData, function(segment) {
                    totalRow.metrics[segment.id.value] = segment.metrics;
                });

                totalRow.metrics._total = null;
            }
        }

        return totalRow;
    }

    /**
     * Append a total column to row, done on whole collection - all params REQUIRED
     * @param  {Collection} sourceData - source row data
     * @param  {Collection} summaryData - same format as source row data, for the totals
     * @param  {String}     dimension
     * @return {Collection} [{"date":"2015-06-01","metrics":{"M":{"search_count":1633,"scrape_count":2914},"D":{"search_count":4216,"scrape_count":2110},"T":{"search_count":8253,"scrape_count":2090},"_total":{"search_count":40000,"scrape_count":4000}}}]
     */
    function _getTableColumnTotals(sourceData, summaryData, dimension) {
        return _.map(sourceData, function(d) {
            var metricsTotal = _.find(summaryData, function(s) {
                return s.id.value  === d[dimension];
            });

            d.metrics._total = metricsTotal.metrics || null;

            return d;
        });
    }

    /**
     * Get an ordered-by-column-header collection of rows
     * @param  {Collection} sourceData - source row data - REQUIRED
     * @param  {Array}      headerIndex - the order of segment code, to ensure cell match
     *    the same column as header - REQUIRED
     * @param  {String}     dimension - REQUIRED
     * @param  {String}     metricsKey - the current view code that dictates which metric to show
     * @return {Collection} [[{"date":"Totals"},{"search_count":"132,810"},{"scrape_count":"58,261"}],[{"date":"01 Jun 15"},{"search_count":"9,913"},{"scrape_count":"2,084"}]
     */
    function _getTableBodyData(sourceData, headerIndex, friendlyNameMap, friendlyViewMap, dimension, metricsKey) {
        var bodyData = [];
        var type = metricsKey ? _.find(friendlyViewMap, { id: metricsKey }).type : '';

        _.forEach(sourceData, function(d, index) {
            var row = [];
            var dimensionMap = {};
            var friendlyName;

            if (index === 0 && (d[dimension] === 'Totals' || d[dimension] === 'Overall')) {
                dimensionMap[dimension] = d[dimension];
            } else if (dimension === 'date') {
                dimensionMap[dimension] = moment(d[dimension]).format('DD MMM YY');
            } else {
                friendlyName = _.find(friendlyNameMap, { code: d[dimension] });
                dimensionMap[dimension] = friendlyName ? friendlyName.name : d[dimension];
            }

            row.push(dimensionMap);

            _.forOwn(d.metrics, function(metrics, key) {
                var arrayIndex = headerIndex.indexOf(key) + 1;
                var colMap = {};
                var value;

                if (metrics && metricsKey) { //is segment
                    value = metrics[metricsKey];
                } else {
                    value = metrics;

                    if (metrics !== null) {
                        type = _.find(friendlyViewMap, { id: key }).type;
                    }
                }

                if (value === null) {
                    colMap[key] = 'N/A';
                } else {
                    switch (type) {
                    case 'count':
                        colMap[key] = $filter('number')(value);
                        break;
                    case 'percentage':
                        colMap[key] = $filter('number')(value, 2) + '%';
                        break;
                    case 'ratio':
                        colMap[key] = $filter('number')(value) + ':1';
                        break;
                    }
                }

                row[arrayIndex] = colMap;
            });

            bodyData.push(row);
        });

        return bodyData;
    }

    /**
     * Given the same chartData format, returns an ng-repeat ready table
     * @param  {Collection} options.data - REQUIRED
     * @param  {String}     options.view - view.id to match key used in raw data, e.g. search_count - REQUIRED
     * @param  {String}     options.dimension - dimension code - REQUIRED
     * @param  {String}     options.segment - segment code
     * @param  {Object/Collection} options.dimensionTotals - in object or collection format
     * @param  {Collection} options.segmentTotals
     * @param  {Object}     options.fieldNames - _CONSTANTS - REQUIRED
     * @returns {Object}    {"header":[{"date":"Date"},{"_total":"Totals"},{"M":"Mobile Phone"},{"T":"Tablet"},{"D":"Desktop"}],"body":[[{"date":"Totals"},{"_total":null},{"M":"343,281"},{"T":"129,941"},{"D":"718,912"}],[{"date":"01 Jun 15"},{"_total":"40,000"},{"M":"1,633"},{"T":"8,253"},{"D":"4,216"}]]}
     */
    function getTableData(options) {
        var data = _.clone(options.data, true);     //array.slice doesn't copy nested object
        var view = options.view;
        var dimension = options.dimension;
        var segment = options.segment;
        var viewTotal = options.viewTotal;

        var dimensionTotals = options.dimensionTotals;
        var segmentTotals = options.segmentTotals;

        var friendlyDimensionMaps = options.fieldNames.dictionary;
        var friendlyViewMaps = options.fieldNames.view;
        var friendlyFilterMaps = options.fieldNames.filters;

        var headerIndexData = {};
        var tableRowTotals = [];
        var tableHeader = [];
        var tableBody = [];
        var viewType = _.find(friendlyViewMaps, { id: view }).type;

        var dimensionMap = {};
        dimensionMap[dimension] = friendlyDimensionMaps[dimension];
        tableHeader.push(dimensionMap);

        if (!segment) {
            headerIndexData = _getTableHeaderRowIndexData(data[0].metrics, friendlyViewMaps, 'id', 'title');
            tableHeader = tableHeader.concat(headerIndexData.data);

            if (!_.isEmpty(dimensionTotals)) {
                tableRowTotals = _getTableRowTotals(dimensionTotals, dimension);
                data.unshift(tableRowTotals);
            }

            tableBody = _getTableBodyData(data, headerIndexData.index, friendlyFilterMaps[dimension], friendlyViewMaps, dimension);
        } else {
            headerIndexData = _getTableHeaderRowIndexData(data[0].metrics, friendlyFilterMaps[segment], 'code', 'name');
            tableHeader = tableHeader.concat(headerIndexData.data);

            if (dimensionTotals.length) {
                headerIndexData.index.splice(0, 0, '_total');
                tableHeader.splice(1, 0, {
                    _total: viewType !== 'count' ? 'Overall' : 'Totals'
                });

                data = _getTableColumnTotals(data, dimensionTotals, dimension);
            }

            if (segmentTotals.length) {
                tableRowTotals = _getTableRowTotals(segmentTotals, dimension, true, viewType);

                if (!_.isEmpty(tableRowTotals)) {
                    data.unshift(tableRowTotals);
                }
            }

            tableBody = _getTableBodyData(data, headerIndexData.index, friendlyFilterMaps[dimension], friendlyViewMaps, dimension, view);
            tableBody[0][1]._total = viewTotal;
        }

        return {
            header: tableHeader,
            body: tableBody
        };
    }

    return {
        getConstants: getConstants,
        getSearchFilterOptions: getSearchFilterOptions,
        getFilterDropdownSettings: getFilterDropdownSettings,
        getActiveFilters: getActiveFilters,
        parseQueryStringParameters: parseQueryStringParameters,
        makeFilterQueryStringParameters: makeFilterQueryStringParameters,
        validateDimension: validateDimension,
        validateSegment: validateSegment,
        validateDate: validateDate,
        validateFilters: validateFilters,
        validateView: validateView,
        validateUrlParams: validateUrlParams,
        getExcludedFilterList: getExcludedFilterList,
        getMetricsSummary: getMetricsSummary,
        getMetricsData: getMetricsData,
        getChart: getChart,
        getChartData: getChartData,
        updateChart: updateChart,
        getTableData: getTableData
    };
});

}());
