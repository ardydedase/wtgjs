(function() {

'use strict';

describe('Service: timedCacheService', function() {

    var rootScope;
    var timedCacheService;
    var mockLocalStorageService;
    var fakeStore;

    beforeEach(module('fcApp.commercial'));

    beforeEach(module('fcApp.commercial', function($provide) {
        mockLocalStorageService = {
            get: function(storeName) {
                if (storeName) {
                    return fakeStore[storeName];
                } else {
                    return fakeStore;
                }
            },

            remove: function(storeName) {
                delete fakeStore[storeName];
            },

            add: function(storeName, data) {
                fakeStore[storeName] = data;
            }
        };

        $provide.value('localStorageService', mockLocalStorageService);
        $provide.value('_', window._);
        $provide.value('moment', window.moment);
    }));

    beforeEach(inject(function($rootScope, _timedCacheService_) {
        rootScope = $rootScope.$new();
        timedCacheService = _timedCacheService_;
    }));

    describe('#getInstance', function() {
        it('should be defined', function() {
            expect(timedCacheService.getInstance).toBeDefined();
        });

        it('should return an instance object with accessible methods', function() {
            var testCache = timedCacheService.getInstance({});

            expect(testCache.get).toBeDefined();
            expect(testCache.save).toBeDefined();
            expect(testCache.remove).toBeDefined();
        });

        it('should set default properties when no parameter is passed', function() {
            var testCache = timedCacheService.getInstance({});

            expect(testCache.name).toEqual('');
            expect(testCache.websiteId).toEqual('');
            expect(testCache.cacheTime).toEqual(1);
            expect(testCache.cacheKey).toEqual('data');
            expect(testCache.cachedData).toEqual([]);
        });

        it('should set internal properties when parameters are passed', function() {
            fakeStore = {
                hello: []
            };

            var testCache = timedCacheService.getInstance({
                name: 'hello',
                websiteId: 'dddd',
                cacheTime: 5,
                cacheKey: 'hullabaloo'
            });

            expect(testCache.name).toEqual('hello');
            expect(testCache.websiteId).toEqual('dddd');
            expect(testCache.cacheTime).toEqual(5);
            expect(testCache.cacheKey).toEqual('hullabaloo');
        });

        describe('methods', function() {
            var defaultOptions;

            beforeEach(function() {
                defaultOptions = {
                    websiteId: 'xxxx',
                    name: 'myConfig',
                    cacheTime: 12,
                    cacheKey: 'regions'
                };

                fakeStore = {
                    irrelevant: 'blah',
                    myConfig: [{
                        websiteId: 'zzzz',
                        date: window.moment().toDate(),
                        regions: [{
                            name: 'Mexico',
                            id: 'MX'
                        }, {
                            name: 'Turkey',
                            id: 'TR'
                        }]
                    }]
                };
            });

            describe('#get', function() {
                it('should return data that matches the parameters', function() {
                    defaultOptions.websiteId = 'zzzz';

                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);

                    expect(timedCacheInstance.get()).toEqual(fakeStore.myConfig[0].regions);
                });

                it('should return nothing if localStorage key doesn\'t exist', function() {
                    fakeStore = {};

                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);

                    expect(timedCacheInstance.get()).toBeUndefined();
                });

                it('should return nothing if there is no data', function() {
                    fakeStore.myConfig = [];

                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);

                    expect(timedCacheInstance.get()).toBeUndefined();
                });

                it('should return nothing if there is no data that matches the websiteId', function() {
                    fakeStore.myConfig[0].websiteId = 'aaaa';

                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);

                    expect(timedCacheInstance.get()).toBeUndefined();
                });

                it('should return nothing if the matched data has expired', function() {
                    fakeStore.myConfig[0].date = window.moment().subtract(13, 'hours').toDate();

                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);

                    expect(timedCacheInstance.get()).toBeUndefined();
                });
            });

            describe('#save', function() {
                var fakeData;

                beforeEach(function() {
                    fakeData = ['Africa', 'Antarctica'];
                });

                it('should save new data to localStorage', function() {
                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);
                    var fakeData = ['black', 'blue'];

                    timedCacheInstance.save(fakeData);

                    expect(fakeStore.myConfig.length).toEqual(2);
                });

                it('should append date time to the new data saved', function() {
                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);

                    timedCacheInstance.save(fakeData);

                    expect(fakeStore.myConfig[1].date).toBeDefined();
                    expect(fakeStore.myConfig[1].date instanceof Date).toBe(true);
                });

                it('should append new data to the existing data', function() {
                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);
                    var fakeData = ['black', 'blue'];

                    timedCacheInstance.save(fakeData);

                    expect(fakeStore.myConfig[0].websiteId).toEqual('zzzz');
                    expect(fakeStore.myConfig[0].regions).toEqual(fakeStore.myConfig[0].regions);
                    expect(fakeStore.myConfig[1].websiteId).toEqual('xxxx');
                    expect(fakeStore.myConfig[1].regions).toEqual(fakeData);
                });

                it('should replace existing data', function() {
                    defaultOptions.websiteId = 'zzzz';
                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);
                    var fakeData = [{
                        name: 'Ireland',
                        id: 'IE'
                    }];

                    timedCacheInstance.save(fakeData);

                    expect(fakeStore.myConfig.length).toEqual(1);
                    expect(fakeStore.myConfig[0].websiteId).toEqual('zzzz');
                    expect(fakeStore.myConfig[0].regions).toEqual(fakeData);
                });
            });

            describe('#remove', function() {
                it('should remove everything', function() {
                    var timedCacheInstance = timedCacheService.getInstance(defaultOptions);

                    timedCacheInstance.remove();

                    expect(fakeStore.myConfig).toBeUndefined();
                });
            });
        });
    });
});

}());
