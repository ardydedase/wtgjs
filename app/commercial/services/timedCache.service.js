(function() {

'use strict';

angular.module('fcApp.commercial').factory('timedCacheService', function(
    _,
    moment,
    localStorageService) {

    /**
     * Return an object that reads from localStorage **UPON INSTANTIATION**
     *   and therefore not to be used when LS values can be updated any time.
     *   This is NOT meant to be a singleton.
     *   See websiteIdService for example of usage.
     *   Creating object without passing parameters doesn't make sense.
     *   All fields are required. Defaults are for failsafe.
     *
     *  Data is saved in this format:
     *  [{
     *      websiteId:"aljp",
     *      date:"2015-06-28T05:39:23.891Z",
     *      cacheKey: your data
     *  }]
     *
     * @param {String}  options.name - the LS key
     * @param {String}  options.websiteId
     * @param {String}  options.cacheKey - the data key identifier
     * @param {Integer} options.cacheTime - cache expiry time, will return values
     *    if cache time is less than this value
     *
     */
    var TimedCache = function(options) {
        this.name = options.name || '';
        this.websiteId = options.websiteId || '';
        this.cacheTime = options.cacheTime || 1;
        this.cacheKey = options.cacheKey || 'data';
        this.cachedData = localStorageService.get(this.name) || [];
    };

    /**
     * Return the cacheKey value if there is a websiteId match and data has
     *     not expired yet.
     */
    TimedCache.prototype.get = function() {
        var cachedTarget;
        var lastCachedTime;

        if (this.cachedData && this.cachedData.length) {
            cachedTarget = _.find(this.cachedData, { websiteId: this.websiteId });

            if (cachedTarget) {
                lastCachedTime = moment(cachedTarget.date);

                if (moment().diff(lastCachedTime, 'hours') < this.cacheTime) {
                    return cachedTarget[this.cacheKey];
                }
            }
        }
    };

    /**
     * Persist data to localStorage by replacing current websiteId data, if exists,
     *     or appending it to existing data, if doesn't exist.
     *
     * @param  {Anything} data - the cacheKey value
     */
    TimedCache.prototype.save = function(data) {
        var cachedWebsiteIdPosition = _.findIndex(this.cachedData, { websiteId: this.websiteId });

        var cachedTargetData = {
            websiteId: this.websiteId,
            date: new Date()
        };

        cachedTargetData[this.cacheKey] = data;

        if (cachedWebsiteIdPosition > -1) {
            this.cachedData[cachedWebsiteIdPosition] = cachedTargetData;
        } else {
            this.cachedData.push(cachedTargetData);
        }

        localStorageService.remove(this.name);
        localStorageService.add(this.name, this.cachedData);
    };

    /**
     * Remove the whole localStorage key entry.
     */
    TimedCache.prototype.remove = function() {
        localStorageService.remove(this.name);
    };

    return {
        getInstance: function(instance) {
            return new TimedCache(instance);
        }
    };
});

}());
