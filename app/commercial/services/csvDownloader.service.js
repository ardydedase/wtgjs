(function() {

'use strict';

// based on https://github.com/AlexLibs/client-side-csv-generator
angular.module('fcApp.commercial').factory('csvDownloader', function() {
    return {
        download: function(data, fileName, raw) {
            var Generator = function(data, fileName, raw) {
                this.data = data;
                this.fileName = fileName;
                this.raw = raw;

                this.getDownloadLink = function() {
                    var txt = this.raw ? this.data : this.data.map(function(row) {
                        return row.join(',');
                    }).join('\n');

                    this.downloadLink = this.downloadLink || 'data:text/csv;charset=utf-8,' + encodeURI(txt);

                    return this.downloadLink;
                };

                this.getLinkElement = function(linkText) {
                    var downloadLink = this.getDownloadLink();
                    this.linkElement = this.linkElement || $('<a>' + (linkText || '') + '</a>', {
                        href: downloadLink,
                        download: this.fileName
                    });

                    return this.linkElement;
                };

                // call with removeAfterDownload = true if you want the link to be removed after downloading
                this.download = function(removeAfterDownload) {
                    this.getLinkElement().css('display', 'none').appendTo('body');
                    this.getLinkElement()[0].click();
                    if (removeAfterDownload) {
                        this.getLinkElement().remove();
                    }
                };
            };

            var csv = new Generator(data, fileName, raw);
            csv.download(true);
        }
    };
});

}());
