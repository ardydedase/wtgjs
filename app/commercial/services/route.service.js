(function() {

'use strict';

angular.module('fcApp.commercial').factory('routeService', function(
    $http,
    config
    ) {

    var baseUrl = config.API_URL + config.API_BASE + '/v0.4/flights/routes/';

    /**
     * Get a list of top destinations sorted by number of redirects from 7 - 2 days ago
     * @param  {String} options.websiteId - REQUIRED
     * @param  {String} options.market - REQUIRED
     * @param  {String} options.origin - origin airport route ID
     * @param  {Number} options.limit - defaults to 10
     * @return {Array} [{"route": {"origin": {}, "destination": {"name": "John F. Kennedy International Airport", "routenode_id": 12345, "iata_id": "JFK"}, "metrics": [{"value": 1000, "type": "redirects", "name": "Redirects", "wow_change": 200 }, {"value": 0.1, "type": "sov", "name": "Share of Voice", "wow_change": -0.05 }]}]
     */
    function getDestinations(options) {
        var url = baseUrl + 'destinations/';

        //PE-791
        options.websiteId  = options.websiteId === 'a001' ? 'aljp' : options.websiteId;

        //--PE-791

        var parameters = {
            limit: options.limit !== undefined ? options.limit : 10,
            website_id: options.websiteId,
            market: options.market
        };

        if (options.origin) {
            parameters.origin = options.origin;
        }

        return $http({
            method: 'get',
            url: url,
            params: parameters
        });
    }

    /**
     * Get a list of origins for a particular destination sorted by number of redirects from 7 - 2 days ago
     * @param  {String} options.websiteId - REQUIRED
     * @param  {String} options.market - REQUIRED
     * @param  {String} options.destination - destination airport route ID
     * @param  {Number} options.limit - defaults to 10
     * @return {Array} [{"route": {"destination": {}, "origin": {"name": "John F. Kennedy International Airport", "routenode_id": 12345, "iata_id": "JFK"}, "metrics": [{"value": 1000, "type": "redirects", "name": "Redirects", "wow_change": 200 }, {"value": 0.1, "type": "sov", "name": "Share of Voice", "wow_change": -0.05 }]}]
     */
    function getOrigins(options) {
        var url = baseUrl + 'origins/';

        //PE-791
        options.websiteId  = options.websiteId === 'a001' ? 'aljp' : options.websiteId;

        //--PE-791

        var parameters = {
            limit: options.limit !== undefined ? options.limit : 10,
            website_id: options.websiteId,
            market: options.market
        };

        if (options.destination) {
            parameters.destination = options.destination;
        }

        return $http({
            method: 'get',
            url: url,
            params: parameters
        });
    }

    return {
        getDestinations: getDestinations,
        getOrigins: getOrigins
    };
});

}());
