(function() {

'use strict';

angular.module('fcApp.commercial', [
    'datetimepicker',
    'angularjs-dropdown-multiselect',
    'infinite-scroll',
    'smart-table',
    'ui.bootstrap',
    'appConfig',
    'LocalStorageModule',
    'fcApp.permission',
    'fcApp.common'
]);

angular.module('fcApp.commercial')
    .constant('_', window._)
    .constant('moment', window.moment)
    .constant('d3', window.d3)
    .constant('c3', window.c3);

}());
