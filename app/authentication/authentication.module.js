(function() {

'use strict';

angular.module('fcApp.authentication', [
    'ngCookies',
    'angulartics',
    'angulartics.google.analytics',
    'appConfig',
    'LocalStorageModule',
    'fcApp.common'
]);

}());
