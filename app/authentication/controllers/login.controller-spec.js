(function() {

'use strict';

describe('Controller: LoginCtrl', function() {

    beforeEach(module('fcApp.authentication'));
    beforeEach(module('ui.bootstrap'));

    var controller;
    var defaultMocks;
    var deferred;
    var scope;
    var q;

    var noop = function() {};

    beforeEach(inject(function($controller, $rootScope, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        defaultMocks = {
            authService: {
                authenticate: function() {
                    deferred = q.defer();
                    return deferred.promise;
                },

                getUserType: function() {
                    return 'fakeType';
                }
            },

            alertService: {
                reset: noop,
                closeAlert: noop
            },

            location: {
                path: noop
            },

            cookies: {}
        };
    }));

    function createController() {
        return controller('LoginCtrl', {
            $scope: scope,
            $cookies: defaultMocks.cookies,
            $location: defaultMocks.location,
            authService: defaultMocks.authService,
            alertService: defaultMocks.alertService
        });
    }

    it('should initialize defaults', function() {
        createController();

        expect(scope.username).toBeDefined();
        expect(scope.password).toBeDefined();
        expect(scope.closeAlert).toBeDefined();
        expect(scope.isValid).toBeDefined();
        expect(scope.authenticate).toBeDefined();
        expect(scope.noPermission).toEqual(false);
    });

    describe('authenticate', function() {
        it('should clear alerts', function() {
            spyOn(defaultMocks.alertService, 'reset');

            createController();
            scope.authenticate();

            expect(defaultMocks.alertService.reset).toHaveBeenCalled();
            expect(scope.noPermission).toEqual(false);
        });

        it('should have lowercase username on authenticate', function() {
            var fakeEmail = 'ADMIN@email.test';
            var expectedEmail = fakeEmail.toLowerCase();

            createController();

            scope.username = fakeEmail;

            scope.authenticate();

            expect(scope.username).toBe(expectedEmail);
        });

        it('should set cookies if there is token and user has technical permission', function() {
            createController();

            scope.username = 'postit';
            scope.authenticate();

            deferred.resolve({
                token: '123',
                websites: ['dell']
            });
            scope.$digest();

            expect(defaultMocks.cookies.username).toEqual('postit');
            expect(defaultMocks.cookies.token).toEqual('123');
        });

        it('should redirect if there is token and user has technical permission', function() {
            spyOn(defaultMocks.location, 'path');

            createController();

            scope.username = 'postit';
            scope.authenticate();

            deferred.resolve({
                token: '123',
                websites: ['dell']
            });
            scope.$digest();

            expect(defaultMocks.location.path).toHaveBeenCalled();
        });

        describe('mixpanel tracking', function() {
            var mixpanelMock;
            var spy;

            beforeEach(function() {
                mixpanelMock = window.mixpanel;
                spy = jasmine.createSpy();
                mixpanelMock.track = spy;
            });

            afterEach(function() {
                spy = undefined;
            });

            it('should send a track event if user successfully authenticates', function() {
                createController();
                scope.username = 'fakeUname';
                scope.authenticate();

                deferred.resolve({
                    token: '123',
                    websites: ['dell']
                });
                scope.$digest();

                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledWith('FC User Authenticated', jasmine.objectContaining({
                    username: 'fakeuname',
                    userType: 'fakeType',
                    success: true
                }));
            });

            it('should send a track event containing hasTechnicalPermission = true if user has technical access', function() {
                createController();
                scope.authenticate();

                deferred.resolve({
                    token: '123',
                    websites: ['dell']
                });
                scope.$digest();

                expect(spy).toHaveBeenCalledWith('FC User Authenticated', jasmine.objectContaining({
                    success: true,
                    hasTechnicalPermission: true
                }));
            });

            it('should send a track event containing hasCommercialPermission = true if user has commercial access', function() {
                createController();
                scope.authenticate();

                deferred.resolve({
                    token: '123',
                    commercial_websites: ['testwebsite'],
                    websites: ['testwebsite']
                });
                scope.$digest();

                expect(spy).toHaveBeenCalledWith('FC User Authenticated', jasmine.objectContaining({
                    success: true,
                    hasCommercialPermission: true
                }));
            });

            it('should send a track event containing success = false if user has no technical access', function() {
                createController();
                scope.authenticate();

                deferred.resolve({
                    token: '123',
                    websites: []
                });
                scope.$digest();

                expect(spy).toHaveBeenCalledWith('FC User Authenticated', jasmine.objectContaining({
                    hasTechnicalPermission: false,
                    success: false
                }));
            });

            it('should send a track event containing success = false if authentication token is not present', function() {
                createController();

                scope.authenticate();
                deferred.resolve({
                    websites: ['test']
                });
                scope.$digest();

                expect(spy).toHaveBeenCalledWith('FC User Authenticated', jasmine.objectContaining({
                    success: false
                }));
            });

            it('should send a track event containing success = false authenticationService API fails', function() {
                createController();

                scope.authenticate();
                deferred.reject();
                scope.$digest();

                expect(spy).toHaveBeenCalledWith('FC User Authenticated', jasmine.objectContaining({
                    success: false
                }));
            });
        });
    });
});

}());
