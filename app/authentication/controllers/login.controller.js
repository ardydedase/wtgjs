(function() {

'use strict';

angular.module('fcApp.authentication').controller('LoginCtrl', function(
    $scope,
    $location,
    $cookies,
    $cookieStore,
    $analytics,
    authService,
    alertService) {

    alertService.reset();

    var defaultPage = '/';

    var loginDestination = ($cookieStore.get('login_destination') !== '/login' && $cookieStore.get('login_destination')) ? $cookieStore.get('login_destination') : defaultPage;

    if ($cookies.token) {
        $location.path(defaultPage);
    }

    $scope.closeAlert = alertService.closeAlert;

    $scope.username = '';
    $scope.password = '';
    $scope.noPermission = false;

    $scope.isValid = function(username, password) {
        return username.length > 0 && password.length > 0;
    };

    $scope.authenticate = function() {
        alertService.reset();
        $scope.noPermission = false;

        $scope.username = $scope.username.toLowerCase();
        $scope.loggingIn = true;

        var username = $scope.username;
        var password = $scope.password;

        var mpEvent = 'FC User Authenticated';
        var mpProps = {
            username: username,
            userType: authService.getUserType(username)
        };

        authService.authenticate(username, password).then(function(response) {
            var hasTechnicalPermission = response.websites && response.websites.length > 0 ? true : false;
            var hasCommercialPermission = response.commercial_websites && response.commercial_websites.length > 0 ? true : false;

            mpProps.hasCommercialPermission = hasCommercialPermission;
            mpProps.hasTechnicalPermission = hasTechnicalPermission;

            if (hasTechnicalPermission) {

                if (response.token) {
                    $cookies.token = response.token;
                    $cookies.username = username;

                    mixpanel.identify(username);
                    mixpanel.people.set({
                        $email: username
                    });

                    mpProps.success = true;
                    mixpanel.track(mpEvent, mpProps);

                    $location.path(loginDestination);
                } else {
                    mpProps.success = false;
                }

            } else {
                mpProps.success = false;
                $scope.noPermission = true;
            }

            $scope.loggingIn = false;
            mixpanel.track(mpEvent, mpProps);
        }, function() {
            //TODO: errInterceptor hijacking response 400, this block never gets evaluated
            $scope.loggingIn = false;
            mpProps.success = false;
            mixpanel.track(mpEvent, mpProps);
        });
    };
});

}());
