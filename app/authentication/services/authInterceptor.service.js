(function() {

'use strict';

angular.module('fcApp.authentication').factory('authInterceptor', function(
    $q,
    $cookies) {

    return {
        request: function(config) {
            config.headers = config.headers || {};
            if ($cookies.token) {
                config.headers.Authorization = 'JWT ' + $cookies.token;
            }

            return config;
        },

        response: function(response) {
            return response || $q.when(response);
        }
    };
});

}());
