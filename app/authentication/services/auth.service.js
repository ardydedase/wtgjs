(function() {

'use strict';

angular.module('fcApp.authentication').factory('authService', function(
    $rootScope,
    $http,
    $window,
    $location,
    $cookies,
    $cookieStore,
    $routeParams,
    alertService,
    localStorageService,
    config) {

    var authService = {};

    function trackUserLoggedOut(username) {
        mixpanel.track('FC User Logged Out', {
            username: username,
            userType: authService.getUserType(username),
            websiteId: localStorageService.get('selectedwebsite')
        });
    }

    function clearMixpanelCookie() {
        if (mixpanel.cookie && typeof (mixpanel.cookie.clear) === 'function') {
            mixpanel.cookie.clear();
        }
    }

    function isCommercialUser() {
        var sites = localStorageService.get('commercial_websites');

        if (sites) {
            return sites.length;
        }
    }

    authService.isInternalUser = function() {
        var account = localStorageService.get('account');

        if (account && account.groups) {
            return account.groups.indexOf('Skyscanner') > -1;
        }
    };

    authService.restrictToGroup = function(group) {
        // if you're not an internal user
        if (localStorageService.get('account').groups.indexOf(group) === -1) {
            $location.path('/tests');
        }
    };

    authService.redirectToWebsiteId = function() {
        if (!$routeParams.hasOwnProperty('websiteId')) {
            $location.path($location.path() + '/' + localStorageService.get('selectedwebsite'));
        }
    };

    authService.authenticate = function(username, password) {
        var selectedwebsite = localStorageService.get('selectedwebsite') ? localStorageService.get('selectedwebsite') : '';
        var postParam = {username: username, password: password};

        if (selectedwebsite) {
            $.extend(postParam, {selectedwebsite: selectedwebsite});
        }

        return $http.post(config.API_URL  + config.API_BASE + '/api-token/', postParam)
            .then(function(response) {
                if (response.data.status === 'nowebsites') {
                    return response.data;
                }

                localStorageService.remove('username');
                localStorageService.add('username', username);
                localStorageService.remove('websites');

                //PE-791
                if (username === 'miland') {
                    response.data.websites.unshift('a001');
                }

                //--PE-791

                localStorageService.add('websites', response.data.websites);

                if (response.data.commercial_websites !== []) {
                    localStorageService.remove('commercial_websites');
                    localStorageService.add('commercial_websites', response.data.commercial_websites);
                }

                localStorageService.remove('account');
                localStorageService.add('account', response.data.account);

                // if there's no selected website by default, select the first in the element
                if (!localStorageService.get('selectedwebsite')) {
                    var websites = localStorageService.get('websites');
                    localStorageService.add('selectedwebsite', websites[0]);
                }

                localStorageService.remove('webconfig');
                localStorageService.add('webconfig', response.data.web_config);

                // should ideally store boolean, but localStorageService doesn't
                var trPermission = response.data.tr_permission || 'false';
                localStorageService.remove('trPermission');
                localStorageService.add('trPermission', trPermission);

                return response.data;
            });
    };

    authService.logout = function(username) {
        var loggedInUser = username || localStorageService.get('username');

        trackUserLoggedOut(loggedInUser);

        // clear the session variables
        delete $cookies.token;
        delete $cookies.username;

        localStorageService.clearAll();

        $rootScope.session = {
            isLoggedin: false,
            username: ''
        };

        // $cookies.login_destination = $location.path();
        //$cookieStore.put('login_destination', $location.path());

        clearMixpanelCookie();

        $location.path('login');
    };

    authService.isLoggedIn = function() {
        if (!$cookies.token || !$cookies.username) {
            return false;
        }

        return true;
    };

    authService.getUserType = function(username) {
        if (username === 'miland') {
            return 'Skyscanner';
        } else {
            if (!username) {
                return 'None';
            }

            if (username.indexOf('skyscanner') > -1) {
                return 'Skyscanner';
            }
        }

        return 'Partner';
    };

    authService.hasCommercialAccess = function() {
        if (authService.isInternalUser()) {
            return true;
        }

        if (isCommercialUser()) {
            return true;
        }

        return false;
    };

    authService.redirectFromCommercialPage = function() {
        if (!authService.hasCommercialAccess()) {
            $location.path('dashboard');
        }
    };

    return authService;
});

}());
