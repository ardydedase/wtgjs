(function() {

'use strict';

describe('Service: authService', function() {

    beforeEach(module('fcApp.authentication'));

    var authService;
    var httpBackend;
    var mockLocalStorageService;
    var fakeStore;
    var config;
    var baseUrl;

    beforeEach(module('fcApp.authentication', function($provide) {
        mockLocalStorageService = {
            get: function(id) {
                return fakeStore[id];
            },

            remove: function() {},

            add: function(id, data) {
                fakeStore = data;
            }
        };

        $provide.value('localStorageService', mockLocalStorageService);
    }));

    beforeEach(inject(function(_authService_, $httpBackend, _config_) {
        authService = _authService_;
        httpBackend = $httpBackend;
        config = _config_;
        fakeStore = {};

        baseUrl = config.API_URL + config.API_BASE;
    }));

    it('should do something', function() {
        expect(!!authService).toBe(true);
    });

    it('should check isLoggedIn', function() {
        expect(authService.isLoggedIn()).toBe(false);
    });

    it('should allow legitimate user', function() {
        var fakeCredentials = {
            username: 'fakeUser',
            password: 'fakePwd'
        };

        httpBackend.whenPOST(baseUrl + '/api-token/').respond({
            web_config: { website_id: 'fakeWebsiteId' },
            commercial_websites: 'fakeWebsiteId',
            websites: ['fakeWebsiteId'],
            account: { username: fakeCredentials.username }
        });

        authService.authenticate(fakeCredentials.username, fakeCredentials.password).then(function() {
            expect(mockLocalStorageService.get('commercial_websites')[0]).toBe('!!!!');
            expect(mockLocalStorageService.get('websites')[0]).toBe('test');
            expect(mockLocalStorageService.get('account').username).toBe(fakeCredentials.username);
            expect(mockLocalStorageService.get('webconfig').website_id).toBe('test');
        });
    });

    it('should block failed', function() {
        httpBackend.whenPOST('/api-token/').respond({
            status: 'nowebsites'
        });

        authService.authenticate('username', 'password').then(function() {
            expect(mockLocalStorageService.get('commercial_websites')).toBe(null);
            expect(mockLocalStorageService.get('websites')).toBe(null);
            expect(mockLocalStorageService.get('account')).toBe(null);
            expect(mockLocalStorageService.get('webconfig')).toBe(null);
        });
    });
});

}());
