(function() {

'use strict';

angular.module('fcApp.travelRankings').filter('ignoreUndefinedSort', function(orderByFilter, _) {
    return function(data, predicate, reverse) {
        var transformedData = [];
        var sortedData;

        var hasMaxSafeIntegerValue = _.some(data, function(d) {
            return Math.abs(d[predicate]) === Number.MAX_SAFE_INTEGER;
        });

        if (hasMaxSafeIntegerValue) {
            _.forEach(data, function(d) {
                d[predicate] = Number(d[predicate]);

                if (Math.abs(d[predicate]) === Number.MAX_SAFE_INTEGER) {
                    d[predicate] = reverse ? -d[predicate] : Math.abs(d[predicate]);
                }

                transformedData.push(d);
            });

            sortedData = orderByFilter(transformedData, predicate, reverse);
        } else {
            sortedData = orderByFilter(data, predicate, reverse);
        }

        return sortedData;
    };
});

}());
