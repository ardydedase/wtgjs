(function() {

'use strict';

/* this test suite is better read in chronological order
    to understand the flow of the code
*/
describe('Controller: TravelRankingsCtrl', function() {

    var controller;
    var store;
    var location;
    var defaultMocks;
    var q;
    var scope;

    var noop = function() {};

    function setUpMocks() {
        var defer = function() {
            var dfr = q.defer();
            return dfr.promise;
        };

        store = {};

        defaultMocks = {
            location: {
                path: function(url) {
                    return url;
                },

                search: function() {
                    return this;
                }
            },
            routeParams: {
                view: 'routes'
            },
            authService: {
                hasCommercialAccess: noop,
                isInternalUser: noop,
                logout: noop,
                getUserType: noop,
                restrictToGroup: noop
            },
            travelRankingsViewModel: {
                places: [],
                view: {}
            },
            travelRankingsApiService: {
                getData: defer,
                getConfigs: defer
            },
            mixpanelService: {
                track: noop
            }
        };
    }

    function createController(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('TravelRankingsCtrl', {
            $scope: scope,
            $routeParams: mocks.routeParams,
            $location: mocks.location,
            travelRankingsApiService: mocks.travelRankingsApiService,
            travelRankingsViewModel: mocks.travelRankingsViewModel,
            authService: mocks.authService,
            mixpanelService: mocks.mixpanelService,
            _: window._,
            moment: window.moment
        });
    }

    beforeEach(module('fcApp.travelRankings'));

    beforeEach(inject(function($controller, $rootScope, $q, $location) {
        scope = $rootScope.$new();
        controller = $controller;
        location = $location;
        q = $q;

        setUpMocks();
    }));

    afterEach(function() {
        store = {};
    });

    it('should initialize defaults', function() {
        createController();

        expect(scope.viewParams).toEqual({});
        expect(scope.places).toEqual([]);

        expect(typeof scope.onSelectedMarket).toEqual('function');
        expect(typeof scope.onSelectedOrigin).toEqual('function');
        expect(typeof scope.onSelectedDestination).toEqual('function');
        expect(typeof scope.clearOrigin).toEqual('function');
        expect(typeof scope.clearDestination).toEqual('function');
        expect(typeof scope.switchView).toEqual('function');
        expect(typeof scope.navigateRoutesView).toEqual('function');
    });

    describe('route structure validation', function() {
        beforeEach(function() {
            defaultMocks.routeParams = {};
        });

        describe('valid url combination - should not redirect to default url /travel-rankings/routes if url', function() {
            it('is already "/routes"', function() {
                spyOn(defaultMocks.location, 'path');

                defaultMocks.routeParams.view = 'routes';
                createController();

                expect(defaultMocks.location.path).not.toHaveBeenCalledWith('/travel-rankings/routes');
            });

            it('is the expected Routes view url structure - /travel-rankings/routes/AU', function() {
                spyOn(defaultMocks.location, 'path');

                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '';
                defaultMocks.routeParams.toPlace = '';
                createController();

                expect(defaultMocks.location.path).not.toHaveBeenCalledWith('/travel-rankings/routes');
            });

            it('is the expected Dates view url structure - /travel-rankings/dates/AU/32809/9129', function() {
                spyOn(defaultMocks.location, 'path');

                defaultMocks.routeParams.view = 'dates';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = 32809;
                defaultMocks.routeParams.toPlace = 9129;
                createController();

                expect(defaultMocks.location.path).not.toHaveBeenCalledWith('/travel-rankings/routes');
            });

            it('is the expected Itineraries view url structure - /travel-rankings/itineraries/AU/32809/9129/2015-01-01/2015-01-02', function() {
                spyOn(defaultMocks.location, 'path');

                defaultMocks.routeParams.view = 'itineraries';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = 32809;
                defaultMocks.routeParams.toPlace = 9129;
                defaultMocks.routeParams.fromDate = '2015-01-01';
                defaultMocks.routeParams.toDate = '2015-01-02';
                createController();

                expect(defaultMocks.location.path).not.toHaveBeenCalledWith('/travel-rankings/routes');
            });
        });

        describe('invalid url combination - should redirect to /travel-rankings/routes if url', function() {
            it('is incomplete', function() {
                spyOn(defaultMocks.location, 'path');

                defaultMocks.routeParams = {};
                createController();

                expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
            });

            it('is not one of "routes", "dates", or "itineraries"', function() {
                spyOn(defaultMocks.location, 'path');

                defaultMocks.routeParams.view = 'abc';
                createController();

                expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
            });

            describe('Dates view', function() {
                it('is missing market - /dates//1234/5678', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'dates';
                    defaultMocks.routeParams.fromPlace = 1234;
                    defaultMocks.routeParams.toPlace = 5678;

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });

                it('is missing origin id - /dates/AU//5656', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'dates';
                    defaultMocks.routeParams.market = 'AU';
                    defaultMocks.routeParams.toPlace = 5656;

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });

                it('is missing destination id - /dates/AU/5656/', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'dates';
                    defaultMocks.routeParams.market = 'AU';
                    defaultMocks.routeParams.fromPlace = 5656;

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });
            });

            describe('Itineraries view', function() {
                it('is missing market - /itineraries//23819/5656/2015-01-01/2015-01-02', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'itineraries';
                    defaultMocks.routeParams.fromPlace = 23819;
                    defaultMocks.routeParams.toPlace = 5656;
                    defaultMocks.routeParams.fromDate = '2015-01-01';
                    defaultMocks.routeParams.toDate = '2015-01-02';

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });

                it('is missing destination id - /itineraries/AU/5656//2015-01-01/2015-01-02', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'itineraries';
                    defaultMocks.routeParams.market = 'AU';
                    defaultMocks.routeParams.fromPlace = 5656;
                    defaultMocks.routeParams.fromDate = '2015-01-01';
                    defaultMocks.routeParams.toDate = '2015-01-02';

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });

                it('is missing origin id - /itineraries/AU//5656/2015-01-01/2015-01-02', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'itineraries';
                    defaultMocks.routeParams.market = 'AU';
                    defaultMocks.routeParams.toPlace = 5656;
                    defaultMocks.routeParams.fromDate = '2015-01-01';
                    defaultMocks.routeParams.toDate = '2015-01-02';

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });

                it('is missing start date - /itineraries/AU/2113/5656//2015-01-02', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'itineraries';
                    defaultMocks.routeParams.market = 'AU';
                    defaultMocks.routeParams.fromPlace = 2113;
                    defaultMocks.routeParams.toPlace = 5656;
                    defaultMocks.routeParams.toDate = '2015-01-02';

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });

                it('is missing end date - /itineraries/AU/2113/5656/2015-01-01/', function() {
                    spyOn(defaultMocks.location, 'path');

                    defaultMocks.routeParams.view = 'itineraries';
                    defaultMocks.routeParams.market = 'AU';
                    defaultMocks.routeParams.fromPlace = 2113;
                    defaultMocks.routeParams.toPlace = 5656;
                    defaultMocks.routeParams.fromDate = '2015-01-01';

                    createController();

                    expect(defaultMocks.location.path).toHaveBeenCalledWith('/travel-rankings/routes');
                });
            });
        });
    });

    describe('config retrieval', function() {
        it('should retrieve configs', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);
            dfr.resolve('testConfigResults');

            createController();
            scope.$digest();

            expect(defaultMocks.travelRankingsApiService.getConfigs).toHaveBeenCalledWith(true);
        });

        it('should display feedback if configs are not set', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);
            dfr.reject();

            createController();
            scope.$digest();

            expect(scope.feedback.message).toBeDefined();
            expect(scope.feedback.type).toEqual('danger');
        });
    });

    describe('initialize market list', function() {
        beforeEach(function() {
            var dfr = q.defer();
            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);
            dfr.resolve({
                markets: [{
                    code: 'ME',
                    name: 'Middle East',
                    allowed: false
                }, {
                    code: 'AM',
                    name: 'Asia Minor',
                    allowed: true
                }, {
                    code: 'NA',
                    allowed: false
                }]
            });
        });

        it('should have a defined markets', function() {
            createController();
            scope.$digest();

            expect(scope.markets).toBeDefined();
            expect(scope.selectedMarket).toBeDefined();
        });

        it('should filter out markets without name', function() {
            createController();
            scope.$digest();

            expect(scope.markets.length).toEqual(3);
            expect(window._.find(scope.markets, { code: 'NA' })).not.toBeDefined();
        });

        it('should sort list of markets from config alphabetically based on the name', function() {
            createController();
            scope.$digest();

            expect(scope.markets[1].name).toEqual('Asia Minor');
            expect(scope.markets[2].name).toEqual('Middle East');
        });

        it('should append markets from configs into scope.markets', function() {
            createController();
            scope.$digest();

            expect(scope.markets.length).toEqual(3);
        });

        it('should have a placeholder text set at first in the list of markets', function() {
            createController();
            scope.$digest();

            expect(scope.markets[0].code).toEqual('none');
            expect(scope.selectedMarket.code).toEqual(scope.markets[0].code);
        });

        it('should set selectedMarket to the route parameter market in the url if present', function() {
            defaultMocks.routeParams.market = 'AM';

            createController();
            scope.$digest();

            expect(scope.selectedMarket.code).toEqual('AM');
            expect(scope.selectedMarket.name).toEqual('Asia Minor');
        });

        it('should set selectedMarket to placeholoder text if the route parameter market is not a valid market code', function() {
            defaultMocks.routeParams.market = 'SS';

            createController();
            scope.$digest();

            expect(scope.selectedMarket.code).toEqual('none');
        });
    });

    describe('initialize display currency', function() {
        var dfr;

        beforeEach(function() {
            dfr = q.defer();
            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);
        });

        it('should set currency key to viewParams', function() {
            dfr.resolve({
                display_currency: 'ABC'
            });

            createController();
            scope.$digest();

            expect(scope.viewParams.currency).toEqual('ABC');
        });

        it('should have a defined currencyIcon', function() {
            dfr.resolve({
                display_currency: 'ABC'
            });

            createController();
            scope.$digest();

            expect(scope.currencyIcon).toBeDefined();
        });

        describe('currency icons', function() {
            function shouldPassIfCurrencyIs(curr) {
                it('should set currencyIcon as "' + curr + '"', function() {
                    dfr.resolve({
                        display_currency: curr
                    });

                    createController();
                    scope.$digest();

                    expect(scope.currencyIcon).toEqual(curr);
                });
            }

            angular.forEach(['EUR', 'INR', 'RMB', 'JPY', 'USD', 'CNY', 'GBP', 'KRW', 'RUB', 'WON', 'TRY'], shouldPassIfCurrencyIs);

            it('should get currencyIcon as "money" if currency is not in the list of available icons', function() {
                dfr.resolve({
                    display_currency: 'ABC'
                });

                createController();
                scope.$digest();

                expect(scope.currencyIcon).toEqual('money');
            });
        });
    });

    describe('route params validation', function() {
        beforeEach(function() {
            var dfr = q.defer();
            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);

            dfr.resolve({
                markets: [{
                    code: 'AU',
                    name: 'Australia'
                }]
            });
        });

        describe('valid params', function() {
            it('should set viewParams.view if value is "routes"', function() {
                defaultMocks.routeParams.view = 'routes';

                createController();
                scope.$digest();

                expect(scope.viewParams.view).toEqual('routes');
            });

            it('should set viewParams.view if value is "dates"', function() {
                defaultMocks.routeParams.view = 'dates';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '1';
                defaultMocks.routeParams.toPlace = '2';

                createController();
                scope.$digest();

                expect(scope.viewParams.view).toEqual('dates');
            });

            it('should set viewParams.view if value is "itineraries"', function() {
                defaultMocks.routeParams.view = 'itineraries';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '1';
                defaultMocks.routeParams.toPlace = '2';
                defaultMocks.routeParams.fromDate = '2015-01-01';
                defaultMocks.routeParams.toDate = '2015-01-02';

                createController();
                scope.$digest();

                expect(scope.viewParams.view).toEqual('itineraries');
            });

            it('should set viewParams.market if market is in the config list', function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';

                createController();
                scope.$digest();

                expect(scope.viewParams.market).toEqual('AU');
            });

            it('should set viewParams.fromPlace if origin id is a number', function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '1093';

                createController();
                scope.$digest();

                expect(scope.viewParams.fromPlace).toEqual('1093');
            });

            it('should set viewParams.toPlace if destination id is a number', function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.toPlace = '12636';

                createController();
                scope.$digest();

                expect(scope.viewParams.toPlace).toEqual('12636');
            });

            it('should set viewParams.fromDate and viewParams.toDate if start date and end date are valid dates in YYYY-MM-DD format', function() {
                defaultMocks.routeParams.view = 'itineraries';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '123';
                defaultMocks.routeParams.toPlace = '567';
                defaultMocks.routeParams.fromDate = '2015-10-11';
                defaultMocks.routeParams.toDate = '2016-01-12';

                createController();
                scope.$digest();

                expect(scope.viewParams.fromDate).toEqual('2015-10-11');
                expect(scope.viewParams.toDate).toEqual('2016-01-12');
            });
        });

        describe('invalid params', function() {
            it('should clear viewParams and display feedback if market is not in the config list', function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'NOTVALID';

                createController();
                scope.$digest();

                expect(scope.viewParams.view).not.toBeDefined();
                expect(scope.viewParams).toEqual({});
                expect(scope.feedback.message).toBeDefined();
            });

            it('should clear viewParams and display feedback if origin id is not a number', function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = 'LAX';

                createController();
                scope.$digest();

                expect(scope.viewParams.fromPlace).not.toBeDefined();
                expect(scope.viewParams).toEqual({});
                expect(scope.feedback.message).toBeDefined();
            });

            it('should clear viewParams and display feedback if destination id is not a number', function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.toPlace = 'SIN';

                createController();
                scope.$digest();

                expect(scope.viewParams.toPlace).not.toBeDefined();
                expect(scope.viewParams).toEqual({});
                expect(scope.feedback.message).toBeDefined();
            });

            it('should clear viewParams and display feedback if start date and end date are not valid dates in YYYY-MM-DD formats', function() {
                defaultMocks.routeParams.view = 'itineraries';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = 'KIX';
                defaultMocks.routeParams.toPlace = 'KUL';
                defaultMocks.routeParams.fromDate = '20-10-2015';
                defaultMocks.routeParams.toDate = '2015-13-33';

                createController();
                scope.$digest();

                expect(scope.viewParams.fromDate).not.toBeDefined();
                expect(scope.viewParams.toDate).not.toBeDefined();
                expect(scope.viewParams).toEqual({});
                expect(scope.feedback.message).toBeDefined();
            });
        });
    });

    describe('querystrings validation', function() {
        beforeEach(function() {
            var dfr = q.defer();
            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);

            dfr.resolve({
                markets: [{
                    code: 'AU',
                    name: 'Australia'
                }]
            });
        });

        it('should set default values for viewParams[querystrings] if the querystring values are empty', function() {
            var mockLocation = location;
            spyOn(mockLocation, 'search').and.returnValue({
                page: '',
                pageSize: '',
                sortBy: '',
                sortOrder: ''
            });

            defaultMocks.routeParams.view = 'routes';
            defaultMocks.routeParams.market = 'AU';

            createController(window._.extend(defaultMocks, {
                location: mockLocation
            }));
            scope.$digest();

            expect(scope.viewParams.page).toEqual(0);
            expect(scope.viewParams.pageSize).toEqual(100);
            expect(scope.viewParams.sortBy).toEqual('searchVolume');
            expect(scope.viewParams.sortOrder).toEqual('desc');
        });

        it('should set viewParams[querystrings] for routes view', function() {
            var mockLocation = location;
            spyOn(mockLocation, 'search').and.returnValue({
                page: 5,
                pageSize: 1000,
                sortBy: 'whatever',
                sortOrder: 'up'
            });

            defaultMocks.routeParams.view = 'routes';
            defaultMocks.routeParams.market = 'AU';

            createController(window._.extend(defaultMocks, {
                location: mockLocation
            }));
            scope.$digest();

            expect(scope.viewParams.page).toEqual(5);
            expect(scope.viewParams.pageSize).toEqual(1000);
            expect(scope.viewParams.sortBy).toEqual('whatever');
            expect(scope.viewParams.sortOrder).toEqual('up');
        });

        it('should not set any viewParams[querystrings] for non-routes view', function() {
            var mockLocation = location;

            defaultMocks.routeParams.view = 'dates';
            defaultMocks.routeParams.market = 'AU';
            defaultMocks.routeParams.fromPlace = '124';
            defaultMocks.routeParams.toPlace = '222';

            spyOn(mockLocation, 'search').and.returnValue({
                page: 5,
                pageSize: 1000,
                sortBy: 'whatever',
                sortOrder: 'up'
            });

            createController(window._.extend(defaultMocks, {
                location: mockLocation
            }));
            scope.$digest();

            expect(scope.viewParams.page).not.toBeDefined();
            expect(scope.viewParams.pageSize).not.toBeDefined();
            expect(scope.viewParams.sortBy).not.toBeDefined();
            expect(scope.viewParams.sortOrder).not.toBeDefined();
        });
    });

    /* this is unnecessary if there is a state-based routing in place */
    it('should populate the (service) viewModel.view with viewParams values so that the routing params can be retained on page navigation', function() {
        var mockLocation = location;
        var dfr = q.defer();

        spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);

        dfr.resolve({
            markets: [{
                code: 'AU',
                name: 'Australia'
            }]
        });

        spyOn(mockLocation, 'search').and.returnValue({
            page: 5,
            pageSize: 1000,
            sortBy: 'whatever',
            sortOrder: 'up'
        });

        defaultMocks.routeParams.view = 'routes';
        defaultMocks.routeParams.market = 'AU';
        defaultMocks.routeParams.fromPlace = '1243';
        defaultMocks.routeParams.toPlace = '3113';

        createController(window._.extend(defaultMocks, {
            location: mockLocation
        }));
        scope.$digest();

        expect(defaultMocks.travelRankingsViewModel.view.view).toEqual('routes');
        expect(defaultMocks.travelRankingsViewModel.view.market).toEqual('AU');
        expect(defaultMocks.travelRankingsViewModel.view.fromPlace).toEqual('1243');
        expect(defaultMocks.travelRankingsViewModel.view.toPlace).toEqual('3113');
        expect(defaultMocks.travelRankingsViewModel.view.page).toEqual(5);
        expect(defaultMocks.travelRankingsViewModel.view.pageSize).toEqual(1000);
        expect(defaultMocks.travelRankingsViewModel.view.sortBy).toEqual('whatever');
        expect(defaultMocks.travelRankingsViewModel.view.sortOrder).toEqual('up');
    });

    describe('getData', function() {
        beforeEach(function() {
            var dfr = q.defer();

            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);

            dfr.resolve({
                markets: [{
                    code: 'AU',
                    name: 'Australia'
                }],
                display_currency: 'EUR'
            });
        });

        it('should make an ajax call to getData passing viewParams values', function() {
            defaultMocks.routeParams.view = 'routes';
            defaultMocks.routeParams.market = 'AU';

            spyOn(defaultMocks.travelRankingsApiService, 'getData').and.callThrough();

            createController();
            scope.$digest();

            expect(defaultMocks.travelRankingsApiService.getData).toHaveBeenCalledWith(jasmine.objectContaining({
                currency: 'EUR',
                view: 'routes',
                market: 'AU'
            }));
        });

        describe('all views data', function() {
            var getDataDfr;

            beforeEach(function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';

                getDataDfr = q.defer();
                spyOn(defaultMocks.travelRankingsApiService, 'getData').and.returnValue(getDataDfr.promise);
            });

            it('should set a loading indicator', function() {
                createController();
                scope.$digest();

                expect(scope.isLoading).toEqual(true);
            });

            it('should set places and viewModel.places from the response', function() {
                createController();
                scope.$digest();

                getDataDfr.resolve({
                    places: ['A']
                });
                scope.$digest();

                expect(scope.places).toBeDefined();
                expect(scope.places).toEqual(['A']);
                expect(defaultMocks.travelRankingsViewModel.places).toBeDefined();
                expect(defaultMocks.travelRankingsViewModel.places).toEqual(['A']);
            });

            it('should set tableData from the response', function() {
                createController();
                scope.$digest();

                getDataDfr.resolve({
                    stats: []
                });
                scope.$digest();

                expect(scope.tableData).toBeDefined();
                expect(scope.tableData.length).toEqual(0);
            });

            it('should set staticTableData as an array copy of tableData (a smart-table requirement)', function() {
                createController();
                scope.$digest();

                getDataDfr.resolve({
                    stats: [{
                        origin_id: 1,
                        destination_id: 2,
                        your_cheapest_itinerary: {
                            position_histogram: [0, 0, 1],
                            rank_histogram: [1, 0, 0],
                            price: 100
                        },
                        your_cheapest_book_button: {
                            position_histogram: [0, 0, 1]
                        },
                        all_itineraries: {
                            position_histogram: [0, 0, 1],
                            rank_histogram: [1, 0, 0]
                        }
                    }],
                    places: [{
                        display_code: 'EI',
                        place_id: 1,
                        name: 'Easter Island'
                    }, {
                        display_code: 'FI',
                        place_id: 2,
                        name: 'Faroe Island'
                    }]
                });
                scope.$digest();

                expect(scope.staticTableData).toBeDefined();
                expect(scope.staticTableData).toEqual(scope.tableData);
            });

            describe('initialize places and names', function() {
                describe('origin and destination exist', function() {
                    beforeEach(function() {
                        defaultMocks.routeParams.fromPlace = '123';
                        defaultMocks.routeParams.toPlace = '999';
                        defaultMocks.travelRankingsViewModel.places = [{
                            display_code: 'EI',
                            place_id: 123,
                            name: 'Easter Island'
                        }, {
                            display_code: 'FI',
                            place_id: 999,
                            name: 'Faroe Islands'
                        }];

                        createController();
                        scope.$digest();

                        getDataDfr.resolve();
                    });

                    it('should set selectedOrigin and viewParam.originName if origin id exists', function() {
                        expect(scope.selectedOrigin).toEqual(defaultMocks.travelRankingsViewModel.places[0]);
                        expect(scope.viewParams.originName).toEqual('Easter Island');
                    });

                    it('should set selectedOrigin and viewParam.originName if destination id exists', function() {
                        expect(scope.selectedDestination).toEqual(defaultMocks.travelRankingsViewModel.places[1]);
                        expect(scope.viewParams.destinationName).toEqual('Faroe Islands');
                    });
                });

                describe('origin and destination do not exist', function() {
                    beforeEach(function() {
                        defaultMocks.travelRankingsViewModel.places = [{
                            display_code: 'EI',
                            place_id: 123,
                            name: 'Easter Island'
                        }, {
                            display_code: 'FI',
                            place_id: 999,
                            name: 'Faroe Islands'
                        }];

                        createController();
                        scope.$digest();

                        getDataDfr.resolve();
                    });

                    it('should clear selectedOrigin and viewParam.originName if origin id does not exist', function() {
                        expect(scope.selectedOrigin).toEqual(null);
                        expect(scope.viewParams.originName).toEqual('');
                    });

                    it('should clear selectedOrigin and viewParam.originName if destination id does not exist', function() {
                        expect(scope.selectedDestination).toEqual(null);
                        expect(scope.viewParams.destinationName).toEqual('');
                    });
                });
            });

            it('should always stop the loading indicator', function() {
                createController();
                scope.$digest();

                getDataDfr.reject();
                scope.$digest();

                expect(scope.isLoading).toEqual(false);
            });
        });

        describe('Routes view data', function() {
            var getDataDfr;
            var fakeData = {
                total_rows: 24,
                stats: [{
                    origin_id: 123,
                    destination_id: 999,
                    your_cheapest_itinerary: {
                        position_histogram: [0.3111, 0.3233, 0.329954],
                        rank_histogram: [0.5, 0.4, 0.1]
                    },
                    your_cheapest_book_button: {
                        position_histogram: [0, 0, 1]
                    },
                    all_itineraries: {
                        rank_histogram: [0.85, 0.15, 0]
                    }
                }],
                places: [{
                    display_code: 'EI',
                    place_id: 123,
                    name: 'Easter Island'
                }, {
                    display_code: 'FI',
                    place_id: 999,
                    name: 'Faroe Islands'
                }]
            };

            beforeEach(function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '123';
                defaultMocks.routeParams.toPlace = '999';

                getDataDfr = q.defer();
                spyOn(defaultMocks.travelRankingsApiService, 'getData').and.returnValue(getDataDfr.promise);
            });

            it('should format the routes table', function() {
                createController();
                scope.$digest();

                getDataDfr.resolve(fakeData);
                scope.$digest();

                expect(scope.tableData[0].originName).toEqual('Easter Island');
                expect(scope.tableData[0].originIata).toEqual('EI');
                expect(scope.tableData[0].destinationName).toEqual('Faroe Islands');
                expect(scope.tableData[0].destinationIata).toEqual('FI');
                expect(scope.tableData[0].histogramPositionItinerary).toEqual([31, 32, 36]);
                expect(scope.tableData[0].histogramRankItinerary).toEqual([50, 40, 10]);
                expect(scope.tableData[0].histogramPositionBookButton).toEqual([0, 0, 100]);
                expect(scope.tableData[0].firstRankItinerary).toEqual(85);
            });

            it('should set pagination', function() {
                createController();
                scope.$digest();

                getDataDfr.resolve(fakeData);
                scope.$digest();

                expect(scope.maxSize).toBeDefined();
                expect(scope.itemsPerPage).toBeDefined();
                expect(scope.totalItems).toEqual(24);
                expect(scope.currentPage).toEqual(1);
            });
        });

        describe('Dates view data', function() {
            var getDataDfr;

            beforeEach(function() {
                defaultMocks.routeParams.view = 'dates';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '123';
                defaultMocks.routeParams.toPlace = '999';

                getDataDfr = q.defer();
                spyOn(defaultMocks.travelRankingsApiService, 'getData').and.returnValue(getDataDfr.promise);
            });

            it('should format the dates table', function() {
                createController();
                scope.$digest();

                getDataDfr.resolve({
                    stats: [{
                        inbound_date_time: '2015-07-18T00:00:00',
                        outbound_date_time: '2015-07-12T00:00:00',
                        origin_id: 123,
                        destination_id: 999,
                        all_itineraries: {
                            rank_histogram: [0.35, 0.4313, 0.3907]
                        },
                        your_cheapest_book_button: {
                            position_histogram: [0, 0, 1],
                            position: 1,
                            price: 122.411
                        }
                    }],
                    places: [{
                        display_code: 'EI',
                        place_id: 123,
                        name: 'Easter Island'
                    }, {
                        display_code: 'FI',
                        place_id: 999,
                        name: 'Faroe Islands'
                    }]
                });
                scope.$digest();

                // expect(scope.tableData[0].outboundDateUnix).toEqual(1436630400);
                // expect(scope.tableData[0].inboundDateUnix).toEqual(1437148800);
                expect(scope.tableData[0].outbound_date_time).toEqual('12 Jul 15');
                expect(scope.tableData[0].inbound_date_time).toEqual('18 Jul 15');
                expect(scope.tableData[0].fromDate).toEqual('2015-07-12');
                expect(scope.tableData[0].toDate).toEqual('2015-07-18');
                expect(scope.tableData[0].firstRankItinerary).toEqual(35);
                expect(scope.tableData[0].your_cheapest_book_button.position).toEqual(1);
                expect(scope.tableData[0].your_cheapest_book_button.capturable_margin).toEqual(0);
                expect(scope.tableData[0].sortPosition).toEqual(1);
            });
        });

        describe('Itineraries view data', function() {
            var getDataDfr;

            beforeEach(function() {
                defaultMocks.routeParams.view = 'itineraries';
                defaultMocks.routeParams.market = 'AU';
                defaultMocks.routeParams.fromPlace = '123';
                defaultMocks.routeParams.toPlace = '999';
                defaultMocks.routeParams.fromDate = '2015-07-12';
                defaultMocks.routeParams.toDate = '2015-07-18';

                getDataDfr = q.defer();
                spyOn(defaultMocks.travelRankingsApiService, 'getData').and.returnValue(getDataDfr.promise);
            });

            it('should format the itineraries table', function() {
                createController();
                scope.$digest();

                getDataDfr.resolve({
                    stats: [{
                        cheapest_ticket_option: {
                            price: 347
                        },
                        your_cheapest_ticket_option: {
                            position: 1,
                            price: 443,
                            max_price: 462,
                            rank: 2,
                            price_diff: 18
                        },
                        flight_numbers: [
                            'LH847',
                            'LH1284'
                        ],
                        inbound_date_time: '2015-07-18T06:25:00',
                        outbound_date_time: '2015-07-12T18:10:00',
                        competitors_cheapest_ticket_option: {
                            price: 347
                        }
                    }],
                    places: [{
                        display_code: 'EI',
                        place_id: 123,
                        name: 'Easter Island'
                    }, {
                        display_code: 'FI',
                        place_id: 999,
                        name: 'Faroe Islands'
                    }]
                });
                scope.$digest();

                expect(scope.tableData[0].outbound_date_time).toEqual('18:10');
                expect(scope.tableData[0].inbound_date_time).toEqual('06:25');
                expect(scope.tableData[0].flight_numbers).toEqual('LH847, LH1284');
                expect(scope.tableData[0].your_cheapest_ticket_option.price_diff).toEqual(18);
                expect(scope.tableData[0].distanceToTop).toEqual(96);
            });

            it('should format the last search date time to minutes if less than 1 hour', function() {
                var utcOffset = window.moment().utcOffset() + 22;
                var timeInUTC = window.moment().subtract(utcOffset, 'minutes');

                createController();
                scope.$digest();

                getDataDfr.resolve({
                    search_date_time: timeInUTC.format(),
                    stats: [{
                        cheapest_ticket_option: {
                            price: 347
                        },
                        your_cheapest_ticket_option: {
                            position: 1,
                            price: 443,
                            max_price: 462,
                            rank: 2,
                            price_diff: 18
                        },
                        flight_numbers: [
                            'LH847',
                            'LH1284'
                        ],
                        inbound_date_time: '2015-07-18T06:25:00',
                        outbound_date_time: '2015-07-12T18:10:00',
                        competitors_cheapest_ticket_option: {
                            price: 347
                        }
                    }],
                    places: [{
                        display_code: 'EI',
                        place_id: 123,
                        name: 'Easter Island'
                    }, {
                        display_code: 'FI',
                        place_id: 999,
                        name: 'Faroe Islands'
                    }]
                });
                scope.$digest();

                expect(scope.itinerarySearchTime).toEqual('22 minutes ago');
            });

            it('should format the last search date time as relative hours if more than 1 hour', function() {
                var utcOffset = window.moment().utcOffset() + 829;
                var timeInUTC = window.moment().subtract(utcOffset, 'minutes');

                createController();
                scope.$digest();

                getDataDfr.resolve({
                    search_date_time: timeInUTC.format(),
                    stats: [{
                        cheapest_ticket_option: {
                            price: 347
                        },
                        your_cheapest_ticket_option: {
                            position: 1,
                            price: 443,
                            max_price: 462,
                            rank: 2,
                            price_diff: 18
                        },
                        flight_numbers: [
                            'LH847',
                            'LH1284'
                        ],
                        inbound_date_time: '2015-07-18T06:25:00',
                        outbound_date_time: '2015-07-12T18:10:00',
                        competitors_cheapest_ticket_option: {
                            price: 347
                        }
                    }],
                    places: [{
                        display_code: 'EI',
                        place_id: 123,
                        name: 'Easter Island'
                    }, {
                        display_code: 'FI',
                        place_id: 999,
                        name: 'Faroe Islands'
                    }]
                });
                scope.$digest();

                expect(scope.itinerarySearchTime).toEqual('14 hours ago');
            });
        });

        describe('failure', function() {
            var getDataDfr;

            beforeEach(function() {
                defaultMocks.routeParams.view = 'routes';
                defaultMocks.routeParams.market = 'AU';

                getDataDfr = q.defer();
                spyOn(defaultMocks.travelRankingsApiService, 'getData').and.returnValue(getDataDfr.promise);
                getDataDfr.reject();
            });

            it('should display an error message', function() {
                createController();
                scope.$digest();

                expect(scope.feedback.message).toBeDefined();
            });

            it('should clear selectedDestination and selectedOrigin', function() {
                createController();
                scope.$digest();

                expect(scope.selectedDestination).toEqual(null);
                expect(scope.selectedOrigin).toEqual(null);
            });
        });
    });

    describe('select the only allowed market', function() {
        var dfr;

        beforeEach(function() {
            dfr = q.defer();
            spyOn(defaultMocks.travelRankingsApiService, 'getConfigs').and.returnValue(dfr.promise);
            spyOn(defaultMocks.location, 'path');
        });

        it('should select the first and the only allowed market in the list', function() {
            var expectedUrl = '/travel-rankings/routes/UK';

            dfr.resolve({
                markets: [{
                    code: 'CN',
                    name: 'China',
                    allowed: false
                }, {
                    code: 'UK',
                    name: 'United Kingdom',
                    allowed: true
                }, {
                    code: 'AU',
                    name: 'Australia',
                    allowed: false
                }]
            });

            createController();
            scope.$digest();

            expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
        });

        it('should not select any market if there is more than 1 allowed market', function() {
            dfr.resolve({
                markets: [{
                    code: 'CN',
                    name: 'China',
                    allowed: true
                }, {
                    code: 'UK',
                    name: 'United Kingdom',
                    allowed: true
                }, {
                    code: 'AU',
                    name: 'Australia',
                    allowed: false
                }]
            });

            createController();
            scope.$digest();

            expect(defaultMocks.location.path).not.toHaveBeenCalled();
        });
    });

    describe('Routing (page navigation)', function() {
        beforeEach(function() {
            spyOn(defaultMocks.location, 'path');
        });

        describe('#switchView', function() {
            it('should, at the mininum, navigate to /travel-rankings/routes/market', function() {
                var expectedUrl = '/travel-rankings/routes/SG';
                createController();

                scope.selectedMarket = {
                    code: 'SG'
                };

                scope.switchView();
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });

            it('should navigate to /travel-rankings/routes/market/origin/destination/startDate/endDate provided the options', function() {
                var expectedUrl = '/travel-rankings/itineraries/SG/1234/5678/2015-01-01/2015-01-02';
                createController();

                scope.switchView({
                    view: 'itineraries',
                    market: 'SG',
                    fromPlace: 1234,
                    toPlace: 5678,
                    fromDate: '2015-01-01',
                    toDate: '2015-01-02'
                });
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });
        });

        describe('#onSelectedMarket', function() {
            it('should navigate to /travel-rankings/routes/market if there is a selected market', function() {
                var expectedUrl = '/travel-rankings/routes/ID';
                createController();

                scope.onSelectedMarket({ code:'ID' });
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });

            it('should clear selectedOrigin and selectedDestination filters if there is market selected', function() {
                createController();

                scope.onSelectedMarket({ code: 'SG' });
                scope.$digest();

                expect(scope.selectedOrigin).toEqual(null);
                expect(scope.selectedDestination).toEqual(null);
            });

            it('should navigate to /travel-rankings/routes if there is no selected market', function() {
                var expectedUrl = '/travel-rankings/routes';
                createController();

                scope.onSelectedMarket();
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });
        });

        describe('#onSelectedOrigin', function() {
            it('should navigate to /travel-rankings/view/market/1234 if no destination exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG/1234';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.onSelectedOrigin({ place_id: 1234 });
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });

            it('should navigate to /travel-rankings/view/market/1234/5678 if destination exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG/1234/5678';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.selectedDestination = { place_id: 5678 };
                scope.onSelectedOrigin({ place_id: 1234 });
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });
        });

        describe('#onSelectedDestination', function() {
            it('should navigate to /travel-rankings/view/market//1234 if no origin exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG//1234';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.onSelectedDestination({ place_id: 1234 });
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });

            it('should navigate to /travel-rankings/view/market/5678/1234 if origin exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG/5678/1234';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.selectedOrigin = { place_id: 5678 };
                scope.onSelectedDestination({ place_id: 1234 });
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });
        });

        describe('#clearOrigin', function() {
            it('should clear selectedOrigin', function() {
                createController();

                scope.selectedOrigin = { code: 'SG' };
                scope.clearOrigin();
                scope.$digest();

                expect(scope.selectedOrigin).toEqual(null);
            });

            it('should navigate to /travel-rankings/view/market if no destination exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.clearOrigin();
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });

            it('should navigate to /travel-rankings/view/market//1234 if destination exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG//1234';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.selectedDestination = { place_id: 1234 };
                scope.clearOrigin();
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });
        });

        describe('#clearDestination', function() {
            it('should clear selectedDestination', function() {
                createController();

                scope.selectedOrigin = { code: 'SG' };
                scope.clearDestination();
                scope.$digest();

                expect(scope.selectedDestination).toEqual(null);
            });

            it('should navigate to /travel-rankings/view/market if no origin exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.clearDestination();
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });

            it('should navigate to /travel-rankings/view/market/1234 if origin exists', function() {
                var expectedUrl = '/travel-rankings/routes/SG/1234';
                createController();

                scope.selectedMarket = { code: 'SG' };
                scope.selectedOrigin = { place_id: 1234 };
                scope.clearDestination();
                scope.$digest();

                expect(defaultMocks.location.path).toHaveBeenCalledWith(expectedUrl);
            });
        });

        describe('#navigateRoutesView', function() {
            it('should navigate to /travel-rankings/routes/market?sortBy=myHeader&sortOrder=asc&page=104 provided the options', function() {
                var mockLocation = location;
                spyOn(mockLocation, 'path');
                spyOn(mockLocation, 'search');

                var expectedPath = '/travel-rankings/routes/SG';
                var expectedQuerystrings = {
                    sortBy: 'myHeader',
                    sortOrder: 'asc',
                    page: 104
                };

                createController(window._.extend(defaultMocks, {
                    location: mockLocation
                }));

                scope.selectedMarket = { code: 'SG' };
                scope.viewParams.sortBy = 'asc';
                scope.navigateRoutesView(expectedQuerystrings);
                scope.$digest();

                expect(mockLocation.path).toHaveBeenCalledWith(expectedPath);
                expect(mockLocation.search).toHaveBeenCalledWith(window._.extend(expectedQuerystrings, { sortOrder: 'desc' }));
            });

            it('should navigate to /travel-rankings/routes/market/1234/5678?sortBy=myHeader&sortOrder=asc&page=104 provided the options and both origin and destination exist', function() {
                var mockLocation = location;
                spyOn(mockLocation, 'path');
                spyOn(mockLocation, 'search');

                var expectedPath = '/travel-rankings/routes/SG/1234/5678';
                var expectedQuerystrings = {
                    sortBy: 'myHeader',
                    sortOrder: 'desc',
                    page: 104
                };

                createController(window._.extend(defaultMocks, {
                    location: mockLocation
                }));

                scope.selectedMarket = { code: 'SG' };
                scope.selectedOrigin = { place_id: 1234 };
                scope.selectedDestination = { place_id: 5678 };
                scope.viewParams.sortBy = 'desc';

                scope.navigateRoutesView(expectedQuerystrings);
                scope.$digest();

                expect(mockLocation.path).toHaveBeenCalledWith(expectedPath);
                expect(mockLocation.search).toHaveBeenCalledWith(window._.extend(expectedQuerystrings, { sortOrder: 'asc' }));
            });
        });
    });
});

}());
