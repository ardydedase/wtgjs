(function() {

'use strict';

angular.module('fcApp.travelRankings').controller('TravelRankingsCtrl', function(
    $scope,
    $q,
    $location,
    $routeParams,
    $modal,
    _,
    moment,
    travelRankingsViewModel,
    travelRankingsApiService,
    authService,
    mixpanelService
    ) {

    var baseUrl = '/travel-rankings/';
    var defaultView = 'routes';
    var defaultSortOrder = 'asc';

    var validRouteParams = ['view', 'market', 'fromPlace', 'toPlace', 'fromDate', 'toDate'];
    var validViews = ['routes', 'dates', 'itineraries'];

    var validQuerystrings = {
        page: 0,
        pageSize: 100,
        sortBy: 'searchVolume',
        sortOrder: 'desc'
    };

    var routingRules = [{
        view: 'routes',
        requiredRoutes: validRouteParams.slice(0, 1)
    }, {
        view: 'dates',
        requiredRoutes: validRouteParams.slice(0, 4)
    }, {
        view: 'itineraries',
        requiredRoutes: validRouteParams.slice(0)
    }];

    function init() {
        $scope.viewParams = {};
        $scope.places = [];

        if (!isValidRoute()) {
            redirect(baseUrl + defaultView);
        } else {
            travelRankingsApiService.getConfigs(true).then(function(config) {
                initializeMarkets(config.markets);
                initializeCurrency(config.display_currency);
                checkRouteParameters();
                checkQueryParameters();

                travelRankingsViewModel.view = $scope.viewParams;

                if ($scope.viewParams.market) {
                    getData();
                } else {
                    selectOnlyAllowedMarket();
                }

                if ($scope.viewParams.view === 'routes') {
                    initializePlaces();
                }
            },

            function() {
                $scope.feedback = {
                    message: 'Something went wrong when processing your request.',
                    type: 'danger'
                };
            });
        }
    }

    function initializeMarkets(markets) {
        var selectedMarketIndex = 0;

        markets = _.filter(markets, 'name');
        markets = _.sortBy(markets, 'name');

        $scope.markets = [{
            code: 'none',
            name: 'Select market'
        }].concat(markets);

        if ($routeParams.market && isValidMarket($routeParams.market)) {
            selectedMarketIndex = _.findIndex($scope.markets, { code: $routeParams.market });
        }

        $scope.selectedMarket = $scope.markets[selectedMarketIndex];
    }

    function initializeCurrency(currency) {
        var icons = ['EUR', 'INR', 'RMB', 'JPY', 'USD', 'CNY', 'GBP', 'KRW', 'RUB', 'WON', 'TRY'];

        $scope.currencyIcon = icons.indexOf(currency) > -1 ? currency : 'money';
        $scope.viewParams.currency = currency || '';
    }

    function initializePlaces() {
        var placeholderText = 'Loading name...';
        var originName;
        var destinationName;

        if ($scope.viewParams.fromPlace) {
            originName = getPlaceDetails(travelRankingsViewModel.places, $scope.viewParams.fromPlace);

            $scope.selectedOrigin = originName || placeholderText;
            $scope.viewParams.originName = originName.name ? originName.name : placeholderText;
        } else {
            $scope.selectedOrigin = null;
            $scope.viewParams.originName = '';
        }

        if ($scope.viewParams.toPlace) {
            destinationName = getPlaceDetails(travelRankingsViewModel.places, $scope.viewParams.toPlace);

            $scope.selectedDestination = destinationName || placeholderText;
            $scope.viewParams.destinationName = destinationName.name ? destinationName.name : placeholderText;
        } else {
            $scope.selectedDestination = null;
            $scope.viewParams.destinationName = '';
        }
    }

    function selectOnlyAllowedMarket() {
        var allowedMarkets = _.filter($scope.markets, 'allowed');

        if (allowedMarkets.length === 1) {
            selectMarket(_.find($scope.markets, 'allowed'));
        }
    }

    function isValidRoute() {
        var valid = true;

        if (_.isEmpty($routeParams) || validViews.indexOf($routeParams.view) < 0) {
            return false;
        }

        _.forEach(routingRules, function(rule) {
            if (rule.view === $routeParams.view) {
                _.forEach(rule.requiredRoutes, function(route) {
                    if (!_.includes(_.keys($routeParams), route)) {
                        valid = false;
                        return false;
                    }
                });
            }
        });

        return valid;
    }

    function checkRouteParameters() {
        _.forEach(validRouteParams, function(value) {
            if (_.has($routeParams, value)) {
                if (isValidParameter(value, $routeParams[value])) {
                    $scope.viewParams[value] = $routeParams[value];
                } else {
                    showFeedback(value, $routeParams[value]);
                    $scope.viewParams = {};
                }
            }
        });
    }

    function checkQueryParameters() {
        var queryParams = $location.search();

        if ($scope.viewParams.view === 'routes') {
            _.forOwn(validQuerystrings, function(value, key) {
                var qsValue = queryParams[key];

                if (qsValue === '' || qsValue === undefined) {
                    qsValue = value;
                }

                $scope.viewParams[key] = qsValue;
            });
        }
    }

    function isValidMarket(marketCode) {
        return _.some($scope.markets.slice(1), { code: marketCode });
    }

    function isValidParameter(param, value) {
        if (param === 'view') {
            return validViews.indexOf(value) > -1;
        }

        if (param === 'market') {
            return isValidMarket(value);
        }

        if (param === 'fromPlace' || param === 'toPlace') {
            return _.isFinite(Number(value));
        }

        if (param === 'fromDate' || param === 'toDate') {
            return moment(value, 'YYYY-MM-DD').isValid();
        }
    }

    function showFeedback(param, value) {
        $scope.feedback = {
            message: 'The ' + param + ' value: "' + value + '" is invalid.',
            type: 'danger'
        };
    }

    function getPlaceDetails(places, id) {
        return _.find(places, { place_id: Number(id) }) || {};
    }

    function getMarketDetails(markets, code) {
        return _.find(markets, { code: code }) || {};
    }

    function getHistogramPercentage(data) {
        var diff = 1 - _.sum(data);

        if (_.sum(data) === 0) {
            data[2] = 0;
        } else {
            data[2] = data[2] + (diff < 1 ? (diff) : -diff);
        }

        return _.map(data, function(d) {
            return parseInt((d * 100), 10);
        });
    }

    function getSortableValue(obj, key) {
        if (obj) {
            return obj[key] === undefined ? Number.MAX_SAFE_INTEGER : obj[key];
        } else {
            return Number.MAX_SAFE_INTEGER;
        }
    }

    function formatRoutesViewData(res) {
        $scope.tableData =  _.map(res.stats, function(r) {
            var origin = getPlaceDetails(res.places, r.origin_id);
            var destination = getPlaceDetails(res.places, r.destination_id);

            return _.extend(r, {
                originName: origin.name ? origin.name : '',
                originIata: origin.name ? origin.display_code : '',
                destinationName: destination ? destination.name : '',
                destinationIata: destination ? destination.display_code : '',
                histogramPositionItinerary: getHistogramPercentage(r.your_cheapest_itinerary.position_histogram),
                histogramRankItinerary: getHistogramPercentage(r.your_cheapest_itinerary.rank_histogram),
                histogramPositionBookButton: getHistogramPercentage(r.your_cheapest_book_button.position_histogram),
                firstRankItinerary: getHistogramPercentage(r.all_itineraries.rank_histogram)[0]
            });
        });
    }

    function formatDatesViewData(res) {
        $scope.tableData =  _.map(res.stats, function(r) {
            var outboundDateTime = moment.utc(r.outbound_date_time);
            var inboundDateTime = moment.utc(r.inbound_date_time);
            var searchDateTime = moment.utc(r.search_date_time);

            return _.extend(r, {
                outboundDateUnix: outboundDateTime.unix(),
                inboundDateUnix: inboundDateTime.unix(),
                searchDateUnix: -(searchDateTime.unix()),
                outbound_date_time: outboundDateTime.format('DD MMM YY'),
                inbound_date_time: inboundDateTime.format('DD MMM YY'),
                search_date_time: getLastSearchTime(r.search_date_time),
                fromDate: moment(r.outbound_date_time).format('YYYY-MM-DD'),
                toDate: moment(r.inbound_date_time).format('YYYY-MM-DD'),
                firstRankItinerary: getHistogramPercentage(r.all_itineraries.rank_histogram)[0],
                your_cheapest_book_button: r.your_cheapest_book_button ? {
                    position: r.your_cheapest_book_button.position ? r.your_cheapest_book_button.position : undefined,
                    price: r.your_cheapest_book_button.price,
                    capturable_margin: r.your_cheapest_book_button.capturable_margin ? r.your_cheapest_book_button.capturable_margin : 0
                } : {},
                sortPosition: getSortableValue(r.your_cheapest_book_button, 'position')
            });
        });
    }

    function formatItinerariesViewData(res) {

        function getDistanceToTopPrice(data) {
            var diff = 0;

            if (data.competitors_cheapest_ticket_option) {
                diff = data.your_cheapest_ticket_option.price - data.competitors_cheapest_ticket_option.price;

                if (diff < 0) {
                    diff = 0;
                }
            }

            return diff;
        }

        $scope.tableData =  _.map(res.stats, function(r) {
            return _.extend(r, {
                outbound_date_time: moment(r.outbound_date_time).format('HH:mm'),
                inbound_date_time: moment(r.inbound_date_time).format('HH:mm'),
                flight_numbers: r.flight_numbers.join(', '),
                your_cheapest_ticket_option: _.extend(r.your_cheapest_ticket_option, {
                    price_diff: r.your_cheapest_ticket_option.price_diff === undefined ? 0 : r.your_cheapest_ticket_option.price_diff
                }),
                distanceToTop: getDistanceToTopPrice(r)
            });
        });
    }

    function getLastSearchTime(dateTime) {
        var diffInMinutes = 0;
        var utcOffset = moment().utcOffset();
        var timeInUTC = moment().subtract(utcOffset, 'minutes');

        if (dateTime !== undefined) {
            diffInMinutes = timeInUTC.diff(dateTime, 'minutes');

            if (diffInMinutes < 60) {
                return diffInMinutes + ' minutes ago';
            } else {
                return moment(dateTime).add(utcOffset, 'minutes').fromNow();
            }
        }
    }

    function setPagination(pageSize, totalRows, currentPage) {
        $scope.maxSize = 15;
        $scope.itemsPerPage = 100;
        $scope.totalItems = totalRows;
        $scope.currentPage = Number(currentPage);
    }

    function getData() {
        var view = $scope.viewParams.view;
        var currentPage = $scope.viewParams.page || 1;

        $scope.isLoading = true;

        $scope.promise = travelRankingsApiService.getData($scope.viewParams).then(function(res) {
            $scope.tableData = res.stats;
            $scope.staticTableData = [].concat($scope.tableData);
            $scope.places = res.places;

            travelRankingsViewModel.places = $scope.places;

            if ($routeParams.fromPlace || $routeParams.toPlace) {
                initializePlaces();
            }

            if (view === 'routes') {
                formatRoutesViewData(res);
                setPagination(res.page_size, res.total_rows, currentPage);
            } else if (view === 'dates') {
                formatDatesViewData(res);
            } else if (view === 'itineraries') {
                formatItinerariesViewData(res);
                $scope.itinerarySearchTime = getLastSearchTime(res.search_date_time);
            }

            mixpanelTracking('Load page');
        },

        function() {
            $scope.feedback = {
                message: 'Something went wrong when processing your request.',
                type: 'danger'
            };
            $scope.selectedDestination = null;
            $scope.selectedOrigin = null;
        }).finally(function() {
            $scope.isLoading = false;
        });
    }

    function redirect(url, params) {
        $location.path(url);

        if (params) {
            $location.search(params);
        } else {
            $location.search({});
        }
    }

    function constructUrl(options) {
        var view = options && options.view || $scope.viewParams.view || 'routes';
        var market = options && options.market ? options.market : ($scope.selectedMarket ? $scope.selectedMarket.code : '');

        var origin = options && options.fromPlace ? options.fromPlace.toString() : '';
        var destination = options && options.toPlace ? options.toPlace.toString() : '';

        var fromDate = options && options.fromDate;
        var toDate = options && options.toDate;

        var url = baseUrl + view + '/' + market;

        if (origin.length) {
            url += '/' + origin;
        }

        if (destination.length) {
            if (!origin.length) {
                url += '/';
            }

            url += '/' + destination;
        }

        if (fromDate && fromDate.length) {
            url += '/' + fromDate;
        }

        if (fromDate && toDate.length) {
            url += '/' + toDate;
        }

        return url;
    }

    function getSortOrder(currentSortBy) {
        var order = defaultSortOrder;

        if (currentSortBy) {
            if (currentSortBy === $scope.viewParams.sortBy) {
                order = ($scope.viewParams.sortOrder === 'asc') ? 'desc' : 'asc';
            }
        }

        return order;
    }

    function selectMarket(market) {
        var marketCode = market && market.code;

        if (market && market.code !== 'none') {
            $scope.selectedOrigin = null;
            $scope.selectedDestination = null;

            mixpanelTracking('Filter market', {
                market: getMarketDetails($scope.markets, market.code).name || '',
                destination: '',
                origin: ''
            });

            redirect(constructUrl({
                market: marketCode
            }));
        } else {
            redirect(baseUrl + defaultView);
        }
    }

    function mixpanelTracking(action, options) {
        var trackingKeys = ['view', 'market', 'fromPlace', 'toPlace', 'fromDate', 'toDate'];
        var nonTrackingKeys = ['destinationName', 'originName', 'pageSize'];
        var viewParams = $scope.viewParams;
        var trackingData = {};

        if (options) {
            _.extend(viewParams, options);
        }

        _.forOwn($scope.viewParams, function(value, key) {
            if (trackingKeys.indexOf(key) > -1) {
                if (key === 'market') {
                    trackingData.market = getMarketDetails($scope.markets, value).name || '';
                    return;
                }

                if (key === 'fromPlace') {
                    trackingData.origin = getPlaceDetails($scope.places, value).name || '';
                    return;
                }

                if (key === 'toPlace') {
                    trackingData.destination = getPlaceDetails($scope.places, value).name || '';
                    return;
                }

                trackingData[key] = value;
            } else {
                if (nonTrackingKeys.indexOf(key) === -1) {
                    trackingData[key] = value;
                }
            }
        });

        trackingData.action = action;

        mixpanelService.track('FC TravelRankings Dashboard', trackingData);
    }

    $scope.onSelectedMarket = selectMarket;

    $scope.onSelectedOrigin = function(origin) {
        mixpanelTracking('Filter origin', {
            origin: getPlaceDetails($scope.places, origin.place_id).name || ''
        });

        redirect(constructUrl({
            fromPlace: origin.place_id,
            toPlace: $scope.selectedDestination ? $scope.selectedDestination.place_id : ''
        }));
    };

    $scope.onSelectedDestination = function(destination) {
        mixpanelTracking('Filter destination', {
            destination: getPlaceDetails($scope.places, destination.place_id).name || ''
        });

        redirect(constructUrl({
            toPlace: destination.place_id,
            fromPlace: $scope.selectedOrigin ? $scope.selectedOrigin.place_id : ''
        }));
    };

    $scope.clearOrigin = function() {
        $scope.selectedOrigin = null;

        mixpanelTracking('Clear origin', {
            origin: ''
        });

        redirect(constructUrl({
            toPlace: $scope.selectedDestination ? $scope.selectedDestination.place_id : ''
        }));
    };

    $scope.clearDestination = function() {
        $scope.selectedDestination = null;

        mixpanelTracking('Clear destination', {
            destination: ''
        });

        redirect(constructUrl({
            fromPlace: $scope.selectedOrigin ? $scope.selectedOrigin.place_id : ''
        }));
    };

    $scope.switchView = function(options) {
        mixpanelTracking('Switch view', options);

        redirect(constructUrl(options));
    };

    $scope.navigateRoutesView = function(options) {
        var action = options.sortBy ? 'Sort routes' : 'Paginate routes';
        mixpanelTracking(action, options);

        redirect(constructUrl({
            view: 'routes',
            fromPlace: $scope.selectedOrigin ? $scope.selectedOrigin.place_id : '',
            toPlace: $scope.selectedDestination ? $scope.selectedDestination.place_id : ''
        }), {
            sortBy: options.sortBy ? options.sortBy : $scope.viewParams.sortBy,
            sortOrder: getSortOrder(options.sortBy),
            page:  options.page ? options.page : $scope.viewParams.page
        });
    };

    $scope.trackSort = function(options) {
        mixpanelTracking('Sort ' + options.view, options);
    };

    $scope.openModal = function() {
        $modal.open({
            templateUrl: 'app/travelRankings/templates/help-modal.html',
            controller: 'StaticModalCtrl',
            backdrop: true,
            size: 'lg'
        });
    };

    init();
});

}());
