(function() {

'use strict';

angular.module('fcApp.travelRankings').factory('travelRankingsApiService', function(
    $http,
    $q,
    config,
    _,
    localStorageService
    ) {

    var baseUrl = config.API_URL + config.API_BASE + '/v0.4/flights/travel_rankings/';

    /* There is no need to pass dfr here if Angular handles promise chaining properly */
    function _getConfig(cache, dfr) {
        var url = baseUrl + 'config/';

        return $http({
            method: 'get',
            url: url
        }).then(function(res) {
            if (cache) {
                localStorageService.add('trConfig', res.data);
            }

            dfr.resolve(res.data);
        },

        function(err) {
            dfr.reject(err);
        });
    }

    /**
     * Get TR data
     * @param  {String} options.view - REQUIRED
     * @param  {String} options.market - REQUIRED
     * @param  {String} options.currency - REQUIRED
     * @param  {Integer} options.fromPlace
     * @param  {Integer} options.toPlace
     * @param  {String} options.fromDate in 'YYYY-MM-DD' format
     * @param  {String} options.toDate in 'YYYY-MM-DD' format
     * @param  {Integer} options.page page number
     * @param  {String} options.pageSize number of results per page
     * @param  {String} options.sortBy
     * @param  {String} options.sortOrder 'asc' or desc'
     * @return {Array} [{ id: 'JP', market: 'Japan' }]
     */
    function getData(options) {
        var url = baseUrl + options.view + '/';

        var parameters = {
            market: options.market,
            locale: 'en-gb',
            currency: options.currency,
            origin: options.fromPlace || '',
            destination: options.toPlace || ''
        };

        var routesParameters = [{
            name: 'page',
            value: (options.page - 1) || 0
        }, {
            name: 'pageSize',
            value: options.pageSize || 100
        }, {
            name: 'sortBy',
            value: options.sortBy || 'fromPlaceName'
        }, {
            name: 'sortOrder',
            value: options.sortOrder || 'asc'
        }];

        _.forEach(routesParameters, function(d) {
            if (options[d.name] || options.view === 'routes') {
                parameters[_.snakeCase(d.name)] = d.value;
            }
        });

        if (options.view === 'itineraries') {
            parameters.outbound_date = options.fromDate || '';
            parameters.inbound_date = options.toDate || '';
        }

        return $http({
            method: 'get',
            url: url,
            params: parameters
        }).then(function(res) {
            return res.data;
        });
    }

    /**
     * Get an object of config
     * @param  {Boolean} cache - if config is to be stored and retrieved from localstorage after initial retrieval
     * @return {Object} {agents: ["xpau"], display_currency: 'USD', markets: [{code: "AU", name: "Australia", allowed: true}]}
     */
    function getConfigs(cache) {
        var dfr = $q.defer();
        var config;

        if (cache) {
            config = localStorageService.get('trConfig');

            if (config) {
                dfr.resolve(config);
            } else {
                _getConfig(true, dfr);
            }
        } else {
            _getConfig(false, dfr);
        }

        return dfr.promise;
    }

    return {
        getData: getData,
        getConfigs: getConfigs
    };
});

}());
