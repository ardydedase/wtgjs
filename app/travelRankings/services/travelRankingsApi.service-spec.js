(function() {

'use strict';

describe('Service: travelRankingsApiService', function() {

    var config;
    var rootScope;
    var travelRankingsApiService;
    var mockLocalStorageService;
    var fakeConfigs;
    var httpBackend;
    var baseUrl;

    beforeEach(module('fcApp.travelRankings'));

    beforeEach(module('fcApp.travelRankings', function($provide) {
        mockLocalStorageService = {
            get: function() {
                return fakeConfigs;
            },

            add: function(id, data) {
                fakeConfigs = data;
            }
        };

        $provide.value('localStorageService', mockLocalStorageService);
        $provide.value('_', window._);
    }));

    beforeEach(inject(function($httpBackend, $rootScope, _config_, _travelRankingsApiService_) {
        config = _config_;
        httpBackend = $httpBackend;
        rootScope = $rootScope.$new();
        travelRankingsApiService = _travelRankingsApiService_;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#getConfigs', function() {
        afterEach(function() {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        it('should be defined', function() {
            expect(travelRankingsApiService.getConfigs).toBeDefined();
        });

        it('should GET /v0.4/flights/travel_rankings/config/', function() {
            var url = baseUrl + '/v0.4/flights/travel_rankings/config/';

            httpBackend.whenGET(url).respond({});

            travelRankingsApiService.getConfigs().then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should return an object of configs', function() {
            var expectedResponse = [{
                display_currency: 'USD',
                markets: [{ code: 'SG', name: 'Singapore', allowed: true }],
                agents: ['xpuk']
            }];

            var url = baseUrl + '/v0.4/flights/travel_rankings/config/';

            httpBackend.whenGET(url).respond(expectedResponse);

            travelRankingsApiService.getConfigs().then(function(res) {
                expect(res).toEqual(expectedResponse);
            });

            httpBackend.flush();
        });

        describe('return cached configs', function() {
            beforeEach(function() {
                fakeConfigs = {
                    display_currency: 'USD',
                    markets: [{
                        code: 'SG',
                        name: 'Singapore',
                        allowed: true
                    }],
                    agents: ['xpuk']
                };
            });

            it('should return a promise', function() {
                travelRankingsApiService.getConfigs(true).then(function(res) {
                    expect(res).toBeDefined();
                });

                rootScope.$apply();
            });

            it('should return cached data if cache exists', function() {
                expect(fakeConfigs).toBeDefined();

                travelRankingsApiService.getConfigs(true).then(function(res) {
                    expect(res.display_currency).toEqual('USD');
                    expect(angular.isArray(res.markets)).toEqual(true);
                });

                rootScope.$apply();
            });

            it('should return remote response if cache does not exist', function() {
                fakeConfigs = null;

                var url = baseUrl + '/v0.4/flights/travel_rankings/config/';
                httpBackend.whenGET(url).respond(['remote']);

                travelRankingsApiService.getConfigs().then(function(res) {
                    expect(res).toEqual(['remote']);
                });

                httpBackend.flush();
            });
        });

        describe('cache configs', function() {
            var expectedRemoteResponse;

            beforeEach(function() {
                fakeConfigs = null;

                expectedRemoteResponse = {
                    display_currency: 'TRY',
                    markets: ['JP']
                };

                var url = baseUrl + '/v0.4/flights/travel_rankings/config/';
                httpBackend.whenGET(url).respond(expectedRemoteResponse);
            });

            it('should cache data to localStorage if cache is true', function() {
                travelRankingsApiService.getConfigs(true).then(function() {
                    expect(fakeConfigs.display_currency).toEqual('TRY');
                    expect(fakeConfigs.markets[0]).toEqual('JP');
                });

                httpBackend.flush();
            });
        });
    });

    describe('#getData', function() {
        afterEach(function() {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        it('should be defined', function() {
            expect(travelRankingsApiService.getData).toBeDefined();
        });

        it('should GET /v0.4/flights/travel_rankings/view/ and send default parameters', function() {
            var expectedUrl = baseUrl + '/v0.4/flights/travel_rankings/routes/?destination=&locale=en-gb&origin=&page=0&page_size=100&sort_by=fromPlaceName&sort_order=asc';

            httpBackend.whenGET(expectedUrl).respond({});

            travelRankingsApiService.getData({
                view: 'routes'
            }).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should send custom parameters', function() {
            var expectedUrl = baseUrl + '/v0.4/flights/travel_rankings/itineraries/?currency=USD&destination=1234&inbound_date=2015-01-02&locale=en-gb&market=GB&origin=5678&outbound_date=2015-01-01';

            var expectedParams = {
                view: 'itineraries',
                market: 'GB',
                currency: 'USD',
                toPlace: 1234,
                fromPlace: 5678,
                fromDate: '2015-01-01',
                toDate: '2015-01-02'
            };

            httpBackend.whenGET(expectedUrl).respond({});

            travelRankingsApiService.getData(expectedParams).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });

        it('should send extra parameters for routes view', function() {
            var expectedUrl = baseUrl + '/v0.4/flights/travel_rankings/routes/?currency=USD&destination=1234&locale=en-gb&market=GB&origin=5678&page=100&page_size=50&sort_by=myHeader&sort_order=desc';

            var expectedParams = {
                view: 'routes',
                market: 'GB',
                currency: 'USD',
                toPlace: 1234,
                fromPlace: 5678,
                page: 101,
                pageSize: 50,
                sortBy: 'myHeader',
                sortOrder: 'desc'
            };

            httpBackend.whenGET(expectedUrl).respond({});

            travelRankingsApiService.getData(expectedParams).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });
    });

});

}());
