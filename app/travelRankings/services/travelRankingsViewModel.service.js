(function() {

'use strict';

angular.module('fcApp.travelRankings').factory('travelRankingsViewModel', function() {
    return {
        view: {},
        places: []
    };
});

}());
