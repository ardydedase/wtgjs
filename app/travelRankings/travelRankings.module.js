(function() {

'use strict';

angular.module('fcApp.travelRankings', [
    'smart-table',
    'ngOptionsDisabled',
    'ui.bootstrap',
    'ngRoute',
    'LocalStorageModule',
    'fcApp.common'
]);

}());
