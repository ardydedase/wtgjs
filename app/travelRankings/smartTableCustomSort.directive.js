(function() {

'use strict';

angular.module('smart-table').directive('stCustomSort', function() {
    return {
        require: '^stTable',
        link: {
            pre: function(scope, element, attrs, ctrl) {
                ctrl.setSortFunction('ignoreUndefinedSort');
            }
        }
    };
});

}());
