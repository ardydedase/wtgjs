(function() {

'use strict';

angular.module('fcApp.apiHealth', [
    'appConfig',
    'LocalStorageModule',
    'smart-table',
    'fcApp.common',
    'fcApp.commercial',
    'fcApp.technical'
]);

angular.module('fcApp.apiHealth')
    .constant('d3', window.d3)
    .constant('c3', window.c3);

}());
