(function() {

'use strict';

describe('Controller: MonitorCtrl', function() {

    var MonitorCtrl;
    var scope;

    beforeEach(module('fcApp.apiHealth'));

    beforeEach(inject(function($controller, $rootScope, $q) {
        scope = $rootScope.$new();
        scope.websites = ['xpsg'];

        var promise = function() {
            var dfr = $q.defer();
            return dfr.promise;
        };

        MonitorCtrl = $controller('MonitorCtrl', {
            $scope: scope,
            monitorService: {
                getDeeplinkFailures: promise,
                getDeeplinkErrors: promise
            }
        });
    }));

    it('should have a defined scope.website_id', function() {
        expect(scope.website_id).toBeDefined();
    });

    it('should have a defined scope.webconfig', function() {
        expect(scope.webconfig).toBeDefined();
    });

    it('should have an initialized scope.dlRowCollection', function() {
        expect(scope.dlRowCollection).toBeDefined();
    });
});

}());
