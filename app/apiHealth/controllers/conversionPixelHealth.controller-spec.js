'use strict';

describe('Controller: ConversionPixelHealthCtrl', function() {

    var controller;
    var defaultMocks;
    var deferred;
    var q;
    var scope;

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('ConversionPixelHealthCtrl', {
            $scope: scope,
            localStorageService: mocks.localStorageService,
            conversionPixelHealthService: mocks.conversionPixelHealthService,
            mixpanelService: mocks.mixpanelService
        });
    };

    beforeEach(module('fcApp.apiHealth'));

    beforeEach(inject(function($controller, $rootScope, $location, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        defaultMocks = {
            localStorageService: {
                get: function() {
                    return 'spfg';
                }
            },
            conversionPixelHealthService: {
                getData: function() {
                    deferred = q.defer();
                    return deferred.promise;
                },

                formatData: function() {
                    return 'formatData';
                },

                getChart: function() {
                    return 'getChart';
                },

                getFailures: function() {
                    return 'getFailures';
                },

                getChartData: function() {
                    return 'getChartData';
                },

                updateChart: function() {
                    return 'updateChart';
                }
            },
            mixpanelService: {
                track: function() {}
            }
        };
    }));

    it('should initialize defaults', function() {
        createController();

        expect(scope.apiLoad).toBeDefined();
    });

    it('should call conversionPixelHealthService.getData', function() {
        spyOn(defaultMocks.conversionPixelHealthService, 'getData').and.callThrough();

        createController();

        expect(defaultMocks.conversionPixelHealthService.getData).toHaveBeenCalledWith('spfg');
    });

    it('should call conversionPixelHealthService.getChart', function() {
        spyOn(defaultMocks.conversionPixelHealthService, 'getChart');

        createController();

        expect(defaultMocks.conversionPixelHealthService.getChart).toHaveBeenCalled();
    });

    it('should set noBookingData = true if there is no success or failure', function() {
        createController();

        deferred.resolve({
            successes: 0,
            failures: 0
        });

        scope.$digest();

        expect(scope.noBookingData).toEqual(true);
    });

    it('should set noBookingData = false if there is failure or success', function() {
        createController();

        deferred.resolve({
            successes: 1,
            failures: 0
        });

        scope.$digest();

        expect(scope.noBookingData).toEqual(false);
    });

    it('should set failureList if there is failure or success', function() {
        createController();

        deferred.resolve({
            successes: 1,
            failures: 0
        });

        scope.$digest();

        expect(scope.failureList).toBeDefined();
    });

    it('should call conversionPixelHealthService.formatData', function() {
        var fakeData = {
            success: 1,
            fake: 1
        };

        spyOn(defaultMocks.conversionPixelHealthService, 'formatData');
        createController();
        deferred.resolve(fakeData);
        scope.$digest();

        expect(defaultMocks.conversionPixelHealthService.formatData).toHaveBeenCalled();
    });

    it('should call conversionPixelHealthService.getChartData', function() {
        var fakeData = {
            success: 1,
            fake: 1
        };

        spyOn(defaultMocks.conversionPixelHealthService, 'getChartData');
        createController();
        deferred.resolve(fakeData);
        scope.$digest();

        expect(defaultMocks.conversionPixelHealthService.getChartData).toHaveBeenCalled();
    });

    it('should call conversionPixelHealthService.getFailures', function() {
        var fakeData = {
            success: 1,
            fake: 1
        };

        spyOn(defaultMocks.conversionPixelHealthService, 'getFailures');
        createController();
        deferred.resolve(fakeData);
        scope.$digest();

        expect(defaultMocks.conversionPixelHealthService.getFailures).toHaveBeenCalled();
    });

    it('should call conversionPixelHealthService.updateChart', function() {
        var fakeData = {
            success: 1,
            fake: 1
        };

        spyOn(defaultMocks.conversionPixelHealthService, 'updateChart');
        createController();
        deferred.resolve(fakeData);
        scope.$digest();

        expect(defaultMocks.conversionPixelHealthService.updateChart).toHaveBeenCalledWith(jasmine.objectContaining({
            data: 'getChartData'
        }));
    });
});
