(function() {

'use strict';

angular.module('fcApp.apiHealth').controller('ConversionPixelHealthCtrl', function(
    $scope,
    localStorageService,
    conversionPixelHealthService,
    mixpanelService
    ) {

    var selectedWebsiteId = localStorageService.get('selectedwebsite') || '';

    var chart;
    var apiData;

    function isBookingDataAvailable(data) {
        if (data.successes === 0 && data.failures === 0) {
            $scope.noBookingData = true;

            mixpanelTracking({
                hasConversionPixel: false
            });

            return false;
        }

        mixpanelTracking({
            hasConversionPixel: true
        });

        return true;
    }

    function drawUI(data) {
        $scope.noBookingData = false;
        $scope.failureList = conversionPixelHealthService.getFailures(apiData);

        conversionPixelHealthService.updateChart({
            chart: chart,
            data: data
        });
    }

    function drawChart() {
        chart = conversionPixelHealthService.getChart('#conversion-chart');
    }

    function init() {
        var chartData;

        $scope.apiLoad = conversionPixelHealthService.getData(selectedWebsiteId).then(function(data) {
            if (isBookingDataAvailable(data)) {
                apiData = conversionPixelHealthService.formatData(data);
                chartData = conversionPixelHealthService.getChartData(apiData);
                drawUI(chartData);
            }
        });
    }

    function mixpanelTracking(trackingData) {
        mixpanelService.track('FC ConversionPixel', trackingData);
    }

    init();
    drawChart();
});

}());
