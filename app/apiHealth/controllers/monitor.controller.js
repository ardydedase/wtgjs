(function() {

'use strict';

angular.module('fcApp.apiHealth').controller('MonitorCtrl', function(
    $scope,
    monitorService,
    localStorageService,
    webConfigService,
    alertService) {

    function init() {
        $scope.closeAlert = alertService.closeAlert;
        $scope.website_id = localStorageService.get('selectedwebsite');
        $scope.webconfig = webConfigService.getFromStorage();

        $scope.show_dl_failures = false;
        $scope.show_nodlfailures = false;

        $scope.show_nofailures_data = false;

        $scope.dlRowCollection = [];
        $scope.show_no_luGridData = false;

        $scope.dl_failures = {
            failures: 0,
            partial_failures: 0
        };
    }

    $scope.deeplink_failures_promise = monitorService.getDeeplinkFailures().then(function(failuresResponse) {

        $scope.deeplink_errors_promise = monitorService.getDeeplinkErrors().then(function(errorsReponse) {

            if (errorsReponse.data.length > 0) {
                $scope.dlRowCollection = errorsReponse.data;
                $scope.show_dl_failures = true;
                $scope.show_nodlfailures = false;
            } else {
                $scope.show_nodlfailures = true;
            }

            $scope.dl_failures.failures = (failuresResponse.failed / failuresResponse.total) * 100;
            if (errorsReponse.partial_failures > failuresResponse.total) {
                errorsReponse.partial_failures = failuresResponse.total;
            }

            $scope.dl_failures.partial_failures = (errorsReponse.partial_failures / failuresResponse.total) * 100;
        });
    });

    $scope.showDeeplinkErrorDetails = function(row) {
        if (row.error === 'OtherError' && row.target_depth !== row.achieved_depth)  {
            return 'The target depth was ' + row.target_depth + ' but the redirect reached ' + row.achieved_depth + '.';
        }
    };

    $scope.readableErrorMessage = function(row) {
        if (row.error === 'OtherError' && row.target_depth !== row.achieved_depth)  {
            return 'Details';
        }

        return row.error;
    };

    init();
});

}());
