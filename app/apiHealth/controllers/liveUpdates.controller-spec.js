'use strict';

describe('Controller: LiveUpdatesCtrl', function() {

    var controller;
    var defaultMocks;
    var q;
    var scope;

    var noop = function() {};

    var defer = function() {
        var dfr = q.defer();
        return dfr.promise;
    };

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('LiveUpdatesCtrl', {
            $scope: scope,
            $window: mocks.window,
            _: window._,
            moment: window.moment,
            localStorageService: mocks.localStorageService,
            apiChartService: mocks.apiChartService,
            apiHealthService: mocks.apiHealthService,
            mixpanelService: mocks.mixpanelService
        });
    };

    beforeEach(module('fcApp.apiHealth'));

    beforeEach(inject(function($controller, $rootScope, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        defaultMocks = {
            window: {
                open: noop
            },
            localStorageService: {
                get: function(key) {
                    if (key === 'selectedwebsite') {
                        return 'abcd';
                    }

                    if (key === 'webconfig') {
                        return {
                            currency: 'sgd',
                            lang: 'en-gb'
                        };
                    }
                }
            },
            apiChartService: {
                getChart: noop,
                getChartData: noop,
                updateChart: noop
            },
            apiHealthService: {
                getSummaryTimeSeries: defer,
                getExamples: defer,
                getOverallEventStatus: function() {
                    return 'ok';
                }
            },
            mixpanelService: {
                track: noop
            }
        };
    }));

    it('should initialize defaults', function() {
        createController();

        expect(scope.examples).toBeDefined();
        expect(scope.feedback).toEqual(false);
        expect(typeof scope.showEventList).toEqual('function');
        expect(typeof scope.getExamples).toEqual('function');
        expect(typeof scope.redirectTest).toEqual('function');
    });

    it('should initialize the chart', function() {
        spyOn(defaultMocks.apiChartService, 'getChart');

        createController();

        expect(defaultMocks.apiChartService.getChart).toHaveBeenCalled();
    });

    it('should retrieve time series and summary data', function() {
        spyOn(defaultMocks.apiHealthService, 'getSummaryTimeSeries').and.callThrough();

        createController();
        scope.$digest();

        expect(defaultMocks.apiHealthService.getSummaryTimeSeries).toHaveBeenCalledWith({
            website_id: 'abcd'
        });
    });

    describe('chart and summary data', function() {
        var dfr;

        var response = {
            field_names: 'hello',
            time_series: [{
                time: 1,
            }, {
                time: 2
            }, {
                time: 3
            }],
            summary: {
                noquotes_breakdown: [{
                    origin: {
                        code: 'PAR',
                        routenode_id: 6073,
                        iata_id: 'PAR',
                        name: 'Paris'
                    },
                    destination: {
                        code: 'MCM',
                        routenode_id: 13931,
                        iata_id: 'MCM',
                        name: 'Monte Carlo'
                    },
                    value: 4
                }],
                error_breakdown: [{
                    type: 'connectiontimeout',
                    value: 3
                }, {
                    type: 'invalidresponse',
                    value: 2
                }],
                totals: [{
                    type: 'success',
                    value: 10
                }, {
                    type: 'noquotes',
                    value: 7
                }, {
                    type: 'failure',
                    value: 3
                }, {
                    type: 'total',
                    value: 20
                }]
            }
        };

        beforeEach(function() {
            dfr = q.defer();
            spyOn(defaultMocks.apiHealthService, 'getSummaryTimeSeries').and.returnValue(dfr.promise);
        });

        it('should populate error codes dictionary', function() {
            createController();

            dfr.resolve(response);
            scope.$digest();

            expect(scope.dictionary).toEqual('hello');
        });

        it('should populate the number of past hours of data collection', function() {
            createController();

            dfr.resolve(response);
            scope.$digest();

            expect(scope.dataLen).toEqual(2);
        });

        it('should format the list of summary totals', function() {
            createController();

            dfr.resolve(response);
            scope.$digest();

            expect(scope.summaries).toEqual({
                success: {
                    total: 10,
                    percentage: 50,
                    status: 'ok'
                },
                noquotes: {
                    total: 7,
                    percentage: 35,
                    status: 'ok'
                },
                failure: {
                    total: 3,
                    percentage: 15,
                    status: 'ok'
                },
                total: {
                    total: 20,
                    percentage: 100,
                    status: 'ok'
                }
            });
        });

        it('should combine the list of noquotes and errors into a list of events', function() {
            createController();

            dfr.resolve(response);
            scope.$digest();

            expect(scope.events.length).toEqual(3);
        });

        it('should format the list of events', function() {
            createController();

            dfr.resolve(response);
            scope.$digest();

            expect(scope.events).toEqual([{
                type: 'connectiontimeout',
                value: 3,
                category: 'failures',
                percentage: 60,
                id: 'error0'
            }, {
                type: 'invalidresponse',
                value: 2,
                category: 'failures',
                percentage: 40,
                id: 'error1'
            }, {
                origin: {
                    code: 'PAR',
                    routenode_id: 6073,
                    iata_id: 'PAR',
                    name: 'Paris'
                },
                destination: {
                    code: 'MCM',
                    routenode_id: 13931,
                    iata_id: 'MCM',
                    name: 'Monte Carlo'
                },
                value: 4,
                category: 'noquotes',
                id: 'noquotes0'
            }]);
        });

        it('should call apiChartService to get a formatted chart data', function() {
            spyOn(defaultMocks.apiChartService, 'getChartData');

            createController();
            dfr.resolve(response);

            scope.$digest();

            expect(defaultMocks.apiChartService.getChartData).toHaveBeenCalledWith(response.time_series, [{
                type: 'success',
                name: 'Success'
            }, {
                type: 'failure',
                name: 'Failure'
            }, {
                type: 'noquote',
                name: 'No Quotes'
            }]);
        });

        it('should call apiChartService to populate chart with formatted data', function() {
            spyOn(defaultMocks.apiChartService, 'getChart').and.returnValue('fakeChart');
            spyOn(defaultMocks.apiChartService, 'getChartData').and.returnValue('fakeChartData');
            spyOn(defaultMocks.apiChartService, 'updateChart');

            createController();
            dfr.resolve(response);

            scope.$digest();

            expect(defaultMocks.apiChartService.updateChart).toHaveBeenCalledWith({
                data: 'fakeChartData',
                chart: 'fakeChart'
            });
        });
    });

    it('should display feedback if data retrieval fails', function() {
        var dfr = q.defer();
        spyOn(defaultMocks.apiHealthService, 'getSummaryTimeSeries').and.returnValue(dfr.promise);

        createController();
        dfr.reject();
        scope.$digest();

        expect(scope.feedback).toEqual(true);
    });

    describe('#showEventList', function() {
        it('should set activeEvent to selected event "failures"', function() {
            createController();

            scope.showEventList('failures');

            expect(scope.activeEvent).toEqual({
                type: 'failures',
                title: 'Failures Details',
                className: 'red'
            });
        });

        it('should set activeEvent to selected event "noquotes"', function() {
            createController();

            scope.showEventList('noquotes');

            expect(scope.activeEvent).toEqual({
                type: 'noquotes',
                title: 'No Quotes Examples',
                className: 'amber'
            });
        });
    });

    describe('#getExamples', function() {
        it('should set a loading indicator if events are being retrieved', function() {
            spyOn(defaultMocks.apiHealthService, 'getExamples').and.callThrough();

            createController();
            scope.getExamples('test', 'testevent');

            expect(scope.examples.testevent.isClicked).toBeDefined();
            expect(scope.examples.testevent.isClicked).toEqual(true);
        });

        it('should call apiHealthService to get a list of examples', function() {
            spyOn(defaultMocks.apiHealthService, 'getExamples').and.callThrough();

            createController();
            scope.getExamples('sometype', 'newevent', { blah: true });

            expect(defaultMocks.apiHealthService.getExamples).toHaveBeenCalledWith(jasmine.objectContaining({
                website_id: 'abcd',
                type: 'sometype',
                blah: true
            }));
        });

        it('should populate list of examples into the cache model', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.apiHealthService, 'getExamples').and.returnValue(dfr.promise);

            createController();
            dfr.resolve({
                data: ['test']
            });

            scope.getExamples('failures', 'newevent');
            scope.$digest();

            expect(scope.examples.newevent.data).toEqual(['test']);
        });

        it('should call apiHealthService again if the cache model already exists', function() {
            spyOn(defaultMocks.apiHealthService, 'getExamples').and.callThrough();

            createController();

            scope.examples = {
                event1: {
                    data: ['test']
                }
            };

            scope.getExamples('failures', 'event1', { blah: true });

            expect(defaultMocks.apiHealthService.getExamples).not.toHaveBeenCalled();
        });

        it('should set error indicator if examples fail to be retrieved', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.apiHealthService, 'getExamples').and.returnValue(dfr.promise);

            createController();
            dfr.reject();

            scope.getExamples('failures', 'newevent');
            scope.$digest();

            expect(scope.examples.newevent.data).toEqual('error');
        });

        it('should retry calling apiHealthService to get a list of examples if previous attempt failed', function() {
            spyOn(defaultMocks.apiHealthService, 'getExamples').and.callThrough();

            createController();

            scope.examples = {
                event1: {
                    data: 'error'
                }
            };

            scope.getExamples('failures', 'event1');

            expect(defaultMocks.apiHealthService.getExamples).toHaveBeenCalled();
        });
    });

    describe('#redirectTest', function() {
        var fakeData;
        var fakeDepartDate = window.moment().subtract(1, 'month');
        var fakeDepartDateUTC = fakeDepartDate.utc().format();

        beforeEach(function() {
            fakeData = {
                origin: {
                    iata_id: 'SIN'
                },
                destination: {
                    iata_id: 'PVG'
                },
                depart_datetime: fakeDepartDateUTC,
                passenger_configuration: {
                    adult: 2,
                    children: 1
                },
                cabin_class: 'economy',
                data_center: 'uk1',
                user_country_id: 'sg'
            };
        });

        it('should redirect to tests page on a new page', function() {
            spyOn(defaultMocks.window, 'open');

            var fakeReturnDate = window.moment().subtract(20, 'days');
            fakeData.return_datetime = fakeReturnDate.utc().format();

            createController();

            scope.activeEvent = {
                type: 'failures'
            };
            scope.redirectTest(fakeData);

            var expectedDepartDate = fakeDepartDate.format('YYYYMMDD');
            var expectedReturnDate = fakeReturnDate.format('YYYYMMDD');
            var expectedUrl = '#/tests/transport/flights/SIN/PVG/' + expectedDepartDate + '/' + expectedReturnDate + '?adults=2&children=1&infants=0&cabinclass=economy&datacenter=uk1&ucy=sg&lang=en-gb&ccy=sgd&website_id=abcd';

            expect(defaultMocks.window.open.calls.mostRecent().args[0]).toEqual(expectedUrl);
            expect(defaultMocks.window.open.calls.mostRecent().args[1]).toEqual('_blank');
        });

        it('should not pass return date in the URL if it is a one-way flight test', function() {
            spyOn(defaultMocks.window, 'open');

            createController();

            scope.activeEvent = {
                type: 'failures'
            };
            scope.redirectTest(fakeData);

            var expectedDepartDate = fakeDepartDate.format('YYYYMMDD');
            var expectedUrl = '#/tests/transport/flights/SIN/PVG/' + expectedDepartDate + '?adults=2&children=1&infants=0&cabinclass=economy&datacenter=uk1&ucy=sg&lang=en-gb&ccy=sgd&website_id=abcd';

            expect(defaultMocks.window.open.calls.mostRecent().args[0]).toEqual(expectedUrl);
        });
    });
});
