(function() {

'use strict';

angular.module('fcApp.apiHealth').controller('LiveUpdatesCtrl', function(
    $scope,
    $window,
    $analytics,
    moment,
    _,
    localStorageService,
    apiChartService,
    apiHealthService,
    mixpanelService
    ) {

    var selectedWebsiteId = localStorageService.get('selectedwebsite') || '';
    var webConfig = localStorageService.get('webconfig') || {};

    function init() {
        var chart = apiChartService.getChart({
            el: '#api-chart',
            colors: ['#5cb85c', '#d9534f', '#f0ad4e'],
            xLabel: 'Local time',
            yLabel: 'Number of API calls'
        });

        $scope.examples = {};
        $scope.feedback = false;

        $scope.summaryPromise = apiHealthService.getSummaryTimeSeries({
            website_id: selectedWebsiteId
        }).then(function(data) {
            $scope.dictionary = data.field_names || {};
            $scope.dataLen = data.time_series.length - 1;

            $scope.summaries = formatSummaryTotals(data.summary.totals);
            $scope.events = formatEventList(data.summary);

            var groups = [{
                type: 'success',
                name: 'Success'
            }, {
                type: 'failure',
                name: 'Failure'
            }, {
                type: 'noquote',
                name: 'No Quotes'
            }];

            var chartData = apiChartService.getChartData(data.time_series, groups);

            apiChartService.updateChart({
                data: chartData,
                chart: chart
            });

            mixpanelTracking({
                action: 'Chart Loaded'
            });
        },

        function() {
            $scope.feedback = true;
        });
    }

    function formatEventList(data) {
        var eventsList = [];
        var eventsDictionary = ['error', 'noquotes'];

        _.each(eventsDictionary, function(str) {
            var breakdownStr = str + '_breakdown';
            var totalEvents = _.sum(data[breakdownStr], 'value');

            if (_.isArray(data[breakdownStr]) && data[breakdownStr].length) {
                eventsList = eventsList.concat(
                    _.map(data[breakdownStr], function(d, index) {
                        d.category = str === 'error' ? 'failures' : 'noquotes';
                        d.id = str + index;

                        if (str === 'error') {
                            d.percentage = (d.value / totalEvents) * 100;
                        }

                        return d;
                    }));
            }
        });

        return eventsList;
    }

    function formatSummaryTotals(data) {
        var summaryTotals = {};

        var totalNumber = _.find(data, { type: 'total' }).value || 1;

        _.forEach(data, function(d) {
            var pct = (d.value / totalNumber) * 100;

            summaryTotals[d.type] = {};
            summaryTotals[d.type].total = d.value;
            summaryTotals[d.type].percentage = pct;
            summaryTotals[d.type].status = apiHealthService.getOverallEventStatus(d.type, pct);
        });

        return summaryTotals;
    }

    function mixpanelTracking(trackingData) {
        mixpanelService.track('FC APIHealth LiveUpdates', trackingData);
    }

    function analyticsTracking(action) {
        $analytics.eventTrack(action, {
            category: selectedWebsiteId,
            label: localStorageService.get('username')
        });
    }

    $scope.showEventList = function(type) {
        $scope.activeEvent = {
            type: type,
            title: type === 'failures' ? 'Failures Details' : 'No Quotes Examples',
            className: type === 'failures' ? 'red' : 'amber'
        };

        mixpanelTracking({
            action: 'Show Breakdown',
            type: type
        });
    };

    $scope.getExamples = function(category, id, params) {
        if ($scope.examples[id] && $scope.examples[id].data) {
            $scope.examples[id].isShown = !$scope.examples[id].isShown;

            if (_.isArray($scope.examples[id].data) || !$scope.examples[id].isShown) {
                return;
            }
        }

        $scope.examples[id] = {};
        $scope.examples[id].isClicked = true;

        var now = moment();
        var pastDay = moment().subtract(24, 'hour');

        var options = {
            website_id: selectedWebsiteId,
            start_datetime: pastDay.unix(),
            end_datetime: now.unix(),
            size: 10,
            type: category || 'failures'
        };

        if (params) {
            options = _.extend(options, params);
        }

        apiHealthService.getExamples(options).then(function(res) {
            $scope.examples[id].data = res.data;
        },

        function() {
            $scope.examples[id].data = 'error';
        })
        .finally(function() {
            $scope.examples[id].isShown = true;
            $scope.examples[id].isClicked = false;
        });

        mixpanelTracking({
            action: 'View Examples',
            type: category,
            rowNumber: (function(rowId) {
                var index = rowId.match(/\d+$/) || '';
                if (index.length) {
                    return parseInt(index, 10) + 1;
                }
            }(id))
        });
    };

    $scope.redirectTest = function(searchObject, eventName) {
        var url = '#/tests/transport/flights/';
        var dateFormat = 'YYYYMMDD';
        var queryStrings = '?';

        var routes = [searchObject.origin.iata_id, searchObject.destination.iata_id, moment(searchObject.depart_datetime).format(dateFormat)];

        if (searchObject.return_datetime) {
            routes.push(moment(searchObject.return_datetime).format(dateFormat));
        }

        var parameters = {
            adults: searchObject.passenger_configuration.adult || 0,
            children: searchObject.passenger_configuration.children || 0,
            infants: searchObject.passenger_configuration.infant || 0,
            cabinclass: searchObject.cabin_class || '',
            datacenter: searchObject.data_center || '',
            ucy: searchObject.user_country_id || '',
            lang: webConfig.lang || '',
            ccy: webConfig.currency || '',
            website_id: selectedWebsiteId
        };

        queryStrings += Object.keys(parameters).map(function(key) {
            return [key, parameters[key]].map(encodeURIComponent).join('=');
        }).join('&');

        url += routes.join('/') + queryStrings;

        $window.open(url, '_blank');

        mixpanelTracking({
            action: 'Replay Search',
            origin: searchObject.origin.iata_id,
            destination: searchObject.destination.iata_id,
            type: $scope.activeEvent.type,
            eventName: eventName
        });

        analyticsTracking('ReplaySearch');
    };

    init();
});

}());
