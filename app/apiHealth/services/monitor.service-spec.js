(function() {

'use strict';

describe('Service: monitorService', function() {

    var monitorService;

    beforeEach(module('fcApp.apiHealth', function($provide) {
        $provide.value('mixpanelService', {});
    }));

    beforeEach(inject(function(_monitorService_) {
        monitorService = _monitorService_;
    }));

    it('should do something', function() {
        expect(!!monitorService).toBe(true);
    });

});

}());
