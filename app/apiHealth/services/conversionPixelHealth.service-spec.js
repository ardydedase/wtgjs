(function() {

'use strict';

describe('Service: conversionPixelHealthService', function() {

    var mockPollerService;
    var conversionPixelHealthService;
    var q;
    var rootScope;
    var baseUrl;
    var mockData;

    beforeEach(module('fcApp.apiHealth', function($provide) {
        mockPollerService = {
            start: function() {
                var dfr = q.defer();
                return dfr.promise;
            },

            poll: function() {
                var dfr = q.defer();
                return dfr;
            }
        };

        $provide.value('pollerService', mockPollerService);
        $provide.value('_', window._);
        $provide.value('moment', window.moment);
        $provide.value('c3', window.c3);
    }));

    beforeEach(inject(function(_conversionPixelHealthService_, $rootScope, $q, _config_) {
        conversionPixelHealthService = _conversionPixelHealthService_;
        rootScope = $rootScope.$new();
        q = $q;

        baseUrl = _config_.API_URL + _config_.API_BASE;

        mockData = {
            successes: 100,
            failures: 1,
            time_series: [{
                time: '04:00'
            }, {
                time: '05:00'
            }, {
                time: '06:00'
            }]
        };
    }));

    describe('#getData', function() {
        it('should be defined', function() {
            expect(conversionPixelHealthService.getData).toBeDefined();
        });

        it('should return a promise', function() {
            var startDfr = q.defer();
            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);

            conversionPixelHealthService.getData('aaaa').then(
                function() {},

                function(res) {
                    expect(res).toBeDefined();
                });

            startDfr.reject('test');
            rootScope.$apply();
        });

        it('should call pollerService.start to get taskId', function() {
            spyOn(mockPollerService, 'start').and.callThrough();

            conversionPixelHealthService.getData('aaaa');

            expect(mockPollerService.start).toHaveBeenCalledWith({
                method: 'post',
                url: 'v0.4/flights/api_health/conversions/actions/async_checks/'
            });
        });

        it('should resolve failure if pollerService.start returns a failure', function() {
            var startDfr = q.defer();
            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);

            conversionPixelHealthService.getData('aaaa').then(
                function() {},

                function(err) {
                    expect(err).toEqual('Nooo!');
                });

            startDfr.reject('Nooo!');
            rootScope.$apply();
        });

        it('should call pollerService.poll if taskId is returned', function() {
            var startDfr = q.defer();
            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'poll').and.callThrough();

            conversionPixelHealthService.getData('aaaa');

            startDfr.resolve('uuuuuuuid');
            rootScope.$apply();

            expect(mockPollerService.poll).toHaveBeenCalledWith(jasmine.objectContaining({
                taskId: 'uuuuuuuid',
                url: 'v0.4/flights/api_health/conversions/actions/async_checks'
            }));
        });

        it('should resolve failure if pollerService.starts does not return taskId', function() {
            var startDfr = q.defer();
            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);

            conversionPixelHealthService.getData('aaaa').then(
                function() {},

                function() {
                    expect(true).toEqual(true); //nothing to assert, just make sure reject goes through
                });

            startDfr.resolve();
            rootScope.$apply();
        });

        it('should call poller.stop if polling is done', function() {
            var startDfr = q.defer();
            var pollDfr = q.defer();

            pollDfr.stop = function() {};

            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'poll').and.returnValue(pollDfr);
            spyOn(pollDfr, 'stop');

            conversionPixelHealthService.getData('aaaa');

            startDfr.resolve('uuuuuuuid');
            rootScope.$apply();

            pollDfr.notify({ data: { ready: true }});
            rootScope.$apply();

            expect(pollDfr.stop).toHaveBeenCalled();
        });

        it('should resolve result if polling state is SUCCESS', function() {
            var startDfr = q.defer();
            var pollDfr = q.defer();
            pollDfr.stop = function() {};

            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'poll').and.returnValue(pollDfr);

            conversionPixelHealthService.getData('aaaa').then(function(res) {
                expect(res).toEqual('hello');
            });

            startDfr.resolve('uuuuuuuid');
            rootScope.$apply();

            pollDfr.notify({ data: {
                ready: true,
                state: 'SUCCESS',
                result: 'hello'
            }});
            rootScope.$apply();
        });

        it('should reject result if polling state is anything but SUCCESS', function() {
            var startDfr = q.defer();
            var pollDfr = q.defer();
            pollDfr.stop = function() {};

            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'poll').and.returnValue(pollDfr);

            conversionPixelHealthService.getData('aaaa').then(
                function() {},

                function(err) {
                    expect(err).toEqual('ANYTHING BUT SUCCESS');
                });

            startDfr.resolve('uuuuuuuid');
            rootScope.$apply();

            pollDfr.notify({ data: {
                ready: true,
                state: 'ANYTHING BUT SUCCESS'
            }});
            rootScope.$apply();
        });
    });

    describe('#formatData', function() {
        it('should be defined', function() {
            expect(conversionPixelHealthService.formatData).toBeDefined();
        });

        it('should return the same object (no deep comparison)', function() {
            var result = conversionPixelHealthService.formatData(mockData);

            expect(result).toEqual(mockData);
        });

        it('should return the same object with time series data formatted to local time', function() {
            var utcOffsetInHours = window.moment().utcOffset() / 60;

            function getLocalTime(time) {
                var local = window.moment(time, 'HH:mm').add(utcOffsetInHours, 'hours').hours();
                return (local < 10 ? ('0' + local) : local) + ':00';
            }

            var result = conversionPixelHealthService.formatData(mockData);

            expect(result.time_series[0].time).toEqual(getLocalTime('04:00'));
            expect(result.time_series[1].time).toEqual(getLocalTime('05:00'));
            expect(result.time_series[2].time).toEqual(getLocalTime('06:00'));
        });

        it('should return the same object with time series data appended with an index', function() {
            var result = conversionPixelHealthService.formatData(mockData);

            expect(result.time_series[0].index).toBeDefined();
            expect(result.time_series[0].index).toEqual(0);
            expect(result.time_series[1].index).toBeDefined();
            expect(result.time_series[1].index).toEqual(1);
            expect(result.time_series[2].index).toBeDefined();
            expect(result.time_series[2].index).toEqual(2);
        });
    });

    describe('#getChartData', function() {
        it('should be defined', function() {
            expect(conversionPixelHealthService.getChartData).toBeDefined();
        });

        it('should return an object', function() {
            var result = conversionPixelHealthService.getChartData(mockData);

            expect(result.data).toBeDefined();
            expect(result.xAxisLabels).toBeDefined();
        });

        it('should return an array of Successes and Failures', function() {
            var result = conversionPixelHealthService.getChartData(mockData);

            expect(result.data[0].length).toEqual(4);
            expect(result.data[1].length).toEqual(4);
            expect(result.data[0][0]).toEqual('Successes');
            expect(result.data[1][0]).toEqual('Failures');
        });
    });

    describe('#getFailures', function() {
        beforeEach(function() {
            $.extend(mockData, {
                error_types: ['sg', 'uk', 'fr', 'vn'],
                field_names: {
                    sg: 'singapore',
                    uk: 'unitedkingdom',
                    fr: 'france',
                    vn: 'vietnam',
                    yaml: 'YAML',
                    haml: 'HAML',
                    error_dt: 'error time'
                },
                time_series: [{
                    time: '04:00',
                    index: 0,
                    failure_events: [{
                        sg: 1,
                        uk: 0,
                        fr: 0,
                        vn: 0,
                        yaml: '100',
                        haml: '',
                        error_dt: '2015-01-01T04:35:18'
                    }]
                }, {
                    time: '05:00',
                    index: 1,
                    failure_events: [{
                        sg: 0,
                        uk: 1,
                        fr: 1,
                        vn: 0,
                        yaml: '-1',
                        haml: '',
                        error_dt: '2015-01-01T05:12:31'
                    }]
                }, {
                    time: '06:00',
                    index: 2,
                    failure_events: [{
                        sg: 0,
                        uk: 0,
                        fr: 1,
                        vn: 1,
                        yaml: '',
                        haml: '1000',
                        error_dt: '2015-01-01T06:17:07'
                    }]
                }]
            });
        });

        it('should be defined', function() {
            expect(conversionPixelHealthService.getFailures).toBeDefined();
        });

        it('should return an array', function() {
            var result = conversionPixelHealthService.getFailures(mockData);

            expect(Array.isArray(result)).toEqual(true);
        });

        it('should return an object within each array item', function() {
            var result = conversionPixelHealthService.getFailures(mockData);

            expect(result[0].time).toBeDefined();
            expect(result[0].index).toBeDefined();
            expect(result[0].failures).toBeDefined();
        });

        it('should reverse the time_series array order', function() {
            var result = conversionPixelHealthService.getFailures(mockData);

            expect(result[0].time).toEqual('06:00');
            expect(result[1].time).toEqual('05:00');
            expect(result[2].time).toEqual('04:00');
        });

        it('should return copy the index value from the array object', function() {
            var result = conversionPixelHealthService.getFailures(mockData);

            expect(result[0].index).toEqual(2);
            expect(result[1].index).toEqual(1);
            expect(result[2].index).toEqual(0);
        });

        it('should return a failure array containing object with bookingData and reasons', function() {
            var result = conversionPixelHealthService.getFailures(mockData);

            expect(result[0].failures).toBeDefined();
            expect(result[0].failures[0].bookingData).toBeDefined();
            expect(result[0].failures[0].reasons).toBeDefined();
        });

        it('should return a list an array of failure reasons', function() {
            var result = conversionPixelHealthService.getFailures(mockData);

            expect(result[0].failures[0].reasons.length).toEqual(2);
            expect(result[0].failures[0].reasons).toContain('vietnam');
            expect(result[0].failures[0].reasons).toContain('france');
        });

        it('should return readable list of booking data', function() {
            var result = conversionPixelHealthService.getFailures(mockData);

            expect(result[0].failures[0].bookingData['error time']).toBeDefined();
            expect(result[0].failures[0].bookingData.YAML).toEqual('<empty>');
            expect(result[0].failures[0].bookingData.HAML).toEqual('1000');
        });
    });

    describe('#getChart', function() {
        it('should be defined', function() {
            expect(conversionPixelHealthService.getChart).toBeDefined();
        });

        it('should call c3.generate with empty chart data', function() {
            var fakeElement = '#fake';

            spyOn(window.c3, 'generate');
            conversionPixelHealthService.getChart(fakeElement);

            expect(window.c3.generate).toHaveBeenCalledWith(jasmine.objectContaining({
                bindto: fakeElement,
                data: {
                    columns: [],
                    groups: []
                }
            }));
        });
    });

    describe('#updateChart', function() {
        it('should be defined', function() {
            expect(conversionPixelHealthService.updateChart).toBeDefined();
        });

        it('should call c3.load with chart data', function() {
            var fakeChart = window.c3.generate({
                data: {
                    columns: [],
                    groups: [],
                    types: {}
                }
            });

            var fakeOptions = {
                data: {
                    data: ['a', 'b'],
                    xAxisLabels: {}
                },
                chart: fakeChart
            };

            spyOn(fakeChart, 'load');

            conversionPixelHealthService.updateChart(fakeOptions);

            expect(fakeChart.load).toHaveBeenCalledWith(jasmine.objectContaining({
                columns: fakeOptions.data.data,
                categories: fakeOptions.data.xAxisLabels
            }));
        });
    });

});

}());
