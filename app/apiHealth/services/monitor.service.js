(function() {

'use strict';

angular.module('fcApp.apiHealth').factory('monitorService', function(
    $http,
    $analytics,
    $location,
    alertService,
    localStorageService,
    authService,
    mixpanelService,
    config) {

    //PE-791
    function resolveFakeWebsiteId() {
        var currentWebsiteId = localStorageService.get('selectedwebsite');
        return currentWebsiteId === 'a001' ? 'aljp' : currentWebsiteId;
    }

    //--PE-791

    return {
        getDeeplinkFailures: function() {
            return $http.get(config.API_URL + config.API_BASE + '/monitor/deeplink/failures/' +

                // + localStorageService.get('selectedwebsite')
                // PE-791
                resolveFakeWebsiteId() +

                // --PE-791
                '/?' + $.param({
                    // website_id: localStorageService.get('selectedwebsite'),
                    // PE-791
                    website_id: resolveFakeWebsiteId()

                    // --PE-791
                }))
                .then(function(response) {
                    $analytics.eventTrack('loadedDeeplinkErrors-' + $location.path(), {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username
                    });

                    mixpanelService.track('FlightsConnect Loaded Deeplink Stats');

                    return response.data;
                });
        },

        getDeeplinkErrors: function() {
            return $http.get(config.API_URL + config.API_BASE + '/monitor/deeplink/errors/' +

                // + localStorageService.get('selectedwebsite')
                // PE-791
                resolveFakeWebsiteId() +

                // --PE-791
                '/?' + $.param({
                    // website_id: localStorageService.get('selectedwebsite'),
                    // PE-791
                    website_id: resolveFakeWebsiteId(),

                    // --PE-791
                }))
                .then(function(response) {
                    $analytics.eventTrack('loadedDeeplinkErrors-' + $location.path(), {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username
                    });

                    mixpanelService.track('FlightsConnect Loaded Deeplink Stats');

                    return response.data;
                });
        }
    };

});

}());
