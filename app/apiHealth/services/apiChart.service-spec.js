(function() {

'use strict';

describe('Service: apiChartService', function() {

    var apiChartService;

    beforeEach(module('fcApp.apiHealth', function($provide) {
        $provide.value('moment', window.moment);
        $provide.value('_', window._);
        $provide.value('c3', window.c3);
    }));

    beforeEach(inject(function(_apiChartService_) {
        apiChartService = _apiChartService_;
    }));

    describe('#getChart', function() {
        it('should be defined', function() {
            expect(apiChartService.getChart).toBeDefined();
        });

        it('should call c3.generate to return a chart with empty data', function() {
            var fakeElement = '#fake';

            spyOn(window.c3, 'generate');
            apiChartService.getChart({
                el: fakeElement
            });

            expect(window.c3.generate).toHaveBeenCalledWith(jasmine.objectContaining({
                bindto: fakeElement,
                data: {
                    columns: [],
                    groups: []
                }
            }));
        });
    });

    describe('#getChartData', function() {
        it('should be defined', function() {
            expect(apiChartService.getChartData).toBeDefined();
        });

        it('should return an object containing chart-ready column data and labels for the X axis', function() {
            var data = [{
                totals: [
                    { type: 'orchard', value: 203 },
                    { type: 'tanglin', value: 11 },
                    { type: 'grange', value: 3 }
                ],
                time: '2015-06-06T06:00:00'
            }, {
                totals: [
                    { type: 'orchard', value: 124 },
                    { type: 'tanglin', value: 16 },
                    { type: 'grange', value: 14 }
                ],
                time: '2015-06-06T07:00:00'
            }, {
                totals: [
                    { type: 'orchard', value: 441 },
                    { type: 'tanglin', value: 18 },
                    { type: 'grange', value: 17 }
                ],
                time: '2015-06-06T08:00:00'
            }];

            var groups = [{
                type: 'tanglin',
                name: 'Tanglin Rd'
            }, {
                type: 'orchard',
                name: 'Orchard Rd'
            }, {
                type: 'grange',
                name: 'Grange Rd'
            }];

            var utcOffsetInHours = window.moment().utcOffset() / 60;
            var expectedLabels = window._.pluck(data, 'time').map(function(t) {
                return window.moment.utc(t).add(utcOffsetInHours, 'hours').format('HH:mm');
            });

            var expectedChartData = [
                ['Tanglin Rd', 11, 16, 18],
                ['Orchard Rd', 203, 124, 441],
                ['Grange Rd', 3, 14, 17]
            ];

            var result = apiChartService.getChartData(data, groups);

            expect(result.xAxisLabels).toEqual(expectedLabels);
            expect(result.data).toEqual(expectedChartData);
        });
    });

    describe('#updateChart', function() {
        it('should be defined', function() {
            expect(apiChartService.updateChart).toBeDefined();
        });

        it('should call c3.load to update the chart with data', function() {
            var fakeChart = window.c3.generate({
                data: {
                    columns: [],
                    groups: [],
                    types: {}
                }
            });

            var fakeData = {
                chart: fakeChart,
                data: {
                    data: [['orchard', 1], ['tanglin', 2]],
                    xAxisLabels: ['time', 100, 200]
                }
            };

            spyOn(fakeChart, 'load');
            apiChartService.updateChart(fakeData);

            expect(fakeChart.load).toHaveBeenCalledWith(jasmine.objectContaining({
                columns: fakeData.data.data,
                categories: fakeData.data.xAxisLabels
            }));
        });
    });
});

}());
