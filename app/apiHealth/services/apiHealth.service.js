(function() {

'use strict';

angular.module('fcApp.apiHealth').factory('apiHealthService', function(
    $http,
    config
    ) {

    var baseUrl = config.API_URL + config.API_BASE + '/v0.4/flights/api_health/live_updates/';

    function _normalizeWebsiteId(options) {
        options.website_id = (options.website_id === 'a001') ? 'aljp' : options.website_id;

        return options;
    }

    /**
     * Get summary level and time series data for api health in the past 24 hours
     * @param  {String} options.website_id - REQUIRED
     * @return {Object} {"time_series": [{"totals": [{"type": "success", "value": 190 }, {"type": "noquote", "value": 6 }, {"type": "failure", "value": 0 } ], "error_breakdown": [], "time": "2015-06-06T05:00:00"}], "field_names": {"noquotes": {"title": "No Quotes", description: "Long description"} }, "summary": {"noquotes_breakdown": [{"origin": {"code": "PAR", "routenode_id": 6073, "iata_id": "PAR", "name": "Paris"}, "destination": {"code": "JNX", "routenode_id": 12791, "iata_id": "JNX", "name": "Naxos"}, "value": 7 } ], "error_breakdown": [{"type": "connectiontimeout", "value": 74 } ], "totals": [{"type": "success", "value": 32536 }, {"type": "noquotes", "value": 1357 }, {"type": "failure", "value": 77 }, {"type": "total", "value": 33970 } ] } }
    */
    function getSummaryTimeSeries(options) {
        return $http({
            method: 'get',
            url: baseUrl,
            params: _normalizeWebsiteId(options)
        }).then(function(res) {
            return res.data;
        });
    }

    /**
     * Get list of examples of failure/noquote event
     *  Currently only returns 10 with no options to paginate
     * @param  {String} options.type  "failures" or "noquotes" - REQUIRED
     * @param  {String} options.website_id - REQUIRED
     * @return {Object}
     *   Example [1] - FAILURE:
     * {"field_names": {"connectiontimeout": {"description": "Long description", "title": "Connection Timeout"}, "failure": {"title": "Failure"}, "total": {"title": "Total"}, "noquote": {"title": "No Quote"}, "success": {"title": "Success"} }, "data": [{"origin": {"code": "BSL", "routenode_id": 10158, "iata_id": "BSL", "name": "Basel Mulhouse Freiburg"}, "destination": {"code": "FAO", "routenode_id": 11469, "iata_id": "FAO", "name": "Faro"}, "timestamp": "2015-06-06T10:26:50.673+0000", "passenger_configuration": {"infant": 0, "adult": 1, "children": 0 }, "return_datetime": "2015-06-17T00:00:00", "data_center": "sg1", "cabin_class": "economy", "error_type": "connectiontimeout", "user_country_id": "FR", "depart_datetime": "2015-06-13T00:00:00"} ] }
     *
     * Example [2] - No Quote:
     * {"field_names": {"failure": {"title": "Failure"}, "total": {"title": "Total"}, "noquote": {"title": "No Quote"}, "success": {"title": "Success"} }, "data": [{"origin": {"code": "NCE", "routenode_id": 14581, "iata_id": "NCE", "name": "Nice"}, "destination": {"code": "RIO", "routenode_id": 6721, "iata_id": "RIO", "name": "Rio De Janeiro"}, "timestamp": "2015-06-06T09:39:15.271+0000", "passenger_configuration": {"infant": 0, "adult": 1, "children": 0 }, "return_datetime": "2016-05-11T00:00:00", "data_center": "sg1", "cabin_class": "economy", "error_type": "", "user_country_id": "FR", "depart_datetime": "2016-01-07T00:00:00"} ] }
     */
    function getExamples(options) {
        var url = baseUrl + options.type + '/';

        return $http({
            method: 'get',
            url: url,
            params: _normalizeWebsiteId(options)
        }).then(function(res) {
            return res.data;
        });
    }

    /**
     * Return the string of "red", "amber", or "red" to indicate the overall status of
     *  of each success, failure, or noquotes event for the API performance
     * @param  {String} type    "success", "failure", "noquotes" - REQUIRED
     * @param  {Integer} number The percentage number of the status type - REQUIRED
     * @return {String}  "red", "amber", "green"
     */
    function getOverallEventStatus(type, number) {
        if (type === 'success') {
            if (number > 90) {
                return 'green';
            }

            if (number >= 70 && number <= 90) {
                return 'amber';
            }

            if (number < 70) {
                return 'red';
            }
        }

        if (type === 'failure') {
            if (number < 5) {
                return 'green';
            }

            if (number >= 5 && number <= 20) {
                return 'amber';
            }

            if (number > 20) {
                return 'red';
            }
        }

        if (type === 'noquotes') {
            if (number < 15) {
                return 'green';
            }

            if (number >= 15 && number <= 35) {
                return 'amber';
            }

            if (number > 35) {
                return 'red';
            }
        }
    }

    return {
        getSummaryTimeSeries: getSummaryTimeSeries,
        getExamples: getExamples,
        getOverallEventStatus: getOverallEventStatus
    };
});

}());
