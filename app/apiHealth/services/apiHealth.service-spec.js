(function() {

'use strict';

describe('Service: apiHealthService', function() {

    var config;
    var apiHealthService;
    var httpBackend;
    var baseUrl;

    beforeEach(module('fcApp.apiHealth'));

    beforeEach(inject(function(_apiHealthService_, _config_, $httpBackend) {
        config = _config_;
        apiHealthService = _apiHealthService_;
        httpBackend = $httpBackend;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#getSummaryTimeSeries', function() {
        it('should be defined', function() {
            expect(apiHealthService.getSummaryTimeSeries).toBeDefined();
        });

        it('should GET /v0.4/flights/api_health/live_updates/', function() {
            var options = {
                website_id: 'aaaa'
            };

            var url = baseUrl + '/v0.4/flights/api_health/live_updates/?website_id=aaaa';

            httpBackend.whenGET(url).respond({});

            apiHealthService.getSummaryTimeSeries(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });
    });

    describe('#getExamples', function() {
        it('should be defined', function() {
            expect(apiHealthService.getExamples).toBeDefined();
        });

        it('should GET /v0.4/flights/api_health/live_updates/[TYPE]/', function() {
            var options = {
                type: 'failures',
                website_id: 'aaaa',
                error_type: 'somethingbroken'
            };

            var url = baseUrl + '/v0.4/flights/api_health/live_updates/failures/?error_type=somethingbroken&type=failures&website_id=aaaa';

            httpBackend.whenGET(url).respond({});

            apiHealthService.getExamples(options).then(function() {
                expect(true).toEqual(true);
            });

            httpBackend.flush();
        });
    });

    describe('#getOverallEventStatus', function() {
        it('should be defined', function() {
            expect(apiHealthService.getOverallEventStatus).toBeDefined();
        });

        function shouldPassWithTheseCombinations(testCase) {
            it('should return ' + testCase.expectedStatus + ' if type is ' + testCase.type + ' and value is ' + testCase.percentage, function() {
                expect(apiHealthService.getOverallEventStatus(testCase.type, testCase.percentage)).toEqual(testCase.expectedStatus);
            });
        }

        var testCases = [{
            type: 'success',
            percentage: 95,
            expectedStatus: 'green'
        }, {
            type: 'success',
            percentage: 90,
            expectedStatus: 'amber'
        }, {
            type: 'success',
            percentage: 70,
            expectedStatus: 'amber'
        }, {
            type: 'success',
            percentage: 15,
            expectedStatus: 'red'
        }, {
            type: 'failure',
            percentage: 4,
            expectedStatus: 'green'
        }, {
            type: 'failure',
            percentage: 20,
            expectedStatus: 'amber'
        }, {
            type: 'failure',
            percentage: 21,
            expectedStatus: 'red'
        }, {
            type: 'noquotes',
            percentage: 14,
            expectedStatus: 'green'
        }, {
            type: 'noquotes',
            percentage: 35,
            expectedStatus: 'amber'
        }, {
            type: 'noquotes',
            percentage: 36,
            expectedStatus: 'red'
        }];

        angular.forEach(testCases, shouldPassWithTheseCombinations);
    });
});

}());
