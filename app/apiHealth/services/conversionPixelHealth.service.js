(function() {

'use strict';

angular.module('fcApp.apiHealth').factory('conversionPixelHealthService', function(
    $q,
    _,
    c3,
    moment,
    pollerService
    ) {

    var successDataSeriesName = 'Successes';
    var failureDataSeriesName = 'Failures';

    function isKeyAnError(errors, key) {
        return _.some(errors, function(err) {
            return err === key;
        });
    }

    function getFormattedBookingDataValue(key, value) {
        if (value === '') {
            return '<empty>';
        }

        if (key === 'error_dt') {
            return moment.utc(value).local().format('HH:mm:ss');
        }

        return value;
    }

    function formatFailure(data, errors, fieldNames) {
        var failures = [];

        _.forEach(data, function(d) {
            var failure = {
                bookingData: {},
                reasons: []
            };

            _.forOwn(d, function(value, key) {
                var fieldName = fieldNames[key];

                if (isKeyAnError(errors, key)) {
                    if (value === 1) {
                        failure.reasons.push(fieldName);
                    }
                } else {
                    failure.bookingData[fieldName] = getFormattedBookingDataValue(key, value);
                }
            });

            failures.push(failure);
        });

        return failures;
    }

    // was previously getConversionPixelHealthData from dataService
    // temporarily housing this code here, should reside within apiChart
    // requires refactoring
    function getData() {
        var dfr = $q.defer();
        var url = 'v0.4/flights/api_health/conversions/actions/async_checks';

        pollerService.start({
            method: 'post',
            url: url + '/'
        })
        .then(function(taskId) {
            var conversionPoller;

            if (taskId) {
                conversionPoller = pollerService.poll({
                    taskId: taskId,
                    url: url,
                    delay: 3000
                });

                conversionPoller.promise.then(null, null, function(res) {
                    if (conversionPoller) {
                        if (res.data.ready) {
                            conversionPoller.stop();

                            // a hack to ditch the poller; bug with this block getting registered multiple times when run in one view session
                            conversionPoller = null;

                            if (res.data.state === 'SUCCESS') {
                                dfr.resolve(res.data.result);
                            } else {
                                dfr.reject(res.data.state);
                            }
                        }
                    }
                });
            } else {
                dfr.reject();
            }
        },

        function(err) {
            dfr.reject(err);
        });

        return dfr.promise;
    }

    function formatData(data) {
        var timeSeries = data.time_series || [];
        var utcOffsetInHours = moment().utcOffset() / 60;

        _.mapValues(timeSeries, function(n, index) {
            var utcTime = n.time;
            var localTime = moment(utcTime, 'HH:mm').add(utcOffsetInHours, 'hours').hours();

            var formattedLocalTime = (localTime < 10 ? ('0' + localTime) : localTime) + ':00';

            n.time = formattedLocalTime;
            n.index = +index;

            return n;
        });

        return data;
    }

    function getChartData(data) {
        var timeSeries = data.time_series;

        var xAxisLabels = _.pluck(timeSeries, 'time');
        var successes = _.pluck(timeSeries, 'successes');
        var failures = _.pluck(timeSeries, 'failures');

        successes.unshift(successDataSeriesName);
        failures.unshift(failureDataSeriesName);

        return {
            data: [successes, failures],
            xAxisLabels: xAxisLabels
        };
    }

    function getChart(el) {
        return c3.generate({
            bindto: el,
            data: {
                columns: [],
                groups: []
            },
            axis: {
                x: {
                    height: 55,
                    type: 'categories',
                    tick: {
                        fit: true,
                        rotate: 45,
                        multiline: false
                    },
                    label: {
                        position: 'outer-right',
                        text: 'Local time'
                    }
                },
                y: {
                    label: {
                        text: 'Number of bookings',
                        position: 'outer-top'
                    }
                }
            },
            grid: {
                focus: {
                    show: false
                }
            },
            legend: {
                position: 'right'
            },
            color: {
                pattern: ['#5cb85c', '#d9534f']
            }
        });
    }

    function updateChart(options) {
        var chartViewType = 'area-spline';
        var types = {};
        var groups = [];
        var data = options.data;

        _.forEach(data.data, function(d) {
            var label = _.first(d);
            types[label] = chartViewType;
            groups.push(label);
        });

        options.chart.groups([groups]);

        options.chart.load({
            columns: data.data,
            types: types,
            categories: data.xAxisLabels
        });
    }

    function getFailures(data) {
        var timeSeries = data.time_series || [];
        var errors = data.error_types || [];
        var fieldNames = data.field_names || {};

        var reversedTimeSeries = _.clone(timeSeries, true).reverse();

        var failuresList = _.map(reversedTimeSeries, function(d) {
            return {
                time: d.time,
                index: d.index,
                failures: formatFailure(d.failure_events, errors, fieldNames)
            };
        });

        return failuresList;
    }

    return {
        getData: getData,
        formatData: formatData,
        getChartData: getChartData,
        getFailures: getFailures,
        getChart: getChart,
        updateChart: updateChart
    };
});

}());
