(function() {

'use strict';

angular.module('fcApp.apiHealth').factory('apiChartService', function(
    moment,
    _,
    c3,
    d3) {

    function _formatLocalTime(data) {
        var utcOffsetInHours = moment().utcOffset() / 60;

        _.mapValues(data, function(n) {
            n.time = moment.utc(n.time).add(utcOffsetInHours, 'hours').format('HH:mm');
            return n;
        });

        return data;
    }

    /**
     * Return a C3 chart with timeseries setting, no data
     * @param  {String} options.el - '#el' - REQUIRED
     * @param  {Array}  options.colors - List of hex colors in order of columns data - REQUIRED
     * @param  {String} options.yLabel - Y axis label
     * @param  {String} options.xLabel - X axis label
     * @return {Object} c3 chart object
    */
    function getChart(options) {
        return c3.generate({
            bindto: options.el,
            data: {
                columns: [],
                groups: []
            },
            transition: {
                duration: 1000
            },
            axis: {
                x: {
                    height: 55,
                    type: 'categories',
                    tick: {
                        fit: true,
                        rotate: 45,
                        multiline: false,
                        centered: true
                    },
                    label: {
                        position: 'outer-right',
                        text: options.xLabel || ''
                    },
                    padding: {
                        left: 0,
                        right: 0
                    }
                },
                y: {
                    label: {
                        text: options.yLabel || '',
                        position: 'outer-top'
                    }
                }
            },
            grid: {
                focus: {
                    show: true
                }
            },
            legend: {
                position: 'right'
            },
            color: {
                pattern: options.colors
            },
            tooltip: {
                format: {
                    value: function(value) {
                        var format = d3.format(',');
                        return format(value);
                    }
                }
            }
        });
    }

    /**
     * Populate and show chart with data
     * @param  {Object} options.chart C3 chart object - REQUIRED
     * @return {Array}  options.data  chart data - REQUIRED
     */
    function updateChart(options) {
        var chartViewType = 'area-spline';
        var types = {};
        var groups = [];
        var data = options.data;

        _.forEach(data.data, function(d) {
            var first = d[0];
            types[first] = chartViewType;
            groups.push(first);
        });

        options.chart.groups([groups]);

        options.chart.load({
            columns: data.data,
            types: types,
            categories: data.xAxisLabels
        });
    }

    /**
     * Returns the data for stacked chart, already grouped according to label.
     * Time is assumed always formatted to local. Accepts UTC format.
     * NOTE: Make sure the labels are ordered and match the actual label
     *  in time series/totals/type. The order must also match the color pattern
     *  order used in getChart().
     *
     * @param  {Array}  data   time_series: [{ time: "utcFormat", totals:[{type: "success", value: 131}] }] - REQUIRED
     * @param  {Array}  groups the labels in desired order - [{type: 'should match type', name: 'chart text'}] - REQUIRED
     * @return {Object} an object in the format of  in the format of
     *   chartData: [['Label1', 1, 2, ...], ['Label2', 10, 20, ...], ...]
     *   xAxisLabels: ['07:00', '08:00']
     */
    function getChartData(data, groups) {
        _formatLocalTime(data);

        var xAxisLabels = [];
        var keys = {};
        var chartData = [];

        _.forEach(groups, function(group) {
            keys[group.type] = [group.name];
        });

        _.forEach(data, function(d) {
            xAxisLabels.push(d.time);

            _.forEach(groups, function(group) {
                var match = _.find(d.totals, { type: group.type }) || {};
                var matchValue = match.value || 0;

                keys[group.type].push(matchValue);
            });
        });

        _.forEach(groups, function(group) {
            chartData.push(keys[group.type]);
        });

        return {
            data: chartData,
            xAxisLabels: xAxisLabels
        };
    }

    return {
        getChart: getChart,
        updateChart: updateChart,
        getChartData: getChartData
    };
});

}());
