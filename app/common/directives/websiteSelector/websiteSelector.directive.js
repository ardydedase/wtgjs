(function() {

'use strict';

angular.module('fcApp.common').directive('fcWebsiteSelector', function() {
    return {
        restrict: 'E',
        templateUrl: 'app/common/directives/websiteSelector/website-selector.html',
        scope: {
            onSelect: '&',
            startEmpty: '@'
        },
        controller: function($scope, $location, localStorageService, webPermissionService, _) {

            var websiteKeys = localStorageService.get('website_keys') || [];
            var selectedWebsiteId = localStorageService.get('selectedwebsite');

            if (websiteKeys.length) {
                $scope.hasWebsites = true;
                setupTypehead(websiteKeys);
            } else {
                getWebsiteList();
            }

            function setupTypehead(list) {
                $scope.dirSelectedWebsite = $scope.startEmpty ? '' : getWebsiteName(list, selectedWebsiteId);
                $scope.dirWebsiteList = list;
            }

            function getWebsiteName(list, id) {
                var website = _.findWhere(list, {id: id});
                var websiteName = website ? website.name_key : '';

                return websiteName;
            }

            function getWebsiteList() {
                $scope.dirDisabled = true;

                var allowedWebsites = _.union(localStorageService.get('websites'), localStorageService.get('commercial_websites'));

                $scope.hasWebsites = allowedWebsites.length ? true : false;

                webPermissionService.getWebsiteKeys(allowedWebsites).then(function(response) {
                    var websiteList = [];

                    angular.forEach(response, function(website, key) {
                        $.extend(website, {
                            id: key,
                            name_key: website.website_name + ' ' + website.home_country + ' (' + key + ')'
                        });

                        websiteList.push(website);
                    });

                    //PE-791
                    if (localStorageService.get('account') && localStorageService.get('account').username === 'miland') {
                        websiteList.push({
                            home_country: 'SG',
                            website_name: 'Fake OTA.com',
                            id:'a001',
                            name_key:'Fake OTA.com (a001)'
                        });
                    }

                    //--PE-791

                    $scope.dirDisabled = false;

                    localStorageService.add('website_keys', websiteList);
                    setupTypehead(websiteList);
                });
            }
        },

        link: function($scope, $element) {
            $element.find('input').on('focus', function() {
                $(this).select();
            });
        }
    };
});

}());
