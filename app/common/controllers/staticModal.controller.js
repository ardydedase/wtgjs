(function() {

'use strict';

angular.module('fcApp.common').controller('StaticModalCtrl', function(
    $scope,
    $modalInstance) {

    $scope.ok = function() {
        $modalInstance.dismiss('cancel');
    };
});

}());
