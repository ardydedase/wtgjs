(function() {

'use strict';

angular.module('fcApp.common').factory('webPermissionService', function(
    $http,
    localStorageService,
    config) {

    var serviceObj = {};

    serviceObj = {
        getWebsiteKeys: function(websiteIds) {
            var postParam = JSON.stringify({
                website_ids: websiteIds
            });

            return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/website_keys/?format=json&website_id=' + localStorageService.get('selectedwebsite'),
                    data: postParam,
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function(response) {
                    return response.data;
                });
        }
    };

    return serviceObj;
});

}());
