(function() {

'use strict';

describe('Service: userSessionService', function() {

    var mockLocalStorageService;
    var mockCommercialPermissionService;
    var mockWebConfigService;
    var mockRoute;
    var mockLocation;
    var userSessionService;
    var store;
    var rootScope;
    var dfr;
    var q;

    beforeEach(module('fcApp.common', function($provide) {
        store = {
            selectedwebsite: 'kkkk',
            webconfig: {},
            websiteDetails: {},
            websites: ['vvvv', 'cccc'],
            commercial_websites: ['ssss']
        };

        mockRoute = {
            current: {
                params: {
                    websiteId: 'vvvv'
                }
            }
        };

        mockLocation = {
            website_id: '',
            search: function() {
                return this;
            },

            // for resolving route.current.params
            url: function() {
                return this;
            },

            hash: function() {}
        };

        mockLocalStorageService = {
            remove: function(id) {
                return store[id];
            },

            get: function(id) {
                return store[id];
            },

            add: function(id) {
                return store[id];
            }
        };

        mockCommercialPermissionService = {
            getWebsitePartners: function() {
                dfr = q.defer();
                dfr.resolve(['asia', 'america']);
                return dfr.promise;
            }
        };

        mockWebConfigService = {
            get: function() {
                dfr = q.defer();
                dfr.resolve({});
                return dfr.promise;
            }
        };

        $provide.value('localStorageService', mockLocalStorageService);
        $provide.value('commercialPermissionService', mockCommercialPermissionService);
        $provide.value('webConfigService', mockWebConfigService);
        $provide.value('$route', mockRoute);
        $provide.value('$location', mockLocation);
        $provide.value('_', window._);
    }));

    beforeEach(inject(function($q, _$rootScope_, _userSessionService_) {
        q = $q;
        rootScope = _$rootScope_;
        userSessionService = _userSessionService_;
    }));

    describe('#updateWebsiteId', function() {
        it('should be defined', function() {
            expect(userSessionService.updateWebsiteId).toBeDefined();
        });

        it('should update localStorage', function() {
            spyOn(mockLocalStorageService, 'remove');
            spyOn(mockLocalStorageService, 'add');

            userSessionService.updateWebsiteId();

            expect(mockLocalStorageService.remove).toHaveBeenCalledWith('selectedwebsite');
            expect(mockLocalStorageService.add).toHaveBeenCalledWith('selectedwebsite', 'vvvv');
        });

        it('should not update localStorage if websiteId is not present in the url routeParams', function() {
            spyOn(mockLocalStorageService, 'remove');
            spyOn(mockLocalStorageService, 'add');

            mockRoute.current.params.websiteId = '';

            userSessionService.updateWebsiteId();

            expect(mockLocalStorageService.remove).not.toHaveBeenCalled();
            expect(mockLocalStorageService.add).not.toHaveBeenCalled();
        });

        it('should not update localStorage if websiteId is not present in the url query string', function() {
            spyOn(mockLocalStorageService, 'remove');
            spyOn(mockLocalStorageService, 'add');

            mockRoute.current.params.websiteId = '';

            userSessionService.updateWebsiteId();

            expect(mockLocalStorageService.remove).not.toHaveBeenCalled();
            expect(mockLocalStorageService.add).not.toHaveBeenCalled();
        });

        it('should not update localStorage if websiteId is the same as localStorage value', function() {
            spyOn(mockLocalStorageService, 'remove');
            spyOn(mockLocalStorageService, 'add');

            store.selectedwebsite = 'vvvv';

            userSessionService.updateWebsiteId();

            expect(mockLocalStorageService.remove).not.toHaveBeenCalled();
            expect(mockLocalStorageService.add).not.toHaveBeenCalled();
        });

        it('should not update localStorage if websiteId is not allowed', function() {
            spyOn(mockLocalStorageService, 'remove');
            spyOn(mockLocalStorageService, 'add');

            store.websites = ['aaaa', 'bbbb'];
            store.commercial_websites = ['mmmm'];

            userSessionService.updateWebsiteId();

            expect(mockLocalStorageService.remove).not.toHaveBeenCalled();
            expect(mockLocalStorageService.add).not.toHaveBeenCalled();
        });
    });

    describe('#updateWebsiteDetails', function() {
        it('should be defined', function() {
            expect(userSessionService.updateWebsiteDetails).toBeDefined();
        });

        it('should call commercialPermissionService.getWebsitePartners', function() {
            spyOn(mockCommercialPermissionService, 'getWebsitePartners').and.callThrough();

            userSessionService.updateWebsiteDetails('test');

            expect(mockCommercialPermissionService.getWebsitePartners).toHaveBeenCalled();
        });

        it('should update localStorage', function() {
            var expectedStoredData = {
                websiteId: 'test',
                partnerName: 'asia',
                partners: ['asia', 'america']
            };

            spyOn(mockLocalStorageService, 'remove');
            spyOn(mockLocalStorageService, 'add');

            userSessionService.updateWebsiteDetails('test').then(function() {
                expect(mockLocalStorageService.remove).toHaveBeenCalledWith('websiteDetails');
                expect(mockLocalStorageService.add).toHaveBeenCalledWith('websiteDetails', expectedStoredData);
            });

            rootScope.$apply();
        });
    });

    describe('#updateWebConfig', function() {
        it('should be defined', function() {
            expect(userSessionService.updateWebConfig).toBeDefined();
        });

        it('should call webConfigService.get', function() {
            spyOn(mockWebConfigService, 'get').and.callThrough();

            userSessionService.updateWebConfig('test');

            expect(mockWebConfigService.get).toHaveBeenCalled();
        });

        it('should update localStorage', function() {
            spyOn(mockLocalStorageService, 'remove');
            spyOn(mockLocalStorageService, 'add');

            userSessionService.updateWebConfig('test').then(function() {
                expect(mockLocalStorageService.remove).toHaveBeenCalledWith('webconfig');
                expect(mockLocalStorageService.add).toHaveBeenCalledWith('webconfig', {});
            });

            rootScope.$apply();
        });
    });
});

}());
