(function() {

'use strict';

describe('Service: webConfigService', function() {
    var $httpBackend;

    beforeEach(module('fcApp.common'));

    var webConfigService;

    beforeEach(inject(function(_webConfigService_, $injector) {
        webConfigService = _webConfigService_;

        $httpBackend = $injector.get('$httpBackend');
    }));

    it('should do something', function() {
        expect(!!webConfigService).toBe(true);
    });
});

}());
