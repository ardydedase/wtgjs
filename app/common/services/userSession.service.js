(function() {

'use strict';

angular.module('fcApp.common').factory('userSessionService', function(
    $route,
    $location,
    _,
    localStorageService,
    commercialPermissionService,
    webConfigService
    ) {

    function getRouteWebsiteId() {
        if ($route.current.params.websiteId && $route.current.params.websiteId.length === 4) {
            return $route.current.params.websiteId;
        }

        if ($location.search().website_id) {
            return $location.search().website_id;
        }

        return '';
    }

    function isWebsiteAllowed(websiteId) {
        var allowedList = _.union(localStorageService.get('websites'), localStorageService.get('commercial_websites'));
        return _.indexOf(allowedList, websiteId) > -1;
    }

    function updateLocalStorageWebsiteDetails(websiteId) {
        return commercialPermissionService.getWebsitePartners(websiteId).then(function(response) {
            var partnerName = response[0] || '';

            var websiteDetails = {
                websiteId: websiteId,
                partnerName: partnerName,
                partners: response
            };

            localStorageService.remove('websiteDetails');
            localStorageService.add('websiteDetails', websiteDetails);

            return websiteDetails;
        });
    }

    function updateLocalStorageWebConfig(websiteId) {
        return webConfigService.get(websiteId).then(function(response) {
            localStorageService.remove('webconfig');
            localStorageService.add('webconfig', response);

            return response;
        });
    }

    function updateLocalStorageWebsiteId() {
        var routeWebsiteId = getRouteWebsiteId();
        var storedWebsiteId = localStorageService.get('selectedwebsite') || '';

        if (routeWebsiteId.length && routeWebsiteId !== storedWebsiteId && isWebsiteAllowed(routeWebsiteId)) {
            localStorageService.remove('selectedwebsite');
            localStorageService.add('selectedwebsite', routeWebsiteId);
        }
    }

    return {
        updateWebsiteId: updateLocalStorageWebsiteId,
        updateWebsiteDetails: updateLocalStorageWebsiteDetails,
        updateWebConfig: updateLocalStorageWebConfig
    };
});

}());
