(function() {

'use strict';

angular.module('fcApp.common').factory('pollerService', function(
    $http,
    $q,
    _,
    moment,
    config,
    poller,
    localStorageService,
    mixpanelService) {

    var baseUrl = config.API_URL + config.API_BASE;

    function start(options) {
        var method = options.method ? options.method.toLowerCase() : 'get';
        var parameters = options.params === undefined ? {} : options.params;

        var httpOptions = {
            method: method,
            url: baseUrl + '/' + options.url,
            ignoreLoadingBar: true
        };

        _.extend(parameters, {
            // website_id: localStorageService.get('selectedwebsite')
            // PE-791
            website_id: localStorageService.get('selectedwebsite') === 'a001' ? 'aljp' : localStorageService.get('selectedwebsite')

            // --PE-791
        });

        if (method === 'post') {
            _.extend(httpOptions, {
                data: JSON.stringify(parameters),
                headers: {'Content-Type': 'application/json'}
            });
        } else {
            httpOptions.url += '/?' + $.param(parameters);
        }

        return $http(httpOptions).then(function(response) {
            return response.data.task_id;
        },

        function(err) {
            return err;
        });
    }

    function poll(options) {
        var url = baseUrl + '/' + options.url + '/';
        var delayTime = options.delay ? options.delay : 6000;
        var taskId = options.taskId;

        // var websiteId = localStorageService.get('selectedwebsite');
        var websiteId = localStorageService.get('selectedwebsite') === 'a001' ? 'aljp' : localStorageService.get('selectedwebsite');

        return poller.get(url, {
            action: 'get',
            delay: delayTime,
            smart: true,
            catchError: true,
            argumentsArray: [{
                params: {
                    uid: taskId,
                    website_id: websiteId
                },
                ignoreLoadingBar: true
            }]
        });
    }

    // TODO: make it more generic. It should be able to accept parameters other than websiteId.
    function genericPoller(url, websiteId) {
        var dfr = $q.defer();

        // var url = 'v0.4/flights/regression/' + action + '/async';

        console.log('url:');
        console.log(url);

        var trackingData = {
            url: url + '/',
            type: 'genericPoller',
            params: $.param({
                // website_id: websiteId
                // PE-791
                website_id: websiteId === 'a001' ? 'aljp' : websiteId

                // --PE-791
            }),
            method: 'GET',

            // websiteId: websiteId
            // PE-791
            websiteId: websiteId === 'a001' ? 'aljp' : websiteId

            // --PE-791

        };

        var startTime = moment();
        console.log('start_url:' + url);
        start({
            method: 'post',
            url: url + '/'
        })
        .then(function(taskId) {
            var genericPoller;

            if (taskId) {
                genericPoller = poll({
                    taskId: taskId,
                    url: url,
                    delay: 3000
                });

                genericPoller.promise.then(null, null, function(res) {
                    if (genericPoller) {
                        if (res.data.ready) {
                            genericPoller.stop();

                            // a hack to ditch the poller; bug with this block getting registered multiple times when run in one view session
                            genericPoller = null;

                            if (res.data.state === 'SUCCESS') {
                                mixpanelService.trackAPI(trackingData, true, startTime);
                                dfr.resolve(res.data.result);
                            } else {
                                mixpanelService.trackAPI(trackingData, false, startTime);
                                dfr.reject(res.data.state);
                            }
                        }
                    }
                });
            } else {
                mixpanelService.trackAPI(trackingData, false, startTime);
                dfr.reject();
            }
        },

        function(err) {
            mixpanelService.trackAPI(trackingData, false, startTime);
            dfr.reject(err);
        });

        return dfr.promise;
    }

    return {
        start: start,
        poll: poll,
        genericPoller: genericPoller
    };

});

}());
