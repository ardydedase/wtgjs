(function() {

'use strict';

angular.module('fcApp.common').factory('mixpanelService', function(
    moment,
    authService,
    localStorageService) {

    return {
        trackAPI: function(data, success, startTime) {
            var endTime = moment();
            var username = localStorageService.get('username');
            var userInfo = {
                username: username,
                userType: authService.getUserType(username)
            };

            data.loadTime = endTime.diff(startTime, 'milliseconds');
            data.success = success;

            var trackingData = $.extend(data, userInfo);

            mixpanel.track('FC API Called', trackingData);
        },

        track: function(eventName, data) {
            var username = localStorageService.get('username');

            var trackingData = {
                username: username,
                userType: authService.getUserType(username),
                websiteId: localStorageService.get('selectedwebsite')
            };

            if (data) {
                $.extend(trackingData, data);
            }

            mixpanel.track(eventName, trackingData);
        }

    };
});

}());
