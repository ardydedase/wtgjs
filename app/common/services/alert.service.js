(function() {

'use strict';

angular.module('fcApp.common').factory('alertService', function(
    $rootScope,
    $timeout,
    $anchorScroll) {

    $rootScope.alerts = [];
    $rootScope.test_panel_alerts = [];

    return {
        add: function(type, msg, errorPosition) {
            if (typeof errorPosition === 'undefined') {
                $rootScope.alerts.push({
                    type: type,
                    msg: msg
                });

                $anchorScroll();

                var timeOutCloseAlert = function(index) {
                    $rootScope.alerts.splice(index, 1);
                };

                $timeout(timeOutCloseAlert, 50000);
            } else if (errorPosition === 'testpanel') {
                $rootScope.test_panel_alerts.push({
                    type: type,
                    msg: msg
                });

                $anchorScroll();

                // var timeOutCloseAlert = function(index) {
                //     $rootScope.test_panel_alerts.splice(index, 1);
                // };

                // $timeout(timeOutCloseAlert, 50000);
            }
        },

        reset: function() {
            $rootScope.alerts = [];
            $rootScope.test_panel_alerts = [];
        },

        closeAlert: function(index, errorPosition) {
            if (typeof errorPosition === 'undefined') {
                $rootScope.alerts.splice(index, 1);
            } else if (errorPosition === 'testpanel') {
                $rootScope.test_panel_alerts.splice(index, 1);
            }

            $anchorScroll();
        }
    };
});

}());
