(function() {

'use strict';

angular.module('fcApp.common').factory('accountService', function(
    $http,
    localStorageService) {

    return {
        /**
         * Check if user is a member of a particular group.
         * @param  {[type]}  group [description]
         * @return {Boolean}       [description]
         */
        isMemberOf: function(group) {
            try {
                if (localStorageService.get('account').groups.indexOf(group) !== -1) {
                    return true;
                }
            } catch (e) {
                return false;
            }
        }
    };
});

}());
