(function() {

'use strict';

describe('Service: emailValidation', function() {

    var emailValidationService;

    beforeEach(module('fcApp.common'));

    beforeEach(inject(function(_emailValidationService_) {
        emailValidationService = _emailValidationService_;
    }));

    describe('invalid email format', function() {

        var shouldFailWhenEmailIsInvalid = function shouldFailWhenEmailIsInvalid(invalidEmail) {
            it('should return false if email is invalid', function() {
                var isValid = emailValidationService.isValid(invalidEmail);

                expect(isValid).toEqual(false);
            });
        };

        var invalidEmails = [
            'thisIsInvalidEmail',
            'this@isAlsoInvalidEmail',
            '@InvalidIt.is',
            '.can@YouSeeItsInvalid?',
            'MoreInvalid@Email.',
            'some@more@invalid.com'
        ];

        window._.forEach(invalidEmails, shouldFailWhenEmailIsInvalid);
    });

    describe('valid email format', function() {

        var shouldPassWhenEmailIsValid = function shouldPassWhenEmailIsValid(validEmail) {
            it('should return true if email is valid', function() {
                var isValid = emailValidationService.isValid(validEmail);

                expect(isValid).toEqual(true);
            });
        };

        var validEmails = [
            'valid@skyscanner.net',
            'with+plus@google.com.sg'
        ];

        validEmails.forEach(shouldPassWhenEmailIsValid);
    });
});

}());
