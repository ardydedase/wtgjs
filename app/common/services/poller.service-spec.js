(function() {

'use strict';

describe('Service: pollerService', function() {

    var pollerService;
    var defaultMocks;
    var localStorageService;
    var config;
    var poller;
    var httpBackend;
    var q;
    var baseUrl;

    var noop = function() {};

    beforeEach(module('fcApp.common', function($provide) {
        defaultMocks = {
            poller: {
                get: noop
            },
            mixpanelService: {
                trackAPI: noop
            }
        };

        $provide.value('poller', defaultMocks.poller);
        $provide.value('mixpanelService', defaultMocks.mixpanelService);
        $provide.value('_', window._);
        $provide.value('moment', window.moment);
    }));

    beforeEach(inject(function($httpBackend, $q, _pollerService_, _localStorageService_, _config_, _poller_) {

        httpBackend = $httpBackend;
        q = $q;
        pollerService = _pollerService_;
        config = _config_;
        localStorageService = _localStorageService_;
        poller = _poller_;

        baseUrl = config.API_URL + config.API_BASE;

        localStorageService.add('selectedwebsite', 'aaaa');
    }));

    afterEach(function() {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    describe('#start', function() {
        it('should be defined', function() {
            expect(pollerService.start).toBeDefined();
        });

        it('should by default send a GET method if unspecified', function() {
            httpBackend.expectGET(baseUrl + '/testurl/?website_id=aaaa').respond(200, {
                task_id: 1
            });

            pollerService.start({
                url: 'testurl'
            });

            httpBackend.flush();
        });

        it('should be able to send a POST method', function() {
            httpBackend.expectPOST(baseUrl + '/testurl').respond(200, {
                task_id: 1
            });

            pollerService.start({
                url: 'testurl',
                method: 'POST'
            });

            httpBackend.flush();
        });

        it('should always send website_id from localStorage', function() {
            var expectedWebsiteId = 'bbbb';

            localStorageService.remove('selectedwebsite');
            localStorageService.add('selectedwebsite', expectedWebsiteId);

            httpBackend.expectGET(baseUrl + '/testurl/?website_id=' + expectedWebsiteId).respond(200, {
                task_id: 1
            });

            pollerService.start({
                url: 'testurl'
            });

            httpBackend.flush();
        });

        it('should be able to send parameters for GET in querystring', function() {
            httpBackend.expectGET(baseUrl + '/testurl/?hello=world&website_id=aaaa').respond(200, {
                task_id: 1
            });

            pollerService.start({
                url: 'testurl',
                params: { hello: 'world' }
            });

            httpBackend.flush();
        });

        it('should be able to send data for POST in request payload', function() {
            var expectedPostData = {
                hello: 'world',
                website_id: 'aaaa'
            };

            httpBackend.expectPOST(baseUrl + '/testurl', JSON.stringify(expectedPostData)).respond(200, {
                task_id: 1
            });

            pollerService.start({
                method: 'POST',
                url: 'testurl',
                params: { hello: 'world' }
            });

            httpBackend.flush();
        });

        it('should return a task_id', function() {
            httpBackend.when('GET', baseUrl + '/testurl/?website_id=aaaa').respond(200, {
                task_id: 'test-uid'
            });

            pollerService.start({
                url: 'testurl'
            }).then(function(taskId) {
                expect(taskId).toBe('test-uid');
            });

            httpBackend.flush();
        });

        it('should resolve failure', function() {
            httpBackend.when('GET', baseUrl + '/testurl/?website_id=aaaa').respond(500, 'Nooo!');

            pollerService.start({
                url: 'testurl'
            }).then(function() {},

            function(err) {
                expect(err).toBe('Nooo!');
            });

            httpBackend.flush();
        });
    });

    describe('#poll', function() {
        it('should be defined', function() {
            expect(pollerService.poll).toBeDefined();
        });

        it('should call poller.get', function() {
            spyOn(defaultMocks.poller, 'get').and.callThrough();

            var expectedUrl = baseUrl + '/testurl/async/';

            pollerService.poll({
                url: 'testurl/async',
                taskId: 'test-uid'
            });

            expect(defaultMocks.poller.get).toHaveBeenCalledWith(expectedUrl, jasmine.objectContaining({
                action: 'get',
                smart: true,
                delay: 6000,
                catchError: true
            }));
        });

        it('should send uid and website_id', function() {
            spyOn(defaultMocks.poller, 'get').and.callThrough();

            var expectedWebsiteId = 'bbbb';

            localStorageService.remove('selectedwebsite');
            localStorageService.add('selectedwebsite', expectedWebsiteId);

            pollerService.poll({
                url: 'testurl/async',
                taskId: 'test-uid'
            });

            expect(defaultMocks.poller.get.calls.mostRecent().args[1].argumentsArray[0].params).toEqual({
                uid: 'test-uid',
                website_id: expectedWebsiteId
            });
        });

        it('should be able to set custom delay time', function() {
            spyOn(defaultMocks.poller, 'get').and.callThrough();

            var expectedUrl = baseUrl + '/testurl/async/';

            pollerService.poll({
                url: 'testurl/async',
                taskId: 'test-uid',
                delay: 1000
            });

            expect(defaultMocks.poller.get).toHaveBeenCalledWith(expectedUrl, jasmine.objectContaining({
                delay: 1000
            }));
        });

        it('should poll the task result', function() {
            var dfr = q.defer();
            spyOn(defaultMocks.poller, 'get').and.returnValue(dfr);

            var getScrapesPoller = pollerService.poll({
                url: 'testurl/async',
                taskId: 'test-uid'
            });

            getScrapesPoller.promise.then(null, null, function(pollerResponse) {
                expect(pollerResponse.data.ready).toEqual('hello');
            });

            dfr.notify({
                data: {
                    ready: 'hello'
                }
            });
        });
    });
});

}());
