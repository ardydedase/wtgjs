(function() {

'use strict';

describe('Service: webPermissionService', function($httpBackend) {

    beforeEach(module('fcApp.common'));

    var webPermissionService;
    var localStorageService;
    var config;

    beforeEach(inject(function(_webPermissionService_, _localStorageService_, _config_,  $injector) {
        webPermissionService = _webPermissionService_;
        config = _config_;
        localStorageService = _localStorageService_;
        localStorageService.add('selectedwebsite', 'test');
        $httpBackend = $injector.get('$httpBackend');

        $httpBackend.when('POST', config.API_URL + config.API_BASE + '/website_keys/?format=json&website_id=test').respond({ website_name: 'OTA.com', home_country: 'US' });
    }));

    it('should do something or it should be there', function() {
        expect(!!webPermissionService).toBe(true);
    });

    it('should have a getWebsiteKeys call', function() {
        webPermissionService.getWebsiteKeys(['blahblah']).then(function(response) {
            expect(response.website_name).toBe('OTA.com');
            expect(response.home_country).toBe('US');
        });

        $httpBackend.flush();
    });
});

}());
