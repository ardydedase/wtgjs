(function() {

'use strict';

angular.module('fcApp.common').factory('errorInterceptor', function(
    $q,
    alertService
    ) {

    function mixpanelTracking(trackingData) {
        mixpanel.track('FC Server Failed', trackingData);
    }

    return {
        responseError: function(response) {
            var trackingData = {};

            switch (response.status) {
            case 403:
                trackingData.message = response.data ? response.data.detail : '403 Not Authorized';
                trackingData.errorType = 'permission_error';

                alertService.add('danger', trackingData.message);

                mixpanelTracking(trackingData);

                return $q.reject(response);

            case 404:
                trackingData.message = response.data ? response.data.detail : '404 Not Found';
                trackingData.errorType = 'missing_resource';

                alertService.add('danger', trackingData.message);

                mixpanelTracking(trackingData);

                return $q.reject(response);

            default:
                var defaultMessage = 'Unknown error response';

                if (response.data) {
                    trackingData.message = response.data.error_message || response.data.detail || defaultMessage;
                    trackingData.errorType = response.data.error_type || 'server_failure';
                }

                alertService.add('danger', trackingData.message);

                mixpanelTracking(trackingData);

                return $q.reject(response);
            }
        }
    };
});

}());
