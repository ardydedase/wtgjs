(function() {

'use strict';

angular.module('fcApp.common').factory('webConfigService', function(
    $http,
    alertService,
    localStorageService,
    config) {

    var serviceObj = {};

    serviceObj = {
        get: function(websiteId) {
            if (typeof websiteId === 'undefined') {
                websiteId = localStorageService.get('selectedwebsite');
            }

            //PE-791
            websiteId = websiteId === 'a001' ? 'aljp' : websiteId;

            //--PE-791

            return $http.get(config.API_URL + config.API_BASE + '/webconfig/?' + $.param({
                website_id: websiteId
            }), {cache: false})
            .then(function(response) {
                return response.data;
            });
        },

        /**
         * Get from the Web Storage if it's available.
         * Otherwise, retrieve it from the API service.
         *
         * @param  {[type]} website_id [description]
         * @return {[type]}            [description]
         */
        getFromStorage: function(force) {
            var webConfig = localStorageService.get('webconfig');

            // if there's no web config and there's a new website selected.
            if (!webConfig || force) {
                try {
                    serviceObj.get(localStorageService.get('selectedwebsite')).then(function(response) {
                        webConfig = response;
                        localStorageService.add('webconfig', response);
                    });
                } catch (e) {
                    webConfig = {};
                    alertService.add('danger', 'Can\'t retrieve your website configuration.');
                }
            }

            return webConfig;
        },

        resetValue: function() {
            return {
                website_id: '',
                pricing_type: '',
                partner_type: '',
                home_country: '',
                homepage: ''
            };
        }
    };

    return serviceObj;
});

}());
