(function() {

'use strict';

angular.module('fcApp.common', [
    'ngCookies',
    'angulartics',
    'angulartics.google.analytics',
    'emguo.poller',
    'appConfig',
    'ngRoute',
    'LocalStorageModule',
    'fcApp.authentication',
    'fcApp.permission'
]);

}());
