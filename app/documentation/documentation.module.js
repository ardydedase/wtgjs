(function() {

'use strict';

angular.module('fcApp.documentation', [
    'yaru22.md',
    'fcApp.common'
]);

}());
