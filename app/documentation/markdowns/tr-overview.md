Skyscanner’s Travel Rankings service is an analytics service which enables you to maximize your booking volumes through dynamic margining.

It is available in multiple delivery forms. It can be consumed as a dashboard, where you can instantly see how you are competing on the routes that you serve. It is also available as a push API, whereby you receive a steady stream of information in real time each time your price is displayed.

Get started exploring your data [here](/connect/#/travel-rankings).

[Contact us](/connect/#/feedback)
