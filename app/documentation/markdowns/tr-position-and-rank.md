### Position and Rank
Every ticket you sell has both a position and a rank. The position of a ticket refers to the position of itinerary relative to other itineraries in the results page. Position 1 means the top of the results page, position 2 is the next one down, and so on. The rank of a ticket, on the other hand, is the rank of the ticket compared to other tickets on the same itinerary. Rank 1 means the book button, rank 2 is the next cheapest price on that itinerary and so on.

![Position and Rank](images/markdown-images/position-and-rank.png)