The dashboard is based on data from user searches on Skyscanner. Every time your price appears in the results when a user searches, we add that information into our dashboard. At the most granular level, you can see rankings for individual itineraries for one search. Based on this we aggregate up to show you all the searches for one route. Finally, we aggregate all the searches so that you can get an overview of all the routes.

The data contained in the dashboard is for all Economy class, return searches done on Skyscanner in the selected market for your selected routes.

### Route View

![Route view](images/markdown-images/route-view.png)

- **From** The origin airport or city for this route.
- **To** The arrival airport or city for this route.
- **Search Volume** The number of searches per route performed in the past 24 hours.
- **Your Cheapest Itinerary** These columns show the cheapest result you showed on Skyscanner across all search dates for the past 24 hours.
  - **Position** This shows the percentage of searches where your cheapest itinerary was in position #1. This histogram shows the distribution between position 1, 2, and 3+.
  - **Rank**  This shows the percentage of searches where your cheapest itinerary was in rank #1. This histogram shows the distribution between rank 1, 2 and 3+. Most clicks to go rank 1 and 2.
  - **Avg. Distance to the Top** This shows the distance between your cheapest itinerary and the position 1 book button. This is the best position on Skyscanner for getting traffic.
- **Your Cheapest Book Button** These columns show the first time you appeared on a book button (rank 1) on the Skyscanner dayview.
  - **Position** This shows the percentage of searches where your cheapest book button was in position #1. This histogram shows the distribution between position 1, 2, and 3+.
  - **Avg. Capturable Margin** This shows the average amount you can increase your price by without losing your current rank (rank 1). If the amount is less than 1, it will be blank.
  - **% First Rank Itineraries** This shows the percentage of itineraries you send to Skyscanner that are in rank 1. This means users are more likely to click your prices if they decide on that itinerary.


### Date View

![Route view](images/markdown-images/date-view.png)

- **Depart** The date the user selected to fly.
- **Return** The date the user selected to return.
- **Your Cheapest Itinerary** These columns show the cheapest result you showed on Skyscanner for a specific date/route combination. We will always show the latest result if there are multiple searches.
  - **Position** This shows the position of your cheapest itinerary on this date/route combination.
  - **Rank** This shows the rank of your cheapest itinerary on this date/route combination.
  - **Distance to the Top** This shows the distance between your cheapest itinerary and the position 1 book button. This is the best position on Skyscanner for getting traffic.
- **Your Cheapest Book Button** These columns show the first time you appeared on a book button (rank 1) this date/route combination.
  - **Position** This shows the position of searches your cheapest book button for this date/route combination.
  - **Capturable Margin** This shows the amount you can increase your price by without losing your current rank (rank 1). If the amount is less than 1, it will be blank.
- **% First Rank Itineraries** This shows the percentage of itineraries you sent to Skyscanner on this date/route combination that are in rank 1. This means users are more likely to click your prices if they decide on that itinerary.


### Itinerary View <a name="itinerary-view"></a>

![Route view](images/markdown-images/itinerary-view.png)

We will always show the latest result if there have been multiple searches on this route/date combination.

- **Time out** The time the first flight will take off in local time.
- **Time back** The time the return flight will take off in local time.
- **Flight Numbers** The flight numbers involved in this itinerary.
- **Position** The position of this itinerary.
- **Rank** The rank of this itinerary.
- **Distance to Rank 1** The difference between your price and the cheapest price on this itinerary.
- **Capturable Margin** The difference between your price and the closest competitor.