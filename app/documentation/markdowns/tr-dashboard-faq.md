### Can I use Travel Rankings to look at markets I don't currently display on Skyscanner?
No, Travel Rankings only supports markets where you already provide pricing data.

### How recent is the data?
Searches will be added to the dashboard hourly, so you can see the most recent searches on Skyscanner. We retain data for 24 hours so less frequent routes will still have searches in the dashboard.

### Can I see search volumes on Travel Rankings?
Not currently. This is one of our top requested features and will be added soon.

### Can I analyze historical data?
Travel Rankings currently does not support this.

### Can I export data from Travel Rankings?
No, the aggregated data is not currently available for export. However, we do offer a Push API to send real time search data directly to your servers. Please contact us if you are interested.

