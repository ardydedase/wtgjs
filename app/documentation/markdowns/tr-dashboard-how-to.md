There is a lot of information on the dashboard. What is the best way to use it? Here are three use cases that you can try.

### Maximizing Margins
A good way to maximize your margins is to increase the price of itineraries where you already have the book button. This is difficult because you need to find those itineraries which present the best opportunity amongst the vast number of itineraries you sell, across all the dates and routes where you sell them. This is where Travel Rankings can help.

#### Step 1
Look for a route with high percentage of book buttons at the top of the results page and also a high average capturable margin.

![Maximizing margin - step 1](images/markdown-images/max-margin-1.png)

#### Step 2
Next you will need to narrow in further to find the particular schedule where you can increase your margins. In order to get there, you will need to find dates which have good potential for increasing margins. Such dates will have a high position (1 is the top result, and 1-10 is the first results page) as well as a high capturable margin.

![Maximizing margin - step 2](images/markdown-images/max-margin-2.png)

#### Step 3
Once you have found such a pair of dates, you can click it to take you through to the list of itineraries on which you appeared on that date. Of these, the most attractive option to the user is the itinerary which has the highest position and rank 1. Rank 1 is the book button, and position refers to your position on the page. Your highest positioned book button will always get the most attention, so you can make a good increase to your margin by raising the price of this.

Now you can use the carrier/route combination to adjust your margins.

![Maximizing margin - step 3](images/markdown-images/max-margin-3.png)


### Increasing Traffic
A good way to increase your traffic is to ensure you have the best prices whenever you can afford to do so. With so many routes, and prices varying depending on the search dates for each route, it’s difficult to know where the opportunities lie for increasing traffic. Travel rankings can help you to achieve this by giving you a high level picture of which routes provide the richest opportunities for increasing traffic.

Traffic can be increased where there are a high number of book buttons which you can take with a small price reduction.

#### Step 1
To find such cases, go to the dashboard and look for routes with a low average difference between your cheapest itinerary and the overall cheapest itinerary along with a low percentage of your cheapest itineraries having position #1 or rank #1.

Such routes represent an opportunity because it doesn’t cost much to be the cheapest, but you are not currently the cheapest.

![Increasing traffic - step 1](images/markdown-images/inc-traffic-1.png)


#### Step 2
Once you have found such a route, click it to view the different search dates that users have searched on this route. From here, you will need to find routes where we have a low difference between your cheapest itinerary and the overall cheapest itinerary along with a position higher than 1 or a rank higher than 1 for your cheapest itinerary.

This is a search where it would only cost you a small amount to get the top spot where you don’t currently have it.

![Increasing traffic - step 2](images/markdown-images/inc-traffic-2.png)


#### Step 3
Click those dates to take you to the complete list of search results. Sort by price to see your cheapest result. This is the itinerary whose price you can change by a small amount to get the cheapest book button.

From here, you can look up the flight times, and you can use these to look up the itinerary in your revenue management system.

![Increasing traffic - step 3](images/markdown-images/inc-traffic-3.png)


### Trimming Bad Routes
Some routes are always going to be costly to maintain and it’s important to find these. A route is going to give you a bad ROI if you have a large gap between you and your nearest competitor and most of your itineraries are ranked #3 or higher.

On such routes, you will not get very many click-throughs, and the cost of closing the gap is large. In these cases, it would be worth looking into turning off these routes in order to save on data costs.

![Bad routes](images/markdown-images/bad-routes.png)