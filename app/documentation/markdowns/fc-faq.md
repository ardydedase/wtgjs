Welcome to the Frequently Asked Questions for FlightsConnect. We've gathered together a list of all the questions we've been asked so far. Have a question that isn't answered here? Send us some [feedback] [feedback form].

##General
###What are the codes in the top left?
 This is your "website id". Skyscanner uses these codes to divide your integration based on criteria from your account manager. These can be per market, per flight type, or by a different criteria. Please get in touch with your account manager if you have any questions about what the website ids mean.
##Traffic Data
###What is an API Query?
 An API query is one call to the API you provide to Skyscanner. If you allow Skyscanner to get prices directly from your website, those queries will be counted here as well.
###What is Look2Book?
 Look2Book is the ratio between API queries and bookings (if you report bookings to Skyscanner). This number is only your Look2Book from Skyscanner's perspective and will be higher than your L2B ratio with your upstream providers (because of caching).
###What is a redirect?
 A redirect is a user clicking on one of your prices and being redirected to your website/booking form.
###These redirect counts are different than my internal tracking. Why?
 Sometimes users will close their browser if the redirect takes too long. They may also click on your prices displayed on Skyscanner more than once. Both of these could result in a higher redirect count on Skyscanner compared to your internal tracking. If you have a specific question about a redirect number that seems out of the ordinary, [get in touch with us][feedback form] or your account manager.
###What is a booking?
 A booking is a confirmed transaction reported to Skyscanner via your Conversion Pixel or booking form.
###My conversion rate is higher/lower than I expected. What can cause this?
 FlightsConnect will report your conversion rate (bookings divided by redirects) exactly as reported to Skyscanner. If there is an issue with a Conversion Pixel sending too many/too few events, the data on FlightsConnect will be incorrect.
###How should I use this data?
 While we do our best to keep the data in sync with your account manager, the data is for your information only. Any billing/authoritative counts of redirects and bookings will come from your account manager.
###What is the current lag for the data?
 2 days. Our data could be inaccurate for today's and yesterday's data.
###What are the time periods for the dashboard traffic data?
 Our data is accurate -2 days from the current date. "This week" means a range from -2 days to -9 days ago. For example, if today is April 9th, "this week" will encompass April 1st to April 7th.
###Some dates are missing from my data. Why and what can I do?
 We cache the data locally to improve dashboard speed, but this means the data can be partial, especially when changing the date ranges. If the graph is incomplete, try refreshing the page after few minutes. If the data doesn't load, [get in touch][feedback form].
###What data is available for me to query?
 FlightsConnect currently contains your redirect and booking data across a number of dimensions:

 * **Date** - The date a redirect/booking occurred. Currently recorded in GMT.
 * **Carrier** - The marketing carrier clicked by a user on Skyscanner. The user may have switched carriers/itineraries after leaving Skyscanner.
 * **Market** - The country selected by the user.
 * **Flight Type** - The type of route selected by the user. If the itinerary begins and ends in the same country, we count it as domestic. Everything else is international. Note the actual itinerary booked by the user can differ if they changed their selection on your site.
 * **Device Type** - The specific device used to access Skyscanner/your website. This dataset may not be complete and contain "None" for some redirects and bookings.

###How can I check mobile redirects for my website?
 Use the "Device Type" dimension described above.
###Can I download the data?
 Yes, just use the "Download CSV" button on the "My Traffic" page once you've decided on the data you want.
##Technical
###What does "No Quotes" mean on the API Health Graph?
 This means Skyscanner successfully contacted your API, but the response contained no quotes for us to display to users.
###What does "OtherError" mean in the reproducible errors?
 These are parsing errors that come about either because of API format changes or unhandled exceptions on the Skyscanner side.

[feedback form]: /connect/#/feedback