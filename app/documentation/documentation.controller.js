(function() {

'use strict';

angular.module('fcApp.documentation').controller('DocumentationCtrl', function(
    $scope,
    authService) {

    authService.redirectFromCommercialPage();
});

}());
