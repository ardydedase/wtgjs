(function() {

'use strict';

angular.module('fcApp.technical', [
    'appConfig',
    'LocalStorageModule',
    'angulartics',
    'angulartics.google.analytics',
    'angularMoment',
    'ui.bootstrap',
    'ui.layout',
    'ui.multiselect',
    'ngPrettyJson',
    'jsonFormatter',
    'ui.ace',
    'pageslide-directive',
    'fcApp.common',
    'infinite-scroll'
]);

angular.module('fcApp.technical')
    .constant('vkbeautify', window.vkbeautify)
    .constant('URI', window.URI);

}());
