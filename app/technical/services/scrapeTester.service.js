(function() {

'use strict';

angular.module('fcApp.technical').factory('scrapeTesterService', function(
    $http,
    $filter,
    config,
    poller,
    localStorageService) {

    var serviceObj = {};

    serviceObj = {

        /**
         * [addCabinClassesToArray description]
         * @param  {[type]} cabinClasses [description]
         * @return {[type]}              [description]
         */
        addCabinClassesToArray: function(cabinClasses) {
            var selectedCabinClasses = [];

            for (var x in cabinClasses) {
                if (cabinClasses[x] === true) {
                    selectedCabinClasses.push(x);
                }
            }

            return selectedCabinClasses;
        },

        /**
         * Util function to convert cabin classes array to a Javascritp Object
         * @param {[type]} cabinClasses [description]
         */
        addCabinClassesToObj: function(cabinClasses) {
            var selectedCabinClasses = {};
            for (var x in cabinClasses) {
                if (cabinClasses.hasOwnProperty(x)) {
                    selectedCabinClasses[cabinClasses[x]] = true;
                }
            }

            return selectedCabinClasses;
        },

        /**
         * Poller when starting the scrape
         * @param  {[type]} taskId [description]
         * @return {[type]}        [description]
         */
        startScrapesPoller: function(taskId) {
            var scrapesPoller = poller.get(config.API_URL + config.API_BASE + '/v0.4/flights/scrapes/new/async', {
                action: 'get',
                delay: 6000,
                argumentsArray: [
                    {
                        params: {
                            uid: taskId,
                            website_id: localStorageService.get('selectedwebsite')
                        },

                        ignoreLoadingBar: true

                    }
                ]
            });
            return scrapesPoller;
        },

        /**
         * Send the initial request to server to start the scrapetester tasks.
         *
         * @param  {[type]} scraperSettings [description]
         * @return {[type]}                 [description]
         */
        start: function(scraperSettings) {
            // update cabin classes
            scraperSettings.cabin_classes = serviceObj.addCabinClassesToArray(scraperSettings.cabin_classes);

            $.extend(scraperSettings, {
                website_id: localStorageService.get('selectedwebsite'),
                min_date: $filter('date')(scraperSettings.min_date, 'yyyy-MM-dd'),
                max_date: $filter('date')(scraperSettings.max_date, 'yyyy-MM-dd')
            });
            return $http({
                method: 'POST',
                url: config.API_URL + config.API_BASE + '/v0.4/flights/scrapes/new/',
                data: JSON.stringify(scraperSettings),
                ignoreLoadingBar: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                return response.data;
            });
        },

        /**
         * Run scrape tests
         * @param  {[type]} scraperSettings [description]
         * @return {[type]}                 [description]
         */
        run: function(scraperSettings) {
            // update cabin classes
            scraperSettings.cabin_classes = serviceObj.addCabinClassesToArray(scraperSettings.cabin_classes);

            if (scraperSettings.user_currency.constructor !== Array) {
                scraperSettings.user_currency = scraperSettings.user_currency.split(',');
            }

            if (scraperSettings.user_languages.constructor !== Array) {
                scraperSettings.user_languages = scraperSettings.user_languages.split(',');
            }

            if (scraperSettings.user_countries.constructor !== Array) {
                scraperSettings.user_countries = scraperSettings.user_countries.split(',');
            }

            $.extend(scraperSettings, {
                website_id: localStorageService.get('selectedwebsite'),
                min_date: $filter('date')(scraperSettings.min_date, 'yyyy-MM-dd'),
                max_date: $filter('date')(scraperSettings.max_date, 'yyyy-MM-dd')
            });

            if ('created_at' in scraperSettings) {
                delete scraperSettings.created_at;
            }

            return $http({
                method: 'POST',
                url: config.API_URL + config.API_BASE + '/v0.4/flights/scrapetester/',
                data: JSON.stringify(scraperSettings),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                return response.data;
            });
        },

        /**
         * Check the scrape test status
         * @param  {[type]} taskId [description]
         * @return {[type]}        [description]
         */
        checkScrape: function(taskId, scraperSettings) {

            var getParams = {
                website_id: localStorageService.get('selectedwebsite'),
                uid: taskId,
                jac_version: scraperSettings.jac_version
            };

            return $http({
                method: 'GET',
                url: config.API_URL + config.API_BASE + '/v0.4/flights/scrapetester/?',
                params: getParams,
                ignoreLoadingBar: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                return response.data;
            });

        },

        /**
         * Get the scrape tests, all per website or by ID
         * @param  {[type]} scrapeId [description]
         * @return {[type]}          [description]
         */
        getScrapeTests: function(scrapeId) {
            var getParams = {
                website_id: localStorageService.get('selectedwebsite')
            };

            if (typeof scrapeId !== 'undefined') {
                $.extend(getParams, {
                    scrape_id: scrapeId
                });
            }

            return $http({
                method: 'GET',
                url: config.API_URL + config.API_BASE + '/v0.4/flights/scrapes/past/?',
                params: getParams,
                ignoreLoadingBar: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                return response.data;
            });
        },

        /**
         * Poller when starting the scrape
         * @param  {[type]} taskId [description]
         * @return {[type]}        [description]
         */
        getScrapeTestsPoller: function(taskId) {
            var apiUrl = config.API_URL + config.API_BASE + '/v0.4/flights/scrapes/past/async';

            var sc = poller.get(apiUrl, {
                action: 'get',
                delay: 6000,
                argumentsArray: [
                    {
                        params: {
                            uid: taskId,
                            website_id: localStorageService.get('selectedwebsite')
                        },

                        ignoreLoadingBar: true
                    }
                ]
            });

            return sc;
        },

        /**
         * Get the scrape routes
         * @return {[type]} [description]
         */
        getScrapeRoutes: function() {
            var getParams = {
                website_id: localStorageService.get('selectedwebsite'),
                start_date: '20141201',
                end_date: '20141210'
            };

            return $http({
                method: 'GET',
                url: config.API_URL + config.API_BASE + '/v0.4/flights/scrapes/past/routes?',
                params: getParams,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                return response.data;
            });
        },

        /**
         * Get all the test cases per website ID
         * @param  {[type]} scrapeId [description]
         * @return {[type]}          [description]
         */
        getTestCases: function(scrapeId) {
            var getParams = {
                website_id: localStorageService.get('selectedwebsite'),
                testsuite: scrapeId
            };

            return $http({
                method: 'GET',
                url: config.API_URL + config.API_BASE + '/v0.4/flights/scrapetestcases/?',
                params: getParams,
                ignoreLoadingBar: true,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                return response.data;
            });

        },

        /**
         * Poller when starting the scrape
         * @param  {[type]} taskId [description]
         * @return {[type]}        [description]
         */
        getTestCasesPoller: function(taskId) {
            var apiUrl = config.API_URL + config.API_BASE + '/v0.4/flights/scrapetestcases/async';

            var sc = poller.get(apiUrl, {
                action: 'get',
                delay: 6000,
                argumentsArray: [
                    {
                        params: {
                            uid: taskId,
                            website_id: localStorageService.get('selectedwebsite')
                        },

                        ignoreLoadingBar: true
                    }
                ]
            });

            return sc;
        },
    };

    return serviceObj;

});

}());
