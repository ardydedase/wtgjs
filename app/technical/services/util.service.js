/**
 * This service provides some commonly used methods/functions that are
 * not available natively on JS.
 */
'use strict';

angular.module('fcApp.technical')
    .factory('utilService', function($q) {

        // this was previously extending native Date
        // changed this to bindable func
        function addDays(days) {
            var dat = new Date(this.valueOf());
            dat.setDate(dat.getDate() + days);
            return dat;
        }

        return {
            isImage: function(src) {
                var deferred = $q.defer();

                var image = new Image();

                image.onerror = function() {
                    deferred.resolve(false);
                };

                image.onload = function() {
                    deferred.resolve(true);
                };

                image.src = src;

                return deferred.promise;
            },

            getDates: function(startDate, stopDate) {
                var dateArray = [];
                var currentDate = startDate;

                while (currentDate <= stopDate) {
                    dateArray.push(new Date (currentDate));
                    currentDate = addDays.bind(currentDate, 1);
                }

                return dateArray;
            },

            getRandomArbitrary: function(min, max) {
                return Math.random() * (max - min) + min;
            },

            encodeURL: function(url) {
                return url.replace(/ucy/g, 'usrplace');
            },

            toTitleCase: function(str) {
                return str.replace(/_/g, ' ').replace(/(?:^|\s)\w/g, function(match) {
                    return match.toUpperCase();
                });
            },

            xFunction: function() {
                return function(d) {
                    return d[0];
                };
            },

            /* not used anywhere, please remove
            xAxisTickFormatFunction: function() {
                return function(d) {
                    return d3.time.format('%d-%b-%Y')(new Date(d * 1000));
                };
            },
            */

            yFunction: function() {
                return function(d) {
                    return d[1];
                };
            },

            yAxisTickFormatFunction: function() {
                return function(d) {
                    return d;
                };
            },

        };
    });
