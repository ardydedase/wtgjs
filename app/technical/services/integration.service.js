(function() {

'use strict';

angular.module('fcApp.technical')
    .factory('integrationService', function(
        $http,
        $location,
        alertService,
        authService,
        modalService,
        localStorageService,
        config) {

        var serviceObj = {};

        //PE-791
        function resolveFakeWebsiteId() {
            var currentWebsiteId = localStorageService.get('selectedwebsite');
            return currentWebsiteId === 'a001' ? 'aljp' : currentWebsiteId;
        }

        //--PE-791

        serviceObj = {
            /**
             * Initialize the settings here.
             * This will make sure that the property/object is present and initialized.
             * Pre-populate if it's not on production.
             *
             * @param  {[type]} setting [The settings object. All the settings/properties are in this object.]
             * @return {[type]}         [description]
             */
            prepopulate: function(setting) {
                var settingsMap = {

                    request_setting: {
                        url: '',
                        request_params: '',
                        response: '',
                        request_type: 'post',
                        request_kwargs: {
                            urlencode_get: true
                        }
                    },
                    cabin_class: {
                        economy: '',
                        business: '',
                        first: ''
                    },
                    test_config: {
                        from_place: '',
                        to_place: '',
                        depart_date: '',
                        return_date: '',
                        pricing_type: '',
                        adults: '1',
                        children: '0',
                        infants: '0',
                        market: 'UK',
                        lang: 'en',
                        currency: 'GBP',
                        cabin_class: 'economy'
                    }
                };

                // do not pre-populate if it's on production
                if ($location.host() !== 'atd.skyscanner.net') {
                    settingsMap = {
                        request_setting: {
                            url: 'https://tickets1.airbaltic.com/app/fb.fly',
                            request_params: {
                                action: 'avail',
                                btisess: 'BTI09_SKY_LV_en_D91C81A723BB3FEF763166B04B6488BA',
                                day0: '23',
                                day1: '23',
                                destin: 'MUC',
                                k: 'b030ce0352ace673695fcfae45712ae7',
                                l: 'en',
                                legs: '1',
                                month0: '03-2014',
                                month1: '03-2014',
                                numadt: '1',
                                numchd: '0',
                                numinf: '0',
                                origin: 'BLL',
                                p: 'SKY',
                                range0: '3',
                                range1: '3',
                                rmselect: '0',
                                traveltype: 'bti'
                            },
                            response: '<fb_avail> <header>  <created>   <date>2013-12-20</date>   <time>09:37:50</time>  </created>  <client>SKY</client>  <language>en</language>  <pos>LV</pos>  <session>   <sessionName>btisess</sessionName>   <sessionId>BTI09_SKY_LV_en_A4BDB00FE29FDBB289B7367232441D3E</sessionId>  </session> </header> <offers>  <offer id="ffe9878d94f86b8d909386bbf832079e,EC,Z~Z,ZRUTH9~ZRUTH9,5,0,0,0,12~12,RIX,T482,86">   <numpax>    <adt>1</adt>    <chd>0</chd>    <inf>0</inf>   </numpax>   <journey id="0">    <summary>     <origin>      <origCode>DME</origCode>      <origName>Moscow (Domodedovo)</origName>     </origin>     <destination>      <destCode>LED</destCode>      <destName>St Petersburg (Pulkovo)</destName>     </destination>     <departure>31.03.2014 0710</departure>     <arrival>31.03.2014 1150</arrival>    </summary>    <compartment>EC</compartment>    <faretype>Basic</faretype>    <itinerary>     <flight id="0">      <fltNumber>417</fltNumber>      <carrier>       <carrCode>BT</carrCode>       <carrName>airBaltic</carrName>       <carrCode type="op">BT</carrCode>       <carrName type="op">airBaltic</carrName>      </carrier>      <origin>       <origCode>DME</origCode>       <depDate>2014-03-31</depDate>       <depTime>07:10</depTime>      </origin>      <destination>       <destCode>RIX</destCode>       <arrDate>2014-03-31</arrDate>       <arrTime>08:20</arrTime>       <duration>4:40</duration>      </destination>      <stops>0</stops>     </flight>     <flight id="1">      <fltNumber>442</fltNumber>      <carrier>       <carrCode>BT</carrCode>       <carrName>airBaltic</carrName>       <carrCode type="op">BT</carrCode>       <carrName type="op">airBaltic</carrName>      </carrier>      <origin>       <origCode>RIX</origCode>       <depDate>2014-03-31</depDate>       <depTime>09:30</depTime>      </origin>      <destination>       <destCode>LED</destCode>       <arrDate>2014-03-31</arrDate>       <arrTime>11:50</arrTime>       <duration>4:40</duration>      </destination>      <stops>0</stops>     </flight>    </itinerary>    <bkgClass flight_id="0">Z</bkgClass>    <bkgClass flight_id="1">Z</bkgClass>    <rules>     <maxConnex/>     <apex>0</apex>     <minStay/>     <maxStay/>     <dow/>     <bookStart>04.12.2013</bookStart>     <bookEnd>04.12.2014</bookEnd>     <fltStart>22.12.2010</fltStart>     <fltEnd>29.10.2014</fltEnd>    </rules>   </journey>   <totalPrice>    <ADT>140.99</ADT>    <CHD>109.99</CHD>    <INF>60.10</INF>    <GROSS>140.99</GROSS>    <currency>EUR</currency>   </totalPrice>   <fare paxtype="ADT">    <fareBase>     <outbound>ZRUTH9</outbound>     <inbound/>    </fareBase>    <currency>EUR</currency>    <net>66.00</net>    <taxes>     <tax type="YQ" comment=">0.86</tax>     <tax type="LV" comment=">0.29</tax>     <tax type="OH" comment="">1.00</tax>     <tax type="RI" comment=">14.19</tax>     <tax type="UH" comment=">4.65</tax>    </taxes>    <taxtotal>74.99</taxtotal>    <gross>140.99</gross>    <type>WEB</type>   </fare>   <sel0>ffe9878d94f86b8d909386bbf832079e,EC,Z~Z,ZRUTH9~ZRUTH9,5,0,0,0,12~12,RIX,T482,86</sel0>  </offer> <offer id="ffe9878d94f86b8d909386bbf832079e,EC,Z~Z,ZRUTH9~ZRUTH9,5,0,0,0,12~12,RIX,T482,86">   <numpax>    <adt>1</adt>    <chd>0</chd>    <inf>0</inf>   </numpax>   <journey id="0">    <summary>     <origin>      <origCode>DME</origCode>      <origName>Moscow (Domodedovo)</origName>     </origin>     <destination>      <destCode>LED</destCode>      <destName>St Petersburg (Pulkovo)</destName>     </destination>     <departure>31.03.2014 0710</departure>     <arrival>31.03.2014 1150</arrival>    </summary>    <compartment>EC</compartment>    <faretype>Basic</faretype>    <itinerary>     <flight id="0">      <fltNumber>417</fltNumber>      <carrier>       <carrCode>BT</carrCode>       <carrName>airBaltic</carrName>       <carrCode type="op">BT</carrCode>       <carrName type="op">airBaltic</carrName>      </carrier>      <origin>       <origCode>DME</origCode>       <depDate>2014-03-31</depDate>       <depTime>07:10</depTime>      </origin>      <destination>       <destCode>RIX</destCode>       <arrDate>2014-03-31</arrDate>       <arrTime>08:20</arrTime>       <duration>4:40</duration>      </destination>      <stops>0</stops>     </flight>     <flight id="1">      <fltNumber>442</fltNumber>      <carrier>       <carrCode>BT</carrCode>       <carrName>airBaltic</carrName>       <carrCode type="op">BT</carrCode>       <carrName type="op">airBaltic</carrName>      </carrier>      <origin>       <origCode>RIX</origCode>       <depDate>2014-03-31</depDate>       <depTime>09:30</depTime>      </origin>      <destination>       <destCode>LED</destCode>       <arrDate>2014-03-31</arrDate>       <arrTime>11:50</arrTime>       <duration>4:40</duration>      </destination>      <stops>0</stops>     </flight>    </itinerary>    <bkgClass flight_id="0">Z</bkgClass>    <bkgClass flight_id="1">Z</bkgClass>    <rules>     <maxConnex/>     <apex>0</apex>     <minStay/>     <maxStay/>     <dow/>     <bookStart>04.12.2013</bookStart>     <bookEnd>04.12.2014</bookEnd>     <fltStart>22.12.2010</fltStart>     <fltEnd>29.10.2014</fltEnd>    </rules>   </journey>   <totalPrice>    <ADT>140.99</ADT>    <CHD>109.99</CHD>    <INF>60.10</INF>    <GROSS>140.99</GROSS>    <currency>EUR</currency>   </totalPrice>   <fare paxtype="ADT">    <fareBase>     <outbound>ZRUTH9</outbound>     <inbound/>    </fareBase>    <currency>EUR</currency>    <net>66.00</net>    <taxes>     <tax type="YQ" comment=">0.86</tax>     <tax type="LV" comment=">0.29</tax>     <tax type="OH" comment="">1.00</tax>     <tax type="RI" comment=">14.19</tax>     <tax type="UH" comment=">4.65</tax>    </taxes>    <taxtotal>74.99</taxtotal>    <gross>140.99</gross>    <type>WEB</type>   </fare>   <sel0>ffe9878d94f86b8d909386bbf832079e,EC,Z~Z,ZRUTH9~ZRUTH9,5,0,0,0,12~12,RIX,T482,86</sel0>  </offer></offers><miscellaneous>  <fares_warning type="info">The availability of fares and flights at the time of your query can change while you are in the process of making your booking. Should your booking not be possible for the fare you selected you will be informed before finishing the booking. The fare is only guaranteed when you have finished the booking and received confirmation with the reservation number.</fares_warning> </miscellaneous> <footer>  <system>   <sysCode>BTI09</sysCode>   <sysName/>  </system> </footer></fb_avail>',
                            request_type: 'post',
                            request_kwargs: {
                                urlencode_get: true
                            }
                        },
                        cabin_class: {
                            economy: 'E, Q, M, D, O, S, H, Y, C',
                            business: 'B',
                            first: 'F'
                        },
                        test_config: {
                            from_place: 'RIX',
                            to_place: 'BCN',
                            depart_date: '2014-05-11',
                            return_date: '2014-05-21',
                            pricing_type: '',
                            adults: '1',
                            children: '0',
                            infants: '0',
                            market: 'UK',
                            lang: 'en',
                            currency: 'GBP',
                            cabin_class: 'economy'
                        }
                    };
                }

                return settingsMap[setting];
            },

            /**
             * Save the Integration state
             *
             * @param  {[type]} postParam [This is the edited/updated Integration settings.]
             * @return {[type]}           [Responds with json whether the call was successful or not.]
             */
            save: function(postParam) {
                // postParam['website_id'] = localStorageService.get('selectedwebsite');
                postParam.website_id = resolveFakeWebsiteId();
                postParam = JSON.stringify(postParam);
                return $http({
                        method: 'POST',
                        url: config.API_URL + config.API_BASE + '/integrationstate/?format=json',
                        data: postParam,
                        headers: {'Content-Type': 'application/json'}
                    })
                    .then(function(response) {
                        // always try to replace the integration state with a new one when updated
                        localStorageService.remove('integrationstate');
                        return response.data;
                    });
            },

            /**
             * Get all the Integration states available.
             *
             * @return {[type]} [description]
             */
            get: function() {
                return $http.get(config.API_URL + config.API_BASE + '/integrationstate/?format=json&website_id=' +

                    // + localStorageService.get('selectedwebsite'))

                    resolveFakeWebsiteId())
                    .then(function(response) {
                        // modalInstance.close();
                        if (!response.data) {
                            alertService.add('danger', 'Something went wrong.');
                        }

                        return response.data;
                    });
            },

            getFromStorage: function() {
                var integrationstate = localStorageService.get('integrationstate');

                if (!integrationstate) {
                    try {
                        serviceObj.get(localStorageService.get('selectedwebsite')).then(function(response) {
                            localStorageService.add('integrationstate', response);
                            return response;
                        });
                    } catch (e) {
                        integrationstate = {};
                        alertService.add('danger', 'Can\'t retrieve your integration state.');
                    }
                } else {
                    return integrationstate;
                }

            },

            /**
             * Mapped cabin classes to be used with the Machine Learning Backend.
             *
             * @param  {[type]} cabinClassMapping [description]
             * @return {[type]}                   [description]
             */
            getCabinClasses: function(cabinClassMapping) {
                var cabinClasses = {
                    economy: [],
                    premium_economy: [],
                    business: [],
                    first: []
                };

                for (var key in cabinClassMapping) {
                    if (cabinClassMapping.hasOwnKey(key)) {
                        cabinClasses[cabinClassMapping[key]].push(key);
                    }
                }

                for (var key2 in cabinClasses) {
                    if (cabinClasses.hasOwnKey(key2)) {
                        cabinClasses[key2] = cabinClasses[key2].join();
                    }
                }

                return cabinClasses;
            }
        };

        return serviceObj;
    });

}());
