(function() {

'use strict';

angular.module('fcApp.technical').factory('regressionService', function(
    $http,
    $q,
    localStorageService,
    alertService,
    pollerService) {

    // var baseUrl = config.API_URL + config.API_BASE + '/connect_api/regression/async';

    /* If not used anywhere, please remove
    function regressionPoller(action, websiteId) {
        var dfr = $q.defer();
        var url = 'regression/' + action + '/async';

        // console.log('url:');
        // console.log(url);

        var trackingData = {
            url: url + '/',
            type: 'regressionTests',
            params: $.param({
                // website_id: websiteId
                // PE-791
                website_id: websiteId === 'a001' ? 'aljp' : websiteId
                // --PE-791
            }),
            method: 'GET',
            // websiteId: websiteId
            // PE-791
            websiteId: websiteId === 'a001' ? 'aljp' : websiteId
            // --PE-791

        };

        var startTime = moment();

        pollerService.start({
            method: 'post',
            url: url + '/'
        })
        .then(function(taskId) {
            var regressionPoller;

            if (taskId) {
                regressionPoller = pollerService.poll({
                    taskId: taskId,
                    url: url,
                    delay: 3000
                });

                regressionPoller.promise.then(null, null, function(res) {
                    if (regressionPoller) {
                        if (res.data.ready) {
                            regressionPoller.stop();
                            // a hack to ditch the poller; bug with this block getting registered multiple times when run in one view session
                            regressionPoller = null;

                            if (res.data.state === 'SUCCESS') {
                                mixpanelService.trackAPI(trackingData, true, startTime);
                                dfr.resolve(res.data.result);
                            } else {
                                mixpanelService.trackAPI(trackingData, false, startTime);
                                dfr.reject(res.data.state);
                            }
                        }
                    }
                });
            } else {
                mixpanelService.trackAPI(trackingData, false, startTime);
                dfr.reject();
            }
        }, function(err) {
            mixpanelService.trackAPI(trackingData, false, startTime);
            dfr.reject(err);
        });

        return dfr.promise;
    }
    */

    function getRegressionTestCases(websiteId) {
        return pollerService.genericPoller('v0.4/flights/regression/test_cases/async', websiteId);
    }

    function generateRegressionTests(websiteId) {
        return pollerService.genericPoller('v0.4/flights/regression/generate/async', websiteId);
    }

    function runRegressionTests(websiteId) {
        return pollerService.genericPoller('v0.4/flights/regression/run/async', websiteId);
    }

    return {
        getRegressionTestCases: getRegressionTestCases,
        generateRegressionTests: generateRegressionTests,
        runRegressionTests: runRegressionTests,
    };

});

}());
