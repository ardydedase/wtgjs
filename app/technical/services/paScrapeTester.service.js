(function() {

'use strict';

angular.module('fcApp.technical')
    .factory('paScrapeTesterService', function(
        $http,
        $filter,
        config,
        poller,
        localStorageService) {

        var serviceObj = {};

        serviceObj = {

            /**
             * Poller when starting the scrape
             * @param  {[type]} taskId [description]
             * @return {[type]}        [description]
             */
            startScrapesPoller: function(taskId) {
                console.log('PA start poller');
                var scrapesPoller = poller.get(config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapes/new/async', {
                    action: 'get',
                    delay: 6000,
                    argumentsArray: [
                        {
                            params: {
                                uid: taskId,
                                website_id: localStorageService.get('selectedwebsite')
                            },

                            ignoreLoadingBar: true

                        }
                    ]
                });
                return scrapesPoller;
            },

            /**
             * Send the initial request to server to start the scrapetester tasks.
             *
             * @param  {[type]} scraperSettings [description]
             * @return {[type]}                 [description]
             */
            start: function(scraperSettings) {
                // update cabin classes
                $.extend(scraperSettings, {
                    website_id: localStorageService.get('selectedwebsite')
                });
                console.log('PA_start');
                return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapes/new/',
                    data: JSON.stringify(scraperSettings),
                    ignoreLoadingBar: true,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });
            },

            /**
             * Run scrape tests
             * @param  {[type]} scraperSettings [description]
             * @return {[type]}                 [description]
             */
            run: function(scraperSettings) {
                // update cabin classes
                if (scraperSettings.user_currency.constructor !== Array) {
                    scraperSettings.user_currency = scraperSettings.user_currency.split(',');
                }

                if (scraperSettings.user_languages.constructor !== Array) {
                    scraperSettings.user_languages = scraperSettings.user_languages.split(',');
                }

                if (scraperSettings.user_countries.constructor !== Array) {
                    scraperSettings.user_countries = scraperSettings.user_countries.split(',');
                }

                $.extend(scraperSettings, {
                    website_id: localStorageService.get('selectedwebsite')
                });

                if ('created_at' in scraperSettings) {
                    delete scraperSettings.created_at;
                }

                return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapetester/',
                    data: JSON.stringify(scraperSettings),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });
            },

            /**
             * Check the scrape test status
             * @param  {[type]} taskId [description]
             * @return {[type]}        [description]
             */
            checkScrape: function(taskId) {

                var getParams = {
                    website_id: localStorageService.get('selectedwebsite'),
                    uid: taskId
                };

                return $http({
                    method: 'GET',
                    url: config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapetester/?',
                    params: getParams,
                    ignoreLoadingBar: true,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });

            },

            /**
             * Get the scrape tests, all per website or by ID
             * @param  {[type]} scrapeId [description]
             * @return {[type]}          [description]
             */
            getScrapeTests: function(scrapeId) {
                var getParams = {
                    website_id: localStorageService.get('selectedwebsite')
                };

                if (typeof scrapeId !== 'undefined') {
                    $.extend(getParams, {
                        scrape_id: scrapeId
                    });
                }

                return $http({
                    method: 'GET',
                    url: config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapes/past/?',
                    params: getParams,
                    ignoreLoadingBar: true,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });
            },

            /**
             * Poller when starting the scrape
             * @param  {[type]} taskId [description]
             * @return {[type]}        [description]
             */
            getScrapeTestsPoller: function(taskId) {
                var apiUrl = config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapes/past/async';

                var sc = poller.get(apiUrl, {
                    action: 'get',
                    delay: 6000,
                    argumentsArray: [
                        {
                            params: {
                                uid: taskId,
                                website_id: localStorageService.get('selectedwebsite')
                            },

                            ignoreLoadingBar: true
                        }
                    ]
                });

                return sc;
            },

            /**
             * Get all the test cases per website ID
             * @param  {[type]} scrapeId [description]
             * @return {[type]}          [description]
             */
            getTestCases: function(scrapeId) {
                var getParams = {
                    website_id: localStorageService.get('selectedwebsite'),
                    testsuite: scrapeId
                };

                return $http({
                    method: 'GET',
                    url: config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapetestcases/?',
                    params: getParams,
                    ignoreLoadingBar: true,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });

            },

            /**
             * Poller when starting the scrape
             * @param  {[type]} taskId [description]
             * @return {[type]}        [description]
             */
            getTestCasesPoller: function(taskId) {
                var apiUrl = config.API_URL + config.API_BASE + '/v0.4/flights/pa_scrapetestcases/async';

                var sc = poller.get(apiUrl, {
                    action: 'get',
                    delay: 6000,
                    argumentsArray: [
                        {
                            params: {
                                uid: taskId,
                                website_id: localStorageService.get('selectedwebsite')
                            },

                            ignoreLoadingBar: true
                        }
                    ]
                });

                return sc;
            },
        };

        return serviceObj;

    });

}());
