(function() {

'use strict';

angular.module('fcApp.technical').factory('testService', function(
    $http,
    $location,
    $analytics,
    $timeout,
    alertService,
    authService,
    modalService,
    quoteService,
    localStorageService,
    config,
    mixpanelService,
    poller) {

    var serviceObj = {};

    serviceObj = {
        /**
         * Cabin class mapping with Jacquard API.
         * @type {Object}
         */
        cabinClassMap: {
            economy: 'Economy',
            premium_economy: 'Premium Economy',
            business: 'Business',
            first: 'First'
        },

        /**
         * Get autosuggest using geo services.
         * Makes it easier for users enter city and airport IATA codes.
         *
         * @param  {[type]} entry [description]
         * @return {[type]}       [description]
         */
        getLocationAutoSuggest: function(entry) {

            var getParams = {
                entry: entry,
                market: 'UK',
                website_id: localStorageService.get('selectedwebsite')
            };

            return $http({
                    method: 'GET',
                    url: config.API_URL + config.API_BASE + '/autosuggest/?',
                    params: getParams,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });
        },

        /**
         * Get the replay link for reproducing the test case.
         * @param  {[type]} row       [description]
         * @param  {[type]} webconfig [description]
         * @return {[type]}           [description]
         */
        getReplayLink: function(row, webconfig) {
            var queryString = {
                adults: row.adult,
                children: row.child,
                infants: row.infant,
                cabinclass: row.class,
                datacenter: 'sg1',
                filter: true,
                ucy: webconfig.market,
                lang: webconfig.lang,
                ccy: webconfig.currency,
                website_id: localStorageService.get('selectedwebsite')
            };

            var pathString = [row.from, row.to, row.out.slice(0, 10).replace(/-/g, '')];

            if (row.back) {
                pathString.push(row.back.slice(0, 10).replace(/-/g, ''));
            }

            return '/tests/transport/flights/' + pathString.join('/') + '/?' + $.param(queryString);
        },

        /**
         * Get month name.
         *
         * @param  {[type]} month_num [description]
         * @return {[type]}           [description]
         */
        getMonthName: function(monthNum) {
            var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];

            return monthNames[parseInt(monthNum - 1, 10)];
        },

        /**
         * Update the test config in the local storage
         * @param  {[type]} test_config [description]
         * @return {[type]}             [description]
         */
        updateTestConfig: function(testConfig) {
            localStorageService.remove('testconfig');
            localStorageService.add('testconfig', testConfig);
        },

        /**
         * Custom service to format date (we might have to deprecate this and use angular's date format function)
         * @param  {[type]} date_str [description]
         * @return {[type]}          [description]
         */
        formatDate: function(dateStr) {
            var dateRes = dateStr.split('-');
            var monthName = serviceObj.getMonthName(dateRes[1]);

            return dateRes[2] + ' ' + monthName + ' ' + dateRes[0];
        },

        /**
         * Check whether a test case is in the past.
         * @param  {[type]}  testConfig [description]
         * @return {Boolean}            [description]
         */
        isInThePast: function(testConfig) {
            try {
                var departDate = parseInt(testConfig.depart_date.replace(/-/g, ''));
                var d = new Date();
                var now = parseInt('' + d.getFullYear() + ('0' + (d.getMonth() + 1)).slice(-2) + ('0' + d.getDate()).slice(-2));

                if (departDate < now) {
                    return true;
                } else {
                    return false;
                }
            } catch (e) {
                return false;
            }
        },

        /**
         * offset the current date
         * @param  {[type]} dateToOffset [description]
         * @return {[type]}              [description]
         */
        offsetDate: function(offsetDays) {
            var newDate = new Date();
            newDate.setDate(newDate.getDate() + offsetDays);

            // newDate = [newDate.getFullYear(), ("0" + (newDate.getMonth() + 1)).slice(-2), ("0" + (newDate.getDate() + 1)).slice(-2)].join("-");

            return newDate;
        },

        /**
         * Get the default test config.
         * Primarily used for testing e.g. protractor e2e tests.
         *
         * @return {[type]} [description]
         */
        getDefaultTestConfig: function() {
            var departureDate = serviceObj.offsetDate(30);
            var returnDate = serviceObj.offsetDate(37);

            var defaultTestConfig = {
                from_place: '',
                to_place: '',
                depart_date: departureDate,
                return_date: returnDate,
                lang: 'en',
                market: 'SG',
                currency: 'SGD',
                adults: '1',
                children: '0',
                infants: '0',
                cabin_class: 'economy',
                datacenter: 'sg1'
            };

            return (localStorageService.get('testconfig')) ? localStorageService.get('testconfig') : defaultTestConfig;
        },

        /**
         * Save test cases
         * @param  {[type]} transport_config [description]
         * @param  {[type]} uid       [description]
         * @return {[type]}           [description]
         */
        save: function(transportConfig, uid) {
            $analytics.eventTrack('save-' + $location.path(), {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username
            });

            mixpanelService.track('FlightsConnect Saved Test Case');

            $.extend(transportConfig, {
                return_date: (typeof transportConfig.return_date === 'undefined') ? '' : transportConfig.return_date,
                depart_date: transportConfig.depart_date
            });

            if (typeof uid !== 'undefined') {
                transportConfig = $.extend(transportConfig, {
                    uid: uid
                });
            }

            transportConfig = JSON.stringify({data: [transportConfig], website_id: localStorageService.get('selectedwebsite')});

            return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/yamltestcase/?' + $.param({
                        format: 'json',
                        website_id: localStorageService.get('selectedwebsite')
                    }),
                    data: transportConfig,
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function(response) {
                    return response.data;
                });
        },

        /**
         * Prepare test sessions.
         * Supposed to speed up loading the route service on Opstest.
         *
         * @param  {[type]} testConfig [description]
         * @return {[type]}            [description]
         */
        prepTestSessions: function(testConfig) {
            testConfig.website_id = localStorageService.get('selectedwebsite');

            return $http({
                    method: 'GET',
                    url: config.API_URL + config.API_BASE + '/prepjacquard/?',
                    params: testConfig,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });
        },

        getTestCasePoller: function(taskId) {
            var apiUrl = config.API_URL + config.API_BASE + '/yamltestcase/async/';

            var sc = poller.get(apiUrl, {
                action: 'get',
                delay: 6000,
                argumentsArray: [
                    {
                        params: {
                            uid: taskId,
                            website_id: localStorageService.get('selectedwebsite')
                        },
                        ignoreLoadingBar: true
                    }
                ]
            });

            return sc;
        },

        /**
         * Get the Test Configs/Cases
         * @param  {[type]} id [description]
         * @return {[type]}    [description]
         */
        get: function(id, skip, limit) {
            $analytics.eventTrack('open-' + $location.path(), {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username
            });

            var API_ACTION = (typeof id !== 'undefined') ? (config.API_BASE + '/yamltestcase/' + id + '/') : config.API_BASE + '/yamltestcase/';
            var startDate = new Date();

            var getParams = {
                website_id: localStorageService.get('selectedwebsite') === 'a001' ? 'aljp' : localStorageService.get('selectedwebsite'),
                skip: skip,
                limit: limit
            };

            return $http({
                    method: 'GET',
                    url: config.API_URL + API_ACTION + '?',
                    params: getParams,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                var dateDiff = Math.abs(new Date() - startDate);
                if (id) {
                    // for a specific test case
                    $analytics.eventTrack('openTestCaseTimeSec-' + response.data.datacenter, {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username,
                        value: (dateDiff / 1000)
                    });

                    mixpanelService.track('FlightsConnect Loaded Specific Test Case', {
                        'Test Case Id': id
                    });
                } else {
                    // for loading all test cases
                    $analytics.eventTrack('loadAllTestCasesTimeSec-' + response.data.datacenter, {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username,
                        value: (dateDiff / 1000)
                    });
                    mixpanelService.track('FlightsConnect Loaded Test Cases');

                    mixpanel.register_once({
                        Username: localStorageService.get('account').username,
                        UserType: (localStorageService.get('account').username.indexOf('skyscanner') > -1 || localStorageService.get('account').username === 'miland') ? 'Skyscanner' : 'Partner'
                    });
                }

                return response.data;

            });
        },

        /**
         * Get the Deeplink Object
         * @param  {[type]} testConfig            [description]
         * @param  {[type]} transportDeeplinkYaml [description]
         * @return {[type]}                       [description]
         */
        getDeeplink: function(testConfig, transportDeeplinkYaml) {
            $.extend(testConfig, {
                website_id: localStorageService.get('selectedwebsite'),
                deeplink_yaml: transportDeeplinkYaml,
                depart_date: testConfig.depart_date.substring(0, 10),
                return_date: testConfig.return_date.substring(0, 10)
            });

            return $http({
                method: 'POST',
                url: config.API_URL + config.API_BASE + '/dayview/',
                data: JSON.stringify(testConfig),
                headers: {'Content-Type': 'application/json'}
            }).then(function(response) {
                return response.data;
            });
        },

        /**
         * Get the constructed skippy deeplink URL.
         *
         * @param  {[type]} test_data [description]
         * @param  {[type]} quote     [description]
         * @param  {[type]} oneway    [description]
         * @return {[type]}           [description]
         */
        getSkippyDeeplink: function(testData, quote, oneway) {
            $analytics.eventTrack('getSkippyDeeplink-' + $location.path(), {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username
            });
            mixpanelService.track('FlightsConnect Deeplinked');

            if (oneway) {
                testData.return_date = '';
            }

            var transportConfig = {
                website_id: localStorageService.get('selectedwebsite'),
                test_data: testData,
                quote: quote
            };

            var startDate = new Date();

            return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/deeplinkurl/',
                    data: JSON.stringify(transportConfig),
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function(response) {
                    var dateDiff = Math.abs(new Date() - startDate);

                    $analytics.eventTrack('getDeeplinkTimeSec', {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username,
                        value: (dateDiff / 1000)
                    });

                    return response.data;
                });
        },

        /**
         * Get the Task UID that will be used to check the Price Accuracy Results
         * @param  {[type]} test_data [description]
         * @param  {[type]} quote     [description]
         * @param  {[type]} oneway    [description]
         * @return {[type]}           [description]
         */
        getPriceAccuracyUID: function(testData, quote, oneway) {
            $analytics.eventTrack('getPriceAccuracyUID-' + $location.path(), {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username
            });
            mixpanelService.track('FlightsConnect Requested PA Check');

            if (oneway) {
                testData.return_date = '';
            }

            var transportConfig = {
                website_id: localStorageService.get('selectedwebsite'),
                test_data: testData,
                quote: quote
            };

            return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/pacheck/',
                    data: JSON.stringify(transportConfig),
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function(response) {
                    return response.data;
                });
        },

        /**
         * Get PA Request to get the PA Results
         *
         * @param  {[type]} uid [description]
         * @return {[type]}     [description]
         */
        checkPARequest: function(uid, datacenter) {

            var getParams = {
                website_id: localStorageService.get('selectedwebsite'),
                uid: uid,
                datacenter: datacenter
            };

            return $http({
                method: 'GET',
                url: config.API_URL + config.API_BASE + '/pacheck/' + '?',
                params: getParams,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(response) {
                return response.data;
            });
        },

        /**
         * Redirect to the deeplink.
         * This will open a new tab/window.
         *
         * @param  {[type]} url       [description]
         * @param  {[type]} form_data [description]
         * @param  {[type]} method    [description]
         * @return {[type]}           [description]
         */
        redirectToDeeplink: function(url, formData, method) {
            $analytics.eventTrack('redirectToDeeplink-' + $location.path(), {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username
            });

            mixpanelService.track('FlightsConnect Sent User - Deeplink');

            var redirect = function(form, url, method) {
                form.method = method;
                form.action = url;
                form.setAttribute('target', '_blank');
                document.body.appendChild(form);
                form.submit();
            };

            var form = document.createElement('form');

            if (formData) {
                $.each(formData, function(name, value) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = name;
                    input.value = value;

                    form.appendChild(input);
                });
            }

            if (method === 'get') {
                window.open(url);
            } else {
                redirect(form, url, method);
            }
        },

        /**
         * Get test config from local storage if available.
         * @return {[type]} [description]
         */
        getFromStorage: function() {
            var testconfigs = localStorageService.get('testconfigs');

            // get it from the backend if it's not in the localStorage
            if (!testconfigs) {
                console.log('no test configs found in local storage');
                serviceObj.get().then(function(response) {
                    localStorageService.add('testconfigs', response);
                    return response;
                });
            } else {
                return testconfigs;
            }
        },

        /**
         * Delete a test case.
         *
         * @param  {[type]} uid [description]
         * @return {[type]}     [description]
         */
        delete: function(uid) {
            $analytics.eventTrack('delete-' + $location.path(), {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username
            });

            mixpanelService.track('FlightsConnect Deleted Test Case');

            // delete test case
            return $http.delete(config.API_URL + config.API_BASE + '/yamltestcase/' + uid + '/?format=json&website_id=' + localStorageService.get('selectedwebsite'), {cache: false})
                .then(function(response) {
                    console.log('response after deleting the test case successfully:');
                    console.log(response);

                    return response.data;
                });
        },

        /**
         * Get the useful debug/repro links for the dropdown.
         * Which is displayed together with the search results.
         *
         * @param  {[type]} transport_config [description]
         * @return {[type]}           [description]
         */
        getReproLinks: function(transportConfig, option) {
            if (typeof option === 'undefined') {
                option = 'save';
            }

            $.extend(transportConfig, {
                return_date: (typeof transportConfig.return_date === 'undefined') ? '' : transportConfig.return_date.slice(0, 10),
                depart_date: transportConfig.depart_date.slice(0, 10),

                // should be in uppercase
                from_place: transportConfig.from_place.toUpperCase(),
                to_place: transportConfig.to_place.toUpperCase()
            });

            var runWithFilters = transportConfig.filter;

            // if (transportYaml) {
            //     $.extend(transportConfig, {atd_yaml: transportYaml});
            // }

            // this should follow the new format. it should be the same as how we save test cases.
            transportConfig = {data: [transportConfig], website_id: localStorageService.get('selectedwebsite')};

            var getParam = {
                website_id: localStorageService.get('selectedwebsite'),
                option: option,
                filter: runWithFilters ? 'on' : ''
            };

            return $http({
                    method: 'POST',
                    url: config.API_URL + config.API_BASE + '/reprolinks/?' + $.param(getParam),
                    data: JSON.stringify(transportConfig),
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function(response) {
                    return response.data.jac_response;
                });
        },

        /**
         * Handle the integration state.
         * This is meant for new integrations.
         *
         * @param  {[type]} transport_config [description]
         * @return {[type]}           [description]
         */
        handleIntegrationState: function(transportConfig, transportYaml) {
            //MBS: Need to insert marker for open axis, this is temporary until we get open axis to use pure GSF
            var integrationstate = localStorageService.get('integrationstate');

            try {
                //__OPEN_AXIS__ and __INCLUDE__ will be use by gene\transport.py to identify open axis config yaml

                if ((integrationstate.engine_type || 'custom') === 'open_axis') {
                    transportYaml = (transportYaml.trim()) ? '__OPEN_AXIS__' + transportYaml + '__INCLUDE__' : '';
                }

                console.log('----TEST SERVICE INTEGRATION STATE ---');
                console.log(transportYaml);

            } catch (e) {
                console.log(e);
            }

            if (transportYaml) {
                $.extend(transportConfig, {atd_yaml: transportYaml});
            }

            return transportConfig;
        },

        /**
         * Get the running task
         * @param  {[type]} uid [description]
         * @return {[type]}     [description]
         */
        getRunningTest: function(uid) {

            var getParams = {
                website_id: localStorageService.get('selectedwebsite'),
                uid: uid
            };

            return $http({
                    method: 'GET',
                    url: config.API_URL + config.API_BASE + '/polljacquard/' + '?',
                    params: getParams,
                    ignoreLoadingBar: true,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(response) {
                    return response.data;
                });

        },

        /**
         * Run test by Test ID.
         * @param  {[type]} test_id [description]
         * @return {[type]}         [description]
         */
        getRunningTestByTestId: function(testId) {
            var tasks = localStorageService.get('tasks');

            for (var x in tasks) {
                if (tasks[x].test_id === testId) {
                    return tasks[x];
                }
            }

            return null;
        },

        /**
         * [removeRunningTestByTestId description]
         * @param  {[type]} test_id [description]
         * @return {[type]}         [description]
         */
        removeRunningTestByTestId: function(testId) {
            var tasks = localStorageService.get('tasks');
            for (var x in tasks) {
                if (tasks[x].test_id === testId) {
                    tasks.splice(x, 1);
                    localStorageService.remove('tasks');
                    localStorageService.add('tasks', tasks);
                    return tasks[x];
                }
            }

            return null;
        },

        /**
         * Cancel running task
         * @return {[type]} [description]
         */
        cancelRunningTest: function(uid) {
            return $http.delete(config.API_URL + config.API_BASE + '/polljacquard/' + '?' + $.param({
                    website_id: localStorageService.get('selectedwebsite'),
                    uid: uid
                })).then(function(response) {
                    console.log('cancelRunningTask  response:');
                    console.log(response);

                    return response.data;
                });
        },

        getIPInfo: function() {
            return $http.jsonp('http://ipinfo.io').then(function(response) {
                return response;
            });
        },

        /**
         * Run Jacquard Tests
         * @param  {[type]} transport_config     [description]
         * @param  {[type]} transport_yaml [description]
         * @param  {[type]} option        [description]
         * @return {[type]}               [description]
         */
        run: function(testConfig, transportYaml, option, poll) {
            var runWithFilters = testConfig.filter;
            var datacenter = testConfig.datacenter;

            // GA Tracking
            var gaFilter = (testConfig.filter) ? 'filter' : 'nofilter';
            $analytics.eventTrack('run-' + gaFilter + '-' + $location.path(), {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username,
            });

            mixpanelService.track('FlightsConnect Ran Test Case', {
                Filter_Type: gaFilter
            });

            if (typeof option === 'undefined') {
                option = 'save';
            }

            // try {
            //     var returnDate = (typeof testConfig.return_date === 'undefined') ? '' : testConfig.return_date.toString().substring(0, 10);
            // } catch (e) {
            //     var returnDate = '';
            // }

            // var departDate = testConfig.depart_date.toString().substring(0, 10);

            testConfig = serviceObj.handleIntegrationState(testConfig, transportYaml);
            serviceObj.updateTestConfig(testConfig);

            // this should follow the new format. it should be the same as how we save test cases.
            testConfig = {data: [testConfig], website_id: localStorageService.get('selectedwebsite')};

            var getParam = {
                website_id: localStorageService.get('selectedwebsite'),
                option: option,
                filter: runWithFilters ? 'on' : ''
            };

            var startDate = new Date();
            var apiCall = poll ? config.API_BASE + '/polljacquard/' : config.API_BASE + '/yamljacquard/';

            return $http({
                    method: 'POST',
                    url: config.API_URL + apiCall + '?' + $.param(getParam),
                    data: JSON.stringify(testConfig),
                    headers: {'Content-Type': 'application/json'}
                })
                .then(function(response) {
                    var dateDiff = Math.abs(new Date() - startDate);

                    $analytics.eventTrack('runTestTimeSec-' + gaFilter + '-' + datacenter, {
                        category: localStorageService.get('selectedwebsite'),
                        label: localStorageService.get('account').username,
                        value: (dateDiff / 1000)
                    });

                    return response.data;
                });
        },
    };

    return serviceObj;
});

}());
