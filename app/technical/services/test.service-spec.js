(function() {

'use strict';

describe('Service: testService', function() {

    var testService;

    beforeEach(module('fcApp.technical'));

    beforeEach(module('fcApp.technical', function($provide) {
        $provide.value('mixpanelService', {
            track: function() {}
        });
    }));

    beforeEach(inject(function(_testService_) {
        testService = _testService_;
    }));

    it('should do something', function() {
        expect(!!testService).toBe(true);
    });

    it('should run tests', function() {
        var postParam = testService.getDefaultTestConfig();

        $.extend(postParam, {
            atd_yaml: 'register:\r\n     modes:\r\n          - flight\r\n     supported_types:\r\n          - S \r\n          - B\r\n     postprocess:\r\n          - commercial_rules\r\n     deeplink_cache_settings:\r\n          timeout_seconds: 600\r\n          usage_count: 0\r\n     support: \r\n\r\n               implemented:  L\n               notes:  \"\"\n               passenger_limits:  \"\"\n               possible:  L\n               reviewed_date:  2014-03-16\n               source:  api\r\ncabin_class_mapping: &cabin_class_mapping\r\n\r\n     B:  business\n     D:  economy\n     E:  economy\n     F:  first\n     H:  economy\n     M:  economy\n     O:  economy\n     Q:  economy\n     Z:  economy\n     A:  economy\n     S:  economy\n     C:  economy\n     Y:  economy\n\nquery_validator: \r\n\r\n     max_adults:  8\n     max_children:  8\n     max_days_in_future:  336\n     max_infants:  8\n     max_passengers:  8\n\ndata_requester_class: \r\n     - SimpleDataRequester\n     - DeeplinkCacheCompatibleDataRequester\n\r\ndata_requester:\r\n\r\n\r\n\r\n                         response_type:  xml\r\n                         search_results: \r\n\r\n                                   get: \r\n\r\n                                             parameters: \r\n\r\n                                                       action:  avail\n                                                       btisess:  BTI09_SKY_LV_en_D91C81A723BB3FEF763166B04B6488BA\n                                                       day0:  23\n                                                       day1:  23\n                                                       destin:  \"{query.to_place.iata_id}\"\n                                                       k:  b030ce0352ace673695fcfae45712ae7\n                                                       l:  en\n                                                       legs:  1\n                                                       month0:  12-2014\n                                                       month1:  12-2014\n                                                       numadt:  \"{query.passengers.adults}\"\n                                                       numchd:  \"{query.passengers.children}\"\n                                                       numinf:  \"{query.passengers.infants}\"\n                                                       origin:  \"{query.from_place.iata_id}\"\n                                                       p:  SKY\n                                                       range0:  3\n                                                       range1:  3\n                                                       rmselect:  0\n                                                       traveltype:  bti\n                                   request_kwargs: \r\n\r\n                                             urlencode_get:  True\n                                   url:  https://tickets1.airbaltic.com/app/fb.fly\r\n\r\n\r\n\r\n\r\n\n\nsegment_parser_class: SimpleSegmentParser\r\nsegment_parser:\r\n          root: !xpath ./journey/itinerary/flight\n          segment: \r\n\r\n                         arr_time: !datetime\r\n\r\n                                   \"%H:%M\": !xpath ./destination/arrTime/text()\n                                   \"%Y-%m-%d\": !xpath ./destination/arrDate/text()\n                         cabin_class: !mapped\r\n\r\n                                   mapping:  *cabin_class_mapping\n                                   value: !xpath ../../bkgClass/text()\n                         dep_time: !datetime\r\n\r\n                                   \"%H:%M\": !xpath ./origin/depTime/text()\n                                   \"%Y-%m-%d\": !xpath ./origin/depDate/text()\n                         from_place: !place\r\n\r\n                                   namespace: iata \n                                   place_code: !xpath ./origin/origCode/text()\n                                   type: airport \n                         marketing_carrier_id: !carrier\r\n\r\n                                   carrier_id: !xpath ./carrier/carrCode/text()\n                                   mode: flight \n                                   namespace: iata \n                         marketing_segment_code: !xpath ./@id\n                         mode: flight \n                         to_place: !place\r\n\r\n                                   namespace: iata \n                                   place_code: !xpath ./destination/destCode/text()\n                                   type: airport \r\n\nquote_parser_class: \r\n     - QuoteListQuoteParser\n     - DayviewDeeplinkCacheCompatibleQuoteParser\n\r\nquote_parser:\r\n          root: !xpath /fb_avail/offers/offer\r\n          outbound: !xpath .\n          inbound: !xpath .\r\n\r\n          price: !price\r\n\r\n                         ctaxpx: !xpath ./totalPrice/CHD/text()\n                         currency_id: !xpath ./totalPrice/currency/text()\n                         itaxpx: !xpath ./totalPrice/INF/text()\n                         taxpx: !xpath ./totalPrice/ADT/text()\n                         totaltaxpx: !xpath ./totalPrice/GROSS/text()\r\n\r\n\r\n\r\n\r\n\n'
        });

        expect(postParam.lang).toBe('en');
    });

    it('should be able to get the correct dates from default test config', function() {
        var departureDate = new Date();
        var returnDate = new Date();

        departureDate.setDate(departureDate.getDate() + 30);
        returnDate.setDate(returnDate.getDate() + 37);

        var testConfig = testService.getDefaultTestConfig();

        // expect(testConfig.depart_date).toBe(departureDate);
        // expect(testConfig.return_date).toBe(returnDate);

        expect(testConfig.depart_date).toBeDefined();
        expect(testConfig.return_date).toBeDefined();
    });

});

}());
