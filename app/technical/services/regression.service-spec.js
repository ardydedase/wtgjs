(function() {

'use strict';

describe('Service: regressionService', function() {
    var regressionService;
    var mockPollerService;
    var httpBackend;
    var rootScope;
    var q;
    var config;
    var baseUrl;

    beforeEach(module('fcApp.technical', function($provide) {
        mockPollerService = {
            start: function() {
                var dfr = q.defer();
                return dfr.promise;
            },

            poll: function() {
                var dfr = q.defer();
                return dfr;
            },

            genericPoller: function() {
                var dfr = q.defer();
                return dfr;
            }
        };

        $provide.value('pollerService', mockPollerService);
    }));

    beforeEach(inject(function(_regressionService_, $httpBackend, $rootScope, $q, _config_) {
        regressionService = _regressionService_;
        httpBackend = $httpBackend;
        rootScope = $rootScope.$new();
        q = $q;
        config = _config_;

        baseUrl = config.API_URL + config.API_BASE;
    }));

    describe('#getRegressionTestCases', function() {
        it('should be defined', function() {
            expect(regressionService.getRegressionTestCases).toBeDefined();
        });

        it('should return a promise', function() {
            var startDfr = q.defer();
            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'genericPoller').and.returnValue(startDfr.promise);

            regressionService.getRegressionTestCases('aaaa').then(
                function() {},

                function(res) {
                    expect(res).toBeDefined();
                });

            startDfr.reject('test');
            rootScope.$apply();
        });

        it('should call genericPoller with correct URL and websiteID', function() {
            spyOn(mockPollerService, 'genericPoller').and.callThrough();

            regressionService.getRegressionTestCases('aaaa');

            expect(mockPollerService.genericPoller).toHaveBeenCalledWith('v0.4/flights/regression/test_cases/async', 'aaaa');
        });

        it('should resolve failure if pollerService.start returns a failure', function() {
            var startDfr = q.defer();
            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'genericPoller').and.returnValue(startDfr.promise);

            regressionService.getRegressionTestCases('aaaa').then(
                function() {},

                function(err) {
                    expect(err).toEqual('Nooo!');
                });

            startDfr.reject('Nooo!');
            rootScope.$apply();
        });

        it('should resolve failure if pollerService.starts does not return taskId', function() {
            var startDfr = q.defer();
            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'genericPoller').and.returnValue(startDfr.promise);

            regressionService.getRegressionTestCases('aaaa').then(
                function() {},

                function() {
                    expect(true).toEqual(true); //nothing to assert, just make sure reject goes through
                });

            startDfr.resolve();
            rootScope.$apply();
        });

        it('should resolve result if polling state is SUCCESS', function() {
            var startDfr = q.defer();
            var pollDfr = q.defer();
            pollDfr.stop = function() {};

            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'poll').and.returnValue(pollDfr);
            spyOn(mockPollerService, 'genericPoller').and.returnValue(startDfr.promise);

            regressionService.getRegressionTestCases('aaaa').then(function(res) {
                expect(res).toEqual('uuuuuuuid');
            });

            startDfr.resolve('uuuuuuuid');
            rootScope.$apply();

            pollDfr.notify({ data: {
                ready: true,
                state: 'SUCCESS',
                result: 'hello'
            }});

            rootScope.$apply();
        });

        it('should reject result if polling state is anything but SUCCESS', function() {
            var startDfr = q.defer();
            var pollDfr = q.defer();
            pollDfr.stop = function() {};

            spyOn(mockPollerService, 'start').and.returnValue(startDfr.promise);
            spyOn(mockPollerService, 'poll').and.returnValue(pollDfr);
            spyOn(mockPollerService, 'genericPoller').and.returnValue(startDfr.promise);

            regressionService.getRegressionTestCases('aaaa').then(
                function() {},

                function(err) {
                    expect(err).toEqual('ANYTHING BUT SUCCESS');
                });

            startDfr.resolve('uuuuuuuid');
            rootScope.$apply();

            pollDfr.notify({ data: {
                ready: true,
                state: 'ANYTHING BUT SUCCESS'
            }});
            rootScope.$apply();
        });
    });
});

}());
