(function() {

'use strict';

describe('Service: scrapeTesterService', function() {

    // load the service's module
    beforeEach(module('fcApp.technical'));

    // instantiate service
    var scrapeTesterService;
    beforeEach(inject(function(_scrapeTesterService_) {
        scrapeTesterService = _scrapeTesterService_;
    }));

    it('should do something', function() {
        expect(!!scrapeTesterService).toBe(true);
    });

});

}());
