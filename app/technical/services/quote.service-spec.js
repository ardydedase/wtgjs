(function() {

'use strict';

describe('Service: quoteService', function() {
    var $httpBackend;

    beforeEach(module('fcApp.technical'));

    var quoteService;

    beforeEach(inject(function(_quoteService_, $injector) {
        $httpBackend = $injector.get('$httpBackend');
        $httpBackend
            .when('GET', '/auth.py')
            .respond({userId: 'userX'}, {'A-Token': 'xxx'});

        quoteService = _quoteService_;
    }));

    it('should do something', function() {
        expect(!!quoteService).toBe(true);
    });

    it('should return me the airlineID only', function() {
        expect(quoteService.getAirlineID('IATA: JT')).toBe('JT');
    });

    it('should check JSON string correctly', function() {
        var jsonstr = '{"username": "ardy", "email": "ardy.dedase@skyscanner.net"}';
        expect(quoteService.isJSON(jsonstr)).toBe(true);
    });

    it('should check XML string correctly', function() {
        var xmlstr = '<?xml version="1.0" encoding="UTF-8"?><note><to>Tove</to><from>Jani</from><heading>Reminder</heading><body>Don\'t forget me this weekend!</body></note>';
        expect(quoteService.isXML(xmlstr)).toBe(true);
    });
});

}());
