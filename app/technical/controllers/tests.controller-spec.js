(function() {

'use strict';

describe('Controller: TestsCtrl', function() {

    var controller;
    var defaultMocks;
    var q;
    var scope;

    var noop = function() {};

    var createPromise = function() {
        var dfr = q.defer();
        return dfr.promise;
    };

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('TestsCtrl', {
            $scope: scope,
            localStorageService: mocks.localStorageService,
            conversionPixelHealthService: mocks.conversionPixelHealthService,
            authService: mocks.authService,
            testService: mocks.testService,
            integrationService: mocks.integrationService,
            mixpanelService: mocks.mixpanelService,
            moment: window.moment,
            URI: window.URI
        });
    };

    beforeEach(module('fcApp.technical'));

    beforeEach(inject(function($controller, $rootScope, $location, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        scope = $rootScope.$new();
        scope.websites = ['xpsg'];
        scope.test_config = {
            lang: 'en',
            market: 'SG',
            currency: 'SGD'
        };

        scope.sampleTestConfig = {
            active: true,
            adults: '2',
            cabin_class: 'economy',
            children: '1',
            counter: 1,
            created_at: '2015-03-20T18:31:44.598Z',
            currency: 'IDR',
            datacenter: 'sg1',
            depart_date: '2015-04-20',
            doc_type: 'jacquard_test_case',
            from_place: 'CGK',
            infants: '1',
            lang: 'id',
            lastrun: {status: 'successful', date: '2015-03-23T14:13:38.660249'},
            market: 'ID',
            return_date: '2015-04-27',
            task_id: '',
            testsuite: '',
            to_place: 'KUL',
            type: 'flights',
            uid: '67c2757e18fe',
            website_id: 'nusa'
        };

        defaultMocks = {
            localStorageService: {
                get: function() {
                    var returnData = {
                        username: 'miland'
                    };
                    return returnData;
                },

                remove: function() {
                    return 'removed';
                },

                add: function() {
                    return 'added';
                }
            },
            conversionPixelHealthService: {
                getData: createPromise(),
                formatData: noop,
                getChart: noop,
                getFailures: noop,
                getChartData: noop,
                updateChart: noop
            },
            authService: {
                getUserType: function() {
                    return 'King';
                }
            },
            testService: {
                save: createPromise(),
                get: createPromise(),
                delete: createPromise(),
                isInThePast: noop,
                offsetDate: noop,
                formatData: noop,
                getDefaultTestConfig: noop,
                getLocationAutoSuggest: createPromise(),
                getRunningTest: createPromise(),
                getRunningTestByTestId: noop,
                cancelRunningTest: createPromise(),
                removeRunningTestByTestId: noop,
                run: createPromise(),
                checkPARequest: createPromise(),
                getTestCasePoller: createPromise(),
                getDeeplink: createPromise(),
                redirectToDeeplink: noop
            },
            integrationService: {
                get: createPromise()
            },
            mixpanelService: {
                track: noop
            }
        };
    }));

    // var test_result_json = {"answers":[{"journey":{"quotes":[{"legs":{"leg_1":[{"arr_time":"2014-06-18T19:00:00","cabin_class":"economy","dep_time":"2014-06-18T17:40:00","from_place":11052,"from_place_iata":"DMK","marketing_carrier_id":"skyscanner:DD","marketing_segment_code":7920,"mode":"flight","operating_carrier_id":"skyscanner:DD","ord":1,"to_place":12917,"to_place_iata":"KBV"}],"leg_2":[{"arr_time":"2014-06-25T21:20:00","cabin_class":"economy","dep_time":"2014-06-25T20:00:00","from_place":12917,"from_place_iata":"KBV","marketing_carrier_id":"skyscanner:DD","marketing_segment_code":7921,"mode":"flight","operating_carrier_id":"skyscanner:DD","ord":1,"to_place":11052,"to_place_iata":"DMK"}]},"passed_commercial_filters":"true","passed_quote_filters":"false","price":{"currency_id":"THB","total":{"taxpx":2180}},"routedate_uid":"noka*11052*1406181740*DD*0*12917*1406181900"}]}}],"created_at":"2014-05-21T15:48:00","elapsed":0.0018019676208496094,"filtering_info":{"all_quotes_filtered":true,"route_enabled":true},"pricing_type":"Scheduled","quotes":[],"raw_api_request":"POST http://www.nokair.com/webservices/services.asmx HTTP/1.1\nContent-Type: text/xml; charset=utf-8\n\n<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"></soap:Envelope>\r\n","show":true,"show_inbound":false};

    beforeEach(function() {
        var dfr = q.defer();
        dfr.reject();
        spyOn(defaultMocks.testService, 'get').and.returnValue(dfr.promise);
    });

    it('should have the default locale settings set to Singapore', function() {
        createController();

        expect(scope.test_config.lang).toBe('en');
        expect(scope.test_config.market).toBe('SG');
        expect(scope.test_config.currency).toBe('SGD');
    });

    it('should have a default Cabin Class Mapping set', function() {
        createController();

        expect(scope.cabinClassMap.economy).toBe('Economy');
        expect(scope.cabinClassMap.premium_economy).toBe('Premium Economy');
        expect(scope.cabinClassMap.business).toBe('Business');
        expect(scope.cabinClassMap.first).toBe('First');
    });

    it('should have a working getAirlineID call', function() {
        createController();

        expect(scope.getAirlineID('IATA: JT')).toBe('JT');
    });

    it('should have a working showRawResponse', function() {
        createController();

        scope.rawResponse = false;
        scope.showRawResponse();
        expect(scope.rawResponse).toBe(true);
    });

    it('should have an initialized scope.cabinClassMap', function() {
        createController();

        expect(scope.cabinClassMap.economy).toBe('Economy');
        expect(scope.cabinClassMap.premium_economy).toBe('Premium Economy');
        expect(scope.cabinClassMap.business).toBe('Business');
        expect(scope.cabinClassMap.first).toBe('First');
    });

    it('should have an initialized scope.cabinClassMap', function() {
        createController();

        expect(scope.cabinClassMap.economy).toBe('Economy');
        expect(scope.cabinClassMap.premium_economy).toBe('Premium Economy');
        expect(scope.cabinClassMap.business).toBe('Business');
        expect(scope.cabinClassMap.first).toBe('First');
    });

    it('should have a defined scope.showLocale', function() {
        createController();

        expect(scope.showLocale).toBeDefined();
    });

    it('should have an initialized scope.test_result', function() {
        createController();

        expect(scope.test_result.elapsed).toBe('');
        expect(scope.test_result.quotes).toBeDefined();
    });

    it('should have a defined scope.loadTestConfigs', function() {
        createController();

        expect(scope.loadTestConfigs).toBeDefined();
    });

    it('should have a defined scope.showAPIresponse', function() {
        createController();

        expect(scope.showAPIresponse).toBeDefined();
    });

    it('should have a defined scope.runTest', function() {
        createController();

        expect(scope.runTest).toBeDefined();
    });

    it('should have a defined scope.saveTest', function() {
        createController();

        expect(scope.saveTest).toBeDefined();
    });

    it('should be able to save tests', function() {
        var dfr = q.defer();
        dfr.resolve();
        spyOn(defaultMocks.testService, 'save').and.returnValue(dfr.promise);

        createController();

        scope.saveTest(scope.sampleTestConfig);
        expect(scope.saving_testcase).toBe(true);
        expect(scope.testLoadingMessage).toEqual('Updating test cases.');
    });

    it('should have a defined scope.deleteTest', function() {
        createController();

        expect(scope.deleteTest).toBeDefined();
    });

    it('should be able to delete tests', function() {
        var dfr = q.defer();
        dfr.resolve();
        spyOn(defaultMocks.testService, 'delete').and.returnValue(dfr.promise);

        createController();

        scope.deleteTest('1234', 'vvvv');
        expect(scope.testLoadingMessage).toEqual('Deleting test case.');
        expect(scope.test_result.quotes).toEqual([]);
        expect(scope.test_result.elapsed).toEqual('');
    });

    it('should initialize scope.rawResponse to false by default', function() {
        createController();

        expect(scope.rawResponse).toBe(false);
    });

    it('should have a defined scope.showRawResponse', function() {
        createController();

        expect(scope.showRawResponse).toBeDefined();
    });

    it('should have a defined scope.closeAlert', function() {
        createController();

        expect(scope.closeAlert).toBeDefined();
    });

    it('should have a defined scope.getDeeplink', function() {
        createController();

        expect(scope.getDeeplink).toBeDefined();
    });

    it('should get the deeplink', function() {
        var dfr = q.defer();
        dfr.resolve();
        spyOn(defaultMocks.testService, 'getDeeplink').and.returnValue(dfr.promise);

        createController();

        var index = 1;
        scope.getDeeplink(scope.sampleTestConfig, index);
        expect(scope.loading_deeplink[index]).toBe(true);
    });

    it('should determine whether the test case date is in the past', function() {
        spyOn(defaultMocks.testService, 'isInThePast').and.returnValue(true);

        createController();

        var testConfig = {
            from_place: 'RIX',
            to_place: 'BCN',
            depart_date: '2014-04-15',
            return_date: '2014-04-22',
            lang: 'en',
            market: 'SG',
            currency: 'SGD',
            adults: 1,
            children: 0,
            infants: 0,
            cabin_class: 'economy',
            datacenter: 'sg1'
        };

        expect(scope.isInThePast(testConfig)).toBe(true);
    });

    it('should be able to update the web_config and test_config', function() {
        createController();

        expect(scope.updateConfig).toBeDefined();
        scope.updateConfig();
        expect(scope.test_config).toEqual({lang: 'en', market: 'SG', currency: 'SGD'});
    });

    it('should have the price accuracy checker method', function() {
        createController();

        expect(scope.checkPriceAccuracy).toBeDefined();
        expect(scope.loading_pa_results).toEqual(false);
        expect(scope.price_diff).toEqual({});
        expect(scope.loading_pa).toEqual({});
    });

    it('should have encodeURL', function() {
        createController();

        expect(scope.encodeURL).toBeDefined();
    });

    it('should have correct filter details', function() {
        createController();

        expect(scope.getFilterDetails).toBeDefined();
        expect(scope.getFilterDetails('remove_duplicates')).toEqual('Found duplicates. They should be removed from the API response.');
    });

    it('should convert the repro link correctly', function() {
        createController();

        //var linkToConvert = 'http://www.skyscanner.net/transport/flights/lhr/edi/150613/150620/airfares-…n=1&preferdirects=false&outboundaltsenabled=false&inboundaltsenabled=false';

        scope.testConfig = scope.sampleTestConfig;
        expect(scope.convertReproLink).toBeDefined();

        // TODO: check with Emi how to properly write a unit test for this with URI object.
    });

    it('should convert Geo value to uppercase', function() {
        createController();
        expect(scope.formatGeoLabel).toBeDefined();

        var location = scope.formatGeoLabel('lhr');
        expect(location).toEqual('LHR');
    });

});

}());
