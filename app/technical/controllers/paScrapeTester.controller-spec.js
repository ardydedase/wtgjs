(function() {

'use strict';

describe('Controller: PAScrapeTesterCtrl', function() {

    var controller;
    var defaultMocks;
    var q;
    var scope;

    var createController = function(mocks) {
        mocks = mocks ? mocks : defaultMocks;

        return controller('PAScrapeTesterCtrl', {
            $scope: scope,
            localStorageService: mocks.localStorageService,
            authService: mocks.authService,
            testService: mocks.testService,
            pollerService: mocks.pollerService,
            paScrapeTesterService: mocks.paScrapeTesterService,
            mixpanelService: mocks.mixpanelService
        });
    };

    var createPromise = function() {
        return function() {
            var dfr = q.defer();
            dfr.resolve({
                status: 200,
                data: []
            });
            return dfr.promise;
        };
    };

    beforeEach(module('fcApp.technical'));

    beforeEach(inject(function($controller, $rootScope, $location, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        scope = $rootScope.$new();
        scope.websites = ['xpsg'];
        scope.test_config = {
            max_infants: 1,
            max_children: 1,
            market: 'SG',
            max_adults: 5,
            is_ota: true
        };

        defaultMocks = {
            localStorageService: {
                get: function() {
                    return {};
                },

                remove: function() {
                    return 'removed';
                },

                add: function() {
                    return 'added';
                }
            },
            authService: {
                restrictToGroup: function() {
                    return 'restrictToGroup';
                },

                redirectToWebsiteId: function() {
                    return 'redirectToWebsiteId';
                },

                isInternalUser: function() {}
            },
            pollerService: {
                start: createPromise(),
                poll: createPromise(),
            },
            paScrapeTesterService: {
                run: createPromise(),
                startScrapesPoller: createPromise(),
                start: createPromise()
            },
            testService: {
                isInThePast: function() {}
            },
            mixpanelService: {
                track: function() {}
            }
        };
    }));

    it('should have a default scraper settings', function() {
        createController();
        expect(scope.scraperSettings).toBeDefined();
    });

    it('should update scraperSettings when running scrapes', function() {

        beforeEach(function() {
            var dfr = q.defer();
            spyOn(defaultMocks.paScrapeTesterService, 'run').and.returnValue(dfr.promise);

            dfr.resolve([]);
        });

        createController();

        var scraperSettings = {
            request_count: 5
        };

        scope.runScrapes(scraperSettings);

        expect(scope.scraperSettings.request_count).toEqual(5);

    });

    it('should be able to start scrapes', function() {
        createController();

        var scraperSettings = {};
        scope.startScrapes(scraperSettings);
        expect(scope.runningScrapes).toEqual(true);
    });

});

}());
