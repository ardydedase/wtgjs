(function() {

'use strict';

describe('Controller: LocaleCtrl', function() {

    beforeEach(module('fcApp.technical'));

    var LocaleCtrl;
    var scope;

    beforeEach(inject(function($controller, $rootScope, $modal) {
        scope = $rootScope.$new();
        scope.websites = ['xpsg'];
        var modalInstance = $modal.open({
            templateUrl: 'views/modal/loading.html',
            controller: 'LoadingCtrl',
            backdrop: 'static',
            resolve: {
                message: function() {
                    return 'testing modal';
                }
            }
        });

        LocaleCtrl = $controller('LocaleCtrl', {
            $scope: scope,
            $modalInstance: modalInstance,
            testConfig: {status: 'ok'}
        });

        scope.test_config = {status: 'ok'};
    }));

    it('should be able to initialize the controller scope variables', function() {
        expect(scope.test_config.status).toBe('ok');
    });
});

}());
