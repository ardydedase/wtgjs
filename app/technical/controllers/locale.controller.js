(function() {

'use strict';

angular.module('fcApp.technical')
  .controller('LocaleCtrl', function($scope, $modalInstance, localStorageService, testConfig) {
    $scope.test_config = testConfig;

    $scope.applyLocale = function() {
        $modalInstance.close($scope.test_config);
    };

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

});

}());
