(function() {

'use strict';

angular.module('fcApp.technical')
    .controller('ScrapeTesterCtrl', function(
        $scope,
        $rootScope,
        $interval,
        $route,
        $location,
        $window,
        $filter,
        $analytics,
        alertService,
        testService,
        utilService,
        authService,
        modalService,
        webConfigService,
        localStorageService,
        scrapeTesterService,
        jiraService,
        mixpanelService,
        pollerService) {

        authService.restrictToGroup('Skyscanner');
        authService.redirectToWebsiteId();

        var scrapeTicker;

        // authService
        $scope.isInternalUser = authService.isInternalUser;

        /**
         * Initialize scope variables.
         * @return {[type]} [description]
         */
        function init() {
            $scope.scrapeTester = true;
            $scope.websiteId = localStorageService.get('selectedwebsite') || $route.current.params.websiteId;
            $scope.cabinClassMap = testService.cabinClassMap;
            $scope.toTitleCase = utilService.toTitleCase;
            $scope.encodeURL = utilService.encodeURL;
            $scope.runningScrapeIdx = 0;
            $scope.scrapeTasksLen = 0;
            $scope.isInThePast = testService.isInThePast;
            $scope.runningTest = {};
            $scope.runningLastRun = {};
            $scope.runningJacResponse = {};
            $scope.runningDeeplink = {};
            $scope.runningResponse = {};
            $scope.webConfig = {};
            $scope.scrapeTests = [];
            $scope.testLoadingMessage = 'Loading Tests..';

            $scope.raisedTicket = {};
            $scope.jiraResponse = {};
            $scope.raisingJira = {};
            $scope.skip = 0;
        }

        function analyticsTracking(action) {
            $analytics.eventTrack(action, {
                category: localStorageService.get('selectedwebsite'),
                label: localStorageService.get('account').username
            });
        }

        /**
         * Initialize scraper settings. Minimize inputs from user.
         * @param  {[type]} updatedWebConfig [description]
         * @return {[type]}                  [description]
         */
        function initScraperSettings(updatedWebConfig, updatedScraperSettings) {
            analyticsTracking('initScraperSettings');

            // default scraper settings
            var webConfig = updatedWebConfig || localStorageService.get('webconfig');
            var pricingMap = {
                Scheduled: 'return',
                Budget: 'budget'
            };

            webConfig.home_country = (webConfig.home_country instanceof Array) ? webConfig.home_country.join() : webConfig.home_country;
            webConfig.lang = (webConfig.lang instanceof Array) ? webConfig.lang.join() : webConfig.lang;
            webConfig.currency = (webConfig.currency instanceof Array) ? webConfig.currency.join() : webConfig.currency;

            $scope.webConfig = webConfig;

            $scope.scraperSettings = {
                testsuite: '',
                isRerun: 0,
                request_count: 2,
                min_date: testService.offsetDate(10),
                max_date: testService.offsetDate(190),
                cabin_classes: {
                    economy: true,
                    premium_economy: true,
                    business: true,
                    first: true
                },
                max_adults: 5,
                max_children: 5,
                max_infants: 5,
                mode: 'both',
                ratio: 0.5,
                max_seats: 5,
                max_passengers: 5,
                datacenter: 'all',
                port:'',
                user_countries: [webConfig.home_country],
                user_languages: [webConfig.lang],
                user_currency: [webConfig.currency],
                origin_routes: 'SIN',
                destination_routes: 'KUL, BKK, MNL',
                is_blacklist: (webConfig.live_update_pool === 2) ? true : false,
                pricing_model: pricingMap[webConfig.pricing_type],
                jac_version: parseInt(webConfig.live_update_engine),
                skippy_version: parseInt(webConfig.deeplink_engine),
                route_settings: 'auto_market',
                market: webConfig.market || webConfig.home_country,
                routes_website_id: $scope.websiteId,
                depart_days: '',
                return_days: ''
            };

            $scope.scraperSettings.datacenter = $scope.scraperSettings.is_blacklist ? 'sg1' : 'all';

            $.extend($scope.scraperSettings, updatedScraperSettings);

            return $scope.scraperSettings;
        }

        /**
         * Stop the scrape ticker when it's done.
         * @return {[type]} [description]
         */
        function stopScrapeTicker() {
            analyticsTracking('stopScrapeTicker');

            if (angular.isDefined(scrapeTicker)) {
                $interval.cancel(scrapeTicker);
                scrapeTicker = undefined;
            }
        }

        /**
         * [runScrapeTicker description]
         * @param  {[type]} scrapeTasks [description]
         * @return {[type]}             [desrciption]
         */
        function runScrapeTicker(tasks, scrapeId, scraperSettings) {
            analyticsTracking('runScrapeTicker');

            var SCRAPETICKER_DELAY = 2000;
            var scrapeTasks;

            if (typeof scraperSettings === 'undefined') {
                scraperSettings = $scope.scraperSettings;
            }

            if (typeof scrapeTasks !== 'undefined') {
                scrapeTasks = tasks;
            } else if (localStorageService.get('scrape_tasks') !== null) {
                scrapeTasks = localStorageService.get('scrape_tasks');
            } else {
                scrapeTasks = [];
            }

            if (scrapeTasks.length > 0) {

                scrapeTicker = $interval(function() {
                    scrapeTasks = localStorageService.get('scrape_tasks');

                    var taskId = null;

                    try {
                        taskId = scrapeTasks[$scope.runningScrapeIdx].task_id;
                    } catch (e) {
                        $scope.runningScrapeIdx = 0;
                        return;
                    }

                    $scope.runningTest[scrapeTasks[$scope.runningScrapeIdx].id] = true;

                    scrapeTesterService.checkScrape(taskId, scraperSettings).then(function(response) {
                        if (response.ready === true) {
                            try {
                                $scope.runningResponse[scrapeTasks[$scope.runningScrapeIdx].id] = response.result;
                                $scope.runningTest[scrapeTasks[$scope.runningScrapeIdx].id] = false;
                                $scope.runningLastRun[scrapeTasks[$scope.runningScrapeIdx].id] = response.lastrun;
                                $scope.runningJacResponse[scrapeTasks[$scope.runningScrapeIdx].id] = response.result.jac_response;
                                $scope.runningDeeplink[scrapeTasks[$scope.runningScrapeIdx].id] = response.result.jac_response.deeplink;
                                analyticsTracking('runScrapeSuccess');
                            } catch (e) {
                                console.log('There was a problem getting the scrapee ID:');
                                console.log(e);
                            }

                            scrapeTasks.splice($scope.runningScrapeIdx, 1);

                            localStorageService.remove('scrape_tasks');
                            localStorageService.add('scrape_tasks', scrapeTasks);

                        }

                        $scope.scrapeTasksLen = scrapeTasks.length;

                        // When there are no more tasks left
                        if ($scope.scrapeTasksLen === 0) {
                            stopScrapeTicker();
                            $scope.runningScrapeIdx = 0;
                        } else {
                            $scope.runningScrapeIdx++;
                        }
                    });

                }, SCRAPETICKER_DELAY);
            }
        }

        function mixpanelTracking(options) {
            var trackingData = {
                serviceType: 'scrapetester'
            };

            if (options) {
                $.extend(trackingData, options);
            }

            mixpanelService.track(options.actionName, trackingData);
        }

        $scope.change = function() {
            if ($scope.scraperSettings.datacenter === 'localhost') {
                $scope.showPortInput = true;
            } else {
                $scope.showPortInput = false;
            }
        };

        $scope.loadedLast = false;

        $scope.loadScrapes = function() {
            if ($scope.loadingScrapeTests) {
                return;
            }

            $scope.loadingScrapeTests = true;
            var params = {skip: $scope.skip, limit: 25};

            analyticsTracking('loadScrapes');

            pollerService.start({
                url: 'v0.4/flights/scrapes/past',
                params: params
            }).then(function(taskId) {
                var loadScrapeTestsPoller = pollerService.poll({
                    url: 'v0.4/flights/scrapes/past/async',
                    taskId: taskId
                });

                loadScrapeTestsPoller.promise.then(null, null, function(pollerResponse) {
                    if (pollerResponse.data.ready) {
                        try {
                            loadScrapeTestsPoller.stop();
                            for (var i = 0; i < pollerResponse.data.result.length; i++) {
                                $scope.scrapeTests.push(pollerResponse.data.result[i]);
                            }

                            if (pollerResponse.data.result.length < 25) {
                                $scope.loadedLast = true;
                            }

                            $scope.loadingScrapeTests = false;

                            analyticsTracking('loadedScrapes');
                            if (pollerResponse.data.result.length < 25) {
                                $scope.loadedLast = true;
                            }

                            $scope.loadingScrapeTests = false;
                            loadScrapeTestsPoller = undefined;
                            analyticsTracking('loadedScrapes');
                        } catch (e) {
                            // TECHDEBT: angular poller cannot be stopped sometimes.
                            // do nothing
                        }
                    }
                });

            });

            $scope.skip = $scope.skip + 25;
        };

        /**
         * Load all the Scrape Tests
         * @return {[type]} [description]
         */

        /* commenting this out because not used.  Please remove.
        function loadScrapeTests() {
            analyticsTracking('loadScrapeTests');

            $scope.loadingScrapeTests = true;
            $scope.skip = 0;
            // remove existing task
            console.log('controller');
            localStorageService.set('scrape_tasks', []);
            var params = {'skip': $scope.skip};

            pollerService.start({
                url: 'v0.4/flights/scrapes/past',
                params: params
            }).then(function(taskId) {
                var loadScrapeTestsPoller = pollerService.poll({
                    url: 'v0.4/flights/scrapes/past/async',
                    taskId: taskId
                });

                loadScrapeTestsPoller.promise.then(null, null, function(pollerResponse) {
                    if (pollerResponse.data.ready) {
                        loadScrapeTestsPoller.stop();
                        $scope.scraperSettings.testsuite = $route.current.params.scrapeTestId;
                        $scope.scrapeTests  = pollerResponse.data.result;

                        analyticsTracking('loadedScrapeTests');

                    }
                        $scope.loadingScrapeTests = false;
                });
            });
            $scope.skip = $scope.skip+25;
        }
        */

        /**
         * [processCabinClasses description]
         * @return {[type]} [description]
         */
        function processCabinClasses(availableCabinClasses) {
            var cabinClasses = $scope.scraperSettings.cabin_classes;

            for (var i = 0; i < availableCabinClasses.length; i++) {
                availableCabinClasses[i] = (availableCabinClasses[i]).toLowerCase();
            }

            for (var key in cabinClasses) {

                if (availableCabinClasses.indexOf(key) === -1) {
                    $scope.scraperSettings.cabin_classes[key] = false;
                }
            }
        }

        /**
         * Check if form is incomplete
         * @return {[type]} [description]
         */
        $scope.formIncomplete = function(scraperSettings) {
            var validCabinClasses = function(cabinClasses) {
                for (var key in cabinClasses) {
                    if (cabinClasses[key] === true) {
                        return true;
                    }
                }

                return false;
            };

            return !validCabinClasses(scraperSettings.cabin_classes) || !scraperSettings.user_countries || !scraperSettings.user_languages || !scraperSettings.user_currency;
        };

        /**
         * Update current selected website.
         * @param  {[type]} selectedWebsite [description]
         * @return {[type]}                 [description]
         */
        $scope.$on('websiteIdChanged', updateConfig);

        function updateConfig(selectedWebsite) {
            $scope.loadSettingsPromise = webConfigService.get(selectedWebsite).then(function(response) {

                // localStorageService.remove('webconfig');
                // localStorageService.add('webconfig', response);

                initScraperSettings(response);

                processCabinClasses(response.cabin_classes);
                $scope.availableCabinClasses = response.cabin_classes;

                var redirectPath = '/scrapetester/' + selectedWebsite;
                if ($route.current.params.scrapeTestId) {
                    redirectPath += '/' + $route.current.params.scrapeTestId;
                }

                $location.path(redirectPath);
            });
        }

        $scope.rerunScrapes = function(scraperSettings) {
            analyticsTracking('rerunScrapes');
            $scope.scrapeTests = [];
            $scope.scraperSettings.isRerun = 1;

            initScraperSettings(null, scraperSettings);
            $scope.runningScrapes = $scope.reRunningScrapes = true;
            $scope.testConfigs = [];

            mixpanelTracking({
                actionName: 'FC Technical Start Scrapes'
            });

            $scope.testConfigsPromise = scrapeTesterService.start(scraperSettings).then(function(response) {
                var startScrapesPoller;

                if (!startScrapesPoller) {
                    startScrapesPoller = scrapeTesterService.startScrapesPoller(response.task_id);

                    mixpanelTracking({
                        actionName: 'FC Technical Successfully Started Scrapes'
                    });

                    $scope.testConfigsPromise = startScrapesPoller.promise.then(null, null, function(pollerResponse) {
                        if (pollerResponse.data.ready) {
                            mixpanelTracking({
                                actionName: 'FC Technical Received Scrape Results'
                            });

                            pollerResponse = pollerResponse.data.result;
                            startScrapesPoller.stop();

                            // confirm that promise is fulfilled
                            $scope.testConfigsPromise = null;

                            // store tasks in local storage
                            localStorageService.remove('scrape_tasks');
                            localStorageService.add('scrape_tasks', pollerResponse.tasks);

                            $scope.testConfigs = pollerResponse.tasks;
                            runScrapeTicker(pollerResponse.tasks, pollerResponse.scrape_id, scraperSettings);
                            $location.path('/scrapetester/' + $scope.websiteId + '/' + scraperSettings.testsuite, false);
                            $scope.runningScrapes = $scope.reRunningScrapes = false;
                        }
                    });
                }

            },

            function() {
                $scope.runningScrapes = $scope.reRunningScrapes = false;
            });
        };

        /**
         * Start Scrape tests.
         * @param  {[type]} scraperSettings [description]
         * @return {[type]}                 [description]
         */
        $scope.startScrapes = function(scraperSettings) {
            analyticsTracking('startScrapes');

            $scope.scraperSettings.isRerun = 0;
            initScraperSettings(null, scraperSettings);
            $scope.runningScrapes = true;

            mixpanelTracking({
                actionName: 'FC Technical Start Scrapes'
            });

            $scope.testConfigsPromise = scrapeTesterService.start(scraperSettings).then(function(response) {
                var startScrapesPoller;
                if (typeof (startScrapesPoller) === 'undefined') {

                    startScrapesPoller = scrapeTesterService.startScrapesPoller(response.task_id);

                    mixpanelTracking({
                        actionName: 'FC Technical Successfully Started Scrapes'
                    });

                    $scope.testConfigsPromise = startScrapesPoller.promise.then(null, null, function(pollerResponse) {

                        if (pollerResponse.data.ready) {
                            mixpanelTracking({
                                actionName: 'FC Technical Received Scrape Results'
                            });

                            pollerResponse = pollerResponse.data.result;
                            startScrapesPoller.stop();

                            // confirm that promise is fulfilled
                            $scope.testConfigsPromise = null;

                            // store tasks in local storage
                            localStorageService.remove('scrape_tasks');
                            localStorageService.add('scrape_tasks', pollerResponse.tasks);

                            $scope.testConfigs = pollerResponse.tasks;
                            runScrapeTicker(pollerResponse.tasks, pollerResponse.scrape_id, scraperSettings);
                            $location.path('/scrapetester/' + $scope.websiteId + '/' + pollerResponse.scrape_id, false);
                            $scope.scraperSettings.testsuite = pollerResponse.scrape_id;
                            $scope.runningScrapes = false;
                        }
                    });
                }

            },

            function() {
                $scope.runningScrapes = false;
            });
        };

        /**
         * Run Scrape tests.
         * @param  {[type]} scraperSettings [description]
         * @return {[type]}                 [description]
         */
        $scope.runScrapes = function(scraperSettings) {
            analyticsTracking('runScrapes');

            $scope.raisedTicket = {};
            initScraperSettings(null, scraperSettings);
            $scope.runningScrapes = true;
            $scope.testConfigsPromise = scrapeTesterService.run(scraperSettings).then(function(response) {

                mixpanelTracking({
                    actionName: 'FC Technical RunScrapes'
                });

                localStorageService.remove('scrape_tasks');
                localStorageService.add('scrape_tasks', response.tasks);
                $scope.testConfigs = response.tasks;
                runScrapeTicker(response.tasks, response.scrape_id, scraperSettings);
                $location.path('/scrapetester/' + $scope.websiteId + '/' + response.scrape_id, false);
                $scope.runningScrapes = false;
            },

            function() {
                $scope.runningScrapes = false;
            });
        };

        $scope.raiseJira = function(testConfigItem, index) {
            analyticsTracking('raiseJira');

            $scope.raisingJira[index] = true;

            jiraService.jira(testConfigItem).then(function(response) {
                response = response.data;
                $scope.raisingJira[index] = false;
                if (response.hasOwnProperty('id')) {
                    $scope.jiraResponse[index] = response;
                    $scope.raisedTicket[index] = true;

                    mixpanelTracking({
                        actionName: 'FC Technical JiraTicketRaised'
                    });

                    alertService.add('success', 'Successfully raised a Jira Ticket.');

                    analyticsTracking('raisedJira');

                } else {
                    $scope.raisedTicket[index] = false;

                    mixpanelTracking({
                        actionName: 'FC Technical FailedJiraTicketRaise'
                    });

                    alertService.add('danger', 'Failed raising a Jira Ticket.');
                    analyticsTracking('raiseJiraFailure');

                }
            });
        };

        /**
         * Show scrape test result
         * @param  {[type]} jacResponse [description]
         * @return {[type]}             [description]
         */
        $scope.showScrapeTestResult = function(jacResponse) {
            analyticsTracking('showScrapeTestResult');

            if (typeof jacResponse === 'undefined') {
                return false;
            }

            modalService.showScrapeTestResult(jacResponse);
        };

        $scope.loading_skippy_deeplink = {};
        $scope.getSkippyDeeplink = function(testConfig, runningJacResponse, oneway, index) {
            analyticsTracking('getSkippyDeeplink');

            mixpanelTracking({
                actionName: 'FC Technical Get Deeplink'
            });

            if ($scope.scraperSettings.jac_version === 4) {
                try {
                    $window.open(testConfig.deeplink.dayview, '_blank');
                } catch (e) {
                    $window.open(runningJacResponse.deeplink.dayview, '_blank');
                }

                return false;
            } else if ($scope.scraperSettings.jac_version === 3) {

                var quote = (testConfig.hasOwnProperty('jac_response')) ? testConfig.jac_response.jac_out.answer[0].journey.quotes[0] : runningJacResponse[testConfig.id].jac_out.answer[0].journey.quotes[0];

                if (!quote) {
                    return false;
                }

                $scope.loading_skippy_deeplink[index] = true;
                $scope.clicked_select = {};
                $scope.clicked_select[index] = true;

                testService.getSkippyDeeplink(testConfig, quote, oneway).then(function(response) {
                    var method = $.isEmptyObject(response.post_data) ? 'get' : 'post';
                    var formData = !$.isEmptyObject(response.post_data) ? response.post_data : response.get_data;
                    testService.redirectToDeeplink(response.url, formData, method);
                    $scope.loading_skippy_deeplink[index] = false;
                },

                function() {
                    $scope.loading_skippy_deeplink[index] = false;
                });
            }
        };

        /**
         * Check the boolean flag whether we have the load the scrape tests or not.
         * @return {[type]} [description]
         */
        $scope.$watch('showScrapeTests', function() {
        });

        /**
         * Check the websiteId route param and update the selected website and webconfig accordingly.
         * @return {[type]} [description]
         */
        $scope.$watch('routeParams.websiteId', function() {
            updateConfig($route.current.params.websiteId);
        });

        /**
         * Check the selected scrape test and load all the tests under it.
         * @return {[type]} [description]
         */
        $scope.$watch('routeParams.scrapeTestId', function() {

            if ($route.current.params.scrapeTestId) {
                var params = {scrape_id: $route.current.params.scrapeTestId};
                $scope.testConfigsPromise = $scope.scraperSettingsPromise = pollerService.start({
                    url: 'v0.4/flights/scrapes/past',
                    params: params
                }).then(function(taskId) {
                // start polling here...
                var getScrapeTestsPoller = pollerService.poll({
                    url: 'v0.4/flights/scrapes/past/async',
                    taskId: taskId
                });

                $scope.testConfigsPromise = $scope.scraperSettingsPromise = getScrapeTestsPoller.promise.then(null, null, function(pollerResponse) {

                    if (pollerResponse.data.ready) {
                        mixpanelTracking({
                            actionName: 'FC Technical Successfully Loaded Scrape Tests'
                        });

                        analyticsTracking('loadedSingleScrapeTest');

                        $scope.scraperSettingsPromise = null;
                        $scope.testConfigsPromise = null;
                        getScrapeTestsPoller.stop();

                        var scraperSettings = pollerResponse.data.result.scrape_test;
                        try {
                            scraperSettings.datacenter = scraperSettings.datacenter ? scraperSettings.datacenter : 'sg1';
                        } catch (e) {
                            scraperSettings.datacenter = 'sg1';
                        }

                        scraperSettings.ratio = scraperSettings.ratio ? scraperSettings.ratio : 0.5;
                        scraperSettings.port = scraperSettings.port ? (scraperSettings.datacenter === 'localhost') : '';
                        scraperSettings.jac_version = scraperSettings.jac_version ? scraperSettings.jac_version : 4;
                        scraperSettings.cabin_classes = scrapeTesterService.addCabinClassesToObj(scraperSettings.cabin_classes);

                        scraperSettings.min_date = scraperSettings.min_date.substring(0, 10);
                        scraperSettings.max_date = scraperSettings.max_date.substring(0, 10);

                        $scope.scraperSettings = scraperSettings;
                        $scope.testConfigs =  pollerResponse.data.result.test_cases;

                        try {
                            $scope.scraperSettings.origin_routes = pollerResponse.data.result.scrape_test.origin_routes.join(',');
                        } catch (e) {
                            console.log('can\'t retrieve origin_routes:');
                            console.log(e);
                        }

                        try {
                            $scope.scraperSettings.destination_routes = pollerResponse.data.result.scrape_test.destination_routes.join(',');
                        } catch (e) {
                            console.log('can\'t retrieve destination_routes:');
                            console.log(e);
                        }

                        $scope.scraperSettings.testsuite =  pollerResponse.data.result.test_cases[0].testsuite;

                    }
                });

            });
            }
        });

        init();
        initScraperSettings();
        runScrapeTicker();

        alertService.reset();
    });

}());
