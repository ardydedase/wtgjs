(function() {

'use strict';

angular.module('fcApp.technical')
    .controller('RegressionCtrl', function(
        $scope,
        $rootScope,
        $route,
        $routeParams,
        pollerService,
        regressionService,
        mixpanelService,
        scrapeTesterService) {

        var selectedWebsiteId = $route.current.params.websiteId;
        var selectedScrapeTestId = $route.current.params.scrapeTestId;

        function init() {
            $scope.regressionAccordionOpen = true;
            $scope.failingAccordionOpen = false;
            $scope.personalAccordionOpen = false;
        }

        function mixpanelTracking(options) {
            var trackingData = {
                serviceType: 'scrapetester'
            };

            if (options) {
                $.extend(trackingData, options);
            }

            mixpanelService.track(options.actionName, trackingData);
        }

        function getRegressionTests() {
            if (selectedWebsiteId) {
                $scope.apiResponsePromise = regressionService.getRegressionTestCases(selectedWebsiteId).then(function(data) {
                    $scope.regressionTest = data;
                });
            }

            //copy the references (you could clone ie angular.copy but then have to go through a dirty checking for the matches)
            $scope.displayedRegressionTest = [].concat($scope.regressionTest);

        }

        $scope.runAllTests = function(testSuiteId) {
            console.log('running test for test suite: ' + testSuiteId);
        };

        $scope.filterDatacenterTests = function(value) {
            console.log('value from resultFilter');
            console.log(value);

            if (value.regression_type === 'datacenter') {
                return value;
            }
        };

        $scope.filterRoutesTests = function(value) {
            console.log('value from resultFilter');
            console.log(value);

            if (value.regression_type === 'routes') {
                return value;
            }
        };

        $scope.filterPaxTests = function(value) {
            console.log('value from resultFilter');
            console.log(value);

            if (value.regression_type === 'pax') {
                return value;
            }
        };

        $scope.filterFailingTests = function(value) {
            if (value.lastrun.status === 'failure') {
                return value;
            }
        };

        $scope.generateRegressionTests = function() {
            $scope.apiResponsePromise = regressionService.generateRegressionTests(selectedWebsiteId).then(function(data) {
                $scope.regressionTest = data;
            });
        };

        $scope.runRegressionTests = function(testSuiteId) {
            $scope.apiResponsePromise = regressionService.runRegressionTests(selectedWebsiteId, testSuiteId).then(function(data) {
                $scope.regressionTest = data;
            });
        };

        /**
         * Check the selected scrape test and load all the tests under it.
         * @return {[type]} [description]
         */
        $scope.$watch('route.current.params.scrapeTestId', function() {

            if (selectedScrapeTestId) {
                var params = {scrape_id: selectedScrapeTestId};
                $scope.testConfigsPromise = $scope.scraperSettingsPromise = pollerService.start({
                    url: 'scrapetests',
                    params: params
                }).then(function(taskId) {
                    // start polling here...
                    var getScrapeTestsPoller = pollerService.poll({
                        url: 'scrapetests/async',
                        taskId: taskId
                    });

                    $scope.testConfigsPromise = $scope.scraperSettingsPromise = getScrapeTestsPoller.promise.then(null, null, function(pollerResponse) {

                        if (pollerResponse.data.ready) {

                            mixpanelTracking({
                                actionName: 'FC Technical Successfully Loaded Scrape Tests'
                            });

                            $scope.scraperSettingsPromise = null;
                            $scope.testConfigsPromise = null;
                            getScrapeTestsPoller.stop();

                            var scraperSettings = pollerResponse.data.result.scrape_test;

                            scraperSettings.datacenter = scraperSettings.datacenter ? scraperSettings.datacenter : 'sg1';
                            scraperSettings.ratio = scraperSettings.ratio ? scraperSettings.ratio : '0.5';
                            scraperSettings.port = scraperSettings.port ? (scraperSettings.datacenter === 'localhost') : '';
                            scraperSettings.jac_version = scraperSettings.jac_version ? scraperSettings.jac_version : 4;
                            scraperSettings.cabin_classes = scrapeTesterService.addCabinClassesToObj(scraperSettings.cabin_classes);

                            scraperSettings.min_date = scraperSettings.min_date.substring(0, 10);
                            scraperSettings.max_date = scraperSettings.max_date.substring(0, 10);

                            $scope.scraperSettings = scraperSettings;
                            $scope.testConfigs =  pollerResponse.data.result.test_cases;
                            $scope.scrapeSummary = pollerResponse.data.result.scrape_summary;

                            try {
                                $scope.scraperSettings.origin_routes = pollerResponse.origin_routes.join(',');
                            } catch (e) {
                                console.log('can\'t retrieve origin_routes:');
                                console.log(e);
                            }

                            try {
                                $scope.scraperSettings.destination_routes = pollerResponse.destination_routes.join(',');
                            } catch (e) {
                                console.log('can\'t retrieve destination_routes:');
                                console.log(e);
                            }
                        }
                    });
                });
            }
        });

        init();
        getRegressionTests();
    });

}());
