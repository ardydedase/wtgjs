(function() {

'use strict';

describe('Controller: RegressionCtrl', function() {

    var controller;
    var defaultMocks;
    var deferred;
    var q;
    var scope;

    beforeEach(module('fcApp.regression'));

    beforeEach(inject(function($controller, $rootScope, $location, $q) {
        scope = $rootScope.$new();
        controller = $controller;
        q = $q;

        scope = $rootScope.$new();
        scope.websites = ['xpsg'];
        scope.test_config = {
            lang: 'en',
            market: 'SG',
            currency: 'SGD'
        };

        scope.sampleTestConfig = {
            active: true,
            adults: '2',
            cabin_class: 'economy',
            children: '1',
            counter: 1,
            created_at: '2015-03-20T18:31:44.598Z',
            currency: 'IDR',
            datacenter: 'sg1',
            depart_date: '2015-04-20',
            doc_type: 'jacquard_test_case',
            from_place: 'CGK',
            infants: '1',
            lang: 'id',
            lastrun: {status: 'successful', date: '2015-03-23T14:13:38.660249'},
            market: 'ID',
            return_date: '2015-04-27',
            task_id: '',
            testsuite: '',
            to_place: 'KUL',
            type: 'flights',
            uid: '67c2757e18fe',
            website_id: 'nusa'
        };

        defaultMocks = {
            jiraService: {
                jira: function() {
                    deferred = q.defer();
                    return deferred.promise;
                }
            }
        };
    }));

    // No test case!
});

}());
