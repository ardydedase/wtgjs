module.exports = function(grunt) {
    'use strict';

    require('load-grunt-tasks')(grunt);

    require('time-grunt')(grunt);

    var env = grunt.option('env') || 'dev';

    console.log('running grunt on environment: ' + env);

    // Define the configuration for all the tasks
    grunt.initConfig({
        shell: {
            djangorunserver: {
                options: {
                    stdout: false
                },
                command: 'cd /opt/CTMProject/ && /opt/CTMProject/env/bin/supervisord -c /opt/CTMProject/supervisord.conf'
            },
            djangorestartserver: {
                options: {
                    stdout: false
                },
                command: 'cd /opt/CTMProject/ && /opt/CTMProject/env/bin/supervisorctl restart flightsconnect_api'
            },
            pythonrununittests: {
                options: {
                    stdout: false,
                    execOptions: {
                        maxBuffer: Infinity
                    }
                },
                command: '/opt/CTMProject/env/bin/python /opt/CTMProject/manage.py test flights.models flights.services flights.views jumala.views jumala.models jumala.services core.views core.models core.services --noinput'
            }
        },

        teamcity: {
            options: {
                suppressGruntLog: true
            },
            all: {} // need a task even if its an empty one
        },

        less: {
            all: {
                options: {
                    paths: ['<%= yeoman.app %>'],
                    relativeUrls: true
                },
                files: {
                    'app.css': 'app.less'
                }
            }
        },

        // Project settings
        yeoman: {
            app: require('./bower.json').appPath || 'app',
            dist: 'dist'
        },

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            js: {
                files: ['<%= yeoman.app %>/**/*.js'],
                tasks: []
            },
            jsTest: {
                files: ['<%= yeoman.app %>/**/*-spec.js'],
                tasks: []
            },
            styles: {
                files: ['app.less', '<%= yeoman.app %>/**/*.less'],
                tasks: ['less']//, 'autoprefixer']
            },
            djangofile: {
                files: [
                    '../{,*/}*.py',
                    '../CTMProject/{,*/}*.py',
                    '../api/fcviews/{,*/}*.py',
                    '../jumala/models/{,*/}*.py',
                    '../jumala/services/{,*/}*.py',
                    '../flights/api/{,*/}*.py',
                    '../flights/views/{,*/}*.py',
                    '../flights/models/{,*/}*.py',
                    '../flights/services/{,*/}*.py',
                    '../core/api/{,*/}*.py',
                    '../core/views/{,*/}*.py',
                    '../core/models/{,*/}*.py',
                    '../core/services/{,*/}*.py',
                    '../jumala/libs/cbwrapper{,*/}*.py'
                ],
                tasks: ['shell:pythonrununittests', 'shell:djangorestartserver']
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    'index.html',
                    'app.js',
                    'app.css',
                    'Gruntfile.js',
                    '<%= yeoman.app %>/**/*.html',
                    '<%= yeoman.app %>/**/*.js',
                    '!<%= yeoman.app %>/**/*-spec.js',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 82,
                hostname: 'localhost',
                livereload: 35729
            },
            livereload: {
                options: {
                    open: true,
                    base: [
                        '.tmp',
                        '.'
                    ]
                }
            },
            test: {
                options: {
                    port: 9000,
                    base: [
                        '.tmp',
                        '<%= yeoman.app %>'
                    ]
                }
            },
            dist: {
                options: {
                    base: '<%= yeoman.dist %>'
                }
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js',
                '<%= yeoman.app %>/**/*.js'
            ],
            test: {
                options: {
                    jshintrc: 'test/.jshintrc'
                },
                src: ['<%= yeoman.app %>/**/*-spec.js']
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/*',
                        '!<%= yeoman.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/css/',
                    src: '{,*/}*.css',
                    dest: '.tmp/css/'
                }]
            }
        },

        // Automatically inject Bower components into the app
        'bower-install': {
            app: {
                html: '<%= yeoman.app %>/index.html',
                ignorePath: '<%= yeoman.app %>/'
            }
        },

        // Renames files for browser caching purposes
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= yeoman.dist %>/js/**/*.js',
                        '<%= yeoman.dist %>/css/**/*.css',
                        '<%= yeoman.dist %>/images/**/*.{png,jpg,jpeg,gif,webp,svg}',
                        '<%= yeoman.dist %>/css/fonts/*'
                    ]
                }
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: 'index.html',
            options: {
                dest: '<%= yeoman.dist %>'
            }
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
            html: ['<%= yeoman.dist %>/index.html'],
            css: ['<%= yeoman.dist %>/css/**/*.css'],
            options: {
                assetsDirs: ['<%= yeoman.dist %>']
            }
        },

        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'images',
                    src: '**/*.{png,jpg,jpeg,gif}',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'images',
                    src: '**/*.svg',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>',
                    src: ['*.html', 'app/**/*.html'],
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        // Allow the use of non-minsafe AngularJS files. Automatically makes it
        // minsafe compatible so Uglify does not destroy the ng references
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/js',
                    src: '*.js',
                    dest: '.tmp/concat/js'
                }]
            }
        },

        // Replace Google CDN references
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/*.html']
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '.',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '.htaccess',
                        '*.html',
                        'app/**/*.html',
                        'bower_components/font-awesome/**/*',
                        'components/**/*',
                        'images/**/*.{webp}',
                        'fonts/*',
                        'app/faq/FAQ.md'
                    ]
                }, {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/images',
                    src: ['generated/*']
                }]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>',
                dest: '.tmp/css/',
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: {
                tasks: [
                    'copy:styles',
                    'shell:djangorunserver'
                ]
            },
            dist: [
                'copy:styles',
                'imagemin',
                'svgmin'
            ]
        },

        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        // cssmin: {
        //   dist: {
        //     files: {
        //       '<%= yeoman.dist %>/styles/main.css': [
        //         '.tmp/styles/{,*/}*.css',
        //         '<%= yeoman.app %>/styles/{,*/}*.css'
        //       ]
        //     }
        //   }
        // },
        // uglify: {
        //   dist: {
        //     files: {
        //       '<%= yeoman.dist %>/scripts/scripts.js': [
        //         '<%= yeoman.dist %>/scripts/scripts.js'
        //       ]
        //     }
        //   }
        // },
        // concat: {
        //   dist: {}
        // },

        // Test settings
        karma: {
            options: {
                configFile: 'karma.conf.js'
            },
            dev: {
                singleRun: false,
                autoWatch: true
            },
            ci: {
                singleRun: true
            }
        },

        protractor: {
            options: {
                configFile: 'protractor.conf.js',
                keepAlive: true,
                noColor: false
            },
            all: {}
        },

        // https://www.npmjs.org/package/grunt-strip
        // Remove JavaScript statements (like console.log) from your source code
        strip: {
            main: {
                src: '<%= yeoman.dist %>/js/*.js',
                options: {
                    inline: true
                }
            }
        },

        ngconstant: {
            options: {
                name: 'appConfig',
                dest: '<%= yeoman.app %>/appconfig.js'
            },
            dev: {
                constants: {
                    config: {
                        API_URL: 'http://flightsconnect.dev:8000',
                        API_BASE: '/connect_api',
                        debug: true
                    }
                }
            },
            dist: {
                constants: {
                    config: {
                        API_URL: '',
                        API_BASE: '/connect_api',
                        debug: false
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-strip');
    grunt.loadNpmTasks('grunt-protractor-runner');
    grunt.loadNpmTasks('grunt-teamcity');
    grunt.loadNpmTasks('grunt-ng-constant');
    grunt.loadNpmTasks('grunt-ng-annotate');

    grunt.registerTask('serve', function() {
        grunt.task.run([
            'shell:djangorunserver',
            'watch:djangofile'
        ]);
    });

    grunt.registerTask('dev', [
        'clean:server',
        'ngconstant:dev',
        'less',
        'connect:livereload',
        'watch'
    ]);

    grunt.registerTask('test', [
        'karma:dev'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'ngconstant:dist',
        'less',
        'useminPrepare',
        'concurrent:dist',
        'autoprefixer',
        'concat',
        'ngAnnotate',
        'copy:dist',
        'cdnify',
        'cssmin',
        'uglify',
        'rev',
        'usemin',
        'htmlmin',
        'strip'
    ]);

    grunt.registerTask('default', [
        'teamcity',
        'newer:jshint',
        'test',
        'build'
    ]);
};
