(function() {
    'use strict';

    var noop = function() {};

    var MixpanelMock = (function() {
        function MixpanelMock() {}

        MixpanelMock.prototype.identify = noop;

        MixpanelMock.prototype.people = {
            set: noop
        };

        MixpanelMock.prototype.track = noop;

        MixpanelMock.prototype.register_once = noop;

        MixpanelMock.prototype.cookie = {
            clear: noop
        };

        return MixpanelMock;

    })();

    window.mixpanel = new MixpanelMock();

}).call(this);
