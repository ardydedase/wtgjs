/** This config applies for both local and TeamCity env.

    By default it should run all specs as specified in "specs", "suites" are for dev only.

    Pass these two mandatory parameters in TC:
     * baseUrl="http://fl2vluk1atd001.int-review.skyscanner.local/"
     * seleniumAddress="http://grid.selenium.test.skyscanner.local:4444/wd/hub"

    For example, in INT-REVIEW environment, run:
    ./node_modules/protractor/bin/protractor protractor.conf.js --baseUrl=http://fl2vluk1atd001.int-review.skyscanner.local/ --seleniumAddress="http://grid.selenium.test.skyscanner.local:4444/wd/hub"
*/

exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',    // dev
    // seleniumAddress: 'http://grid.selenium.test.skyscanner.local:4444/wd/hub',   // teamcity

    params: {
        website_id: 'xpsg'
    },

    specs: [
        'e2e/**/*spec.js',
        '!e2e/helpers/*.js',
        '!e2e/**/*helpers.js'
    ],

    suites: {
        technical: ['e2e/test_dashboard/runsingletest_spec.js', 'e2e/test_dashboard/testcases_spec.js', 'e2e/scrapetester/scrapetester_spec.js'],
        traffic: ['e2e/traffic/*-spec.js'],
        travelRankings:  ['e2e/travelRankings/travelRankings-spec.js'],
        apiHealth:  ['e2e/apiHealth/liveUpdates-spec.js'],

        singletest: ['e2e/test_dashboard/runsingletest_spec.js'],
        testcases:  ['e2e/test_dashboard/testcases_spec.js'],
        scrapetester:  ['e2e/scrapetester/scrapetester_spec.js']
    },

    capabilities: {
        browserName: 'chrome'
    },

    allScriptsTimeout: 60 * 60 * 1000,

    baseUrl: 'http://localhost:1025/', // dev

    framework: 'jasmine2',

    jasmineNodeOpts: {
        onComplete: null,
        isVerbose: false,
        showColors: true,
        includeStackTrace: true,
        defaultTimeoutInterval: 20 * 60 * 1000,
        print: function() {}
    },

    onPrepare: function() {
        'use strict';

        var width = 1920;
        var height = 1080;

        var jasmineReporters = require('jasmine-reporters');
        var SpecReporter = require('jasmine-spec-reporter');

        jasmine.getEnv().addReporter(new jasmineReporters.TeamCityReporter());
        jasmine.getEnv().addReporter(new SpecReporter({
            displaySpecDuration: true,
            displaySuiteNumber: true,
            displayPendingSpec: true,
            colors: {
                success: 'green',
                failure: 'red',
                pending: 'cyan'
            },
            prefixes: {
                success: '✓ ',
                failure: '✗ ',
                pending: '- '
            },
        }));

        browser.driver.manage().window().setSize(width, height);

        protractor.testCasesHelper = require('./e2e/helpers/testcase_helper.js');
    }
};
